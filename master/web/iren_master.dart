/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

import 'dart:async';
import 'dart:convert';
import 'dart:html';
import 'dart:js';
import 'dart:typed_data';

import 'package:convert/convert.dart' show hex;
import 'package:initialize/initialize.dart' as init;
import 'package:iren_client/iren_client.dart';
import 'package:iren_proto/common.pb.dart';
import 'package:misc_utils/dialogs.dart';
import 'package:misc_utils/electron.dart';
import 'package:misc_utils/misc_utils.dart';
import 'package:web_helpers/translator.dart';

import 'languages.dart';
import 'supervisor_screen.dart';

const String _LIB = "iren_master";

Future<Null> main() async {
  await window.onLoad.first;
  disableDragAndDrop(document.documentElement);

  registerMasterDictionaries();
  registerClientDictionaries();
  registerMiscUtilsDictionaries();
  selectDictionaryForCurrentLanguage();

  await init.run();

  registerExtensions();
  registerAreaWidgets();

  document.title = translate(_LIB, "Iren");

  ipcRenderer_once("connectionConfig", allowInterop((_, String connectionConfig) => _connect(connectionConfig)));
  ipcRenderer_send("getConnectionConfig");
}

Future<Null> _connect(String connectionConfig) async {
  dynamic config = JSON.decode(connectionConfig);

  WebSocket socket = new WebSocket("ws://127.0.0.1:${config["port"]}/websocket/")
      ..binaryType = "arraybuffer";

  WebSocketConnection connection = new WebSocketConnection(socket);
  socket
      ..onMessage.listen((MessageEvent e) => _onSocketMessage(e, connection))
      ..onClose.listen((_) => _onSocketClose(connection));

  await socket.onOpen.first;

  connection.send(new ClientMessage()
      ..type = ClientMessageType.SUPERVISE
      ..supervise = (new Supervise()
          ..supervisorKey = hex.decode(config["supervisorKey"])));

  ServerMessage reply = await connection.onMessage.first;
  switch (reply.type) {
    case ServerMessageType.SHOW_SUPERVISOR_SCREEN:
      showScreen(new SupervisorScreen(reply.showSupervisorScreen, connection));
      break;
    case ServerMessageType.INCORRECT_SUPERVISOR_KEY:
      document.body.appendText("Wrong supervisor key.");
      break;
  }
}

void _onSocketMessage(MessageEvent e, WebSocketConnection connection) {
  ByteBuffer b = e.data;
  connection.messageController.add(new ServerMessage.fromBuffer(b.asUint8List(), protoExtensionRegistry));
}

void _onSocketClose(WebSocketConnection connection) {
  if (connection.disconnectExpected) {
    window.location.reload();
  } else {
    ConnectionLostDialog.openModal();
  }
}
