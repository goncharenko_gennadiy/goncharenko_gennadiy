/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

import 'dart:async';
import 'dart:html';
import 'dart:typed_data';

import 'package:iren_client/iren_client.dart';
import 'package:iren_proto/common.pb.dart';
import 'package:misc_utils/electron.dart';
import 'package:misc_utils/misc_utils.dart';
import 'package:misc_utils/test_utils.dart';
import 'package:web_helpers/translator.dart';

import 'watch_screen.dart';

class SupervisorScreen extends ProgramScreen {
  final Connection _connection;
  /* nullable */StreamSubscription<ServerMessage> _messageSubscription;
  @override final DivElement element = new DivElement();
  final DivElement _workList = new DivElement();

  SupervisorScreen(ShowSupervisorScreen message, Connection this._connection) {
    element
        ..append(_workList)
        ..append(new BRElement())
        ..append(new ButtonElement()
            ..text = _tr("Assign...")
            ..onClick.listen((_) => _onAssignClick()))
        ..append(new BRElement())
        ..append(new BRElement())
        ..append(new SpanElement()
            ..classes.add("ru-irenproject-supervisorScreen-serverAddressCaption")
            ..text = "${_tr("Server address:")} ")
        ..append(new SpanElement()
            ..classes.add("ru-irenproject-supervisorScreen-serverAddress")
            ..text = message.serverAddress);

    _messageSubscription = _connection.onMessage.listen(_onServerMessage);
  }

  @override void onDismiss() {
    _messageSubscription?.cancel();
    _messageSubscription = null;
  }

  void _onServerMessage(ServerMessage m) {
    switch (m.type) {
      case ServerMessageType.WORK_LIST:
        _handleWorkList(m.workList);
        break;
    }
  }

  void _handleWorkList(WorkList m) {
    _workList.nodes.clear();

    for (WorkDescriptor w in sortWorksByTitle(m.workDescriptor)) {
      _workList
          ..append(new AnchorElement(href: "#")
              ..text = w.title
              ..draggable = false
              ..onClick.listen((MouseEvent e) {
                e.preventDefault();
                _watchWork(w.id, w.title);
              }))
          ..append(new BRElement());
    }
  }

  void _watchWork(String workId, String workTitle) {
    _connection.send(new ClientMessage()
        ..type = ClientMessageType.WATCH_WORK
        ..watchWork = (new WatchWork()
            ..workId = workId));

    showScreen(new WatchScreen(workTitle, _connection));
  }

  Future<Null> _onAssignClick() async {
    for (String fileName in showOpenTestDialog()) {
      lockUi();
      try {
        Uint8List data = null;
        try {
          data = readFileSync(fileName);
        } catch (e) {
          showErrorMessage(e.toString());
        }

        if (data != null) {
          _connection.send(new ClientMessage()
              ..type = ClientMessageType.CREATE_WORK
              ..createWork = (new CreateWork()
                  ..test = data
                  ..title = getFileTitle(fileName)
                  ..language = window.navigator.language));

          await for (ServerMessage m in _connection.onMessage) {
            if (m.type == ServerMessageType.CREATE_WORK_COMPLETE) {
              if (m.createWorkComplete.result == CreateWorkComplete_Result.INCORRECT_TEST_FILE) {
                showErrorMessage(_tr("CANNOT_LOAD_TEST")(fileName));
              }
              break;
            }
          }
        }
      } finally {
        unlockUi();
      }
    }
  }
}

const String _LIB = "supervisor_screen";

dynamic _tr(String key) => translate(_LIB, key);
