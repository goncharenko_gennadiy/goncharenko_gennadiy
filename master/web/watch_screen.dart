/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

import 'dart:async';
import 'dart:collection';
import 'dart:html';
import 'dart:math' as math;

import 'package:fixnum/fixnum.dart';
import 'package:iren_client/iren_client.dart';
import 'package:iren_proto/common.pb.dart';
import 'package:misc_utils/electron.dart';
import 'package:misc_utils/misc_utils.dart';
import 'package:misc_utils/test_utils.dart';
import 'package:web_helpers/translator.dart';

class WatchScreen extends ProgramScreen {
  static const int _USER_NAME_COLUMN = 0;
  static const int _CHART_COLUMN = 1;
  static const int _RESULT_COLUMN = 2;
  static const int _MARK_COLUMN = 3;
  static const int _TIME_COLUMN = 4;
  static const int _COLUMN_COUNT = 5;

  static final Map<int, String> _columnTitles = {
      _USER_NAME_COLUMN: _tr("Student"),
      _RESULT_COLUMN: _tr("Result, %"),
      _MARK_COLUMN: _tr("Grade"),
      _TIME_COLUMN: _tr("Time")};

  final Connection _connection;
  /* nullable */StreamSubscription<ServerMessage> _messageSubscription;
  @override final DivElement element = new DivElement();
  final TableElement _table = new TableElement();
  final List<SpanElement> _headers = [];

  final Map<Int64, SessionScore> _scores = {};
  SplayTreeSet<SessionScore> _sortedScores;

  int _sortColumn = _RESULT_COLUMN;
  bool _sortAscending = false;

  /* nullable */num _referenceServerTimeMilliseconds;
  /* nullable */num _referenceServerTimeArrivalMilliseconds;
  /* nullable */Timer _timer;

  /* nullable */WatchedSessionPanel _watchedSessionPanel;

  WatchScreen(String workTitle, Connection this._connection) {
    _sort();

    TableRowElement headerRow = _table.createTHead().addRow();
    for (int i = 0; i < _COLUMN_COUNT; ++i) {
      SpanElement header = new SpanElement();
      if (i != _CHART_COLUMN) {
        header
            ..classes.add("ru-irenproject-scoreTable-header")
            ..text = _columnTitles[i]
            ..onClick.listen((_) => _setSortColumn(i));
      }
      _headers.add(header);
      headerRow.addCell().append(header);
    }
    _table.createTBody();
    _displaySortIndicator();

    element
        ..classes.add("ru-irenproject-watchScreen")
        ..append(new DivElement()
            ..classes.add("ru-irenproject-watchScreen-scoreTablePanel")
            ..append(new DivElement()
                ..classes.add("ru-irenproject-watchScreen-scoreTableTopPanel")
                ..append(new SpanElement()
                    ..classes.add("ru-irenproject-watchScreen-workTitle")
                    ..text = workTitle)
                ..append(new AnchorElement(href: "#")
                    ..classes.add("ru-irenproject-watchScreen-workList")
                    ..text = _tr("Assignment List")
                    ..draggable = false
                    ..onClick.listen((MouseEvent e) {
                      e.preventDefault();
                      _onWorkListClick();
                    })))
            ..append(new DivElement()
                ..classes.add("ru-irenproject-watchScreen-scoreTableMainPanel")
                ..append(_table
                    ..classes.add("ru-irenproject-scoreTable")))
            ..append(new DivElement()
                ..classes.add("ru-irenproject-watchScreen-scoreTableBottomPanel")
                ..append(new ButtonElement()
                    ..text = _tr("Save to File...")
                    ..onClick.listen((_) => _onSaveResultsClick()))
                ..append(new ButtonElement()
                    ..text = _tr("Delete Assignment")
                    ..onClick.listen((_) => _onDeleteWorkClick()))));

    _messageSubscription = _connection.onMessage.listen(_onServerMessage);
  }

  @override void onDismiss() {
    _messageSubscription?.cancel();
    _messageSubscription = null;

    _timer?.cancel();
    _timer = null;
  }

  @override void onResize() {
    _watchedSessionPanel?.layOut();
  }

  void _displaySortIndicator() {
    _headers.asMap().forEach((int i, SpanElement e) {
      e.classes
          ..toggle("ru-irenproject-scoreTable-header-sortedUp", (i == _sortColumn) && _sortAscending)
          ..toggle("ru-irenproject-scoreTable-header-sortedDown", (i == _sortColumn) && !_sortAscending);
    });
  }

  void _onServerMessage(ServerMessage m) {
    switch (m.type) {
      case ServerMessageType.SERVER_TIME:
        _handleServerTime(m.serverTime);
        break;
      case ServerMessageType.SESSION_SCORE:
        _handleSessionScore(m.sessionScore);
        break;
      case ServerMessageType.SHOW_WATCHED_SESSION:
        _showWatchedSession(m.showWatchedSession);
        break;
      case ServerMessageType.SHOW_QUESTION_STATUS:
        _watchedSessionPanel.showQuestionStatus(m.showQuestionStatus);
        break;
      case ServerMessageType.SHOW_WATCHED_QUESTION:
        _watchedSessionPanel.showWatchedQuestion(m.showWatchedQuestion);
        break;
      case ServerMessageType.UPDATE_WATCHED_QUESTION:
        _watchedSessionPanel.updateWatchedQuestion(m.updateWatchedQuestion);
        break;
    }
  }

  void _handleServerTime(ServerTime m) {
    _referenceServerTimeMilliseconds = m.serverTimeMilliseconds;
    _referenceServerTimeArrivalMilliseconds = window.performance.now();

    _displayRemainingTime();
    _timer ??= new Timer.periodic(const Duration(seconds: 1), (_) => _displayRemainingTime());
  }

  void _handleSessionScore(Iterable<SessionScore> sessionScores) {
    for (SessionScore s in sessionScores) {
      SessionScore oldScore = _scores[s.id];
      if (oldScore != null) {
        _sortedScores.remove(oldScore);
      }

      _scores[s.id] = s;
      _sortedScores.add(s);
    }
    _display();
  }

  void _display() {
    TableSectionElement tbody = _table.tBodies.first;

    int rowIndex = 0;
    for (SessionScore s in _sortedScores) {
      if (rowIndex >= tbody.rows.length) {
        TableRowElement row = tbody.addRow()
            ..onClick.listen(_onRowClick);
        for (int i = 0; i < _COLUMN_COUNT; ++i) {
          row.addCell();
        }
      }

      tbody.rows[rowIndex]
          ..cells[_USER_NAME_COLUMN].text = s.userName
          ..cells[_CHART_COLUMN].children = [_renderChart(s)]
          ..cells[_RESULT_COLUMN].text = "${scaledDecimalToIntegerPercent(s.scaledResult)}"
          ..cells[_MARK_COLUMN].text = s.mark
          ..classes.toggle("ru-irenproject-scoreTable-finished", s.finished)
          ..classes.toggle("ru-irenproject-scoreTable-watched", _watchedSessionPanel?.sessionId == s.id);

      ++rowIndex;
    }

    _displayRemainingTime();
  }

  void _onRowClick(MouseEvent e) {
    lockUi();
    int index = (e.currentTarget as TableRowElement).sectionRowIndex;
    _connection.send(new ClientMessage()
        ..type = ClientMessageType.WATCH_SESSION
        ..watchSession = (new WatchSession()
            ..sessionId = _sortedScores.elementAt(index).id));
  }

  void _displayRemainingTime() {
    TableSectionElement tbody = _table.tBodies.first;
    num now = _referenceServerTimeMilliseconds
        + (window.performance.now() - _referenceServerTimeArrivalMilliseconds);

    int rowIndex = 0;
    for (SessionScore s in _sortedScores) {
      num remaining = s.hasDeadlineMilliseconds() ? math.max(s.deadlineMilliseconds - now, 0) : null;

      tbody.rows[rowIndex]
          ..cells[_TIME_COLUMN].text = s.hasDeadlineMilliseconds() ? formatRemainingTime(remaining) : ""
          ..classes.toggle("ru-irenproject-scoreTable-timedOut", remaining == 0);

      ++rowIndex;
    }
  }

  Element _renderChart(SessionScore s) {
    num percentCorrect = scaledDecimalToPercent(s.scaledResult);
    num percentWrong = s.currentAchievableScore / s.totalAchievableScore * 100 - percentCorrect;

    return new DivElement()
        ..classes.add("ru-irenproject-scoreTable-chart")
        ..append(new DivElement()
            ..classes.add("ru-irenproject-scoreTable-chart-correct")
            ..style.width = "${formatCssNumber(percentCorrect)}%")
        ..append(new DivElement()
            ..classes.add("ru-irenproject-scoreTable-chart-wrong")
            ..style.width = "${formatCssNumber(percentWrong)}%");
  }

  void _setSortColumn(int column) {
    if (_sortColumn == column) {
      _sortAscending = !_sortAscending;
    } else {
      _sortColumn = column;
      _sortAscending = (column == _USER_NAME_COLUMN);
    }
    _displaySortIndicator();

    _sort();
    _display();
  }

  void _sort() {
    _sortedScores = new SplayTreeSet(_createComparator(_sortColumn, _sortAscending));
    _sortedScores.addAll(_scores.values);
  }

  static Comparator<SessionScore> _createComparator(int sortColumn, bool sortAscending) {
    return (SessionScore a, SessionScore b) {
      int res;
      switch (sortColumn) {
        case _USER_NAME_COLUMN:
          res = a.userName.compareTo(b.userName); //TODO localeCompare
          break;
        case _RESULT_COLUMN:
        case _MARK_COLUMN:
          res = a.scaledScore.compareTo(b.scaledScore);
          break;
        case _TIME_COLUMN:
          res = (a.hasDeadlineMilliseconds() ? a.deadlineMilliseconds : -1).compareTo(
              b.hasDeadlineMilliseconds() ? b.deadlineMilliseconds : -1);
          break;
        default:
          throw false;
      }

      if (!sortAscending) {
        res *= -1;
      }

      if (res == 0) {
        if (sortColumn != _USER_NAME_COLUMN) {
          res = a.userName.compareTo(b.userName); //TODO localeCompare
        }
        if (res == 0) {
          res = a.id.compareTo(b.id);
        }
      }

      return res;
    };
  }

  void _showWatchedSession(ShowWatchedSession m) {
    if (_watchedSessionPanel != null) {
      _watchedSessionPanel.element.remove();
      _watchedSessionPanel = null;
    }

    if (m.hasSessionId()) {
      _watchedSessionPanel = new WatchedSessionPanel(m, _connection);
      element.append(_watchedSessionPanel.element);
      _watchedSessionPanel.layOut();
    }

    _display();
    unlockUi();
  }

  void _onSaveResultsClick() {
    StringBuffer buffer = new StringBuffer()
        ..writeAll([_tr("Student"), _tr("Result, %"), _tr("Grade")], "\t")
        ..writeln();

    RegExp r = new RegExp(r"[\t\n\r]");

    for (SessionScore s in _sortedScores) {
      buffer
          ..writeAll([s.userName, scaledDecimalToIntegerPercent(s.scaledResult), s.mark.replaceAll(r, "")], "\t")
          ..writeln();
    }

    String fileName = showSaveFileDialog("tsv", _tr("TSV Files"));
    if (fileName != null) {
      try {
        fs_writeFileSync(fileName, buffer.toString());
      } catch (e) {
        showErrorMessage(e.toString());
      }
    }
  }

  void _onDeleteWorkClick() {
    if (showMessageBox(new ShowMessageBoxOptions(
        type: "warning",
        buttons: [_tr("Delete"), _tr("Cancel")],
        message: _tr("The results of all students will be deleted."),
        cancelId: 1)) == 0) {
      lockUi();
      _connection
          ..send(new ClientMessage()
              ..type = ClientMessageType.DELETE_WORK)
          ..expectDisconnect();
    }
  }

  void _onWorkListClick() {
    lockUi();
    _connection.close();
  }
}

class WatchedSessionPanel {
  static const String _RESPONSE_HEADER = "ru-irenproject-watchedSession-responseHeader";
  static const String _SELECTED = "ru-irenproject-watchedSession-responseHeader-selected";

  final Connection _connection;

  final Element element = new DivElement();
  final SelectorBar _selectorBar;
  final DivElement _questionContainer = new DivElement();
  DialogWidget _dialogWidget;

  final ButtonElement _previousButton = new ButtonElement();
  final ButtonElement _nextButton = new ButtonElement();

  final SpanElement _resultLabel = new SpanElement();
  final SpanElement _weightLabel = new SpanElement();
  final SpanElement _scoreLabel = new SpanElement();

  final int _questionCount;
  final Int64 sessionId;
  bool _showingCorrectResponse = false;
  ShowWatchedQuestion _question;

  WatchedSessionPanel(ShowWatchedSession m, this._connection)
      : _selectorBar = new SelectorBar(m.questionDescriptor.length),
        _questionCount = m.questionDescriptor.length,
        sessionId = m.sessionId {
    setUpSelectorBar(m.questionDescriptor, _selectorBar);
    _selectorBar.onSelectItem.listen((SelectItemEvent e) => _watchQuestion(e.itemIndex));

    element
        ..classes.add("ru-irenproject-watchedSession")
        ..append(new DivElement()
            ..classes.add("ru-irenproject-watchedSession-topPanel")
            ..append(_selectorBar.element))
        ..append(new DivElement()
            ..classes.add("ru-irenproject-watchedSession-responseSelector")
            ..append(makeKeyboardAccessible(new DivElement())
                ..classes.addAll([_RESPONSE_HEADER, _SELECTED])
                ..text = _tr("Student's Answer")
                ..onClick.listen((MouseEvent e) => _onHeaderClick(e, false)))
            ..append(makeKeyboardAccessible(new DivElement())
                ..classes.add(_RESPONSE_HEADER)
                ..text = _tr("Correct Answer")
                ..onClick.listen((MouseEvent e) => _onHeaderClick(e, true))))
        ..append(_questionContainer
            ..classes.add("ru-irenproject-watchedSession-questionContainer"))
        ..append(new DivElement()
            ..classes.add("ru-irenproject-watchedSession-detailsPanel")
            ..appendText("${_tr("Result:")} ")
            ..append(_resultLabel
                ..classes.add("ru-irenproject-watchedSession-result"))
            ..appendText("${_tr("Question weight:")} ")
            ..append(_weightLabel
                ..classes.add("ru-irenproject-watchedSession-weight"))
            ..appendText("${_tr("Points earned:")} ")
            ..append(_scoreLabel))
        ..append(new DivElement()
            ..classes.add("ru-irenproject-watchedSession-bottomPanel")
            ..append(_previousButton
                ..classes.add("ru-irenproject-watchedSession-previousButton")
                ..text = _tr("Previous")
                ..onClick.listen((_) => _watchQuestion(_question.questionIndex - 1)))
            ..append(_nextButton
                ..classes.add("ru-irenproject-watchedSession-nextButton")
                ..text = _tr("Next")
                ..onClick.listen((_) => _watchQuestion(_question.questionIndex + 1)))
            ..append(new ButtonElement()
                ..classes.add("ru-irenproject-watchedSession-closeButton")
                ..text = _tr("Close")
                ..onClick.listen((_) => _onCloseClick())));

    _question = m.question;
    _showQuestion();
  }

  void _showQuestion() {
    _selectorBar.setCurrentItem(_question.questionIndex);

    _dialogWidget = new DialogWidget(_question.dialog, _showingCorrectResponse ?
        _question.correctResponse : _question.update.response);
    _dialogWidget.setReadOnly(true);
    _questionContainer.children = [_dialogWidget.element];

    if (isAttached(element)) {
      _dialogWidget.layOut();
    }

    _questionContainer.scrollTop = 0;

    _resultLabel.text = "${scaledDecimalToIntegerPercent(_question.update.scaledResult)}%";
    _weightLabel.text = "${_question.weight}";
    _scoreLabel.text = "${formatScaledDecimal(_question.update.scaledScore)}";

    _previousButton.disabled = (_question.questionIndex == 0);
    _nextButton.disabled = (_question.questionIndex == _questionCount - 1);
  }

  void layOut() {
    _selectorBar.layOut();
    _dialogWidget.layOut();
  }

  void _onCloseClick() {
    lockUi();
    _connection.send(new ClientMessage()
        ..type = ClientMessageType.WATCH_SESSION
        ..watchSession = new WatchSession());
  }

  void showQuestionStatus(ShowQuestionStatus m) {
    _selectorBar.setItemColor(m.questionIndex, QUESTION_COLORS[m.status]);
  }

  void _onHeaderClick(MouseEvent e, bool showCorrectResponse) {
    if (_selectHeader(e.currentTarget)) {
      _showingCorrectResponse = showCorrectResponse;

      int saved = _questionContainer.scrollTop;
      _showQuestion();
      _questionContainer.scrollTop = saved;
    }
  }

  bool _selectHeader(Element header) {
    bool res = !header.classes.contains(_SELECTED);
    if (res) {
      for (Element e in header.parent.children) {
        e.classes.toggle(_SELECTED, identical(e, header));
      }
    }
    return res;
  }

  void showWatchedQuestion(ShowWatchedQuestion m) {
    _question = m;
    _showQuestion();
    unlockUi();
  }

  void updateWatchedQuestion(UpdateWatchedQuestion m) {
    _question.update = m;

    int saved = _questionContainer.scrollTop;
    _showQuestion();
    _questionContainer.scrollTop = saved;
  }

  void _watchQuestion(int questionIndex) {
    lockUi();
    _connection.send(new ClientMessage()
        ..type = ClientMessageType.WATCH_QUESTION
        ..watchQuestion = (new WatchQuestion()
            ..questionIndex = questionIndex));
  }
}

const String _LIB = "watch_screen";

dynamic _tr(String key) => translate(_LIB, key);
