/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

final Map<String, Map<String, dynamic>> ru = {
  "iren_master": {
    "Iren": "Айрен",
  },

  "supervisor_screen": {
    "CANNOT_LOAD_TEST": (String fileName) => 'Ошибка при загрузке теста из файла "$fileName".',
    "Assign...": "Назначить...",
    "Server address:": "Адрес сервера:",
  },

  "watch_screen": {
    "Student": "Тестируемый",
    "Result, %": "Результат, %",
    "Grade": "Оценка",
    "Time": "Время",
    "Assignment List": "Список работ",
    "Save to File...": "Сохранить в файл...",
    "TSV Files": "Файлы TSV",
    "Delete Assignment": "Удалить работу",
    "The results of all students will be deleted.": "Результаты всех тестируемых будут удалены.",
    "Delete": "Удалить",
    "Cancel": "Отмена",
    "Student's Answer": "Ответ тестируемого",
    "Correct Answer": "Верный ответ",
    "Result:": "Оценка ответа:",
    "Question weight:": "Вес вопроса:",
    "Points earned:": "Набранные баллы:",
    "Previous": "Назад",
    "Next": "Вперед",
    "Close": "Закрыть",
  }
};
