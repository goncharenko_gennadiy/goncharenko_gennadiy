/*
  © Translation contributors, see the TRANSLATORS file

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

final Map<String, Map<String, dynamic>> uk = {
  "iren_master": {
    "Iren": "Айрен",
  },

  "supervisor_screen": {
    "CANNOT_LOAD_TEST": (String fileName) => 'Помилка під час завантаження теста з файлу "$fileName".',
    "Assign...": "Призначити...",
    "Server address:": "Адреса сервера:",
  },

  "watch_screen": {
    "Student": "Студент",
    "Result, %": "Результат, %",
    "Grade": "Оцінка",
    "Time": "Час",
    "Assignment List": "Перелік робіт",
    "Save to File...": "Зберегти у файл...",
    "TSV Files": "Файли TSV",
    "Delete Assignment": "Видалити роботу",
    "The results of all students will be deleted.": "Результати усіх студентів буде видалено.",
    "Delete": "Видалити",
    "Cancel": "Відміна",
    "Student's Answer": "Відповідь студента",
    "Correct Answer": "Правильна відповідь",
    "Result:": "Оцінка відповіді:",
    "Question weight:": "Вага відповіді:",
    "Points earned:": "Набрані бали:",
    "Previous": "Назад",
    "Next": "Вперед",
    "Close": "Закрити",
  }
};
