/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

@HtmlImport("modifier_screen.html")
library iren_editor.modifier_screen;

import 'dart:html';
import 'dart:js';

import 'package:initialize/initialize.dart';
import 'package:iren_proto/editor/modifier_screen.pb.dart';
import 'package:iren_proto/modifier/add_negative_choice_modifier.pb.dart';
import 'package:iren_proto/modifier/script_modifier.pb.dart';
import 'package:iren_proto/modifier/set_evaluation_model_modifier.pb.dart';
import 'package:iren_proto/modifier/set_negative_choice_content_modifier.pb.dart';
import 'package:iren_proto/modifier/set_weight_modifier.pb.dart';
import 'package:iren_proto/modifier/shuffle_choices_modifier.pb.dart';
import 'package:iren_proto/modifier/suppress_single_choice_hint_modifier.pb.dart';
import 'package:iren_proto/question/select_question.pb.dart';
import 'package:misc_utils/electron.dart';
import 'package:misc_utils/misc_utils.dart';
import 'package:misc_utils/test_utils.dart';
import 'package:web_components/web_components.dart';
import 'package:web_helpers/translator.dart';

import 'common.dart';
import 'editor_infra.dart';

@CustomElement(ModifierScreen.TAG)
class ModifierScreen extends Widget<XModifierScreen> {
  static const String TAG = "ru-irenproject-modifier-screen";

  @ui ButtonElement _closeButton;

  ModifierListEditor _modifierListEditor;

  ModifierScreen.created() : super.created();

  factory ModifierScreen(String name, Dispatcher dispatcher) => new Widget(TAG, name, dispatcher);

  @override initialize() {
    _modifierListEditor = new ModifierListEditor(shape.modifierListEditor, dispatcher)
        ..id = "list"
        ..autoScrollToSelection = true;
    findElement("listPlaceholder").replaceWith(_modifierListEditor);

    _closeButton.onClick.listen((_) => _close());

    windowShortcutHandler.add(KeyCode.ESC, KeyModifierState.NONE, _close);
  }

  @override void render() {
    super.render();
    _modifierListEditor.render();
  }

  void _close() {
    performAction(new XModifierScreen_Close());
  }
}

@CustomElement(ModifierListEditor.TAG)
class ModifierListEditor extends Widget<XModifierListEditor> {
  static const String TAG = "ru-irenproject-modifier-list-editor";

  static final List<String> _hiddenModifierTypes = [
      SetNegativeChoiceContentModifierType.setNegativeChoiceContent.name,
      SetWeightModifierType.setWeight.name];

  @ui ButtonElement _addButton;
  @ui ButtonElement _deleteButton;
  @ui ButtonElement _moveUpButton;
  @ui ButtonElement _moveDownButton;
  @ui SpanElement _caption;
  @ui DivElement _main;

  bool autoScrollToSelection = false;

  ModifierListEditor.created() : super.created();

  factory ModifierListEditor(String name, Dispatcher dispatcher) => new Widget(TAG, name, dispatcher);

  @override initialize() {
    _addButton.onClick.listen((_) => _onAddClick());
    _deleteButton.onClick.listen((_) => _delete());
    _moveUpButton.onClick.listen((_) => _moveUp());
    _moveDownButton.onClick.listen((_) => _moveDown());

    new ShortcutHandler(shadowRoot)
        ..add(KeyCode.DELETE, KeyModifierState.NONE, _delete)
        ..add(KeyCode.UP, KeyModifierState.ALT, _moveUp)
        ..add(KeyCode.DOWN, KeyModifierState.ALT, _moveDown);

    if (shape.hasQuestionType()) {
      _caption.text = tr("Question Modifiers");
    } else if (!shape.hasSectionName()) {
      _caption.style.display = "none";
    }
  }

  @override void render() {
    super.render();
    if (shapeChanged) {
      if (shape.hasSectionName()) {
        _caption.text = tr("SECTION_MODIFIERS")(getPrintableSectionName(shape.sectionName));
      }

      _main.nodes.clear();
      for (String type in shape.type) {
        ModifierDescriptor d = getModifierDescriptor(type);
        _main.append(new DivElement()
            ..text = d.title
            ..title = d.title
            ..onClick.listen(_onModifierClick));
      }

      if (shape.hasSelectedIndex()) {
        _main.children[shape.selectedIndex].classes.add("selected");
        if (autoScrollToSelection) {
          scheduleScrollIntoView(_main.children[shape.selectedIndex]);
        }
      }

      _deleteButton.disabled = !shape.hasSelectedIndex();
      _moveUpButton.disabled = !_canMoveUp();
      _moveDownButton.disabled = !_canMoveDown();
    }
  }

  void _onModifierClick(MouseEvent e) {
    performAction(new XModifierListEditor_Select()
        ..index = _main.children.indexOf(e.currentTarget));
  }

  void _onAddClick() {
    Menu menu = new Menu();

    for (ModifierDescriptor d in modifierDescriptors) {
      bool applicable = (d.questionTypes == null) || !shape.hasQuestionType()
          || d.questionTypes.contains(shape.questionType);
      if (applicable && !_hiddenModifierTypes.contains(d.type)) {
        menu.append(new MenuItem(new MenuItemOptions(
            //ignore: argument_type_not_assignable
            click: allowInterop((_, __) => _add(d)),
            label: d.title,
            enabled: !shape.type.contains(d.type))));
      }
    }

    menu.popup(_addButton.borderEdge.left.round(), _addButton.borderEdge.bottom.round());
  }

  void _add(ModifierDescriptor d) {
    performAction(new XModifierListEditor_Add()
        ..type = d.type);
  }

  void _delete() {
    if (shape.hasSelectedIndex()) {
      performAction(new XModifierListEditor_Delete());
    }
  }

  void _moveUp() {
    if (_canMoveUp()) {
      performAction(new XModifierListEditor_Move()
          ..forward = false);
    }
  }

  bool _canMoveUp() => shape.hasSelectedIndex() && (shape.selectedIndex > 0);

  void _moveDown() {
    if (_canMoveDown()) {
      performAction(new XModifierListEditor_Move()
          ..forward = true);
    }
  }

  bool _canMoveDown() => shape.hasSelectedIndex() && (shape.selectedIndex < shape.type.length - 1);
}

const String _LIB = "modifier_screen";

@initMethod void initialize() {
  registerShapeParsers([
      (List<int> b) => new XModifierScreen.fromBuffer(b),
      (List<int> b) => new XModifierListEditor.fromBuffer(b)]);

  registerModifierDescriptor(new ModifierDescriptor(
      type: SuppressSingleChoiceHintModifierType.suppressSingleChoiceHint.name,
      title: translate(_LIB, "Allow Multiple Selection for Questions with a Single Correct Answer"),
      questionTypes: [SelectQuestionType.select.name]));

  registerModifierDescriptor(new ModifierDescriptor(
      type: AddNegativeChoiceModifierType.addNegativeChoice.name,
      title: translate(_LIB, 'Add the "No Correct Answers" Choice'),
      questionTypes: [SelectQuestionType.select.name]));

  registerModifierDescriptor(new ModifierDescriptor(
      type: SetNegativeChoiceContentModifierType.setNegativeChoiceContent.name,
      title: translate(_LIB, 'Set the "No Correct Answers" Choice Text'),
      questionTypes: [SelectQuestionType.select.name]));

  registerModifierDescriptor(new ModifierDescriptor(
      type: ShuffleChoicesModifierType.shuffleChoices.name,
      title: translate(_LIB, "Shuffle Choices"),
      questionTypes: [SelectQuestionType.select.name]));

  registerModifierDescriptor(new ModifierDescriptor(
      type: SetEvaluationModelModifierType.setEvaluationModel.name,
      title: translate(_LIB, "Set Scoring Mode [editing not implemented yet]")));

  registerModifierDescriptor(new ModifierDescriptor(
      type: SetWeightModifierType.setWeight.name,
      title: translate(_LIB, "Set Question Weight")));

  registerModifierDescriptor(new ModifierDescriptor(
      type: ScriptModifierType.script.name,
      title: translate(_LIB, "Script [editing not implemented yet]")));
}
