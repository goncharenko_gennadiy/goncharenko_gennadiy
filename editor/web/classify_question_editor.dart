/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

@HtmlImport("classify_question_editor.html")
library iren_editor.classify_question_editor;

import 'dart:async';
import 'dart:html';

import 'package:initialize/initialize.dart';
import 'package:iren_proto/editor/classify_question_editor.pb.dart';
import 'package:iren_proto/question/classify_question.pb.dart';
import 'package:meta/meta.dart';
import 'package:misc_utils/misc_utils.dart';
import 'package:protobuf/protobuf.dart' show GeneratedMessage;
import 'package:web_components/web_components.dart';
import 'package:web_helpers/translator.dart';

import 'common.dart';
import 'editor_infra.dart';
import 'pad_editor.dart';

@QuestionEditor(ClassifyQuestionEditor.TAG, XClassifyQuestionEditor)
class ClassifyQuestionEditor extends Widget<XClassifyQuestionEditor> {
  static const String TAG = "ru-irenproject-classify-question-editor";

  @ui DivElement _toolbar;
  @ui ButtonElement _addItemButton;
  @ui ButtonElement _addCategoryButton;
  @ui ButtonElement _deleteElementButton;
  @ui ButtonElement _moveElementUpButton;
  @ui ButtonElement _moveElementDownButton;
  @ui ButtonElement _optionsButton;
  @ui DivElement _elementPanel;

  PadEditor _formulation;

  /* nullable */int _selectElement;
  /* nullable */String _selectElementName;
  /* nullable */int _focusedElement;

  ClassifyQuestionEditor.created() : super.created();

  @override void initialize() {
    _addItemButton.onClick.listen((_) => _addItem());
    _addCategoryButton.onClick.listen((_) => _addCategory());
    _deleteElementButton.onClick.listen((_) => _deleteElement());
    _moveElementUpButton.onClick.listen((_) => _moveElementUp());
    _moveElementDownButton.onClick.listen((_) => _moveElementDown());
    _optionsButton.onClick.listen((_) => _showOptionsDialog());

    windowShortcutHandler
        ..add(KeyCode.F5, KeyModifierState.NONE, _addItem)
        ..add(KeyCode.F6, KeyModifierState.NONE, _addCategory)
        ..add(KeyCode.F8, KeyModifierState.NONE, _deleteElement);

    new ShortcutHandler(shadowRoot)
        ..add(KeyCode.UP, KeyModifierState.ALT, _moveElementUp)
        ..add(KeyCode.DOWN, KeyModifierState.ALT, _moveElementDown);

    _formulation = new PadEditor(shape.formulationEditor, dispatcher)
        ..id = "formulation";
    findElement("formulationPlaceholder").replaceWith(_formulation);

    Element.focusEvent.forTarget(shadowRoot, useCapture: true).listen((_) => _onFocus());
    Element.blurEvent.forTarget(shadowRoot, useCapture: true).listen(_onBlur);
  }

  @override void render() {
    super.render();
    _formulation.render();

    if (shapeChanged) {
      _focusedElement = null;

      _elementPanel.nodes = shape.element.map((ClassifyElement e) => new PadEditor(e.editor, dispatcher)
          ..classes.toggle("category", !e.hasItemIndex()));

      _addCategoryButton.disabled = !shape.canAddCategory;
      _optionsButton.classes.toggle("edited", !shape.hasDefaultOptions);
    }

    for (PadEditor e in _elementPanel.children) {
      e.render();
    }

    if ((_selectElement != null) && shape.element.isNotEmpty) {
      (_elementPanel.children[_selectElement.clamp(0, shape.element.length - 1)] as PadEditor).focusComponent();
    }
    _selectElement = null;

    if (_selectElementName != null) {
      (_elementPanel.children.firstWhere((Element e) => (e as PadEditor).name == _selectElementName, orElse: () => null)
          as PadEditor)?.focusComponent();
    }
    _selectElementName = null;

    _updateUi();
  }

  void _onFocus() {
    int index = _elementPanel.children.indexOf(shadowRoot.activeElement);
    if (index != -1) {
      _focusedElement = index;
    }
    _updateUi();
  }

  void _onBlur(Event e) {
    if (!_toolbar.contains((e as FocusEvent).relatedTarget)) {
      _focusedElement = null;
    }
    _updateUi();
  }

  void _updateUi() {
    _addItemButton.disabled = !_canAddItem();
    _deleteElementButton.disabled = (_focusedElement == null);
    _moveElementUpButton.disabled = !_canMoveElementUp();
    _moveElementDownButton.disabled = !_canMoveElementDown();
  }

  void _addItem() {
    if (_canAddItem()) {
      performAction(new XClassifyQuestionEditor_AddItem()
          ..categoryIndex = shape.element[_focusedElement].categoryIndex);

      _selectElement = _focusedElement;
      do {
        ++_selectElement;
      } while ((_selectElement < shape.element.length) && shape.element[_selectElement].hasItemIndex());
    }
  }

  bool _canAddItem() => shape.canAddItem && (_focusedElement != null);

  void _addCategory() {
    if (shape.canAddCategory) {
      performAction(new XClassifyQuestionEditor_AddCategory());
      _selectElement = shape.element.length;
    }
  }

  void _deleteElement() {
    if (_focusedElement != null) {
      ClassifyElement e = shape.element[_focusedElement];
      GeneratedMessage action = e.hasItemIndex() ?
          (new XClassifyQuestionEditor_DeleteItem()
              ..categoryIndex = e.categoryIndex
              ..itemIndex = e.itemIndex) :
          (new XClassifyQuestionEditor_DeleteCategory()
              ..index = e.categoryIndex);
      performAction(action);

      bool deletedItemInFrontOfNextCategory = e.hasItemIndex() && (_focusedElement < shape.element.length - 1)
          && !shape.element[_focusedElement + 1].hasItemIndex();
      _selectElement = deletedItemInFrontOfNextCategory ? (_focusedElement - 1) : _focusedElement;
    }
  }

  void _moveElementUp() {
    if (_canMoveElementUp()) {
      _moveElement(forward: false);
    }
  }

  bool _canMoveElementUp() => (_focusedElement != null) && shape.element[_focusedElement].canMoveUp;

  void _moveElementDown() {
    if (_canMoveElementDown()) {
      _moveElement(forward: true);
    }
  }

  bool _canMoveElementDown() => (_focusedElement != null) && shape.element[_focusedElement].canMoveDown;

  void _moveElement({@required bool forward}) {
    ClassifyElement e = shape.element[_focusedElement];
    GeneratedMessage action = e.hasItemIndex() ?
        (new XClassifyQuestionEditor_MoveItem()
            ..categoryIndex = e.categoryIndex
            ..itemIndex = e.itemIndex
            ..forward = forward) :
        (new XClassifyQuestionEditor_MoveCategory()
            ..index = e.categoryIndex
            ..forward = forward);
    performAction(action);

    _selectElementName = e.editor;
  }

  void _showOptionsDialog() {
    ClassifyQuestionOptionsDialog.openModal(shape, dispatcher, name);
  }

  @override void focusComponent() {
    _formulation.focusComponent();
  }
}

@CustomElement(ClassifyQuestionOptionsDialog.TAG)
class ClassifyQuestionOptionsDialog extends ModalDialog {
  static const String TAG = "ru-irenproject-classify-question-options-dialog";

  static Future<Null> openModal(XClassifyQuestionEditor source, Dispatcher dispatcher, String targetName) =>
      ((new Element.tag(TAG) as ClassifyQuestionOptionsDialog)
          .._source = source
          .._dispatcher = dispatcher
          .._targetName = targetName)._doOpenModal();

  @ui DialogElement _dialog;
  @ui RadioButtonInputElement _randomItems;
  @ui NumberInputElement _itemLimit;
  @ui NumberInputElement _minItemsPerCategory;

  XClassifyQuestionEditor _source;
  Dispatcher _dispatcher;
  String _targetName;

  ClassifyQuestionOptionsDialog.created() : super.created();

  Future<Null> _doOpenModal() async {
    _itemLimit.max = _minItemsPerCategory.max = _source.maxItems.toString();

    if (_source.hasItemLimit()) {
      _randomItems.checked = true;
      _itemLimit.valueAsNumber = _source.itemLimit;
      _minItemsPerCategory.valueAsNumber = _source.minItemsPerCategory;
    }

    _randomItems.onClick.listen((_) => focusInputElement(_itemLimit));

    _updateUi();
    _dialog.onChange.listen((_) => _updateUi());

    await open(_dialog);

    if (_dialog.returnValue.isNotEmpty) {
      XClassifyQuestionEditor_SetOptions action = new XClassifyQuestionEditor_SetOptions();
      if (_randomItems.checked) {
        action
            ..itemLimit = _itemLimit.valueAsNumber
            ..minItemsPerCategory = _minItemsPerCategory.valueAsNumber;
      }
      await _dispatcher.performAction(_targetName, action);
    }
  }

  void _updateUi() {
    _itemLimit.disabled = _minItemsPerCategory.disabled = !_randomItems.checked;
  }
}

const String _LIB = "classify_question_editor";

@initMethod void initialize() {
  registerShapeParser((List<int> b) => new XClassifyQuestionEditor.fromBuffer(b));

  registerQuestionDescriptor(new QuestionDescriptor(
      type: ClassifyQuestionType.classify.name,
      title: translate(_LIB, "Classification Question"),
      priority: 5000,
      icon: "classify_question_editor.resources/classifyQuestion.png",
      initialText: translate(_LIB, "Classify the items.")));
}
