/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

@HtmlImport("section_tree_editor.html")
library iren_editor.section_tree_editor;

import 'dart:async';
import 'dart:html';

import 'package:initialize/initialize.dart';
import 'package:iren_proto/editor/editor.pb.dart';
import 'package:misc_utils/electron.dart';
import 'package:misc_utils/misc_utils.dart';
import 'package:misc_utils/test_utils.dart';
import 'package:web_components/web_components.dart';

import 'common.dart';
import 'common_dialogs.dart';
import 'editor_infra.dart';

@CustomElement(SectionTreeEditor.TAG)
class SectionTreeEditor extends Widget<XSectionTreeEditor> {
  static const String TAG = "ru-irenproject-section-tree-editor";

  @ui DivElement _main;
  @ui ButtonElement _addButton;
  @ui ButtonElement _deleteButton;
  @ui ButtonElement _setNameButton;
  @ui ButtonElement _modifiersButton;
  @ui ButtonElement _moveUpButton;
  @ui ButtonElement _moveDownButton;

  List<XSectionEditor> _rendered;

  SectionTreeEditor.created() : super.created();

  factory SectionTreeEditor(String name, Dispatcher dispatcher) => new Widget(TAG, name, dispatcher);

  @override void initialize() {
    _addButton.onClick.listen((_) => _add());
    _deleteButton.onClick.listen((_) => _delete());
    _setNameButton.onClick.listen((_) => _setName());
    _modifiersButton.onClick.listen((_) => _showModifiers());
    _moveUpButton.onClick.listen((_) => _moveUp());
    _moveDownButton.onClick.listen((_) => _moveDown());

    windowShortcutHandler
        ..add(KeyCode.F2, KeyModifierState.SHIFT, _add)
        ..add(KeyCode.F3, KeyModifierState.SHIFT, _setName)
        ..add(KeyCode.F4, KeyModifierState.SHIFT, _showModifiers);

    new ShortcutHandler(shadowRoot)
        ..add(KeyCode.DELETE, KeyModifierState.NONE, _delete)
        ..add(KeyCode.UP, KeyModifierState.ALT, _moveUp)
        ..add(KeyCode.DOWN, KeyModifierState.ALT, _moveDown);
  }

  @override void render() {
    super.render();
    if (shapeChanged) {
      _main.nodes.clear();
      _createNode(shape.root, 0);
      _rendered = new List.filled(_main.children.length, null);
    }

    _renderNodes();

    _deleteButton.disabled = _rootSelected();
    _moveUpButton.disabled = !_selection().canMoveUp;
    _moveDownButton.disabled = !_selection().canMoveDown;
  }

  void _createNode(SectionNode node, int level) {
    _main.append(new DivElement()
        ..dataset["name"] = node.editor
        ..style.marginLeft = "${level * 2}em"
        ..onClick.listen(_onSectionClick)
        ..onDoubleClick.listen((_) => _setName())
        ..append(new SpanElement()
            ..classes.add("icon"))
        ..append(new SpanElement()
            ..classes.add("name")
            ..dataset["untitled"] = getPrintableSectionName("")));
    for (SectionNode child in node.child) {
      _createNode(child, level + 1);
    }
  }

  void _onSectionClick(MouseEvent e) {
    dispatcher.performAction((e.currentTarget as Element).dataset["name"], new XSectionEditor_Select());
  }

  void _renderNodes() {
    int rowIndex = 0;
    String selectedEditorName = _selection().selectedEditor;

    void renderNode(SectionNode node) {
      XSectionEditor editor = scene.shapes(node.editor);
      DivElement row = _main.children[rowIndex];

      if (!identical(editor, _rendered[rowIndex])) {
        _rendered[rowIndex] = editor;
        row.children.first.classes.toggle("hasModifiers", editor.hasModifiers);
        row.children[1]
            ..text = editor.name
            ..classes.toggle("populated", editor.questionCount > 0);
      }

      bool selected = (node.editor == selectedEditorName);
      if (selected && !row.classes.contains("selected")) {
        scheduleScrollIntoView(row);
      }
      row.classes.toggle("selected", selected);

      ++rowIndex;

      for (SectionNode child in node.child) {
        renderNode(child);
      }
    }

    renderNode(shape.root);
  }

  XSectionTreeSelection _selection() => scene.shapes(shape.selection);

  Future<Null> _add() async {
    String name = await inputString(prompt: tr("Section name:"), maxLength: shape.maxSectionNameLength);
    if (name != null) {
      performAction(new XSectionTreeEditor_Add()
          ..name = collapseSpaces(name));
    }
  }

  void _delete() {
    if (!_rootSelected() && showMessageBox(new ShowMessageBoxOptions(
        type: "question",
        buttons: [tr("Delete"), tr("Cancel")],
        message: tr("DELETE_SECTION")(getPrintableSectionName(_selectedEditor().name)),
        cancelId: 1)) == 0) {
      performAction(new XSectionTreeEditor_Delete());
    }
  }

  XSectionEditor _selectedEditor() => scene.shapes(_selection().selectedEditor);

  bool _rootSelected() => _selection().selectedEditor == shape.root.editor;

  void _moveUp() {
    if (_selection().canMoveUp) {
      performAction(new XSectionTreeEditor_Move()
          ..forward = false);
    }
  }

  void _moveDown() {
    if (_selection().canMoveDown) {
      performAction(new XSectionTreeEditor_Move()
          ..forward = true);
    }
  }

  Future<Null> _setName() async {
    String name = await inputString(prompt: tr("Section name:"), maxLength: shape.maxSectionNameLength,
        initial: _selectedEditor().name);
    if (name != null) {
      dispatcher.performAction(_selection().selectedEditor, new XSectionEditor_SetName()
          ..name = collapseSpaces(name));
    }
  }

  void _showModifiers() {
    dispatcher.performAction(_selection().selectedEditor, new XSectionEditor_ShowModifiers());
  }
}

@initMethod void initialize() {
  registerShapeParsers([
      (List<int> b) => new XSectionTreeEditor.fromBuffer(b),
      (List<int> b) => new XSectionTreeSelection.fromBuffer(b),
      (List<int> b) => new XSectionEditor.fromBuffer(b)]);
}
