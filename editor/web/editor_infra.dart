/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

import 'dart:async';
import 'dart:html';
import 'dart:typed_data';

import 'package:fixnum/fixnum.dart' show Int64;
import 'package:initialize/initialize.dart';
import 'package:iren_proto/editor/infra.pb.dart';
import 'package:meta/meta.dart';
import 'package:misc_utils/misc_utils.dart';
import 'package:protobuf/protobuf.dart' show GeneratedMessage;
import 'package:web_helpers/web_helpers.dart';

typedef GeneratedMessage MessageParser(List<int> buffer);

class Scene {
  final Map<String, GeneratedMessage> _shapes = {};

  GeneratedMessage shapes(String name) {
    GeneratedMessage res = _shapes[name];
    if (res == null) {
      throw "Shape '$name' not found.";
    }
    return res;
  }

  // used by the library
  void _applyDelta(SceneDelta delta) {
    for (Shape shape in delta.changed) {
      MessageParser p = _shapeParsers[shape.type];
      if (p == null) {
        throw "No message parser for '${shape.type}'";
      }
      _shapes[shape.name] = p(shape.data);
    }

    delta.deleted.forEach(_shapes.remove);
  }
}

class ServerConnection {
  final WebSocket _socket;
  final Map<String, _ReplyHandler> _replyHandlersByChannel = {};

  ServerConnection(this._socket) {
    _socket.onMessage.listen(_onSocketMessage);
  }

  void _onSocketMessage(MessageEvent e) {
    ByteBuffer b = e.data;
    Reply reply = new Reply.fromBuffer(b.asUint8List());

    _ReplyHandler handler = _replyHandlersByChannel[reply.channel];
    if (handler != null) {
      handler(reply);
    }
  }

  void addReplyHandler(String channel, void handler(Reply reply)) {
    _replyHandlersByChannel[channel] = handler;
  }

  void removeReplyHandler(String channel) {
    _replyHandlersByChannel.remove(channel);
  }

  void send(Request request) {
    _socket.sendTypedData(request.writeToBuffer());
  }

  bool get connected => _socket.readyState == WebSocket.OPEN;
}

typedef void _ReplyHandler(Reply reply);

class Dispatcher {
  final ServerConnection connection;
  final Scene scene;
  final String channel;
  Int64 _lastRequestId = Int64.ZERO;
  Int64 _lastReplyId = Int64.ZERO;
  bool _uiLocked = false;

  final StreamController<Null> _onChangeScene = new StreamController.broadcast(sync: true);
  Stream<Null> get onChangeScene => _onChangeScene.stream;

  final Map<Int64, Completer<ActionResult>> _actionsInProgress = {};

  Dispatcher(this.connection, this.scene, this.channel) {
    connection.addReplyHandler(channel, _handleReply);
  }

  void _handleReply(Reply reply) {
    _lastReplyId = reply.id;

    Completer<ActionResult> completer = _actionsInProgress.remove(reply.id);
    check(completer != null);
    if (reply.ok) {
      completer.complete();
    } else {
      completer.completeError(new ActionException());
    }

    if (!waitingForReply && _uiLocked) {
      unlockUi();
      _uiLocked = false;
    }

    if (reply.hasSceneDelta()) {
      scene._applyDelta(reply.sceneDelta);
      _onChangeScene.add(null);
    }
  }

  Future<ActionResult> performAction(String targetName, GeneratedMessage input, {bool disableUi: true}) {
    if (disableUi && !_uiLocked) {
      lockUi();
      _uiLocked = true;
    }

    ++_lastRequestId;

    Completer<ActionResult> completer = new Completer.sync();
    _actionsInProgress[_lastRequestId] = completer;

    Request request = new Request()
        ..channel = channel
        ..targetName = targetName
        ..actionName = input.info_.messageName.substring(input.info_.messageName.lastIndexOf("_") + 1)
        ..input = input.writeToBuffer();

    connection.send(request);
    return completer.future;
  }

  bool get waitingForReply => _lastRequestId != _lastReplyId;

  void stop() {
    connection.removeReplyHandler(channel);
    if (_uiLocked) {
      unlockUi();
      _uiLocked = false;
    }
  }
}

class ActionResult {}

class ActionException implements Exception {}

class Widget<X extends GeneratedMessage> extends WebComponent {
  String _name;
  String get name => _name;

  Dispatcher _dispatcher;
  Dispatcher get dispatcher => _dispatcher;

  Scene _scene;
  Scene get scene => _scene;

  X _shape;
  @protected X get shape => _shape;

  bool _shapeChanged;
  @protected bool get shapeChanged => _shapeChanged;

  Widget.created() : super.created();

  factory Widget(String tag, String name, Dispatcher dispatcher) => (new Element.tag(tag) as Widget)
      .._name = name
      .._dispatcher = dispatcher
      .._scene = dispatcher.scene
      .._shape = dispatcher.scene.shapes(name)
      //ignore: invalid_use_of_protected_member
      ..initialize();

  @protected void initialize() {}

  void render() {
    X newShape = _scene.shapes(_name) as X;
    _shapeChanged = (_shapeChanged == null) ? true : !identical(_shape, newShape);
    _shape = newShape;
  }

  @protected Future<ActionResult> performAction(GeneratedMessage input, {bool disableUi: true}) =>
      _dispatcher.performAction(_name, input, disableUi: disableUi);
}

final Map<String, MessageParser> _shapeParsers = {};

void registerShapeParsers(Iterable<MessageParser> parsers) {
  parsers.forEach(registerShapeParser);
}

void registerShapeParser(MessageParser parser) {
  String messageName = parser([]).info_.messageName;
  if (_shapeParsers.containsKey(messageName)) {
    throw "Duplicate parser for '$messageName'.";
  }
  _shapeParsers[messageName] = parser;
}

@initMethod void initialize() {
  registerShapeParser((List<int> b) => new XBlob.fromBuffer(b));
}
