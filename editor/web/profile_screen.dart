/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

@HtmlImport("profile_screen.html")
library iren_editor.profile_screen;

import 'dart:async';
import 'dart:html';

import 'package:initialize/initialize.dart';
import 'package:iren_proto/editor/profile_screen.pb.dart';
import 'package:iren_proto/profile.pb.dart';
import 'package:misc_utils/misc_utils.dart';
import 'package:web_components/web_components.dart';
import 'package:web_helpers/web_helpers.dart';

import 'common.dart';
import 'editor_infra.dart';
import 'modifier_screen.dart';

@CustomElement(ProfileScreen.TAG)
class ProfileScreen extends Widget<XProfileScreen> {
  static const String TAG = "ru-irenproject-profile-screen";

  @ui DivElement _profilePanel;
  @ui ButtonElement _closeButton;

  ProfileListEditor _profileListEditor;
  /* nullable */ProfileEditor _profileEditor;

  ProfileScreen.created() : super.created();

  factory ProfileScreen(String name, Dispatcher dispatcher) => new Widget(TAG, name, dispatcher);

  @override void initialize() {
    _profileListEditor = new ProfileListEditor(shape.profileListEditor, dispatcher)
        ..id = "list";
    findElement("listPlaceholder").replaceWith(_profileListEditor);

    _closeButton.onClick.listen((_) => _close());

    windowShortcutHandler.add(KeyCode.ESC, KeyModifierState.NONE, _close);
  }

  @override void render() {
    super.render();
    _profileListEditor.render();

    if (shape.hasProfileEditor()) {
      if (shape.profileEditor != _profileEditor?.name) {
        _profileEditor = new ProfileEditor(shape.profileEditor, dispatcher);
        _profilePanel.nodes = [_profileEditor];
      }
      _profileEditor.render();
    } else {
      _profileEditor?.remove();
      _profileEditor = null;
    }
  }

  void _close() {
    performAction(new XProfileScreen_Close());
  }
}

@CustomElement(ProfileListEditor.TAG)
class ProfileListEditor extends Widget<XProfileListEditor> {
  static const String TAG = "ru-irenproject-profile-list-editor";

  @ui ButtonElement _addButton;
  @ui ButtonElement _deleteButton;
  @ui ButtonElement _moveUpButton;
  @ui ButtonElement _moveDownButton;
  @ui DivElement _main;

  ProfileListEditor.created() : super.created();

  factory ProfileListEditor(String name, Dispatcher dispatcher) => new Widget(TAG, name, dispatcher);

  @override void initialize() {
    _addButton.onClick.listen((_) => _add());
    _deleteButton.onClick.listen((_) => _delete());
    _moveUpButton.onClick.listen((_) => _moveUp());
    _moveDownButton.onClick.listen((_) => _moveDown());

    windowShortcutHandler.add(KeyCode.F2, KeyModifierState.NONE, _add);

    new ShortcutHandler(shadowRoot)
        ..add(KeyCode.DELETE, KeyModifierState.NONE, _delete)
        ..add(KeyCode.UP, KeyModifierState.ALT, _moveUp)
        ..add(KeyCode.DOWN, KeyModifierState.ALT, _moveDown);
  }

  @override void render() {
    super.render();
    if (shapeChanged) {
      _main.nodes.clear();
      for (String title in shape.title) {
        _main.append(new DivElement()
            ..text = title
            ..dataset["untitled"] = tr("untitled")
            ..title = title
            ..onClick.listen(_onProfileClick));
      }

      if (shape.hasSelectedIndex()) {
        _main.children[shape.selectedIndex].classes.add("selected");
        scheduleScrollIntoView(_main.children[shape.selectedIndex]);
      }

      _deleteButton.disabled = !shape.hasSelectedIndex();
      _moveUpButton.disabled = !_canMoveUp();
      _moveDownButton.disabled = !_canMoveDown();
    }
  }

  void _onProfileClick(MouseEvent e) {
    performAction(new XProfileListEditor_Select()
        ..index = _main.children.indexOf(e.currentTarget));
  }

  void _add() {
    performAction(new XProfileListEditor_Add());
  }

  void _delete() {
    if (shape.hasSelectedIndex()) {
      performAction(new XProfileListEditor_Delete());
    }
  }

  void _moveUp() {
    if (_canMoveUp()) {
      performAction(new XProfileListEditor_Move()
          ..forward = false);
    }
  }

  bool _canMoveUp() => shape.hasSelectedIndex() && (shape.selectedIndex > 0);

  void _moveDown() {
    if (_canMoveDown()) {
      performAction(new XProfileListEditor_Move()
          ..forward = true);
    }
  }

  bool _canMoveDown() => shape.hasSelectedIndex() && (shape.selectedIndex < shape.title.length - 1);
}

@CustomElement(ProfileEditor.TAG)
class ProfileEditor extends Widget<XProfileEditor> {
  static const String TAG = "ru-irenproject-profile-editor";

  @ui TextInputElement _title;
  @ui FormElement _options;
  @ui RadioButtonInputElement _selectAll;
  @ui RadioButtonInputElement _selectRandom;
  @ui NumberInputElement _questionsPerSection;
  @ui CheckboxInputElement _useLabelFilter;
  @ui TextInputElement _labelFilter;
  @ui CheckboxInputElement _shuffleQuestions;
  @ui RadioButtonInputElement _unlimitedTime;
  @ui RadioButtonInputElement _limitedTime;
  @ui NumberInputElement _timeLimit;
  @ui CheckboxInputElement _editableAnswers;
  @ui CheckboxInputElement _instantAnswerCorrectness;
  @ui CheckboxInputElement _instantTotalPercentCorrect;
  @ui CheckboxInputElement _browsableQuestions;
  @ui CheckboxInputElement _weightCues;
  @ui FormElement _score;
  @ui CheckboxInputElement _showScore;
  @ui CheckboxInputElement _percentCorrect;
  @ui CheckboxInputElement _mark;
  @ui CheckboxInputElement _points;
  @ui CheckboxInputElement _showQuestionScore;
  @ui CheckboxInputElement _questionPercentCorrect;
  @ui CheckboxInputElement _questionCorrectAnswer;
  @ui CheckboxInputElement _questionPoints;
  @ui CheckboxInputElement _showSectionScore;
  @ui CheckboxInputElement _sectionPercentCorrect;
  @ui CheckboxInputElement _sectionPoints;
  @ui CheckboxInputElement _sectionQuestionCount;
  @ui CheckboxInputElement _sectionQuestionList;
  @ui TableElement _markScale;
  @ui ButtonElement _addMarkButton;

  ModifierListEditor _modifierListEditor;

  ProfileEditor.created() : super.created();

  factory ProfileEditor(String name, Dispatcher dispatcher) => new Widget(TAG, name, dispatcher);

  @override void initialize() {
    Element.submitEvent.forTarget(shadowRoot).listen((Event e) => e.preventDefault());

    _title.onInput.listen((_) => _onTitleInput());

    listenToChangeAndInput(_options, _onOptionsChange);
    _options.onClick.listen(_onOptionsClick);
    _selectRandom.onClick.listen((_) => focusInputElement(_questionsPerSection));
    _questionsPerSection.onBlur.listen((_) => _renderOptions());
    _limitedTime.onClick.listen((_) => focusInputElement(_timeLimit));
    _timeLimit.onBlur.listen((_) => _renderOptions());

    _modifierListEditor = new ModifierListEditor(shape.modifierListEditor, dispatcher);
    findElement("modifierListEditorPlaceholder").replaceWith(_modifierListEditor);

    _score.onChange.listen((_) => _onScoreChange());
    _showScore.onClick.listen((_) => _onShowScoreClick());
    _showQuestionScore.onClick.listen((_) => _onShowQuestionScoreClick());
    _showSectionScore.onClick.listen((_) => _onShowSectionScoreClick());

    _addMarkButton.onClick.listen((_) => _addMark());
  }

  @override void render() {
    super.render();
    if (shapeChanged) {
      updateTextInput(_title, shape.title, collapseSpaces);
      _renderOptions();
      _renderScore();
      _renderMarkScale();
    }

    _modifierListEditor
        ..render()
        ..autoScrollToSelection = true;
  }

  void _onTitleInput() {
    performAction(new XProfileEditor_SetTitle()
        ..title = collapseSpaces(_title.value), disableUi: false);
  }

  void _renderOptions() {
    if (shape.options.hasQuestionsPerSection()) {
      _selectRandom.checked = true;
      updateNumberInput(_questionsPerSection, shape.options.questionsPerSection);
    } else {
      _selectAll.checked = true;
    }

    _useLabelFilter.checked = shape.options.hasLabelFilter();
    if (shape.options.hasLabelFilter()) {
      updateTextInput(_labelFilter, shape.options.labelFilter);
    }

    _shuffleQuestions.checked = shape.options.shuffleQuestions;

    if (shape.options.hasDurationMinutes()) {
      _limitedTime.checked = true;
      updateNumberInput(_timeLimit, shape.options.durationMinutes);
    } else {
      _unlimitedTime.checked = true;
    }

    _editableAnswers.checked = shape.options.editableAnswers;
    _instantAnswerCorrectness.checked = shape.options.instantAnswerCorrectness;
    _instantTotalPercentCorrect.checked = shape.options.instantTotalPercentCorrect;
    _browsableQuestions.checked = shape.options.browsableQuestions;
    _weightCues.checked = shape.options.weightCues;

    _updateOptionsUi();
  }

  void _updateOptionsUi() {
    _questionsPerSection.disabled = _selectAll.checked;
    _useLabelFilter.disabled = !_useLabelFilter.checked; //TODO
    _timeLimit.disabled = _unlimitedTime.checked;
    _instantAnswerCorrectness.disabled = _instantTotalPercentCorrect.disabled = _editableAnswers.checked;
  }

  void _onOptionsChange(Event e) {
    _updateOptionsUi();
    if (_options.checkValidity()) {
      ProfileOptions options = new ProfileOptions();

      if (_selectRandom.checked) {
        options.questionsPerSection = _questionsPerSection.valueAsNumber;
      }

      if (_useLabelFilter.checked) {
        options.labelFilter = _labelFilter.value;
      }

      options.shuffleQuestions = _shuffleQuestions.checked;

      if (_limitedTime.checked) {
        options.durationMinutes = _timeLimit.valueAsNumber;
      }

      options
          ..editableAnswers = _editableAnswers.checked
          ..instantAnswerCorrectness = !_editableAnswers.checked && _instantAnswerCorrectness.checked
          ..instantTotalPercentCorrect = !_editableAnswers.checked && _instantTotalPercentCorrect.checked
          ..browsableQuestions = _browsableQuestions.checked
          ..weightCues = _weightCues.checked;

      performAction(new XProfileEditor_SetOptions()
          ..options = options, disableUi: e.type != "input");
    }
  }

  void _onOptionsClick(MouseEvent e) {
    if ((e.target == _questionsPerSection) && _questionsPerSection.disabled) {
      _selectRandom.dispatchEvent(new MouseEvent("click"));
    } else if ((e.target == _timeLimit) && _timeLimit.disabled) {
      _limitedTime.dispatchEvent(new MouseEvent("click"));
    }
  }

  void _renderScore() {
    _showScore.checked = shape.hasScore();
    _percentCorrect.checked = shape.hasScore() && shape.score.percentCorrect;
    _mark.checked = shape.hasScore() && shape.score.mark;
    _points.checked = shape.hasScore() && shape.score.points;

    AvailableQuestionScore q = (shape.hasScore() && shape.score.hasForQuestions()) ? shape.score.forQuestions : null;
    _showQuestionScore.checked = (q != null);
    _questionPercentCorrect.checked = q?.percentCorrect ?? false;
    _questionCorrectAnswer.checked = q?.correctAnswer ?? false;
    _questionPoints.checked = q?.points ?? false;

    AvailableSectionScore s = (shape.hasScore() && shape.score.hasForSections()) ? shape.score.forSections : null;
    _showSectionScore.checked = (s != null);
    _sectionPercentCorrect.checked = s?.percentCorrect ?? false;
    _sectionPoints.checked = s?.points ?? false;
    _sectionQuestionCount.checked = s?.questionCount ?? false;
    _sectionQuestionList.checked = s?.questionList ?? false;

    _updateScoreUi();
  }

  void _updateScoreUi() {
    _percentCorrect.disabled = _mark.disabled = _points.disabled = _showQuestionScore.disabled =
        _showSectionScore.disabled = !_showScore.checked;
    _questionPercentCorrect.disabled = _questionCorrectAnswer.disabled = _questionPoints.disabled =
        !_showQuestionScore.checked;
    _sectionPercentCorrect.disabled = _sectionPoints.disabled = _sectionQuestionCount.disabled =
        !_showSectionScore.checked;
    _sectionQuestionList.disabled = !(_showQuestionScore.checked && _showSectionScore.checked);
  }

  void _onScoreChange() {
    XProfileEditor_SetScore action = new XProfileEditor_SetScore();
    if (_showScore.checked) {
      AvailableScore score = new AvailableScore()
          ..percentCorrect = _percentCorrect.checked
          ..mark = _mark.checked
          ..points = _points.checked;
      if (_showQuestionScore.checked) {
        score.forQuestions = new AvailableQuestionScore()
            ..percentCorrect = _questionPercentCorrect.checked
            ..correctAnswer = _questionCorrectAnswer.checked
            ..points = _questionPoints.checked;
      }
      if (_showSectionScore.checked) {
        score.forSections = new AvailableSectionScore()
            ..percentCorrect = _sectionPercentCorrect.checked
            ..points = _sectionPoints.checked
            ..questionCount = _sectionQuestionCount.checked
            ..questionList = _sectionQuestionList.checked && _showQuestionScore.checked;
      }
      action.score = score;
    }
    performAction(action);
  }

  void _onShowScoreClick() {
    if (_showScore.checked) {
      _percentCorrect.checked = _mark.checked = true;
    }
  }

  void _onShowQuestionScoreClick() {
    if (_showQuestionScore.checked) {
      _questionPercentCorrect.checked = true;
    }
  }

  void _onShowSectionScoreClick() {
    if (_showSectionScore.checked) {
      _sectionPercentCorrect.checked = true;
    }
  }

  void _renderMarkScale() {
    TableSectionElement tbody = _markScale.tBodies.first
        ..nodes.clear();
    for (Mark mark in shape.markScale.mark) {
      tbody.addRow()
          ..addCell().text = _normalizedDecimalToUiPercent(mark.lowerBound)
          ..addCell().text = mark.title
          ..onClick.listen(_onMarkRowClick);
    }
    _addMarkButton.disabled = (shape.markScale.mark.length == shape.maxMarks);
  }

  Future<Null> _addMark() async {
    if (await MarkDialog.openModal(dispatcher, name, index: shape.markScale.mark.isEmpty ? 0 : null)) {
      scheduleScrollIntoView(_addMarkButton);
      _addMarkButton.focus();
    }
  }

  void _onMarkRowClick(MouseEvent e) {
    int index = (e.currentTarget as TableRowElement).sectionRowIndex;
    MarkDialog.openModal(dispatcher, name, mark: shape.markScale.mark[index], index: index);
  }
}

@CustomElement(MarkDialog.TAG)
class MarkDialog extends ModalDialog {
  static const String TAG = "ru-irenproject-mark-dialog";

  static Future<bool> openModal(Dispatcher dispatcher, String targetName, {Mark mark, int index}) =>
      ((new Element.tag(TAG) as MarkDialog)
          .._dispatcher = dispatcher
          .._targetName = targetName
          .._mark = mark
          .._index = index)._doOpenModal();

  @ui DialogElement _dialog;
  @ui NumberInputElement _lowerBound;
  @ui TextInputElement _title;
  @ui DivElement _buttons;

  Dispatcher _dispatcher;
  String _targetName;
  /* nullable */Mark _mark;
  /* nullable */int _index;

  MarkDialog.created() : super.created();

  Future<bool> _doOpenModal() async {
    bool res;

    if (_mark == null) {
      _buttons.children.insert(0, new ButtonElement()
          ..value = "add"
          ..text = tr("Add"));
    } else {
      _lowerBound.valueAsNumber = num.parse(anyDecimalMarkToDot(_normalizedDecimalToUiPercent(_mark.lowerBound)));
      _title.value = _mark.title;

      _buttons.children
          ..insert(0, new ButtonElement()
              ..value = "update"
              ..text = tr("Update"))
          ..insert(1, new ButtonElement()
              ..value = "delete"
              ..text = tr("Delete")
              ..formNoValidate = true);
    }

    if (_index == 0) {
      _lowerBound
          ..valueAsNumber = 0
          ..disabled = true;
    }

    await open(_dialog);

    switch (_dialog.returnValue) {
      case "add":
        await _dispatcher.performAction(_targetName, new XProfileEditor_AddMark()
            ..mark = _editedMark());
        res = true;
        break;
      case "update":
      case "delete":
        XProfileEditor_UpdateMark action = new XProfileEditor_UpdateMark()
            ..index = _index;
        if (_dialog.returnValue == "update") {
          action.mark = _editedMark();
        }
        await _dispatcher.performAction(_targetName, action);
        res = true;
        break;
      default:
        res = false;
    }

    return res;
  }

  Mark _editedMark() => new Mark()
      ..title = collapseSpaces(_title.value)
      ..lowerBound = _percentToNormalizedDecimal(_lowerBound.valueAsNumber);
}

String _normalizedDecimalToUiPercent(String d) =>
    "${int.parse(d.substring(0, 1) + d.substring(2, 4))}$uiDecimalMark${d.substring(4, 6)}";

String _percentToNormalizedDecimal(num value) => (value / 100).toStringAsFixed(4);

@initMethod void initialize() {
  registerShapeParsers([
      (List<int> b) => new XProfileScreen.fromBuffer(b),
      (List<int> b) => new XProfileListEditor.fromBuffer(b),
      (List<int> b) => new XProfileEditor.fromBuffer(b)]);
}
