/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

@HtmlImport("common_dialogs.html")
library iren_editor.common_dialogs;

import 'dart:async';
import 'dart:html';
import 'dart:math' as math;

import 'package:misc_utils/misc_utils.dart';
import 'package:web_components/web_components.dart';

import 'common.dart';

@CustomElement(InputStringDialog.TAG)
class InputStringDialog extends ModalDialog {
  static const String TAG = "ru-irenproject-input-string-dialog";

  @ui DialogElement _dialog;
  @ui SpanElement _prompt;
  @ui InputElement _value;

  InputStringDialog.created() : super.created();

  Future<Null> _open({String prompt}) {
    if (prompt != null) {
      _prompt.text = prompt;
    }

    focusInputElement(_value);
    return open(_dialog);
  }
}

Future</* nullable */int> inputInteger({String prompt, int min, int max, int initial}) async {
  InputStringDialog d = new Element.tag(InputStringDialog.TAG);
  d._value
      ..type = "number"
      ..required = true;
  NumberInputElement value = d._value;

  if (min != null) {
    value.min = min.toString();
  }

  if (max != null) {
    value.max = max.toString();
  }

  if (initial != null) {
    value.valueAsNumber = initial;
  }

  await d._open(prompt: prompt);
  return d._dialog.returnValue.isEmpty ? null : value.valueAsNumber;
}

Future</* nullable */String> inputString({String prompt, int maxLength, String initial}) async {
  InputStringDialog d = new Element.tag(InputStringDialog.TAG);

  if (maxLength != null) {
    d._value
        ..maxLength = maxLength
        ..size = math.min(maxLength, 50);
  }

  if (initial != null) {
    d._value.value = initial;
  }

  await d._open(prompt: prompt);
  return d._dialog.returnValue.isEmpty ? null : d._value.value;
}
