/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

@HtmlImport("select_question_editor.html")
library iren_editor.select_question_editor;

import 'dart:html';

import 'package:initialize/initialize.dart';
import 'package:iren_proto/editor/select_question_editor.pb.dart';
import 'package:iren_proto/question/select_question.pb.dart';
import 'package:misc_utils/misc_utils.dart';
import 'package:web_components/web_components.dart';
import 'package:web_helpers/translator.dart';

import 'common.dart';
import 'editor_infra.dart';
import 'pad_editor.dart';

@QuestionEditor(SelectQuestionEditor.TAG, XSelectQuestionEditor)
class SelectQuestionEditor extends Widget<XSelectQuestionEditor> {
  static const String TAG = "ru-irenproject-select-question-editor";

  @ui DivElement _toolbar;
  @ui ButtonElement _addChoiceButton;
  @ui ButtonElement _deleteChoiceButton;
  @ui ButtonElement _moveChoiceUpButton;
  @ui ButtonElement _moveChoiceDownButton;
  @ui ButtonElement _addNegativeChoiceButton;
  @ui ButtonElement _fixChoiceButton;
  @ui DivElement _choicePanel;

  PadEditor _formulation;

  /* nullable */int _selectChoice;
  /* nullable */int _focusedChoice;

  SelectQuestionEditor.created() : super.created();

  @override void initialize() {
    _addChoiceButton.onClick.listen((_) => _addChoice());
    _deleteChoiceButton.onClick.listen((_) => _deleteChoice());
    _moveChoiceUpButton.onClick.listen((_) => _moveChoiceUp());
    _moveChoiceDownButton.onClick.listen((_) => _moveChoiceDown());
    _addNegativeChoiceButton.onClick.listen((_) => _addNegativeChoice());
    _fixChoiceButton.onClick.listen((_) => _fixChoice());

    windowShortcutHandler
        ..add(KeyCode.F5, KeyModifierState.NONE, _addChoice)
        ..add(KeyCode.F8, KeyModifierState.NONE, _deleteChoice)
        ..add(KeyCode.F6, KeyModifierState.NONE, _addNegativeChoice);

    new ShortcutHandler(shadowRoot)
        ..add(KeyCode.UP, KeyModifierState.ALT, _moveChoiceUp)
        ..add(KeyCode.DOWN, KeyModifierState.ALT, _moveChoiceDown);

    _formulation = new PadEditor(shape.formulationEditor, dispatcher)
        ..id = "formulation";
    findElement("formulationPlaceholder").replaceWith(_formulation);

    Element.focusEvent.forTarget(shadowRoot, useCapture: true).listen((_) => _onFocus());
    Element.blurEvent.forTarget(shadowRoot, useCapture: true).listen(_onBlur);
  }

  @override void render() {
    super.render();
    _formulation.render();

    if (shapeChanged) {
      _focusedChoice = null;
      _choicePanel.nodes = shape.choiceEditor.map((String name) => new ChoiceEditor(name, dispatcher));
      _addChoiceButton.disabled = !shape.canAddChoice;
      _addNegativeChoiceButton.disabled = !shape.canAddNegativeChoice;
    }

    for (ChoiceEditor e in _choicePanel.children) {
      e.render();
    }

    if ((_selectChoice != null) && shape.choiceEditor.isNotEmpty) {
      (_choicePanel.children[_selectChoice.clamp(0, shape.choiceEditor.length - 1)] as ChoiceEditor).focusComponent();
    }
    _selectChoice = null;

    _updateUi();
  }

  void _addChoice() {
    if (shape.canAddChoice) {
      performAction(new XSelectQuestionEditor_AddChoice());
      _selectChoice = shape.choiceEditor.length;
    }
  }

  void _deleteChoice() {
    if (_focusedChoice != null) {
      performAction(new XSelectQuestionEditor_DeleteChoice()
          ..index = _focusedChoice);
      _selectChoice = _focusedChoice;
    }
  }

  void _onFocus() {
    int index = _choicePanel.children.indexOf(shadowRoot.activeElement);
    if (index != -1) {
      _focusedChoice = index;
    }
    _updateUi();
  }

  void _onBlur(Event e) {
    if (!_toolbar.contains((e as FocusEvent).relatedTarget)) {
      _focusedChoice = null;
    }
    _updateUi();
  }

  void _updateUi() {
    _deleteChoiceButton.disabled = (_focusedChoice == null);
    _moveChoiceUpButton.disabled = !_canMoveChoiceUp();
    _moveChoiceDownButton.disabled = !_canMoveChoiceDown();

    _fixChoiceButton
        ..disabled = (_focusedChoice == null)
        ..classes.toggle("highlighted", (_focusedChoice == null) ?
            false : (_choicePanel.children[_focusedChoice] as ChoiceEditor).fixed);
  }

  void _moveChoiceUp() {
    if (_canMoveChoiceUp()) {
      performAction(new XSelectQuestionEditor_MoveChoice()
          ..index = _focusedChoice
          ..forward = false);
     _selectChoice = _focusedChoice - 1;
    }
  }

  bool _canMoveChoiceUp() => (_focusedChoice != null) && (_focusedChoice > 0);

  void _moveChoiceDown() {
    if (_canMoveChoiceDown()) {
      performAction(new XSelectQuestionEditor_MoveChoice()
          ..index = _focusedChoice
          ..forward = true);
     _selectChoice = _focusedChoice + 1;
    }
  }

  bool _canMoveChoiceDown() => (_focusedChoice != null) && (_focusedChoice < shape.choiceEditor.length - 1);

  void _addNegativeChoice() {
    if (shape.canAddNegativeChoice) {
      performAction(new XSelectQuestionEditor_AddNegativeChoice()
          ..text = tr("None of the above are correct."));
      _selectChoice = shape.choiceEditor.length;
    }
  }

  void _fixChoice() {
    if (_focusedChoice != null) {
      ChoiceEditor choiceEditor = _choicePanel.children[_focusedChoice] as ChoiceEditor;
      choiceEditor.performSetFixed(!choiceEditor.fixed);
    }
  }

  @override void focusComponent() {
    _formulation.focusComponent();
  }
}

@CustomElement(ChoiceEditor.TAG)
class ChoiceEditor extends Widget<XSelectQuestionChoiceEditor> {
  static const String TAG = "ru-irenproject-select-question-choice-editor";

  @ui CheckboxInputElement _correct;
  @ui ImageElement _negativeChoiceIndicator;
  @ui ImageElement _fixedChoiceIndicator;

  PadEditor _padEditor;

  ChoiceEditor.created() : super.created();

  factory ChoiceEditor(String name, Dispatcher dispatcher) => new Widget(TAG, name, dispatcher);

  @override void initialize() {
    _correct.onClick.listen((_) => _onCorrectClick());

    _padEditor = new PadEditor(shape.padEditor, dispatcher)
        ..id = "padEditor";
    findElement("padEditorPlaceholder").replaceWith(_padEditor);
  }

  @override void render() {
    super.render();

    if (shapeChanged) {
      _correct.checked = shape.correct;
      _padEditor.classes.toggle("correct", shape.correct);

      _negativeChoiceIndicator.style.visibility = shape.negative ? "" : "hidden";
      _fixedChoiceIndicator.style.visibility = shape.fixed ? "" : "hidden";
    }

    _padEditor.render();
  }

  void _onCorrectClick() {
    performAction(new XSelectQuestionChoiceEditor_SetCorrect()
        ..correct = _correct.checked);
  }

  @override void focusComponent() {
    _padEditor.focusComponent();
  }

  bool get fixed => shape.fixed;

  void performSetFixed(bool value) {
    performAction(new XSelectQuestionChoiceEditor_SetFixed()
        ..fixed = value);
  }
}

const String _LIB = "select_question_editor";

@initMethod void initialize() {
  registerShapeParsers([
      (List<int> b) => new XSelectQuestionEditor.fromBuffer(b),
      (List<int> b) => new XSelectQuestionChoiceEditor.fromBuffer(b)]);

  registerQuestionDescriptor(new QuestionDescriptor(
      type: SelectQuestionType.select.name,
      title: translate(_LIB, "Multiple Choice Question"),
      priority: 1000,
      icon: "select_question_editor.resources/selectQuestion.png",
      initialText: ""));
}
