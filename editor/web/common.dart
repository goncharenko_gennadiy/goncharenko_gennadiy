/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

import 'dart:async';
import 'dart:collection';
import 'dart:html';
import 'dart:js';

import 'package:meta/meta.dart';
import 'package:misc_utils/electron.dart';
import 'package:protobuf/protobuf.dart' show ProtobufEnum;
import 'package:web_components/web_components.dart' show CustomElement;
import 'package:web_helpers/web_helpers.dart';

class QuestionDescriptor {
  final String type;
  final String title;
  final int priority;
  final String icon;
  final String initialText;

  QuestionDescriptor({@required this.type, @required this.title, @required this.priority, @required this.icon,
      @required this.initialText});
}

class QuestionEditor extends CustomElement {
  final Type shapeClass;

  const QuestionEditor(String tag, this.shapeClass) : super(tag);

  @override initialize(Type target) {
    super.initialize(target);
    check(!_questionEditors.containsKey(shapeClass));
    _questionEditors[shapeClass] = tag;
  }
}

class ModifierDescriptor {
  final String type;
  final String title;
  final /* nullable */List<String> questionTypes;

  ModifierDescriptor({@required this.type, @required this.title, this.questionTypes});
}

final Map<String, QuestionDescriptor> _questionDescriptorsByType = {};
final SplayTreeMap<int, QuestionDescriptor> _questionDescriptorsByPriority = new SplayTreeMap();
final Map<Type, String> _questionEditors = {};
final LinkedHashMap<String, ModifierDescriptor> _modifierDescriptorsByType = new LinkedHashMap();

void registerQuestionDescriptor(QuestionDescriptor d) {
  check(!_questionDescriptorsByType.containsKey(d.type));
  check(!_questionDescriptorsByPriority.containsKey(d.priority));

  _questionDescriptorsByType[d.type] = d;
  _questionDescriptorsByPriority[d.priority] = d;
}

/* nullable */QuestionDescriptor getQuestionDescriptor(String type) => _questionDescriptorsByType[type];

Iterable<QuestionDescriptor> get questionDescriptors => _questionDescriptorsByPriority.values;

String getQuestionEditorTag(Type shapeClass) {
  String res = _questionEditors[shapeClass];
  if (res == null) {
    throw "No question editor for '$shapeClass'";
  }
  return res;
}

void registerModifierDescriptor(ModifierDescriptor d) {
  check(!_modifierDescriptorsByType.containsKey(d.type));
  _modifierDescriptorsByType[d.type] = d;
}

/* nullable */ModifierDescriptor getModifierDescriptor(String type) => _modifierDescriptorsByType[type];

Iterable<ModifierDescriptor> get modifierDescriptors => _modifierDescriptorsByType.values;

String collapseSpaces(String s) => s.trim().replaceAll(new RegExp(r"\s+"), " ");

String dotToUiDecimalMark(String s) => s.replaceFirst(".", uiDecimalMark);

String anyDecimalMarkToDot(String s) => s.replaceFirst(",", ".");

ProtobufEnum getProtobufEnumByName(Iterable<ProtobufEnum> values, String name) =>
    values.firstWhere((ProtobufEnum v) => v.name == name);

void normalizeNode(Node node) {
  new JsObject.fromBrowserObject(node).callMethod("normalize");
}

void focusInputElement(InputElement e) {
  Timer.run(() {
    e
        ..focus()
        ..select();
  });
}

void listenToChangeAndInput(Element element, void handler(Event event)) {
  element
      ..onChange.listen((Event e) {
        if (!((e.target is InputElement) && ["text", "password", "number"].contains((e.target as InputElement).type))) {
          handler(e);
        }
      })
      ..onInput.listen(handler);
}

void updateTextInput(TextInputElement element, String value, [String oldValueNormalizer(String)]) {
  String oldValue = (oldValueNormalizer == null) ? element.value : oldValueNormalizer(element.value);
  if (oldValue != value) {
    element.value = value;
  }
}

void updateNumberInput(NumberInputElement element, num value) {
  if (element.valueAsNumber != value) {
    element.valueAsNumber = value;
  }
}

void scheduleScrollIntoView(Element e) {
  window.animationFrame.then((_) => e.scrollIntoView());
}

Future<Null> get postAnimationFrame async {
  await window.animationFrame;
  await new Future<Null>.delayed(Duration.ZERO);
}

String getAbsoluteAppPath(String path) => "${app_getAppPath()}/$path";
