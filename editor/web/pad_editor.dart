/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

@HtmlImport("pad_editor.html")
library iren_editor.pad_editor;

import 'dart:html';
import 'dart:js';
import 'dart:math' as math;
import 'dart:typed_data';

import 'package:image/image.dart' show DecodeInfo, Decoder, JpegDecoder, PngDecoder;
import 'package:initialize/initialize.dart';
import 'package:iren_proto/editor/infra.pb.dart';
import 'package:iren_proto/editor/pad_editor.pb.dart';
import 'package:misc_utils/electron.dart';
import 'package:misc_utils/misc_utils.dart';
import 'package:misc_utils/test_utils.dart';
import 'package:web_components/web_components.dart';

import 'common.dart';
import 'editor_infra.dart';

typedef Decoder _DecoderFactory();

@CustomElement(PadEditor.TAG)
class PadEditor extends Widget<XPadEditor> {
  static const String TAG = "ru-irenproject-pad-editor";
  static const String _PAD_MIME_TYPE = "application/x-irenproject.ru-pad";
  static const String _TEXT_MIME_TYPE = "text/plain";
  static const String _PNG_MIME_TYPE = "image/png";

  static final Map<String, _DecoderFactory> _imageDecoders = {
      "image/png": () => new PngDecoder(),
      "image/jpeg": () => new JpegDecoder()};

  @ui DivElement _root;

  /* nullable */int _desiredCaretPosition;
  /* nullable */int _desiredCaretOffsetFromEnd;
  Map<String, _Image> _imageCache = {};

  SpanElement _caretHelper = new SpanElement()
      ..id = "caretHelper"
      ..text = "|";

  /* nullable */int _selectionStartBeforeComposition;
  /* nullable */int _selectionEndBeforeComposition;

  PadEditor.created() : super.created();

  factory PadEditor(String name, Dispatcher dispatcher) => new Widget(TAG, name, dispatcher);

  @override void initialize() {
    _root
        ..onKeyPress.listen(_onKeyPress)
        ..onKeyDown.listen(_onKeyDown)
        ..onBlur.listen((_) => _onBlur())
        ..onCopy.listen(_onCopy)
        ..onCut.listen(_onCut)
        ..onPaste.listen(_onPaste)
        ..onMouseDown.listen(_onMouseDown)
        ..onDoubleClick.listen(_onDoubleClick)
        ..onContextMenu.listen(_onContextMenu)
        ..on["compositionstart"].listen((_) => _onCompositionStart())
        ..on["compositionend"].listen(_onCompositionEnd);

    new ShortcutHandler(_root)
        ..add(KeyCode.Z, KeyModifierState.CTRL, () {})
        ..add(KeyCode.Z, KeyModifierState.CTRL_SHIFT, () {})
        ..add(KeyCode.Y, KeyModifierState.CTRL, () {})
        ..add(KeyCode.A, KeyModifierState.CTRL_SHIFT, () {});
  }

  @override void detached() {
    for (_Image image in _imageCache.values) {
      image._release();
    }
    _imageCache.clear();
    super.detached();
  }

  @override void render() {
    super.render();
    if (shapeChanged) {
      for (_Image image in _imageCache.values) {
        image.used = false;
      }

      int i = 0;
      int p = 0;
      Node caretNode = null;
      int caretOffset = null;

      if (_desiredCaretOffsetFromEnd != null) {
        int totalLength = 0;
        for (Block block in shape.block) {
          totalLength += (block.type == BlockType.TEXT) ? block.textBlock.text.length : 1;
        }
        _desiredCaretPosition = math.max(totalLength - _desiredCaretOffsetFromEnd, 0);
        _desiredCaretOffsetFromEnd = null;
      }

      if (_desiredCaretPosition == 0) {
        caretNode = _root;
        caretOffset = 0;
        _desiredCaretPosition = null;
      }

      for (Block block in shape.block) {
        Node existing = (i < _root.nodes.length) ? _root.nodes[i] : null;

        switch (block.type) {
          case BlockType.TEXT:
            String text = block.textBlock.text;
            if (existing is Text) {
              existing.text = text;
            } else {
              _appendOrReplaceWith(existing, new Text(text));
            }
            break;
          case BlockType.IMAGE:
            _Image cachedImage = _getCachedImage(block.imageBlock.data, block.imageBlock.mimeType)
                ..used = true;
            bool alreadyRendered = (existing is ImageElement) && (existing.src == cachedImage.blobUrl);
            if (!alreadyRendered) {
              _appendOrReplaceWith(existing, new ImageElement()
                  ..style.width = "${cachedImage.size.width}px"
                  ..style.height = "${cachedImage.size.height}px"
                  ..dataset["blob"] = block.imageBlock.data
                  ..dataset["mimeType"] = block.imageBlock.mimeType
                  ..src = cachedImage.blobUrl);
            }
            break;
          case BlockType.LINE_FEED:
            if (existing is! BRElement) {
              _appendOrReplaceWith(existing, new BRElement());
            }
            break;
          default:
            throw "Unknown block type '${block.type}'.";
        }

        if (_desiredCaretPosition != null) {
          Node current = _root.nodes[i];
          if ((current is Text) && (p + current.length > _desiredCaretPosition)) {
            caretNode = current;
            caretOffset = _desiredCaretPosition - p;
            _desiredCaretPosition = null;
          }

          if (_desiredCaretPosition != null) {
            p += _getNodeLength(current);
            if (p >= _desiredCaretPosition) {
              caretNode = _root;
              caretOffset = i + 1;
              _desiredCaretPosition = null;
            }
          }
        }

        ++i;
      }

      Node existing = (i < _root.nodes.length) ? _root.nodes[i] : null;
      if (existing is! BRElement) {
        _appendOrReplaceWith(existing, new BRElement());
      }
      ++i;

      while (_root.nodes.length > i) {
        _root.lastChild.remove();
      }

      _desiredCaretPosition = null;

      if (caretNode != null) {
        shadowRoot.getSelection()
            ..collapse(caretNode, caretOffset)
            ..getRangeAt(0).insertNode(_caretHelper);
        _caretHelper
            ..scrollIntoView()
            ..remove();
        normalizeNode(_root);
        shadowRoot.getSelection().collapse(caretNode, caretOffset);
      }

      _removeUnusedCachedImages();
    }
  }

  void _appendOrReplaceWith(/* nullable */Node existing, Node fresh) {
    if (existing == null) {
      _root.append(fresh);
    } else {
      existing.replaceWith(fresh);
    }
  }

  _Image _getCachedImage(String blobName, String mimeType) => _imageCache.putIfAbsent("$mimeType:$blobName", () {
    List<int> data = (scene.shapes(blobName) as XBlob).blob;
    _ImageSize size = _getImageSize(data, mimeType);
    Blob blob = new Blob([data], mimeType);
    return new _Image(Url.createObjectUrlFromBlob(blob), size);
  });

  _ImageSize _getImageSize(List<int> imageData, String mimeType) {
    _DecoderFactory f = _imageDecoders[mimeType];
    if (f == null) {
      throw "No image decoder for '$mimeType'.";
    }
    DecodeInfo info = f().startDecode(imageData);
    return new _ImageSize(info.width, info.height);
  }

  void _removeUnusedCachedImages() {
    List<String> unused = [];
    _imageCache.forEach((String key, _Image image) {
      if (!image.used) {
        image._release();
        unused.add(key);
      }
    });
    unused.forEach(_imageCache.remove);
  }

  void _onKeyPress(KeyboardEvent e) {
    if (e.charCode < 32) {
      stopFurtherProcessing(e);
    } else {
      _deleteSelection();

      int p = _caretPosition();
      String text = new String.fromCharCode(e.charCode);

      performAction(new XPadEditor_InsertText()
          ..position = p
          ..text = text, disableUi: false);

      _desiredCaretPosition = p + text.length;
    }
  }

  int _caretPosition() {
    int res;

    Selection s = shadowRoot.getSelection();
    if (s.rangeCount == 1) {
      Range r = s.getRangeAt(0);
      res = _computePosition(r.startContainer, r.startOffset);
    } else {
      res = 0;
    }

    return res;
  }

  int _computePosition(Node node, int offset) {
    int res;
    Node previous;

    if (node is Text) {
      res = offset;
      previous = node.previousNode;
    } else if (node == _root) {
      res = 0;
      previous = (offset > 0 ) ? _root.nodes[offset - 1] : null;
    } else { // should not happen
      res = 0;
      previous = null;
    }

    while (previous != null) {
      res += _getNodeLength(previous);
      previous = previous.previousNode;
    }

    return res;
  }

  int _getNodeLength(Node node) => (node is Text) ? node.length : 1;

  void _onCompositionStart() {
    Range r = _selectedRange();
    if (r == null) {
      _selectionStartBeforeComposition = _selectionEndBeforeComposition = null;
    } else {
      _selectionStartBeforeComposition = _computePosition(r.startContainer, r.startOffset);
      _selectionEndBeforeComposition = _computePosition(r.endContainer, r.endOffset);
    }
  }

  void _onCompositionEnd(CompositionEvent e) {
    if (_selectionStartBeforeComposition != null) {
      performAction(new XPadEditor_Delete()
          ..position = _selectionStartBeforeComposition
          ..length = _selectionEndBeforeComposition - _selectionStartBeforeComposition);
      _selectionStartBeforeComposition = _selectionEndBeforeComposition = null;
    }

    int p = _caretPosition();
    performAction(new XPadEditor_InsertText()
        ..position = p
        ..text = e.data);
    _desiredCaretPosition = p + e.data.length;
  }

  void _onKeyDown(KeyboardEvent e) {
    switch (e.keyCode) {
      case KeyCode.ENTER:
        _handleEnter(e);
        break;
      case KeyCode.BACKSPACE:
        _handleBackspace(e);
        break;
      case KeyCode.DELETE:
        _handleDelete(e);
        break;
      case KeyCode.PAGE_DOWN:
        _handlePageDown(e);
        break;
      case KeyCode.PAGE_UP:
        _handlePageUp(e);
        break;
    }
  }

  void _handleEnter(KeyboardEvent e) {
    stopFurtherProcessing(e);
    _deleteSelection();

    int p = _caretPosition();
    performAction(new XPadEditor_InsertText()
        ..position = p
        ..text = "\n");

    _desiredCaretPosition = p + 1;
  }

  void _handleBackspace(KeyboardEvent e) {
    stopFurtherProcessing(e);
    if (!_deleteSelection()) {
      int p = _caretPosition();
      if (p > 0) {
        //TODO There may be more than one code unit to delete
        performAction(new XPadEditor_Delete()
            ..position = p - 1
            ..length = 1);
        _desiredCaretPosition = p - 1;
      }
    }
  }

  void _handleDelete(KeyboardEvent e) {
    if (getKeyModifiers(e) != KeyModifierState.SHIFT) {
      stopFurtherProcessing(e);
      if (!_deleteSelection()) {
        int p = _caretPosition();
        if (p < _totalLength()) {
          //TODO There may be more than one code unit to delete
          performAction(new XPadEditor_Delete()
              ..position = p
              ..length = 1);
          _desiredCaretPosition = p;
        }
      }
    }
  }

  int _totalLength() {
    int res = -1; // adjustment for synthetic trailing <br>
    for (Node node in _root.nodes) {
      res += _getNodeLength(node);
    }
    res = math.max(res, 0);
    return res;
  }

  bool _deleteSelection() {
    Range r = _selectedRange();
    bool res = (r != null);
    if (res) {
      int start = _computePosition(r.startContainer, r.startOffset);
      int end = _computePosition(r.endContainer, r.endOffset);
      performAction(new XPadEditor_Delete()
          ..position = start
          ..length = end - start);

      r.deleteContents();
      shadowRoot.getSelection().collapseToStart();

      _desiredCaretPosition = start;
    }
    return res;
  }

  /* nullable */Range _selectedRange() {
    Range res;

    Selection s = shadowRoot.getSelection();
    if (s.rangeCount == 1) {
      res = s.getRangeAt(0);
      if (res.collapsed) {
        res = null;
      }
    } else {
      res = null;
    }

    return res;
  }

  void _handlePageDown(KeyboardEvent e) {
    if ((getKeyModifiers(e) == KeyModifierState.NONE) && (_selectedRange() == null)
        && (_caretPosition() == _totalLength())) {
      stopFurtherProcessing(e);
    }
  }

  void _handlePageUp(KeyboardEvent e) {
    if ((getKeyModifiers(e) == KeyModifierState.NONE) && (_selectedRange() == null) && (_caretPosition() == 0)) {
      stopFurtherProcessing(e);
    }
  }

  @override void focusComponent() {
    _root.focus();
  }

  void _onBlur() {
    if ((shadowRoot.activeElement != _root) && (shadowRoot.getSelection().anchorNode != null)) {
      shadowRoot.getSelection().removeAllRanges();
    }
  }

  void _onCopy(ClipboardEvent e) {
    stopFurtherProcessing(e);
    Range r = _selectedRange();
    if (r != null) {
      int start = _computePosition(r.startContainer, r.startOffset);
      int end = _computePosition(r.endContainer, r.endOffset);
      performAction(new XPadEditor_Copy()
          ..position = start
          ..length = end - start);
      e.clipboardData
          ..setData(_PAD_MIME_TYPE, "")
          ..setData(_TEXT_MIME_TYPE, _rangeToText(r));
    }
  }

  void _onCut(ClipboardEvent e) {
    stopFurtherProcessing(e);
    Range r = _selectedRange();
    if (r != null) {
      int start = _computePosition(r.startContainer, r.startOffset);
      int end = _computePosition(r.endContainer, r.endOffset);
      performAction(new XPadEditor_Cut()
          ..position = start
          ..length = end - start);
      e.clipboardData
          ..setData(_PAD_MIME_TYPE, "")
          ..setData(_TEXT_MIME_TYPE, _rangeToText(r));
      _desiredCaretPosition = start;
    }
  }

  void _onPaste(ClipboardEvent e) {
    stopFurtherProcessing(e);

    if (e.clipboardData.types.contains(_PAD_MIME_TYPE)) {
      _deleteSelection();
      int p = _caretPosition();
      performAction(new XPadEditor_Paste()
          ..position = p);
      _desiredCaretOffsetFromEnd = _totalLength() - p;
    } else {
      bool imagePasted = false;

      for (int i = 0; i < e.clipboardData.items.length; ++i) {
        if (e.clipboardData.items[i].type == _PNG_MIME_TYPE) {
          lockUi();
          FileReader reader = new FileReader();
          reader
              ..onLoad.listen((_) {
                Uint8List data = reader.result;
                if (data.isNotEmpty) {
                  _deleteSelection();
                  int p = _caretPosition();
                  performAction(new XPadEditor_InsertImage()
                      ..position = p
                      ..mimeType = _PNG_MIME_TYPE
                      ..data = data);
                  _desiredCaretPosition = p + 1;
                }
              })
              ..onLoadEnd.listen((_) => unlockUi())
              ..readAsArrayBuffer(e.clipboardData.items[i].getAsFile());
          imagePasted = true;
          break;
        }
      }

      if (!imagePasted) {
        String s = e.clipboardData.getData(_TEXT_MIME_TYPE);
        if (s.isNotEmpty) {
          _deleteSelection();
          int p = _caretPosition();
          performAction(new XPadEditor_InsertText()
              ..position = p
              ..text = s);
          _desiredCaretOffsetFromEnd = _totalLength() - p;
        }
      }
    }
  }

  String _rangeToText(Range r) {
    StringBuffer sb = new StringBuffer();
    Node previous = null;
    String spacing = "";

    for (Node n in r.cloneContents().nodes) {
      if (n is Text) {
        sb.writeAll([spacing, n.data]);
        spacing = "";
      } else if (n is BRElement) {
        sb.write(os_EOL);
        spacing = "";
      } else if (n is ImageElement) {
        if (previous is Text) {
          spacing = " ";
        }
      }

      previous = n;
    }

    return sb.toString();
  }

  void _onMouseDown(MouseEvent e) {
    if (e.button == 2) {
      if (shadowRoot.activeElement == _root) {
        stopFurtherProcessing(e);
      }
    } else if (e.target is ImageElement) {
      stopFurtherProcessing(e);

      ImageElement image = e.target;
      if (e.offset.x < image.clientWidth / 2) {
        shadowRoot.getSelection().collapse(image);
      } else {
        shadowRoot.getSelection().collapse(_root, _root.nodes.indexOf(image) + 1);
      }
    }
  }

  void _onDoubleClick(Event e) {
    if (e.target is ImageElement) {
      stopFurtherProcessing(e);
      shadowRoot.getSelection()
          ..removeAllRanges()
          ..addRange(new Range()
              ..selectNode(e.target));
    }
  }

  void _onContextMenu(MouseEvent e) {
    stopFurtherProcessing(e);
    new Menu()
        ..append(new MenuItem(new MenuItemOptions(
            //ignore: argument_type_not_assignable
            click: allowInterop((_, __) => _insertImageFromFile()),
            label: tr("Insert Picture from File..."))))
        ..append(new MenuItem(new MenuItemOptions(
            //ignore: argument_type_not_assignable
            click: allowInterop((_, __) => _saveImageToFile()),
            label: tr("Save Picture to File..."),
            enabled: _selectedImageDataset() != null)))
        ..popup();
  }

  void _insertImageFromFile() {
    List<String> fileNames = dialog_showOpenDialog(getCurrentWindow(), new ShowOpenDialogOptions(
        filters: [
            new FileDialogFilter(name: tr("Pictures"), extensions: ["png", "jpeg", "jpg"]),
            new FileDialogFilter(name: tr("All Files"), extensions: ["*"])],
        properties: ["openFile"]));
    if (fileNames != null) {
      Uint8List data = null;
      try {
        data = readFileSync(fileNames.first);
      } catch (e) {
        showErrorMessage(e.toString());
      }

      if (data != null) {
        String mimeType = _detectImageMimeType(data);
        if (mimeType == null) {
          showErrorMessage(tr("Unknown image type."));
        } else {
          _deleteSelection();
          int p = _caretPosition();
          performAction(new XPadEditor_InsertImage()
              ..position = p
              ..mimeType = mimeType
              ..data = data);
          _desiredCaretPosition = p + 1;
        }
      }
    }
  }

  /* nullable */String _detectImageMimeType(List<int> data) {
    for (String mimeType in _imageDecoders.keys) {
      try {
        if (_imageDecoders[mimeType]().isValidFile(data)) {
          return mimeType;
        }
      } catch (_) {}
    }
    return null;
  }

  void _saveImageToFile() {
    Map<String, String> dataset = _selectedImageDataset();
    if (dataset != null) {
      String fileName = showSaveFileDialog(dataset["mimeType"].split("/")[1], tr("Pictures"));
      if (fileName != null) {
        Uint8List data = castOrCopyToUint8List((scene.shapes(dataset["blob"]) as XBlob).blob);
        try {
          fs_writeFileSync(fileName, Buffer_from(data.buffer, data.offsetInBytes, data.lengthInBytes));
        } catch (e) {
          showErrorMessage(e.toString());
        }
      }
    }
  }

  /* nullable */Map<String, String> _selectedImageDataset() {
    Map<String, String> res = null;
    Range r = _selectedRange();
    if ((r != null)
        && (_computePosition(r.endContainer, r.endOffset) - _computePosition(r.startContainer, r.startOffset) == 1)) {
      Node node = r.cloneContents().nodes.first;
      if (node is ImageElement) {
        res = node.dataset;
      }
    }
    return res;
  }
}

class _Image {
  /* nullable */String blobUrl;
  final _ImageSize size;
  bool used = true;

  _Image(this.blobUrl, this.size);

  void _release() {
    if (blobUrl != null) {
      Url.revokeObjectUrl(blobUrl);
      blobUrl = null;
    }
  }
}

class _ImageSize {
  final int width;
  final int height;
  _ImageSize(this.width, this.height);
}

@initMethod void initialize() {
  registerShapeParser((List<int> b) => new XPadEditor.fromBuffer(b));
}
