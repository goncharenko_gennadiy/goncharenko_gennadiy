/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

@HtmlImport("input_question_editor.html")
library iren_editor.input_question_editor;

import 'dart:async';
import 'dart:html';
import 'dart:js';

import 'package:initialize/initialize.dart';
import 'package:iren_proto/editor/input_question_editor.pb.dart';
import 'package:iren_proto/question/input_question.pb.dart';
import 'package:misc_utils/electron.dart';
import 'package:misc_utils/misc_utils.dart';
import 'package:protobuf/protobuf.dart';
import 'package:web_components/web_components.dart';
import 'package:web_helpers/translator.dart';

import 'common.dart';
import 'editor_infra.dart';
import 'pad_editor.dart';

@QuestionEditor(InputQuestionEditor.TAG, XInputQuestionEditor)
class InputQuestionEditor extends Widget<XInputQuestionEditor> {
  static const String TAG = "ru-irenproject-input-question-editor";

  @ui DivElement _toolbar;
  @ui ButtonElement _addPatternButton;
  @ui ButtonElement _addPatternDropDown;
  @ui ButtonElement _deletePatternButton;
  @ui ButtonElement _movePatternUpButton;
  @ui ButtonElement _movePatternDownButton;
  @ui ImageElement _patternTypeImage;
  @ui DivElement _patternPanel;

  PadEditor _formulation;

  /* nullable */int _selectPattern;
  /* nullable */int _focusedPattern;
  _PatternType _selectedPatternType;

  InputQuestionEditor.created() : super.created();

  @override void initialize() {
    _addPatternButton.onClick.listen((_) => _addPattern());
    _addPatternDropDown.onClick.listen((_) => _onAddPatternDropDownClick());
    _deletePatternButton.onClick.listen((_) => _deletePattern());
    _movePatternUpButton.onClick.listen((_) => _movePatternUp());
    _movePatternDownButton.onClick.listen((_) => _movePatternDown());

    windowShortcutHandler
        ..add(KeyCode.F5, KeyModifierState.NONE, _addPattern)
        ..add(KeyCode.F8, KeyModifierState.NONE, _deletePattern);

    new ShortcutHandler(shadowRoot)
        ..add(KeyCode.UP, KeyModifierState.ALT, _movePatternUp)
        ..add(KeyCode.DOWN, KeyModifierState.ALT, _movePatternDown);

    _formulation = new PadEditor(shape.formulationEditor, dispatcher)
        ..id = "formulation";
    findElement("formulationPlaceholder").replaceWith(_formulation);

    Element.focusEvent.forTarget(shadowRoot, useCapture: true).listen((_) => _onFocus());
    Element.blurEvent.forTarget(shadowRoot, useCapture: true).listen(_onBlur);

    _selectPatternType(_textPatternType);
  }

  @override void render() {
    super.render();
    _formulation.render();

    if (shapeChanged) {
      _focusedPattern = null;
      _patternPanel.nodes = shape.patternEditor.map((String name) => new PatternEditor(name, dispatcher));
      _addPatternButton.disabled = _addPatternDropDown.disabled = !_canAddPattern();
    }

    for (PatternEditor e in _patternPanel.children) {
      e.render();
    }

    if ((_selectPattern != null) && shape.patternEditor.isNotEmpty) {
      (_patternPanel.children[_selectPattern.clamp(0, shape.patternEditor.length - 1)] as PatternEditor)
          .focusComponent();
    }
    _selectPattern = null;

    _updateUi();
  }

  bool _canAddPattern() => shape.patternEditor.length < shape.maxPatterns;

  void _addPattern() {
    if (_canAddPattern()) {
      performAction(_selectedPatternType.addAction);
      _selectPattern = shape.patternEditor.length;
    }
  }

  void _onAddPatternDropDownClick() {
    Menu menu = new Menu();

    for (_PatternType t in _patternTypes) {
      menu.append(new MenuItem(new MenuItemOptions(
          //ignore: argument_type_not_assignable
          click: allowInterop((_, __) {
            _selectPatternType(t);
            _addPattern();
          }),
          label: t.title,
          icon: getAbsoluteAppPath(t.icon))));
    }

    menu.popup(_addPatternButton.borderEdge.left.round(), _addPatternButton.borderEdge.bottom.round());
  }

  void _selectPatternType(_PatternType t) {
    _selectedPatternType = t;
    _patternTypeImage.src = t.icon;
    _addPatternButton.title = "${t.addTitle} (F5)";
  }

  void _deletePattern() {
    if (_focusedPattern != null) {
      performAction(new XInputQuestionEditor_DeletePattern()
          ..index = _focusedPattern);
      _selectPattern = _focusedPattern;
    }
  }

  void _movePatternUp() {
    if (_canMovePatternUp()) {
      performAction(new XInputQuestionEditor_MovePattern()
          ..index = _focusedPattern
          ..forward = false);
      _selectPattern = _focusedPattern - 1;
    }
  }

  bool _canMovePatternUp() => (_focusedPattern != null) && (_focusedPattern > 0);

  void _movePatternDown() {
    if (_canMovePatternDown()) {
      performAction(new XInputQuestionEditor_MovePattern()
          ..index = _focusedPattern
          ..forward = true);
     _selectPattern = _focusedPattern + 1;
    }
  }

  bool _canMovePatternDown() => (_focusedPattern != null) && (_focusedPattern < shape.patternEditor.length - 1);

  void _onFocus() {
    int index = _patternPanel.children.indexOf(shadowRoot.activeElement);
    if (index != -1) {
      _focusedPattern = index;
    }
    _updateUi();
  }

  void _onBlur(Event e) {
    if (!_toolbar.contains((e as FocusEvent).relatedTarget)) {
      _focusedPattern = null;
    }
    _updateUi();
  }

  void _updateUi() {
    _deletePatternButton.disabled = (_focusedPattern == null);
    _movePatternUpButton.disabled = !_canMovePatternUp();
    _movePatternDownButton.disabled = !_canMovePatternDown();
  }

  @override void focusComponent() {
    _formulation.focusComponent();
  }
}

@CustomElement(PatternEditor.TAG)
class PatternEditor extends Widget<XPatternEditor> {
  static const String TAG = "ru-irenproject-input-question-pattern-editor";

  @ui ImageElement _patternTypeImage;
  @ui TextInputElement _pattern;
  @ui NumberInputElement _quality;
  @ui ButtonElement _optionsButton;

  PatternEditor.created() : super.created();

  factory PatternEditor(String name, Dispatcher dispatcher) => new Widget(TAG, name, dispatcher);

  @override void initialize() {
    _patternTypeImage
        ..src = _patternType().icon
        ..title = _patternType().title;
    _pattern
        ..maxLength = shape.maxLength
        ..onInput.listen((_) => _onPatternInput());
    _quality
        ..onInput.listen((_) => _onQualityInput())
        ..onBlur.listen((_) => _showQuality());
    _optionsButton.onClick.listen((_) => _showOptionsDialog());
  }

  _PatternType _patternType() => shape.hasRegexpSupplement() ? _regexpPatternType : _textPatternType;

  @override void render() {
    super.render();
    if (shapeChanged) {
      updateTextInput(_pattern, shape.value);
      _showQuality();
      _optionsButton.classes.toggle("edited", !shape.hasDefaultOptions);
    }
  }

  void _showQuality() {
    updateNumberInput(_quality, shape.qualityPercent);
    _quality.classes.toggle("fullyCorrect", shape.qualityPercent == 100);
  }

  void _onPatternInput() {
    performAction(new XPatternEditor_SetValue()
        ..value = _pattern.value, disableUi: false);
  }

  void _onQualityInput() {
    if (_quality.validity.valid) {
      performAction(new XPatternEditor_SetQuality()
          ..qualityPercent = _quality.valueAsNumber, disableUi: false);
    }
  }

  @override void focusComponent() {
    _pattern.focus();
  }

  void _showOptionsDialog() {
    if (_patternType() == _textPatternType) {
      TextPatternOptionsDialog.openModal(shape.textSupplement, dispatcher, name);
    } else {
      RegexpPatternOptionsDialog.openModal(shape.regexpSupplement, dispatcher, name);
    }
  }
}

@CustomElement(TextPatternOptionsDialog.TAG)
class TextPatternOptionsDialog extends ModalDialog {
  static const String TAG = "ru-irenproject-text-pattern-options-dialog";

  static Future<Null> openModal(TextSupplement source, Dispatcher dispatcher, String targetName) =>
      ((new Element.tag(TAG) as TextPatternOptionsDialog)
          .._source = source
          .._dispatcher = dispatcher
          .._targetName = targetName)._doOpenModal();

  @ui DialogElement _dialog;
  @ui CheckboxInputElement _wildcard;
  @ui CheckboxInputElement _recognizeNumbers;
  @ui TextInputElement _precision;
  @ui CheckboxInputElement _spaceDelimited;
  @ui CheckboxInputElement _ignoreCase;
  @ui ButtonElement _okButton;

  TextSupplement _source;
  Dispatcher _dispatcher;
  String _targetName;

  TextPatternOptionsDialog.created() : super.created();

  Future<Null> _doOpenModal() {
    _wildcard.checked = _source.wildcard;
    _recognizeNumbers
        ..checked = _source.hasPrecision()
        ..onClick.listen((_) => _updateUi());
    _spaceDelimited.checked = _source.spaceDelimited;
    _ignoreCase.checked = !_source.caseSensitive;
    _precision
        ..maxLength = _source.maxPrecisionLength
        ..size = _source.maxPrecisionLength
        ..value = _source.hasPrecision() ? dotToUiDecimalMark(_source.precision) : "0";
    _okButton.onClick.listen(_onOkClick);

    _updateUi();
    return open(_dialog);
  }

  void _onOkClick(Event e) {
    e.preventDefault();
    _continueOnOkClick();
  }

  Future<Null> _continueOnOkClick() async {
    XPatternEditor_SetTextPatternOptions action = new XPatternEditor_SetTextPatternOptions()
        ..wildcard = _wildcard.checked
        ..spaceDelimited = _spaceDelimited.checked
        ..caseSensitive = !_ignoreCase.checked;
    if (_recognizeNumbers.checked) {
      action.precision = anyDecimalMarkToDot(_precision.value.trim());
    }

    try {
      await _dispatcher.performAction(_targetName, action);
      _dialog.close("");
    } on ActionException {
      focusInputElement(_precision);
    }
  }

  void _updateUi() {
    _precision.disabled = !_recognizeNumbers.checked;
  }
}

@CustomElement(RegexpPatternOptionsDialog.TAG)
class RegexpPatternOptionsDialog extends ModalDialog {
  static const String TAG = "ru-irenproject-regexp-pattern-options-dialog";

  static Future<Null> openModal(RegexpSupplement source, Dispatcher dispatcher, String targetName) =>
      ((new Element.tag(TAG) as RegexpPatternOptionsDialog)
          .._source = source
          .._dispatcher = dispatcher
          .._targetName = targetName)._doOpenModal();

  @ui DialogElement _dialog;
  @ui DivElement _spacePreprocessModePanel;
  @ui CheckboxInputElement _ignoreCase;

  RegexpSupplement _source;
  Dispatcher _dispatcher;
  String _targetName;

  RegexpPatternOptionsDialog.created() : super.created();

  Future<Null> _doOpenModal() async {
    (_spacePreprocessModePanel.querySelector("[value=${_source.spacePreprocessMode.name}]") as RadioButtonInputElement)
        .checked = true;
    _ignoreCase.checked = !_source.caseSensitive;

    await open(_dialog);

    if (_dialog.returnValue.isNotEmpty) {
      await _dispatcher.performAction(_targetName, new XPatternEditor_SetRegexpPatternOptions()
          ..spacePreprocessMode = getProtobufEnumByName(SpacePreprocessMode.values,
              (_spacePreprocessModePanel.querySelector(":checked") as RadioButtonInputElement).value)
          ..caseSensitive = !_ignoreCase.checked);
    }
  }
}

class _PatternType {
  final String title;
  final String addTitle;
  final String icon;
  final GeneratedMessage addAction;

  _PatternType(this.title, this.addTitle, this.icon, this.addAction);
}

const String _LIB = "input_question_editor";

final _PatternType _textPatternType = new _PatternType(translate(_LIB, "Text Pattern"),
    translate(_LIB, "Add Text Pattern"),
    "input_question_editor.resources/textPattern.png", new XInputQuestionEditor_AddTextPattern());

final _PatternType _regexpPatternType = new _PatternType(translate(_LIB, "Regular Expression"),
    translate(_LIB, "Add Regular Expression"),
    "input_question_editor.resources/regexpPattern.png", new XInputQuestionEditor_AddRegexpPattern());

final List<_PatternType> _patternTypes = [_textPatternType, _regexpPatternType];

@initMethod void initialize() {
  registerShapeParsers([
      (List<int> b) => new XInputQuestionEditor.fromBuffer(b),
      (List<int> b) => new XPatternEditor.fromBuffer(b)]);

  registerQuestionDescriptor(new QuestionDescriptor(
      type: InputQuestionType.input.name,
      title: translate(_LIB, "Text Input Question"),
      priority: 2000,
      icon: "input_question_editor.resources/inputQuestion.png",
      initialText: ""));
}
