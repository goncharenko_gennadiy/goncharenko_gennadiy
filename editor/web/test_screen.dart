/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

@HtmlImport("test_screen.html")
library iren_editor.test_screen;

import 'dart:async';
import 'dart:html';

import 'package:initialize/initialize.dart';
import 'package:iren_proto/editor/editor.pb.dart';
import 'package:misc_utils/misc_utils.dart';
import 'package:web_components/web_components.dart';

import 'common.dart';
import 'editor_infra.dart';
import 'question_list_editor.dart';
import 'section_tree_editor.dart';

@CustomElement(TestScreen.TAG)
class TestScreen extends Widget<XTestScreen> {
  static const String TAG = "ru-irenproject-test-screen";

  @ui DivElement _left;
  @ui DivElement _right;

  QuestionListEditor _questionListEditor;
  SectionTreeEditor _sectionTreeEditor;
  /* nullable */Widget _questionEditor;
  bool _focusQuestionEditor = false;
  StreamSubscription<ClipboardEvent> _pasteSubscription;

  TestScreen.created() : super.created();

  factory TestScreen(String name, Dispatcher dispatcher) => new Widget(TAG, name, dispatcher);

  @override void initialize() {
    _sectionTreeEditor = new SectionTreeEditor(shape.sectionTreeEditor, dispatcher)
        ..id = "sectionTreeEditor";
    _left.append(_sectionTreeEditor);
  }

  @override void attached() {
    super.attached();
    _pasteSubscription = document.onPaste.listen(_paste);
  }

  @override void detached() {
    _pasteSubscription?.cancel();
    _pasteSubscription = null;
    super.detached();
  }

  @override void render() {
    super.render();

    if (shape.questionListEditor != _questionListEditor?.name) {
      _questionListEditor = new QuestionListEditor(shape.questionListEditor, dispatcher)
          ..id = "questionListEditor"
          ..onAddQuestion.listen((_) => _focusQuestionEditor = true)
          ..onShowProfiles.listen((_) => performAction(new XTestScreen_ShowProfiles()));
      findElement("questionListEditor").replaceWith(_questionListEditor);
    }
    _questionListEditor.render();

    _sectionTreeEditor.render();

    if (shape.hasQuestionEditor()) {
      if (shape.questionEditor != _questionEditor?.name) {
        _questionEditor = new Widget(getQuestionEditorTag(scene.shapes(shape.questionEditor).runtimeType),
            shape.questionEditor, dispatcher);
        _right.nodes = [_questionEditor];
      }
      _questionEditor.render();
    } else {
      _questionEditor?.remove();
      _questionEditor = null;
    }

    if (_focusQuestionEditor) {
      _focusQuestionEditor = false;
      Timer.run(() {
        if (_questionEditor != null) {
          _questionEditor.focusComponent();
          document.execCommand("selectAll", false, null);
        }
      });
    }
  }

  void _paste(ClipboardEvent e) {
    if (shadowRoot.activeElement == _sectionTreeEditor) {
      stopFurtherProcessing(e);
      _questionListEditor.pasteQuestions(e);
    }
  }
}

@initMethod void initialize() {
  registerShapeParser((List<int> b) => new XTestScreen.fromBuffer(b));
}
