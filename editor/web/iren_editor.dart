/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

@HtmlImport("iren_editor.html")
library iren_editor;

import 'dart:async';
import 'dart:convert';
import 'dart:html';
import 'dart:js';
import 'dart:typed_data';

import 'package:convert/convert.dart' show hex;
import 'package:initialize/initialize.dart' as init;
import 'package:misc_utils/dialogs.dart';
import 'package:misc_utils/electron.dart';
import 'package:misc_utils/misc_utils.dart';
import 'package:web_components/web_components.dart';
import 'package:web_helpers/translator.dart';

import 'classify_question_editor.dart';
import 'editor_infra.dart';
import 'file_manager.dart';
import 'input_question_editor.dart';
import 'languages.dart';
import 'match_question_editor.dart';
import 'order_question_editor.dart';
import 'select_question_editor.dart';
import 'test_document_editor.dart';

/// Reference the classes to suppress warnings
/// [SelectQuestionEditor], [InputQuestionEditor], [MatchQuestionEditor]
/// [OrderQuestionEditor], [ClassifyQuestionEditor]

@CustomElement(EditorMainScreen.TAG)
class EditorMainScreen extends WebComponent {
  static const String TAG = "ru-irenproject-editor-main-screen";

  static Future<EditorMainScreen> create(ServerConnection connection) async {
    EditorMainScreen res = new Element.tag(TAG);
    res._connection = connection;
    await res._initialize();
    return res;
  }

  @ui DivElement _main;

  ServerConnection _connection;
  FileManager _fileManager;
  final Map<String, TestDocumentEditor> _editorsByChannel = {};
  /* nullable */TestDocumentEditor _currentEditor;

  EditorMainScreen.created() : super.created();

  Future<Null> _initialize() async {
    _fileManager = await FileManager.create(_connection)
        ..id = "fileManager"
        ..channelDispatcherFactory = _createChannelDispatcher
        ..onSelectChannel.listen((_) => _showEditor())
        ..onCloseChannel.listen(_closeEditor)
        ..onUpdateTitle.listen((_) => _updateTitle())
        ..onAfterSave.listen((_) => _currentEditor?.markAsUnmodified());
    await _fileManager.activate();
    findElement("fileManagerPlaceholder").replaceWith(_fileManager);

    if ((remote_process_argv.length >= 2) && !remote_process_argv[1].startsWith("-")) {
      await _fileManager.openFile(path_resolve(remote_process_argv[1]));
    } else {
      await _fileManager.createAutoClosableFileIfEmpty();
    }

    ipcRenderer_on("openFile", allowInterop((_, String fileName) {
      if (!modalDialogOpen && !uiLocked) {
        _fileManager.openFile(fileName);
      }
    }));

    ipcRenderer_on("requestClose", allowInterop((_) => _closeWindow()));
    ipcRenderer_send("relayCloseRequests");
  }

  void _showEditor() {
    String newChannel = _fileManager.selectedChannel;
    if (_currentEditor?.dispatcher?.channel != newChannel) {
      _currentEditor?.style?.display = "none";
      _currentEditor = null;

      if (newChannel != null) {
        _currentEditor = _editorsByChannel.putIfAbsent(newChannel, () => _createEditor(newChannel))
            ..style.display = "";
      }
    }

    _updateFileManager();
  }

  TestDocumentEditor _createEditor(String channel) {
    Dispatcher editorDispatcher = _fileManager.getDispatcher(channel);

    TestDocumentEditor res = new TestDocumentEditor(editorDispatcher);
    _main.append(res);
    res.render();

    editorDispatcher.onChangeScene.listen((_) {
      if (!editorDispatcher.waitingForReply) {
        res.render();
        _fileManager.removeAutoClosableMark();
        _updateFileManager();
      }
    });

    return res;
  }

  void _closeEditor(String channel) {
    TestDocumentEditor editor = _editorsByChannel.remove(channel);
    if (editor != null) {
      editor.remove();
      if (identical(_currentEditor, editor)) {
        _currentEditor = null;
      }
    }
  }

  void _updateTitle() {
    String title = _fileManager.selectedFileTitle;
    document.title = (title == null) ? tr("Iren") : tr("WINDOW_TITLE")(title);
  }

  void _updateFileManager() {
    _fileManager.selectedFileModified = (_currentEditor != null) && _currentEditor.modified;
  }

  Future<Dispatcher> _createChannelDispatcher(String channel) async {
    Dispatcher res = new Dispatcher(_connection, new Scene(), channel);
    try {
      await TestDocumentEditor.loadScene(res);
    } catch (_) {
      res.stop();
      rethrow;
    }
    return res;
  }

  Future<Null> _closeWindow() async {
    if (_connection.connected) {
      if (!modalDialogOpen) {
        if (uiLocked) {
          window.location.reload();
        } else if (await _fileManager.closeAll()) {
          getCurrentWindow().destroy();
        }
      }
    } else {
      getCurrentWindow().destroy();
    }
  }
}

Future<Null> main() async {
  await window.onLoad.first;
  disableDragAndDrop(document.documentElement);

  registerEditorDictionaries();
  registerMiscUtilsDictionaries();
  selectDictionaryForCurrentLanguage();

  await init.run();

  ipcRenderer_once("connectionConfig", allowInterop((_, String connectionConfig) => _connect(connectionConfig)));
  ipcRenderer_send("getConnectionConfig");
}

Future<Null> _connect(String connectionConfig) async {
  dynamic config = JSON.decode(connectionConfig);

  WebSocket socket = new WebSocket("ws://127.0.0.1:${config["port"]}/websocket/")
      ..binaryType = "arraybuffer"
      ..onClose.listen((_) => ConnectionLostDialog.openModal());
  await socket.onOpen.first;
  socket.sendTypedData(new Uint8List.fromList(hex.decode(config["editorKey"])));

  document.body.append(await EditorMainScreen.create(new ServerConnection(socket)));
}
