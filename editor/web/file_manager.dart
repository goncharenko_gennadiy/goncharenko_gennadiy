/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

@HtmlImport("file_manager.html")
library iren_editor.file_manager;

import 'dart:async';
import 'dart:html';

import 'package:initialize/initialize.dart';
import 'package:iren_proto/editor/editor.pb.dart';
import 'package:misc_utils/electron.dart';
import 'package:misc_utils/misc_utils.dart';
import 'package:misc_utils/test_utils.dart';
import 'package:protobuf/protobuf.dart';
import 'package:web_components/web_components.dart';

import 'about_dialog.dart';
import 'common.dart';
import 'editor_infra.dart';

@CustomElement(FileManager.TAG)
class FileManager extends Widget<XFileManager> {
  static const String TAG = "ru-irenproject-file-manager";
  static const String _WIDGET_NAME = "";
  static const List<String> _IMPORTED_TEST_EXTENSIONS = const ["it3", "it2"];

  static Future<FileManager> create(ServerConnection connection) async {
    Dispatcher dispatcher = new Dispatcher(connection, new Scene(), FileManagerChannel.fileManager.name);
    await dispatcher.performAction(_WIDGET_NAME, new XFileManager_Nop());
    await dispatcher.onChangeScene.first;
    return new Widget(TAG, _WIDGET_NAME, dispatcher);
  }

  @ui ButtonElement _createButton;
  @ui ButtonElement _openButton;
  @ui ButtonElement _saveButton;
  @ui ButtonElement _aboutButton;
  @ui ButtonElement _exitButton;
  @ui ButtonElement _closeButton;
  @ui DivElement _fileSwitcher;

  final Map<String, Dispatcher> _dispatchersByChannel = {};
  ChannelDispatcherFactory channelDispatcherFactory;
  /* nullable */String _selectedChannel;
  /* nullable */String get selectedChannel => _selectedChannel;
  bool _selectedFileModified = false;
  bool _hasAutoClosableFile = false;

  final StreamController<Null> _onSelectChannel = new StreamController.broadcast(sync: true);
  Stream<Null> get onSelectChannel => _onSelectChannel.stream;

  final StreamController<String> _onCloseChannel = new StreamController.broadcast(sync: true);
  Stream<String> get onCloseChannel => _onCloseChannel.stream;

  final StreamController<Null> _onUpdateTitle = new StreamController.broadcast(sync: true);
  Stream<Null> get onUpdateTitle => _onUpdateTitle.stream;

  final StreamController<Null> _onAfterSave = new StreamController.broadcast(sync: true);
  Stream<Null> get onAfterSave => _onAfterSave.stream;

  FileManager.created() : super.created();

  @override void initialize() {
    _createButton.onClick.listen((_) => _create());
    _openButton.onClick.listen((_) => _open());
    _saveButton.onClick.listen((_) => _save());
    _aboutButton.onClick.listen((_) => AboutDialog.openModal());
    _exitButton.onClick.listen((_) => _exit());
    _closeButton.onClick.listen((_) => _close());

    windowShortcutHandler
        ..add(KeyCode.N, KeyModifierState.CTRL, _create)
        ..add(KeyCode.O, KeyModifierState.CTRL, _open)
        ..add(KeyCode.S, KeyModifierState.CTRL, _save)
        ..add(KeyCode.Q, KeyModifierState.ALT_SHIFT, _exit)
        ..add(KeyCode.F4, KeyModifierState.CTRL, _close)
        ..add(KeyCode.PAGE_UP, KeyModifierState.CTRL, () => _selectAdjacentFile(-1))
        ..add(KeyCode.PAGE_DOWN, KeyModifierState.CTRL, () => _selectAdjacentFile(1));
  }

  Future<Null> activate() async {
    render();
    dispatcher.onChangeScene.listen((_) {
      if (!dispatcher.waitingForReply) {
        render();
      }
    });

    for (OpenFile f in shape.file) {
      _dispatchersByChannel[f.channel] = await channelDispatcherFactory(f.channel);
    }

    _selectChannel(shape.file.isEmpty ? null : shape.file.last.channel);
  }

  void _selectChannel(/* nullable */String channel) {
    _selectedChannel = channel;
    _updateUiForSelectedChannel();
    _onSelectChannel.add(null);
  }

  @override void render() {
    super.render();

    shape.file.asMap().forEach((int i, OpenFile f) {
      ButtonElement button = (i < _fileSwitcher.children.length) ?
          _fileSwitcher.children[i] : _fileSwitcher.append(_createFileButton());
      button
          ..dataset["channel"] = f.channel
          ..text = getFileTitle(f.name)
          ..title = f.name;
    });

    while (_fileSwitcher.children.length > shape.file.length) {
      _fileSwitcher.children.removeLast();
    }

    _updateUiForSelectedChannel();
    _closeButton.disabled = shape.file.isEmpty;
  }

  /* nullable */String get selectedFileTitle {
    OpenFile f = _selectedFile();
    return (f == null) ? null : getFileTitle(f.name);
  }

  void _updateUiForSelectedChannel() {
    for (ButtonElement button in _fileSwitcher.children) {
      bool selected = (button.dataset["channel"] == _selectedChannel);
      button.classes.toggle("selected", selected);
      if (selected && _fileSwitcher.contains(shadowRoot.activeElement)) {
        button.focus();
      }
    }
    _onUpdateTitle.add(null);
  }

  ButtonElement _createFileButton() => new ButtonElement()
      ..onClick.listen(_onFileButtonClick);

  void _onFileButtonClick(MouseEvent e) {
    _selectChannel((e.currentTarget as ButtonElement).dataset["channel"]);
    if (e.button == 1) {
      _continueOnFileButtonClick();
    }
  }

  Future<Null> _continueOnFileButtonClick() async {
    await postAnimationFrame;
    await _close();
  }

  Future<Null> _open() async {
    for (String fileName in showOpenTestDialog()) {
      await openFile(fileName);
    }
  }

  Future<Null> openFile(String fileName) async {
    OpenFile existing = shape.file.firstWhere((OpenFile f) => f.name == fileName, orElse: () => null);
    if (existing == null) {
      try {
        if (_hasAutoClosableFile) {
          _hasAutoClosableFile = false;
          await _close();
        }

        _selectChannel(await _openOrCreate(new XFileManager_Open()
            ..fileName = fileName
            ..createNew = false));
      } on ActionException {
        showErrorMessage(tr("CANNOT_OPEN_FILE")(fileName));
      }
    } else {
      _selectChannel(existing.channel);
    }
  }

  Future<String> _openOrCreate(GeneratedMessage action) async {
    await performAction(action);
    await dispatcher.onChangeScene.first;

    String res = shape.file.last.channel;
    _dispatchersByChannel[res] = await channelDispatcherFactory(res);

    return res;
  }

  Future<Null> _create() async {
    _selectChannel(await _openOrCreate(new XFileManager_Open()
        ..fileName = ""
        ..createNew = true));
    _hasAutoClosableFile = false;
  }

  Future<bool> _close() async {
    bool res;
    OpenFile f = _selectedFile();
    if (f == null) {
      res = false;
    } else {
      if (!_selectedFileModified) {
        res = true;
      } else {
        int response = showMessageBox(new ShowMessageBoxOptions(
            type: "question",
            buttons: [tr("Save"), tr("Don't Save"), tr("Cancel")],
            message: tr("SAVE_CHANGES")(getFileTitle(f.name)),
            cancelId: 2));
        res = (response == 0) ? await _save() : (response == 1);
      }

      if (res) {
        int index = shape.file.indexOf(f);
        String channelToSelect;
        if (index < shape.file.length - 1) {
          channelToSelect = shape.file[index + 1].channel;
        } else if (index > 0) {
          channelToSelect = shape.file[index - 1].channel;
        } else {
          channelToSelect = null;
        }

        await performAction(new XFileManager_Close()
            ..channel = _selectedChannel);
        _onCloseChannel.add(_selectedChannel);
        _dispatchersByChannel.remove(_selectedChannel)?.stop();

        _selectChannel(channelToSelect);
        _hasAutoClosableFile = false;
        await dispatcher.onChangeScene.first;
      }
    }

    return res;
  }

  Future<bool> closeAll() async {
    lockUi();
    try {
      while (await _close()) {
        await postAnimationFrame;
      }
    } finally {
      unlockUi();
    }

    return shape.file.isEmpty;
  }

  /* nullable */OpenFile _selectedFile() => shape.file.firstWhere((OpenFile f) => f.channel == _selectedChannel,
      orElse: () => null);

  void _selectAdjacentFile(int offset) {
    OpenFile f = _selectedFile();
    if ((f != null) && (shape.file.length >= 2)) {
      _selectChannel(shape.file[(shape.file.indexOf(f) + offset) % shape.file.length].channel);
    }
  }

  Future<bool> _save() async {
    bool res;
    OpenFile f = _selectedFile();
    if ((f != null) && !_saveButton.disabled) {
      String fileName = f.name;

      String defaultNewName;
      bool queryName;
      if (fileName.isEmpty) {
        defaultNewName = null;
        queryName = true;
      } else {
        defaultNewName = dropFileExtension(fileName, _IMPORTED_TEST_EXTENSIONS);
        queryName = (defaultNewName != fileName);
      }

      if (queryName) {
        fileName = showSaveFileDialog("itx", testFilterName, defaultPathWithoutExtension: defaultNewName);
      }

      if (fileName == null) {
        res = false;
      } else {
        try {
          await performAction(new XFileManager_Save()
              ..channel = _selectedChannel
              ..fileName = fileName);
          _hasAutoClosableFile = false;
          _onAfterSave.add(null);
          res = true;
        } on ActionException {
          res = false;
          showErrorMessage(tr("CANNOT_SAVE_FILE")(fileName));
        }
      }
    } else {
      res = false;
    }

    return res;
  }

  set selectedFileModified(bool value) {
    _selectedFileModified = value;
    _updateSaveButton();
  }

  void _updateSaveButton() {
    OpenFile f = _selectedFile();
    _saveButton.disabled = (f == null) || (f.name.isNotEmpty && !_selectedFileModified);
  }

  Dispatcher getDispatcher(String channel) => _dispatchersByChannel[channel];

  Future<Null> createAutoClosableFileIfEmpty() async {
    if (shape.file.isEmpty) {
      await _create();
      _hasAutoClosableFile = true;
    }
  }

  void removeAutoClosableMark() {
    _hasAutoClosableFile = false;
  }

  void _exit() {
    getCurrentWindow().destroy();
  }
}

typedef Future<Dispatcher> ChannelDispatcherFactory(String channel);

@initMethod void initialize() {
  registerShapeParser((List<int> b) => new XFileManager.fromBuffer(b));
}
