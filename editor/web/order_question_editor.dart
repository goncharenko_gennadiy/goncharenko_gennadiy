/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

@HtmlImport("order_question_editor.html")
library iren_editor.order_question_editor;

import 'dart:async';
import 'dart:html';

import 'package:initialize/initialize.dart';
import 'package:iren_proto/editor/order_question_editor.pb.dart';
import 'package:iren_proto/question/order_question.pb.dart';
import 'package:misc_utils/misc_utils.dart';
import 'package:web_components/web_components.dart';
import 'package:web_helpers/translator.dart';

import 'common.dart';
import 'editor_infra.dart';
import 'pad_editor.dart';

@QuestionEditor(OrderQuestionEditor.TAG, XOrderQuestionEditor)
class OrderQuestionEditor extends Widget<XOrderQuestionEditor> {
  static const String TAG = "ru-irenproject-order-question-editor";

  @ui DivElement _toolbar;
  @ui ButtonElement _addElementButton;
  @ui ButtonElement _deleteElementButton;
  @ui ButtonElement _moveElementUpButton;
  @ui ButtonElement _moveElementDownButton;
  @ui ButtonElement _optionsButton;
  @ui ButtonElement _itemsButton;
  @ui ButtonElement _distractorsButton;
  @ui DivElement _elementPanel;

  PadEditor _formulation;

  /* nullable */int _selectElement;
  /* nullable */int _focusedElement;

  OrderQuestionEditor.created() : super.created();

  @override void initialize() {
    _addElementButton.onClick.listen((_) => _addElement());
    _deleteElementButton.onClick.listen((_) => _deleteElement());
    _moveElementUpButton.onClick.listen((_) => _moveElementUp());
    _moveElementDownButton.onClick.listen((_) => _moveElementDown());
    _optionsButton.onClick.listen((_) => _showOptionsDialog());

    windowShortcutHandler
        ..add(KeyCode.F5, KeyModifierState.NONE, _addElement)
        ..add(KeyCode.F8, KeyModifierState.NONE, _deleteElement);

    new ShortcutHandler(shadowRoot)
        ..add(KeyCode.UP, KeyModifierState.ALT, _moveElementUp)
        ..add(KeyCode.DOWN, KeyModifierState.ALT, _moveElementDown);

    _itemsButton.onClick.listen((_) => _selectPage(Page.ITEMS));
    _distractorsButton.onClick.listen((_) => _selectPage(Page.DISTRACTORS));

    _formulation = new PadEditor(shape.formulationEditor, dispatcher)
        ..id = "formulation";
    findElement("formulationPlaceholder").replaceWith(_formulation);

    Element.focusEvent.forTarget(shadowRoot, useCapture: true).listen((_) => _onFocus());
    Element.blurEvent.forTarget(shadowRoot, useCapture: true).listen(_onBlur);
  }

  @override void render() {
    super.render();
    _formulation.render();

    if (shapeChanged) {
      _focusedElement = null;

      _itemsButton.classes
          ..toggle("selected", shape.page == Page.ITEMS)
          ..toggle("notEmpty", shape.itemCount > 0);
      _distractorsButton.classes
          ..toggle("selected", shape.page == Page.DISTRACTORS)
          ..toggle("notEmpty", shape.distractorCount > 0);

      _elementPanel.nodes = shape.elementEditor.map((String name) => new PadEditor(name, dispatcher));

      _addElementButton.disabled = !_canAddElement();
      _optionsButton.classes.toggle("edited", !shape.hasDefaultOptions);
    }

    for (PadEditor e in _elementPanel.children) {
      e.render();
    }

    if ((_selectElement != null) && shape.elementEditor.isNotEmpty) {
      (_elementPanel.children[_selectElement.clamp(0, shape.elementEditor.length - 1)] as PadEditor).focusComponent();
    }
    _selectElement = null;

    _updateUi();
  }

  void _selectPage(Page page) {
    performAction(new XOrderQuestionEditor_SelectPage()
        ..page = page);
  }

  void _addElement() {
    if (_canAddElement()) {
      performAction(new XOrderQuestionEditor_AddElement());
      _selectElement = shape.elementEditor.length;
    }
  }

  bool _canAddElement() => shape.elementEditor.length < shape.maxElements;

  void _deleteElement() {
    if (_focusedElement != null) {
      performAction(new XOrderQuestionEditor_DeleteElement()
          ..index = _focusedElement);
      _selectElement = _focusedElement;
    }
  }

  void _onFocus() {
    int index = _elementPanel.children.indexOf(shadowRoot.activeElement);
    if (index != -1) {
      _focusedElement = index;
    }
    _updateUi();
  }

  void _onBlur(Event e) {
    if (!_toolbar.contains((e as FocusEvent).relatedTarget)) {
      _focusedElement = null;
    }
    _updateUi();
  }

  void _updateUi() {
    _deleteElementButton.disabled = (_focusedElement == null);
    _moveElementUpButton.disabled = !_canMoveElementUp();
    _moveElementDownButton.disabled = !_canMoveElementDown();
  }

  void _moveElementUp() {
    if (_canMoveElementUp()) {
      performAction(new XOrderQuestionEditor_MoveElement()
          ..index = _focusedElement
          ..forward = false);
      _selectElement = _focusedElement - 1;
    }
  }

  void _moveElementDown() {
    if (_canMoveElementDown()) {
      performAction(new XOrderQuestionEditor_MoveElement()
          ..index = _focusedElement
          ..forward = true);
     _selectElement = _focusedElement + 1;
    }
  }

  bool _canMoveElementUp() => (_focusedElement != null) && (_focusedElement > 0);

  bool _canMoveElementDown() => (_focusedElement != null) && (_focusedElement < shape.elementEditor.length - 1);

  void _showOptionsDialog() {
    OrderQuestionOptionsDialog.openModal(shape, dispatcher, name);
  }

  @override void focusComponent() {
    _formulation.focusComponent();
  }
}

@CustomElement(OrderQuestionOptionsDialog.TAG)
class OrderQuestionOptionsDialog extends ModalDialog {
  static const String TAG = "ru-irenproject-order-question-options-dialog";

  static Future<Null> openModal(XOrderQuestionEditor source, Dispatcher dispatcher, String targetName) =>
      ((new Element.tag(TAG) as OrderQuestionOptionsDialog)
          .._source = source
          .._dispatcher = dispatcher
          .._targetName = targetName)._doOpenModal();

  @ui DialogElement _dialog;
  @ui RadioButtonInputElement _randomItems;
  @ui NumberInputElement _itemLimit;
  @ui RadioButtonInputElement _randomDistractors;
  @ui NumberInputElement _distractorLimit;

  XOrderQuestionEditor _source;
  Dispatcher _dispatcher;
  String _targetName;

  OrderQuestionOptionsDialog.created() : super.created();

  Future<Null> _doOpenModal() async {
    _itemLimit.max = _distractorLimit.max = _source.maxElements.toString();

    if (_source.hasItemLimit()) {
      _randomItems.checked = true;
      _itemLimit.valueAsNumber = _source.itemLimit;
    }

    if (_source.hasDistractorLimit()) {
      _randomDistractors.checked = true;
      _distractorLimit.valueAsNumber = _source.distractorLimit;
    }

    _randomItems.onClick.listen((_) => focusInputElement(_itemLimit));
    _randomDistractors.onClick.listen((_) => focusInputElement(_distractorLimit));

    _updateUi();
    _dialog.onChange.listen((_) => _updateUi());

    await open(_dialog);

    if (_dialog.returnValue.isNotEmpty) {
      XOrderQuestionEditor_SetOptions action = new XOrderQuestionEditor_SetOptions();
      if (_randomItems.checked) {
        action.itemLimit = _itemLimit.valueAsNumber;
      }
      if (_randomDistractors.checked) {
        action.distractorLimit = _distractorLimit.valueAsNumber;
      }
      await _dispatcher.performAction(_targetName, action);
    }
  }

  void _updateUi() {
    _itemLimit.disabled = !_randomItems.checked;
    _distractorLimit.disabled = !_randomDistractors.checked;
  }
}

const String _LIB = "order_question_editor";

@initMethod void initialize() {
  registerShapeParser((List<int> b) => new XOrderQuestionEditor.fromBuffer(b));

  registerQuestionDescriptor(new QuestionDescriptor(
      type: OrderQuestionType.order.name,
      title: translate(_LIB, "Ordering Question"),
      priority: 4000,
      icon: "order_question_editor.resources/orderQuestion.png",
      initialText: translate(_LIB, "Order the items.")));
}
