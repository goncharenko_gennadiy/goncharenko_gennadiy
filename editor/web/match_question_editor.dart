/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

@HtmlImport("match_question_editor.html")
library iren_editor.match_question_editor;

import 'dart:async';
import 'dart:html';

import 'package:initialize/initialize.dart';
import 'package:iren_proto/editor/match_question_editor.pb.dart';
import 'package:iren_proto/question/match_question.pb.dart';
import 'package:misc_utils/misc_utils.dart';
import 'package:web_components/web_components.dart';
import 'package:web_helpers/translator.dart';

import 'common.dart';
import 'editor_infra.dart';
import 'pad_editor.dart';

@QuestionEditor(MatchQuestionEditor.TAG, XMatchQuestionEditor)
class MatchQuestionEditor extends Widget<XMatchQuestionEditor> {
  static const String TAG = "ru-irenproject-match-question-editor";

  @ui DivElement _toolbar;
  @ui ButtonElement _addPairButton;
  @ui ButtonElement _addDistractorButton;
  @ui ButtonElement _deleteRowButton;
  @ui ButtonElement _moveRowUpButton;
  @ui ButtonElement _moveRowDownButton;
  @ui ButtonElement _optionsButton;
  @ui DivElement _itemPanel;

  PadEditor _formulation;

  /* nullable */int _selectRow;
  /* nullable */int _focusedRow;

  MatchQuestionEditor.created() : super.created();

  @override void initialize() {
    _addPairButton.onClick.listen((_) => _addPair());
    _addDistractorButton.onClick.listen((_) => _addDistractor());
    _deleteRowButton.onClick.listen((_) => _deleteRow());
    _moveRowUpButton.onClick.listen((_) => _moveRowUp());
    _moveRowDownButton.onClick.listen((_) => _moveRowDown());
    _optionsButton.onClick.listen((_) => _showOptionsDialog());

    windowShortcutHandler
        ..add(KeyCode.F5, KeyModifierState.NONE, _addPair)
        ..add(KeyCode.F6, KeyModifierState.NONE, _addDistractor)
        ..add(KeyCode.F8, KeyModifierState.NONE, _deleteRow);

    new ShortcutHandler(shadowRoot)
        ..add(KeyCode.UP, KeyModifierState.ALT, _moveRowUp)
        ..add(KeyCode.DOWN, KeyModifierState.ALT, _moveRowDown);

    _formulation = new PadEditor(shape.formulationEditor, dispatcher)
        ..id = "formulation";
    findElement("formulationPlaceholder").replaceWith(_formulation);

    Element.focusEvent.forTarget(shadowRoot, useCapture: true).listen((_) => _onFocus());
    Element.blurEvent.forTarget(shadowRoot, useCapture: true).listen(_onBlur);
  }

  @override void render() {
    super.render();
    _formulation.render();

    if (shapeChanged) {
      _focusedRow = null;
      _itemPanel.nodes.clear();

      shape.leftEditor.asMap().forEach((int i, String leftName) => _itemPanel.append(new DivElement()
          ..classes.add("pair")
          ..append(new PadEditor(leftName, dispatcher))
          ..append(new PadEditor(shape.rightEditor[i], dispatcher))));

      for (String rightName in shape.rightEditor.sublist(shape.leftEditor.length)) {
        _itemPanel.append(new PadEditor(rightName, dispatcher)
            ..classes.add("distractor"));
      }

      _addPairButton.disabled = _addDistractorButton.disabled = !_canAdd();
      _optionsButton.classes.toggle("edited", !shape.hasDefaultOptions);
    }

    for (PadEditor e in _itemPanel.querySelectorAll(PadEditor.TAG)) {
      e.render();
    }

    if ((_selectRow != null) && shape.rightEditor.isNotEmpty) {
      _selectRow = _selectRow.clamp(0, shape.rightEditor.length - 1);
      Element e = _itemPanel.children[_selectRow];
      if (!_isDistractorRow(_selectRow)) {
        e = e.children.first;
      }
      (e as PadEditor).focusComponent();
    }
    _selectRow = null;

    _updateUi();
  }

  void _addPair() {
    if (_canAdd()) {
      performAction(new XMatchQuestionEditor_AddPair());
      _selectRow = shape.leftEditor.length;
    }
  }

  bool _canAdd() => shape.rightEditor.length < shape.maxRows;

  void _addDistractor() {
    if (_canAdd()) {
      performAction(new XMatchQuestionEditor_AddDistractor());
      _selectRow = shape.rightEditor.length;
    }
  }

  void _deleteRow() {
    if (_focusedRow != null) {
      performAction(new XMatchQuestionEditor_DeleteRow()
          ..index = _focusedRow);
      _selectRow = _focusedRow;
    }
  }

  void _moveRowUp() {
    if (_canMoveRowUp()) {
      performAction(new XMatchQuestionEditor_MoveRow()
          ..index = _focusedRow
          ..forward = false);
      _selectRow = _focusedRow - 1;
    }
  }

  void _moveRowDown() {
    if (_canMoveRowDown()) {
      performAction(new XMatchQuestionEditor_MoveRow()
          ..index = _focusedRow
          ..forward = true);
     _selectRow = _focusedRow + 1;
    }
  }

  bool _canMoveRowUp() => (_focusedRow != null) && (_focusedRow > 0)
      && (_isDistractorRow(_focusedRow) == _isDistractorRow(_focusedRow - 1));

  bool _isDistractorRow(int row) => row >= shape.leftEditor.length;

  bool _canMoveRowDown() => (_focusedRow != null) && (_focusedRow < shape.rightEditor.length - 1)
      && (_isDistractorRow(_focusedRow) == _isDistractorRow(_focusedRow + 1));

  void _onFocus() {
    int index = _itemPanel.children.indexOf(shadowRoot.activeElement?.parent);
    if (index == -1) {
      index = _itemPanel.children.indexOf(shadowRoot.activeElement);
    }
    if (index != -1) {
      _focusedRow = index;
    }

    _updateUi();
  }

  void _onBlur(Event e) {
    if (!_toolbar.contains((e as FocusEvent).relatedTarget)) {
      _focusedRow = null;
    }
    _updateUi();
  }

  void _updateUi() {
    _deleteRowButton.disabled = (_focusedRow == null);
    _moveRowUpButton.disabled = !_canMoveRowUp();
    _moveRowDownButton.disabled = !_canMoveRowDown();
  }

  void _showOptionsDialog() {
    MatchQuestionOptionsDialog.openModal(shape, dispatcher, name);
  }

  @override void focusComponent() {
    _formulation.focusComponent();
  }
}

@CustomElement(MatchQuestionOptionsDialog.TAG)
class MatchQuestionOptionsDialog extends ModalDialog {
  static const String TAG = "ru-irenproject-match-question-options-dialog";

  static Future<Null> openModal(XMatchQuestionEditor source, Dispatcher dispatcher, String targetName) =>
      ((new Element.tag(TAG) as MatchQuestionOptionsDialog)
          .._source = source
          .._dispatcher = dispatcher
          .._targetName = targetName)._doOpenModal();

  @ui DialogElement _dialog;
  @ui RadioButtonInputElement _randomPairs;
  @ui NumberInputElement _pairLimit;
  @ui RadioButtonInputElement _randomDistractors;
  @ui NumberInputElement _distractorLimit;

  XMatchQuestionEditor _source;
  Dispatcher _dispatcher;
  String _targetName;

  MatchQuestionOptionsDialog.created() : super.created();

  Future<Null> _doOpenModal() async {
    _pairLimit.max = _distractorLimit.max = _source.maxRows.toString();

    if (_source.hasPairLimit()) {
      _randomPairs.checked = true;
      _pairLimit.valueAsNumber = _source.pairLimit;
    }

    if (_source.hasDistractorLimit()) {
      _randomDistractors.checked = true;
      _distractorLimit.valueAsNumber = _source.distractorLimit;
    }

    _randomPairs.onClick.listen((_) => focusInputElement(_pairLimit));
    _randomDistractors.onClick.listen((_) => focusInputElement(_distractorLimit));

    _updateUi();
    _dialog.onChange.listen((_) => _updateUi());

    await open(_dialog);

    if (_dialog.returnValue.isNotEmpty) {
      XMatchQuestionEditor_SetOptions action = new XMatchQuestionEditor_SetOptions();
      if (_randomPairs.checked) {
        action.pairLimit = _pairLimit.valueAsNumber;
      }
      if (_randomDistractors.checked) {
        action.distractorLimit = _distractorLimit.valueAsNumber;
      }
      await _dispatcher.performAction(_targetName, action);
    }
  }

  void _updateUi() {
    _pairLimit.disabled = !_randomPairs.checked;
    _distractorLimit.disabled = !_randomDistractors.checked;
  }
}

const String _LIB = "match_question_editor";

@initMethod void initialize() {
  registerShapeParser((List<int> b) => new XMatchQuestionEditor.fromBuffer(b));

  registerQuestionDescriptor(new QuestionDescriptor(
      type: MatchQuestionType.match.name,
      title: translate(_LIB, "Matching Question"),
      priority: 3000,
      icon: "match_question_editor.resources/matchQuestion.png",
      initialText: translate(_LIB, "Match the items.")));
}
