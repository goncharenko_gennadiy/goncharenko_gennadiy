/*
  © Translation contributors, see the TRANSLATORS file

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

final Map<String, Map<String, dynamic>> uk = {
  "classify_question_editor": {
    "Classification Question": "Питання на класифікацію",
    "Classify the items.": "Виконати класифікацію.",
  },

  "input_question_editor": {
    "Text Input Question": "Питання з введенням відповіді",
    "Text Pattern": "Текстовий еталон",
    "Add Text Pattern": "Додати текстовий еталон",
    "Regular Expression": "Регулярний вираз",
    "Add Regular Expression": "Додати регулярний вираз",
  },

  "match_question_editor": {
    "Matching Question": "Питання на відповідність",
    "Match the items.": "Встановити відповідність.",
  },

  "modifier_screen": {
    "Allow Multiple Selection for Questions with a Single Correct Answer":
      "Заборона підказки про єдину правильну відповідь",
    'Add the "No Correct Answers" Choice': 'Додавання варіанту "Немає правильних відповідей"',
    'Set the "No Correct Answers" Choice Text': 'Зміна тексту варіанту "Немає правильних відповідей"',
    "Shuffle Choices": "Перемішування варіантів відповідей",
    "Set Scoring Mode [editing not implemented yet]": "Параметри моделі оцінювання [можливість редагування тимчасово відсутня]",
    "Set Question Weight": "Зміна ваги питання",
    "Script [editing not implemented yet]": "Сценарій [можливість редагування тимчасово відсутня]",
  },

  "order_question_editor": {
    "Ordering Question": "Питання на впорядкування",
    "Order the items.": "Встановити правильну послідовність.",
  },

  "select_question_editor": {
    "Multiple Choice Question": "Питання з вибором відповіді",
  },

  "ru-irenproject-about-dialog": {
    "Iren": "Айрен",
    "Version": "Версія",
    "© 2012–2016 Sergey Ostanin": "© 2012–2016 Сергій Останін",
    "License...": "Ліцензія...",
    "Libraries...": "Бібліотеки...",
    "Translators...": "Перекладачі...",
    "Close": "Закрити",
  },

  "ru-irenproject-classify-question-editor": {
    "Add Item to Selected Category (F5)": "Додати елемент в обрану категорію (F5)",
    "Add Category (F6)": "Додати категорію (F6)",
    "Delete Item or Category (F8)": "Видалити елемент чи категорію (F8)",
    "Move Item or Category Up (Alt+Up)": "Перемістити елемент чи категорію вище (Alt+Вгору)",
    "Move Item or Category Down (Alt+Down)": "Перемістити елемент чи категорію нижче (Alt+Вниз)",
    "Options": "Параметри",
  },

  "ru-irenproject-classify-question-options-dialog": {
    "Items": "Елементи",
    "Offer all": "Запропонувати все",
    "Randomly select": "Обрати випадково",
    "From each category, select at least": "З кожної категорії не менше",
    "Cancel": "Відміна",
  },

  "ru-irenproject-credits-dialog": {
    "Close": "Закрити",
  },

  "ru-irenproject-editor-main-screen": {
    "WINDOW_TITLE": (String fileTitle) => "$fileTitle — Айрен",
    "Iren": "Айрен",
  },

  "ru-irenproject-file-manager": {
    "CANNOT_OPEN_FILE": (String fileName) => 'Помилка при відкритті файлу "$fileName".',
    "CANNOT_SAVE_FILE": (String fileName) => 'Помилка при збереженні файлу "$fileName".',
    "SAVE_CHANGES": (String fileName) => 'Зберегти зміни в "$fileName"?',
    "New (Ctrl+N)": "Створити (Ctrl+N)",
    "Open (Ctrl+O)": "Відкрити (Ctrl+O)",
    "Save (Ctrl+S)": "Зберегти (Ctrl+S)",
    "About": "Про програму",
    "Remember Current State and Exit (Alt+Shift+Q)": "Запам'ятати поточний стан і вийти (Alt+Shift+Q)",
    "Close Test (Ctrl+F4)": "Закрити тест (Ctrl+F4)",
    "Save": "Зберегти",
    "Don't Save": "Не зберігати",
    "Cancel": "Відміна",
  },

  "ru-irenproject-input-question-editor": {
    "Delete Pattern (F8)": "Видалити еталон (F8)",
    "Move Pattern Up (Alt+Up)": "Перемістити еталон вище (Alt+Вгору)",
    "Move Pattern Down (Alt+Down)": "Перемістити еталон нижче (Alt+Вниз)",
  },

  "ru-irenproject-input-question-pattern-editor": {
    "Correctness": "Ступінь коректності",
    "Options": "Параметри",
  },

  "ru-irenproject-input-string-dialog": {
    "Cancel": "Відміна",
  },

  "ru-irenproject-mark-dialog": {
    "Lower bound:": "Нижня межа:",
    "Grade:": "Оцінка:",
    "Add": "Додати",
    "Update": "Змінити",
    "Delete": "Видалити",
    "Cancel": "Відміна",
  },

  "ru-irenproject-match-question-editor": {
    "Add Pair (F5)": "Додати пару елементів (F5)",
    "Add Unpaired Item (F6)": "Додати зайвий елемент (F6)",
    "Delete Pair or Item (F8)": "Видалити один чи пару елементів (F8)",
    "Move Pair or Item Up (Alt+Up)": "Перемістити один чи пару елементів вище (Alt+Вгору)",
    "Move Pair or Item Down (Alt+Down)": "Перемістити один чи пару елементів нижче (Alt+Вниз)",
    "Options": "Параметри",
  },

  "ru-irenproject-match-question-options-dialog": {
    "Pairs": "Пари",
    "Offer all": "Запропонувати все",
    "Randomly select": "Обрати випадково",
    "Unpaired Items": "Зайві елементи",
    "Cancel": "Відміна",
  },

  "ru-irenproject-modifier-list-editor": {
    "SECTION_MODIFIERS": (String sectionName) => 'Модифікатори розділу "$sectionName"',
    "Add Modifier": "Додати модифікатор",
    "Delete Modifier (Del)": "Видалити модифікатор (Del)",
    "Move Modifier Up (Alt+Up)": "Перемістити модифікатор вище (Alt+Вгору)",
    "Move Modifier Down (Alt+Down)": "Перемістити модифікатор нижче (Alt+Вниз)",
    "Question Modifiers": "Модифікатори питання",
  },

  "ru-irenproject-modifier-screen": {
    "Close": "Закрити",
  },

  "ru-irenproject-order-question-editor": {
    "Add Item (F5)": "Додати елемент (F5)",
    "Delete Item (F8)": "Видалити елемент (F8)",
    "Move Item Up (Alt+Up)": "Перемістити елемент вище (Alt+Вгору)",
    "Move Item Down (Alt+Down)": "Перемістити елемент нижче (Alt+Вниз)",
    "Options": "Параметри",
    "Correct Sequence": "Правильна послідовність",
    "Wrong Items": "Зайві елементи",
  },

  "ru-irenproject-order-question-options-dialog": {
    "Sequence Items": "Елементи послідовності",
    "Offer all": "Запропонувати все",
    "Randomly select": "Обрати випадково",
    "Wrong Items": "Зайві елементи",
    "Cancel": "Відміна",
  },

  "ru-irenproject-pad-editor": {
    "Insert Picture from File...": "Вставити рисунок з файлу...",
    "Pictures": "Рисунки",
    "All Files": "Усі файли",
    "Unknown image type.": "Невідомий тип зображення.",
    "Save Picture to File...": "Зберегти рисунок у файл...",
  },

  "ru-irenproject-profile-editor": {
    "Profile:": "Профіль:",
    "Question Selection": "Вибір питань",
    "All": "Усі",
    "Randomly select": "По",
    "from each section": "з кожного розділу",
    "With labels": "З позначками",
    "Shuffle questions": "Перемішувати питання",
    "Time Limit": "Обмеження часу",
    "None": "Немає",
    "minutes": "хв.",
    "Test Process": "Хід тестування",
    "Allow editing answers": "Дозволити виправлення відповідей",
    "Instantly show if the answer is correct": "Повідомляти про правильність відповідей",
    "Show current result in percent": "Показувати поточний результат у відсотках",
    "Display": "Вигляд екрану студента",
    "Allow browsing through questions": "Дозволити огляд питань",
    "Reflect question weights on diagram": "Будувати діаграму з врахуванням ваги питань",
    "Results": "Результати",
    "Show results after testing": "Показати результати після закінчення тестування",
    "Result in percent": "Підсумок у відсотках",
    "Grade": "Оцінка",
    "Points earned": "Сума набраних балів",
    "Show question details": "Показати подробиці за питаннями",
    "Outcome (right/wrong)": "Правильність відповіді студента",
    "Correct answer": "Правильна відповідь",
    "Question weight": "Вага питання",
    "Show section details": "Показати подробиці за розділами",
    "Number of questions": "Кількість питань",
    "Question list": "Перелік питань",
    "Grading Scale": "Шкала оцінок",
    "Lower Bound, %": "Нижня межа, %",
    "Add...": "Додати...",
    "Modifiers": "Модифікатори",
  },

  "ru-irenproject-profile-list-editor": {
    "Profiles": "Профілі",
    "untitled": "безіменний",
    "Add Profile (F2)": "Додати профіль (F2)",
    "Delete Profile (Del)": "Видалити профіль (Del)",
    "Move Profile Up (Alt+Up)": "Перемістити профіль вище (Alt+Вгору)",
    "Move Profile Down (Alt+Down)": "Перемістити профіль нижче (Alt+Вниз)",
  },

  "ru-irenproject-profile-screen": {
    "Close": "Закрити",
  },

  "ru-irenproject-question-list-editor": {
    "ADD_QUESTION": (String title) => "Додати $title (F2)",
    "USE_TITLE_CASE": "false",
    "Delete Question (Del)": "Видалити питання (Del)",
    "Set Question Weight (F3)": "Задати вагу питання (F3)",
    "Question Modifiers (F4)": "Модифікатори питання (F4)",
    "Forbid Use": "Заборонити використання",
    "Move Question Up (Alt+Up)": "Перемістити питання вище (Alt+Вгору)",
    "Move Question Down (Alt+Down)": "Перемістити питання нижче (Alt+Вниз)",
    "Questions": "Питання",
    "Text": "Текст",
    "Weight": "Вага",
    "Question weight:": "Вага питання:",
    "Profiles": "Профілі",
  },

  "ru-irenproject-regexp-pattern-options-dialog": {
    "Space removal from the examinee's answer before matching":
      "Видалення пробілів з відповіді студента перед співставленням",
    "Remove leading and trailing spaces, leave one space between words":
      "Видалити початкові і кінцеві пробіли, залишити по одному пробілу між словами",
    "Remove all spaces": "Видалити усі пробіли",
    "Keep spaces": "Зберегти пробіли",
    "Ignore case": "Ігнорувати реєстр літер",
    "Cancel": "Відміна",
  },

  "ru-irenproject-section-tree-editor": {
    "DELETE_SECTION": (String name) => 'Видалити розділ "$name"?',
    "Section name:": "Назва розділу:",
    "Delete": "Видалити",
    "Cancel": "Відміна",
    "Sections": "Розділи",
    "Add Section (Shift+F2)": "Додати розділ (Shift+F2)",
    "Delete Section (Del)": "Видалити розділ (Del)",
    "Rename Section (Shift+F3)": "Перейменувати розділ (Shift+F3)",
    "Section Modifiers (Shift+F4)": "Модифікатори розділу (Shift+F4)",
    "Move Section Up (Alt+Up)": "Перемістити розділ вище (Alt+Вгору)",
    "Move Section Down (Alt+Down)": "Перемістити розділ нижче (Alt+Вниз)",
  },

  "ru-irenproject-select-question-choice-editor": {
    "Correct choice marker": "Індикатор правильної відповіді",
    'This is the "No Correct Answers" choice': 'Варіант "Немає правильних відповідей"',
    "This choice is pinned (won't move when shuffling the choices)":
      "Положення цього варіанту відповіді фіксоване (не змінюватиметься при перемішуванні відповідей)",
  },

  "ru-irenproject-select-question-editor": {
    "None of the above are correct.": "Немає правильного варіанту серед запропонованих.",
    "Add Choice (F5)": "Додати варіант відповіді (F5)",
    "Delete Choice (F8)": "Видалити варіант відповіді (F8)",
    "Move Choice Up (Alt+Up)": "Перемістити варіант відповіді вище (Alt+Вгору)",
    "Move Choice Down (Alt+Down)": "Перемістити варіант відповіді нижче (Alt+Вниз)",
    'Add the "No Correct Answers" Choice (F6)': 'Додати варіант "Немає правильних відповідей" (F6)',
    "Pin Choice": "Зафіксувати положення варіанту відповіді",
  },

  "ru-irenproject-text-pattern-options-dialog": {
    "* matches any number of characters (including none at all), ? matches any single character":
      "Вважати * і ? в еталоні спеціальними символами, що дозволяють враховувати різні варіанти"
      " написання правильної відповіді.\n\n"
      "* означає, що в даному місці слова може знаходитись будь-яка кількість довільних символів"
      " (в тому числі жодного).\n\n"
      "? замінює собою один довільний символ.\n\n"
      'Наприклад, еталону "м*н* вар*" відповідають відповіді "малинове варення" і "мінімальна варіація",'
      ' еталону "гра?іт" — "граніт" і "графіт".',
    "Interpret * and ? as wildcards": "Вважати * і ? символами підстановки",
    'Allow both "," and "." for a decimal mark, ignore leading and trailing (in fractional part) zeros.\n\n'
    'For example, "9,03" = "9.03" = "009,030".':
      "Порівнювати числові відповіді з еталоном за їхнім значенням, ігноруючи відмінності у формі запису.\n\n"
      'Наприклад, "9,03" = "9.03" = "009,030".',
    "Recognize numbers": "Розпізнавати числові відповіді",
    "Tolerance:": "Допустима похибка:",
    "Compare word-by-word": "Вважати пробіли розділювачами слів",
    "Take spaces into account": "Враховувати пробіли",
    "Don't distinguish between upper and lower case letters":
      "Не розрізняти прописні і рядкові букви при порівнянні відповіді з еталоном",
    "Ignore case": "Ігнорувати реєстр літер",
    "Cancel": "Відміна",
  }
};
