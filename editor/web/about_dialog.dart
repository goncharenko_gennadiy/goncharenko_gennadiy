/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

@HtmlImport("about_dialog.html")
library iren_editor.about_dialog;

import 'dart:async';
import 'dart:html';

import 'package:misc_utils/electron.dart';
import 'package:misc_utils/misc_utils.dart';
import 'package:misc_utils/test_utils.dart';
import 'package:web_components/web_components.dart';

import 'common.dart';

@CustomElement(AboutDialog.TAG)
class AboutDialog extends ModalDialog {
  static const String TAG = "ru-irenproject-about-dialog";

  static Future<Null> openModal() => (new Element.tag(TAG) as AboutDialog)._doOpenModal();

  @ui DialogElement _dialog;
  @ui AnchorElement _link;
  @ui ButtonElement _licenseButton;
  @ui ButtonElement _librariesButton;
  @ui ButtonElement _translatorsButton;

  AboutDialog.created() : super.created();

  Future<Null> _doOpenModal() {
    _link
        ..onClick.listen(_onLinkClick)
        ..onFocus.first.then((_) => _link.blur());
    _licenseButton.onClick.listen((_) => CreditsDialog.openModal("LICENSE", 30));
    _librariesButton.onClick.listen((_) => CreditsDialog.openModal("credits/CREDITS", 30));
    _translatorsButton.onClick.listen((_) => CreditsDialog.openModal("TRANSLATORS", 8));
    return open(_dialog);
  }

  void _onLinkClick(MouseEvent e) {
    stopFurtherProcessing(e);
    shell_openExternal(_link.text);
  }
}

@CustomElement(CreditsDialog.TAG)
class CreditsDialog extends ModalDialog {
  static const String TAG = "ru-irenproject-credits-dialog";

  static Future<Null> openModal(String fileName, int rows) => ((new Element.tag(TAG) as CreditsDialog)
      .._fileName = fileName
      .._rows = rows)._doOpenModal();

  @ui DialogElement _dialog;
  @ui TextAreaElement _credits;

  String _fileName;
  int _rows;

  CreditsDialog.created() : super.created();

  Future<Null> _doOpenModal() {
    _credits
        ..rows = _rows
        ..value = fs_readFileSync_String(path_normalize(getAbsoluteAppPath(
            "${developmentMode ? "" : "../"}../../$_fileName")), "utf8")
        ..setSelectionRange(0, 0);
    return open(_dialog);
  }
}
