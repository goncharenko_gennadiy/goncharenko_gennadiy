/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

@HtmlImport("question_list_editor.html")
library iren_editor.question_list_editor;

import 'dart:async';
import 'dart:collection';
import 'dart:html';
import 'dart:js';

import 'package:initialize/initialize.dart';
import 'package:iren_proto/editor/editor.pb.dart';
import 'package:misc_utils/electron.dart';
import 'package:misc_utils/misc_utils.dart';
import 'package:protobuf/protobuf.dart';
import 'package:quiver/iterables.dart' show range;
import 'package:web_components/web_components.dart';

import 'common.dart';
import 'common_dialogs.dart';
import 'editor_infra.dart';

@CustomElement(QuestionListEditor.TAG)
class QuestionListEditor extends Widget<XQuestionListEditor> {
  static const String TAG = "ru-irenproject-question-list-editor";
  static const String _QUESTION_LIST_MIME_TYPE = "application/x-irenproject.ru-question-list";

  @ui ButtonElement _addButton;
  @ui ImageElement _questionTypeImage;
  @ui ButtonElement _addDropDown;
  @ui ButtonElement _deleteButton;
  @ui ButtonElement _setWeightButton;
  @ui ButtonElement _setEnabledButton;
  @ui ButtonElement _moveUpButton;
  @ui ButtonElement _moveDownButton;
  @ui ButtonElement _modifiersButton;
  @ui ButtonElement _profilesButton;
  @ui DivElement _scrollable;
  @ui TableElement _table;

  List<XQuestionItemEditor> _rendered;
  QuestionDescriptor _selectedQuestionType;
  bool _scrollToSelectionStart = false;
  StreamSubscription<ClipboardEvent> _copySubscription;
  StreamSubscription<ClipboardEvent> _cutSubscription;
  StreamSubscription<ClipboardEvent> _pasteSubscription;

  final StreamController<Null> _onAddQuestion = new StreamController.broadcast(sync: true);
  Stream<Null> get onAddQuestion => _onAddQuestion.stream;

  final StreamController<Null> _onShowProfiles = new StreamController.broadcast(sync: true);
  Stream<Null> get onShowProfiles => _onShowProfiles.stream;

  QuestionListEditor.created() : super.created();

  factory QuestionListEditor(String name, Dispatcher dispatcher) => new Widget(TAG, name, dispatcher);

  @override void initialize() {
    _addButton.onClick.listen((_) => _add());
    _addDropDown.onClick.listen((_) => _onAddDropDownClick());
    _deleteButton.onClick.listen((_) => _delete());
    _setWeightButton.onClick.listen((_) => _setWeight());
    _modifiersButton.onClick.listen((_) => _showModifiers());
    _setEnabledButton.onClick.listen((_) => _setEnabled());
    _moveUpButton.onClick.listen((_) => _moveUp());
    _moveDownButton.onClick.listen((_) => _moveDown());
    _profilesButton.onClick.listen((_) => _onShowProfiles.add(null));

    windowShortcutHandler
        ..add(KeyCode.F2, KeyModifierState.NONE, _add)
        ..add(KeyCode.F3, KeyModifierState.NONE, _setWeight)
        ..add(KeyCode.F4, KeyModifierState.NONE, _showModifiers);

    new ShortcutHandler(shadowRoot)
        ..add(KeyCode.DELETE, KeyModifierState.NONE, _delete)
        ..add(KeyCode.UP, KeyModifierState.ALT, _moveUp)
        ..add(KeyCode.DOWN, KeyModifierState.ALT, _moveDown)
        ..add(KeyCode.A, KeyModifierState.CTRL, _selectAll);

    new ShortcutHandler(_scrollable)
        ..add(KeyCode.UP, KeyModifierState.NONE, () => _shiftSingleSelection(-1))
        ..add(KeyCode.DOWN, KeyModifierState.NONE, () => _shiftSingleSelection(1))
        ..add(KeyCode.PAGE_UP, KeyModifierState.NONE, () => _shiftSingleSelection(-_rowsVisible()))
        ..add(KeyCode.PAGE_DOWN, KeyModifierState.NONE, () => _shiftSingleSelection(_rowsVisible()))
        ..add(KeyCode.HOME, KeyModifierState.NONE, _handleHome)
        ..add(KeyCode.HOME, KeyModifierState.CTRL, () {})
        ..add(KeyCode.END, KeyModifierState.NONE, _handleEnd)
        ..add(KeyCode.END, KeyModifierState.CTRL, () {})
        ..add(KeyCode.SPACE, KeyModifierState.NONE, () {})
        ..add(KeyCode.SPACE, KeyModifierState.SHIFT, () {});

    if (_lastSelectedQuestionType == null) {
      if (questionDescriptors.isNotEmpty) {
        _selectQuestionType(questionDescriptors.first);
      }
    } else {
      _selectQuestionType(_lastSelectedQuestionType);
    }
  }

  @override void attached() {
    super.attached();
    _copySubscription = document.onCopy.listen((ClipboardEvent e) => _copyOrCut(e, new XQuestionListEditor_Copy()));
    _cutSubscription = document.onCut.listen((ClipboardEvent e) => _copyOrCut(e, new XQuestionListEditor_Cut()));
    _pasteSubscription = document.onPaste.listen(_paste);
  }

  @override void detached() {
    _copySubscription?.cancel();
    _copySubscription = null;
    _cutSubscription?.cancel();
    _cutSubscription = null;
    _pasteSubscription?.cancel();
    _pasteSubscription = null;
    super.detached();
  }

  @override void render() {
    super.render();
    if (shapeChanged) {
      int rowsNeeded = shape.itemEditor.length;

      if (_table.rows.length < rowsNeeded) {
        for (int i = _table.rows.length + 1; i <= rowsNeeded; ++i) {
          _table.addRow()
              ..addCell().append(new DivElement()
                  ..classes.add("typeIcon"))
              ..addCell().text = i.toString()
              ..addCell()
              ..addCell()
              ..onClick.listen(_onRowClick);
        }
      } else {
         for (int rowsToDelete = _table.rows.length - rowsNeeded, i = 0; i < rowsToDelete; ++i) {
           _table.deleteRow(-1);
         }
      }

      _rendered = new List.filled(shape.itemEditor.length, null);
    }

    List<int> selectedIndices = _selection().selectedIndex;
    int s = 0;

    shape.itemEditor.asMap().forEach((int index, String name) {
      XQuestionItemEditor itemEditor = scene.shapes(name);
      TableRowElement row = _table.rows[index];

      if (!identical(itemEditor, _rendered[index])) {
        _rendered[index] = itemEditor;
        QuestionDescriptor d = getQuestionDescriptor(itemEditor.type);
        row.cells.first.children.first
            ..style.backgroundImage = "url(${d.icon})"
            ..classes.toggle("hasModifiers", itemEditor.hasModifiers)
            ..title = d.title;
        row
            ..cells[2].text = itemEditor.text
            ..cells[3].text = itemEditor.weight.toString()
            ..title = itemEditor.text
            ..classes.toggle("disabled", !itemEditor.enabled);
      }

      bool selected = (s < selectedIndices.length) && (selectedIndices[s] == index);
      if (selected) {
        if ((s == 0) && (_scrollToSelectionStart || shapeChanged)) {
          scheduleScrollIntoView(row);
        }
        ++s;
      }
      row.classes.toggle("selected", selected);
    });

    _deleteButton.disabled = _setWeightButton.disabled = _setEnabledButton.disabled =
        _selection().selectedIndex.isEmpty;
    _moveUpButton.disabled = !_canMoveUp();
    _moveDownButton.disabled = !_canMoveDown();
    _modifiersButton.disabled = (_selection().selectedIndex.length != 1);

    _setEnabledButton.classes.toggle("highlighted", _disabledItemsSelected());

    _scrollToSelectionStart = false;
  }

  XQuestionListSelection _selection() => scene.shapes(shape.selection);

  void _onRowClick(MouseEvent e) {
    int index = (e.currentTarget as TableRowElement).sectionRowIndex;

    if (e.ctrlKey) {
      SplayTreeSet<int> indices = new SplayTreeSet.from(_selection().selectedIndex);
      if (!indices.add(index)) {
        indices.remove(index);
      }
      _select(indices);
    } else if (e.shiftKey) {
      if (_selection().selectedIndex.length == 1) {
        int s = _selection().selectedIndex.first;
        if (index < s) {
          _select(range(index, s + 1));
        } else if (index > s) {
          _select(range(s, index + 1));
        }
      }
    } else {
      _select([index]);
    }
  }

  void _select(Iterable<int> sortedIndices) {
    performAction(new XQuestionListEditor_Select()
        ..index.addAll(sortedIndices));
  }

  void _shiftSingleSelection(int offset) {
    if (_selection().selectedIndex.length == 1) {
      _select([(_selection().selectedIndex.first + offset).clamp(0, shape.itemEditor.length - 1)]);
      _scrollToSelectionStart = true;
    }
  }

  int _rowsVisible() {
    int res;
    if (_table.rows.isEmpty) {
      res = 0;
    } else {
      int h = _table.rows.first.offsetHeight;
      res = (h == 0) ? 0 : _scrollable.clientHeight ~/ h;
    }
    return res;
  }

  void _handleHome() {
    if (shape.itemEditor.isNotEmpty) {
      _select([0]);
      _scrollToSelectionStart = true;
    }
  }

  void _handleEnd() {
    if (shape.itemEditor.isNotEmpty) {
      _select([shape.itemEditor.length - 1]);
      _scrollToSelectionStart = true;
    }
  }

  void _onAddDropDownClick() {
    Menu menu = new Menu();

    for (QuestionDescriptor d in questionDescriptors) {
      menu.append(new MenuItem(new MenuItemOptions(
          //ignore: argument_type_not_assignable
          click: allowInterop((_, __) {
            _selectQuestionType(d);
            _add();
          }),
          label: d.title,
          icon: getAbsoluteAppPath(d.icon))));
    }

    menu.popup(_addButton.borderEdge.left.round(), _addButton.borderEdge.bottom.round());
  }

  void _selectQuestionType(QuestionDescriptor d) {
    _selectedQuestionType = d;
    _questionTypeImage.src = d.icon;
    _addButton.title = tr("ADD_QUESTION")((tr("USE_TITLE_CASE") == "true") ? d.title : d.title.toLowerCase());

    _lastSelectedQuestionType = d;
  }

  void _add() {
    if (_selectedQuestionType != null) {
      performAction(new XQuestionListEditor_Add()
          ..type = _selectedQuestionType.type
          ..initialText = _selectedQuestionType.initialText);
      _onAddQuestion.add(null);
    }
  }

  void _delete() {
    if (_selection().selectedIndex.isNotEmpty) {
      performAction(new XQuestionListEditor_Delete());
    }
  }

  void _selectAll() {
    _select(range(0, shape.itemEditor.length));
  }

  void _moveUp() {
    if (_canMoveUp()) {
      performAction(new XQuestionListEditor_Move()
          ..forward = false);
    }
  }

  bool _canMoveUp() => _selection().selectedIndex.isNotEmpty && (_selection().selectedIndex.first > 0);

  void _moveDown() {
    if (_canMoveDown()) {
      performAction(new XQuestionListEditor_Move()
          ..forward = true);
    }
  }

  bool _canMoveDown() => _selection().selectedIndex.isNotEmpty
      && (_selection().selectedIndex.last < shape.itemEditor.length - 1);

  Future<Null> _setWeight() async {
    if (_selection().selectedIndex.isNotEmpty) {
      int oldWeight = _selectedItems().first.weight;
      if (!_selectedItems().every((XQuestionItemEditor item) => item.weight == oldWeight)) {
        oldWeight = null;
      }

      int newWeight = await inputInteger(prompt: tr("Question weight:"), min: 1, max: shape.maxWeight,
          initial: oldWeight);

      if (newWeight != null) {
        performAction(new XQuestionListEditor_SetWeight()
            ..weight = newWeight);
      }
    }
  }

  Iterable<XQuestionItemEditor> _selectedItems() => _selection().selectedIndex
      .map((int index) => scene.shapes(shape.itemEditor[index]) as XQuestionItemEditor);

  void _setEnabled() {
    performAction(new XQuestionListEditor_SetEnabled()
        ..enabled = _disabledItemsSelected());
  }

  bool _disabledItemsSelected() => _selectedItems().any((XQuestionItemEditor item) => !item.enabled);

  void _showModifiers() {
    if (_selection().selectedIndex.length == 1) {
      dispatcher.performAction(shape.itemEditor[_selection().selectedIndex.first],
          new XQuestionItemEditor_ShowModifiers());
    }
  }

  void _copyOrCut(ClipboardEvent e, GeneratedMessage action) {
    if (shadowRoot.activeElement != null) {
      stopFurtherProcessing(e);
      if (_selection().selectedIndex.isNotEmpty) {
        performAction(action);
        e.clipboardData.setData(_QUESTION_LIST_MIME_TYPE, "");
      }
    }
  }

  void _paste(ClipboardEvent e) {
    if (shadowRoot.activeElement != null) {
      stopFurtherProcessing(e);
      pasteQuestions(e);
    }
  }

  void pasteQuestions(ClipboardEvent e) {
    if (e.clipboardData.types.contains(_QUESTION_LIST_MIME_TYPE)) {
      performAction(new XQuestionListEditor_Paste());
    }
  }
}

QuestionDescriptor _lastSelectedQuestionType;

@initMethod void initialize() {
  registerShapeParsers([
      (List<int> b) => new XQuestionListEditor.fromBuffer(b),
      (List<int> b) => new XQuestionListSelection.fromBuffer(b),
      (List<int> b) => new XQuestionItemEditor.fromBuffer(b)]);
}
