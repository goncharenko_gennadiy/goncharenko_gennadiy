/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

@HtmlImport("test_document_editor.html")
library iren_editor.test_document_editor;

import 'dart:async';

import 'package:initialize/initialize.dart';
import 'package:iren_proto/editor/editor.pb.dart';
import 'package:iren_proto/editor/modifier_screen.pb.dart';
import 'package:iren_proto/editor/profile_screen.pb.dart';
import 'package:web_components/web_components.dart';

import 'editor_infra.dart';
import 'modifier_screen.dart';
import 'profile_screen.dart';
import 'test_screen.dart';

@CustomElement(TestDocumentEditor.TAG)
class TestDocumentEditor extends Widget<XTestDocumentEditor> {
  static const String TAG = "ru-irenproject-test-document-editor";
  static const String _WIDGET_NAME = "";

  static final Map<Type, String> _screens = {
      XTestScreen: TestScreen.TAG,
      XProfileScreen: ProfileScreen.TAG,
      XModifierScreen: ModifierScreen.TAG};

  static Future<Null> loadScene(Dispatcher dispatcher) async {
    await dispatcher.performAction(_WIDGET_NAME, new XTestDocumentEditor_Nop());
    await dispatcher.onChangeScene.first;
  }

  Widget _screen;

  TestDocumentEditor.created() : super.created();

  factory TestDocumentEditor(Dispatcher dispatcher) => new Widget(TAG, _WIDGET_NAME, dispatcher);

  @override void render() {
    super.render();

    if (shape.screen != _screen?.name) {
      Widget newScreen = new Widget(_getScreenTag(scene.shapes(shape.screen).runtimeType), shape.screen, dispatcher);
      if (_screen == null) {
        shadowRoot.append(newScreen);
      } else {
        _screen.replaceWith(newScreen);
      }
      _screen = newScreen;
    }

    _screen.render();
  }

  String _getScreenTag(Type shapeClass) {
    String res = _screens[shapeClass];
    if (res == null) {
      throw "No screen for '$shapeClass'";
    }
    return res;
  }

  bool get modified => shape.modified;

  void markAsUnmodified() {
    performAction(new XTestDocumentEditor_MarkAsUnmodified());
  }
}

@initMethod void initialize() {
  registerShapeParser((List<int> b) => new XTestDocumentEditor.fromBuffer(b));
}
