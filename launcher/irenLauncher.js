/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

"use strict";

const child_process = require("child_process");
const electron = require("electron");
const fs = require("original-fs");
const path = require("path");

const app = electron.app;
const BrowserWindow = electron.BrowserWindow;
const ipcMain = electron.ipcMain;

let mainWindow = null;
const devel = process.argv.includes("--devel");
const devtoolsSwitch = "--devtools";

const dataDir = path.join(
    process.platform == "linux" ? path.join(app.getPath("home"), ".local", "share") : app.getPath("appData"),
    "irenproject.ru");

let server = null;
let connectionConfig = null;
let connectionConfigRequested = false;

app.on("window-all-closed", () => {
  if (!server) {
    app.quit();
  }
});

if (process.argv.includes("--master")) {
  runMaster();
} else {
  runEditor();
}

function runMaster() {
  app.setName("irenMaster");
  app.setAppPath(devel ?
      path.join(__dirname, "..", "..", "..", "..", "master", "web") :
      path.join(__dirname, "..", "irenMaster.asar"));
  app.setPath("userData", path.join(dataDir, "master", "shellData"));
  app.setPath("userCache", path.join(app.getPath("cache"), "irenproject.ru", "master", "shellCache"));

  if (ensureSingleInstance()) {
    if (devel) {
      connectionConfig = JSON.stringify({
          port: 9981,
          supervisorKey: fs.readFileSync(path.join(dataDir, "supervisorKey")).toString("hex")});
    } else {
      startServer("irenServer", "ru.irenproject.work.Server");
    }

    app.on("ready", () => {
      createWindow("irenMaster", "#ffffff");
      mainWindow.loadURL(devel ? "http://127.0.0.1:8083/" : `file://${__dirname}/../irenMaster.asar/index.html`);
    });
  }
}

function runEditor() {
  app.setName("irenEditor");
  app.setAppPath(devel ?
      path.join(__dirname, "..", "..", "..", "..", "editor", "web") :
      path.join(__dirname, "..", "irenEditor.asar"));
  app.setPath("userData", path.join(dataDir, "editor", "shellData"));
  app.setPath("userCache", path.join(app.getPath("cache"), "irenproject.ru", "editor", "shellCache"));

  if (ensureSingleInstance()) {
    if (devel) {
      connectionConfig = JSON.stringify({
          port: 9982,
          editorKey: fs.readFileSync(path.join(dataDir, "editor", "editorKey")).toString("hex")});
    } else {
      startServer("irenEditorServer", "ru.irenproject.editor.EditorServer");
    }

    app.on("ready", () => {
      createWindow("irenEditor", "#f0f0f0");

      let relayCloseRequests = false;
      mainWindow.webContents.on("did-start-loading", () => {
        relayCloseRequests = false;
      });

      ipcMain.on("relayCloseRequests", () => {
        relayCloseRequests = true;
      });

      mainWindow.on("close", e => {
        if (relayCloseRequests) {
          e.preventDefault();
          mainWindow.webContents.send("requestClose");
        }
      });

      mainWindow.loadURL(devel ? "http://127.0.0.1:8082/" : `file://${__dirname}/../irenEditor.asar/index.html`);
    });
  }
}

function ensureSingleInstance() {
  const alreadyRunning = app.makeSingleInstance((argv, workingDirectory) => {
    if (mainWindow) {
      if (mainWindow.isMinimized()) {
        mainWindow.restore();
      }
      mainWindow.focus();

      if (argv.includes(devtoolsSwitch)) {
        mainWindow.webContents.openDevTools();
      }

      if ((argv.length >= 2) && !argv[1].startsWith("-")) {
        mainWindow.webContents.send("openFile", path.resolve(workingDirectory, argv[1]));
      }
    }
  });

  if (alreadyRunning) {
    app.quit();
  }

  return !alreadyRunning;
}

function startServer(exeName, className) {
  const topDir = path.join(__dirname, "..", "..", "..");

  server = child_process.spawn(
      path.join(topDir, "jre", "bin", exeName + (process.platform == "win32" ? ".exe" : "")),
      ["-classpath", path.join(topDir, "lib", "*"), className, "--companion"],
      {stdio: ["pipe", "pipe", process.stderr], detached: true});
  server.on("exit", () => {
    app.exit(0);
  });

  let connectionConfigBuffer = "";
  server.stdout.on("data", data => {
    connectionConfigBuffer += data;
  });

  server.stdout.on("end", () => {
    connectionConfig = connectionConfigBuffer;
    sendConnectionConfigIfPossible();
  });
}

function createWindow(partition, backgroundColor) {
  mainWindow = new BrowserWindow({
      webPreferences: {
          partition: partition,
          experimentalFeatures: true,
          backgroundThrottling: false},
      width: 1000,
      height: 700,
      useContentSize: true,
      title: "...",
      backgroundColor: backgroundColor});

  mainWindow.webContents.on("did-start-loading", () => {
    connectionConfigRequested = false;
  });

  mainWindow.webContents.on("will-navigate", (e, url) => {
    if (url != mainWindow.webContents.getURL()) {
      e.preventDefault();
    }
  });

  mainWindow.on("closed", () => {
    mainWindow = null;
    if (server) {
      server.stdin.end();
    }
  });

  ipcMain.on("getConnectionConfig", () => {
    connectionConfigRequested = true;
    sendConnectionConfigIfPossible();
  });

  if (devel || process.argv.includes(devtoolsSwitch)) {
    mainWindow.webContents.openDevTools();
  }
}

function sendConnectionConfigIfPossible() {
  if (connectionConfigRequested && (connectionConfig != null) && mainWindow) {
    mainWindow.webContents.send("connectionConfig", connectionConfig);
    connectionConfigRequested = false;
  }
}
