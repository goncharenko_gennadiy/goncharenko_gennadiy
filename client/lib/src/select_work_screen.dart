/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

import 'dart:async';
import 'dart:html';

import 'package:iren_proto/common.pb.dart';

import 'common.dart';

class SelectWorkScreen extends ProgramScreen {
  final Connection _connection;
  @override final DivElement element = new DivElement();
  /* nullable */StreamSubscription<ServerMessage> _messageSubscription;

  SelectWorkScreen(Connection this._connection) {
    _messageSubscription = _connection.onMessage.listen(_onServerMessage);
  }

  @override void onDismiss() {
    _messageSubscription?.cancel();
    _messageSubscription = null;
  }

  void _onServerMessage(ServerMessage m) {
    switch (m.type) {
      case ServerMessageType.WORK_LIST:
        _handleWorkList(m.workList);
        break;
    }
  }

  void _handleWorkList(WorkList m) {
    element.nodes.clear();

    if (m.workDescriptor.isEmpty) {
      element.appendText(tr("No tests are available."));
    } else {
      for (WorkDescriptor w in sortWorksByTitle(m.workDescriptor)) {
        element
            ..append(new AnchorElement(href: "#")
                ..text = w.title
                ..draggable = false
                ..onClick.listen((MouseEvent e) {
                  e.preventDefault();
                  _selectWork(w.id);
                }))
            ..append(new BRElement());
      }
    }
  }

  void _selectWork(String id) {
    Keeper.workId = id;

    _connection.send(new ClientMessage()
        ..type = ClientMessageType.SELECT_WORK
        ..selectWork = (new SelectWork()
            ..id = id));

    _messageSubscription?.cancel();
    _messageSubscription = null;
    element.nodes.clear();
  }
}
