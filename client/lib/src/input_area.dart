/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

import 'dart:html';

import 'package:iren_proto/common.pb.dart';
import 'package:iren_proto/input_area.pb.dart';

import 'common.dart';

class InputAreaWidget extends AreaWidget {
  final InputArea _area;
  final InputResponse _response;

  @override final DivElement element = new DivElement();
  final TextInputElement _inputElement = new TextInputElement();

  InputAreaWidget(Area area, AreaResponse response)
      : _area = area.getExtension(InputArea.inputArea),
        _response = response.getExtension(InputResponse.inputResponse),
        super(area, response) {
    element.classes.add("ru-irenproject-inputArea");

    element.append(new SpanElement()
        ..classes.add("ru-irenproject-inputArea-label")
        ..text = tr("Answer:"));

    _inputElement
        ..classes.add("ru-irenproject-inputArea-inputBox")
        ..maxLength = _area.maxSize
        ..value = _response.input
        ..placeholder = " " /* work around a bug in Chrome (35) with 'align-items: baseline' and empty TextInputElement,
            https://stackoverflow.com/questions/20847687/flexbox-vertical-alignment-issue-on-input-tag */
        ..onInput.listen((_) => _onInput());

    element.append(_inputElement);
  }

  void _onInput() {
    if (_response.input != _inputElement.value) {
      _response.input = _inputElement.value;
      triggerResponse();
    }
  }

  @override bool setFocus() {
    _inputElement
        ..focus()
        ..setSelectionRange(_inputElement.value.length, _inputElement.value.length);
    return true;
  }

  @override void setReadOnly(bool readOnly) {
    super.setReadOnly(readOnly);
    _inputElement.readOnly = readOnly;

    // work around https://connect.microsoft.com/IE/feedbackdetail/view/804785
    if ((document.activeElement == _inputElement) && window.navigator.userAgent.contains("Trident/")) {
      document.body.focus();
      _inputElement.focus();
    }
  }
}
