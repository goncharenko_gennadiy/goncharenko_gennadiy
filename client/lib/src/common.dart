/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

import 'dart:async';
import 'dart:collection';
import 'dart:html';
import 'dart:math' as math;

import 'package:fixnum/fixnum.dart';
import 'package:image/image.dart' show DecodeInfo, Decoder, JpegDecoder, PngDecoder;
import 'package:iren_proto/common.pb.dart';
import 'package:meta/meta.dart';
import 'package:protobuf/protobuf.dart';
import 'package:web_helpers/translator.dart';
import 'package:web_helpers/web_helpers.dart';

typedef AreaWidget AreaWidgetFactory(Area area, /* nullable */AreaResponse response);

class DialogWidget {
  final DivElement element = new DivElement();
  final LinkedHashMap<String, AreaWidget> _areas = new LinkedHashMap();

  final StreamController<DialogEvent> _onResponse = new StreamController.broadcast(sync: true);
  Stream<DialogEvent> get onResponse => _onResponse.stream;

  DialogWidget(Dialog dialog, DialogResponse response) {
    for (Area area in dialog.area) {
      AreaWidgetFactory f = _areaWidgetRegistry[area.kind];
      if (f != null) {
        AreaResponse ar = getAreaResponseIfExists(area.id, response);
        AreaWidget areaWidget = f(area, ar);

        _areas[area.id] = areaWidget;
        areaWidget.onResponse.listen((_) => _onResponse.add(new DialogEvent(this)));
        element.append(areaWidget.element);
      }
    }
  }

  DialogResponse get response => new DialogResponse()
      ..areaResponse.addAll(_areas.values
          .map((AreaWidget w) => w.response)
          .where((AreaResponse r) => r != null));

  void layOut() {
    for (AreaWidget w in _areas.values) {
      w.layOut();
    }
  }

  void setFocus() {
    for (AreaWidget w in _areas.values) {
      if (w.setFocus()) {
        break;
      }
    }
  }

  void setReadOnly(bool readOnly) {
    for (AreaWidget w in _areas.values) {
      w.setReadOnly(readOnly);
    }
  }
}

class DialogEvent {
  final DialogWidget source;
  DialogEvent(DialogWidget this.source);
}

abstract class AreaWidget {
  final Area area;
  final /* nullable */AreaResponse response;
  Element get element;

  bool _readOnly = false;

  final StreamController<Null> _onResponse = new StreamController.broadcast(sync: true);
  Stream<Null> get onResponse => _onResponse.stream;

  AreaWidget(Area this.area, /* nullable */AreaResponse this.response);

  @protected void triggerResponse() {
    _onResponse.add(null);
  }

  void layOut() {}

  bool setFocus() => false;

  bool get isReadOnly => _readOnly;

  void setReadOnly(bool readOnly) {
    _readOnly = readOnly;
    element.classes.toggle("ru-irenproject-area-readOnly", readOnly);
  }
}

class FlowAreaWidget extends AreaWidget {
  final FlowArea _area;
  @override DivElement element;

  FlowAreaWidget(Area area, /* nullable */AreaResponse response)
      : _area = area.getExtension(FlowArea.flowArea),
        super(area, response) {
    element = renderFlow(_area.flow)
        ..classes.add("ru-irenproject-flowArea");
  }
}

class YesNoDialog {
  final DivElement _element = new DivElement();
  final Element _curtain;
  final ButtonElement _yesButton = new ButtonElement();
  final ButtonElement _noButton = new ButtonElement();

  YesNoDialog(String message, String yes, String no, String className, void onYesClick())
      : _curtain = attachCurtain() {
    document.body.append(_element
        ..classes.addAll(["ru-irenproject-yesNoDialog", className])
        ..appendText(message)
        ..append(new BRElement())
        ..append(_yesButton
            ..text = yes
            ..onClick.listen((e) {
              _close();
              onYesClick();
            }))
        ..append(_noButton
            ..text = no
            ..onClick.listen((_) => _close()))
        ..onKeyDown.listen(_onKeyDown)
        ..onKeyPress.listen((KeyboardEvent e) => e.stopPropagation())
        ..onKeyUp.listen((KeyboardEvent e) => e.stopPropagation()));

    _yesButton.focus();
  }

  void _close() {
    _element.remove();
    _curtain.remove();
  }

  void _onKeyDown(KeyboardEvent e) {
    e.stopPropagation();

    switch (e.keyCode) {
      case KeyCode.ESC:
        _close();
        break;
      case KeyCode.TAB:
        e.preventDefault();
        ((document.activeElement == _yesButton) ? _noButton : _yesButton).focus();
        break;
    }
  }
}

class _ImageSize {
  final int width;
  final int height;
  _ImageSize(this.width, this.height);
}

abstract class ProgramScreen {
  Element get element;
  void onShow() {}
  void onResize() {}
  void onDismiss() {}
}

abstract class Connection {
  Stream<ServerMessage> get onMessage;
  void send(ClientMessage message);
  void close();
  void expectDisconnect();
}

class Keeper {
  static const String _WORK_ID = "workId";
  static const String _SESSION_KEY = "sessionKey";

  static bool get hasWorkId => window.localStorage.containsKey(_WORK_ID);
  static String get workId => window.localStorage[_WORK_ID];

  static set workId(String value) {
    window.localStorage[_WORK_ID] = value;
  }

  static bool get hasSessionKey => window.localStorage.containsKey(_SESSION_KEY)
      || window.sessionStorage.containsKey(_SESSION_KEY);

  static String get sessionKey => window.localStorage[_SESSION_KEY] ?? window.sessionStorage[_SESSION_KEY];

  static void setPersistentSessionKey(String value) {
    window
        ..localStorage[_SESSION_KEY] = value
        ..sessionStorage.remove(_SESSION_KEY);
  }

  static void setTransientSessionKey(String value) {
    window
        ..sessionStorage[_SESSION_KEY] = value
        ..localStorage.remove(_SESSION_KEY);
  }
}

typedef Decoder _DecoderFactory();

const int _SCALED_DECIMAL_FRACTION_DIGITS = 4; // must correspond to the value on the server
const int _FORMATTED_SCALED_DECIMAL_FRACTION_DIGITS = 2;

const String _KEY_BLOCKER_CLASS = "ru-irenproject-keyBlocker";

bool _globalKeyHandlerInstalled = false;

final Map<String, _DecoderFactory> _imageDecoders = {
    "image/png": () => new PngDecoder(),
    "image/jpeg": () => new JpegDecoder()};

final Map<int, AreaWidgetFactory> _areaWidgetRegistry = {};

/* nullable */ProgramScreen _currentScreen;
/* nullable */StreamSubscription<Event> _windowResizeSubscription;

DivElement renderFlow(Flow flow) {
  DivElement res = new DivElement()
      ..classes.add("ru-irenproject-flow")
      ..style.userSelect = "none";
  ParagraphElement p;

  void createParagraph() {
    p = new ParagraphElement();
    res.append(p);
  }

  createParagraph();

  for (Block block in flow.block) {
    switch (block.type) {
      case BlockType.TEXT:
        p.appendText(block.textBlock.text);
        break;
      case BlockType.IMAGE:
        _ImageSize size = _getImageSize(block.imageBlock.data, block.imageBlock.mimeType);
        DivElement image = new DivElement()
            ..classes.add("ru-irenproject-flow-image")
            ..style.width = "${size.width}px"
            ..style.height = "${size.height}px";
        p.append(image);

        FileReader reader = new FileReader();
        reader
            ..onLoad.listen((_) => image.style.backgroundImage = "url(${reader.result})")
            ..readAsDataUrl(new Blob([block.imageBlock.data], block.imageBlock.mimeType));
        break;
      case BlockType.LINE_FEED:
        createParagraph();
        break;
    }
  }

  return res;
}

_ImageSize _getImageSize(List<int> image, String mimeType) {
  _DecoderFactory f = _imageDecoders[mimeType];
  if (f == null) {
    throw "No image decoder for $mimeType.";
  }
  DecodeInfo info = f().startDecode(image);
  return new _ImageSize(info.width, info.height);
}

/* nullable */AreaResponse getAreaResponseIfExists(String areaId, DialogResponse dialogResponse) =>
    dialogResponse.areaResponse.firstWhere(
        (AreaResponse e) => e.areaId == areaId, orElse: () => null);

num scaledDecimalToPercent(int scaledDecimal) =>
    scaledDecimal / math.pow(10, _SCALED_DECIMAL_FRACTION_DIGITS - 2);

int scaledDecimalToIntegerPercent(int scaledDecimal) =>
    scaledDecimal ~/ math.pow(10, _SCALED_DECIMAL_FRACTION_DIGITS - 2);

String formatScaledDecimal(Int64 scaledDecimal) {
  Int64 n = scaledDecimal ~/ math.pow(10, _SCALED_DECIMAL_FRACTION_DIGITS - _FORMATTED_SCALED_DECIMAL_FRACTION_DIGITS);

  int d = math.pow(10, _FORMATTED_SCALED_DECIMAL_FRACTION_DIGITS);
  Int64 whole = n ~/ d;
  int fraction = (n % d).toInt();

  String fs = formatWithLeadingZeros(fraction, _FORMATTED_SCALED_DECIMAL_FRACTION_DIGITS);
  return "$whole$uiDecimalMark$fs";
}

String formatWithLeadingZeros(int n, int digits) {
  String s = n.toString();
  String padding = new List.filled(math.max(digits - s.length, 0), "0").join();
  return padding + s;
}

String formatCssNumber(num value) => value.toStringAsFixed(6);

bool isAttached(Element e) => document.documentElement.contains(e);

String formatRemainingTime(num millisecondsRemaining, {bool showLastSeconds: false}) {
  String format(int n) => formatWithLeadingZeros(n, 2);

  const int LIMIT = 23*60*60*1000;
  int seconds = math.min(millisecondsRemaining, LIMIT) ~/ 1000;

  String s;
  if (showLastSeconds && (seconds <= 30)) {
    s = "00:00:${format(seconds)}";
  } else {
    int minutes = (seconds + 59) ~/ 60;
    s = "${format(minutes ~/ 60)}:${format(minutes % 60)}";
  }

  return ((millisecondsRemaining > LIMIT) ? "> " : "") + s;
}

void _installGlobalKeyHandler() {
  if (!_globalKeyHandlerInstalled) {
    window.onKeyDown.listen(_globalKeyHandler);
    window.onKeyPress.listen(_globalKeyHandler);
    window.onKeyUp.listen(_globalKeyHandler);

    _globalKeyHandlerInstalled = true;
  }
}

void _globalKeyHandler(KeyboardEvent e) {
  if (keyboardInputBlocked()) {
    e.preventDefault();
  }
}

bool keyboardInputBlocked() => querySelector(".$_KEY_BLOCKER_CLASS") != null;

Element attachCurtain() {
  _installGlobalKeyHandler();

  DivElement res = new DivElement()
      ..classes.addAll(["ru-irenproject-curtain", _KEY_BLOCKER_CLASS]);
  document.body.append(res);
  return res;
}

Element makeKeyboardAccessible(Element e) {
  return e
      ..tabIndex = 0
      ..onKeyDown.listen(_convertToClick);
}

void _convertToClick(KeyboardEvent e) {
  if (!keyboardInputBlocked() && [KeyCode.ENTER, KeyCode.SPACE].contains(e.keyCode)) {
    e
        ..preventDefault()
        ..stopImmediatePropagation()
        ..target.dispatchEvent(new MouseEvent("click"));
  }
}

List<WorkDescriptor> sortWorksByTitle(Iterable<WorkDescriptor> works) =>
    new List.from(works)
        ..sort(_compareWorksByTitle);

int _compareWorksByTitle(WorkDescriptor a, WorkDescriptor b) {
  int res = a.title.compareTo(b.title); //TODO localeCompare
  if (res == 0) {
    res = a.id.compareTo(b.id);
  }
  return res;
}

void registerAreaWidget(Extension extension, AreaWidgetFactory factory) {
  check(!_areaWidgetRegistry.containsKey(extension.tagNumber));
  _areaWidgetRegistry[extension.tagNumber] = factory;
}

void showScreen(ProgramScreen screen) {
  Element screenHost = document.getElementById("screenHost");
  if (screenHost == null) {
    throw "screenHost not found.";
  }
  _windowResizeSubscription ??= window.onResize.listen((_) => _currentScreen?.onResize());

  screenHost.nodes.clear();
  _currentScreen?.onDismiss();

  _currentScreen = screen;
  screenHost.append(_currentScreen.element);
  _currentScreen.onShow();
}

dynamic tr(String key) => translate("iren_client", key);
