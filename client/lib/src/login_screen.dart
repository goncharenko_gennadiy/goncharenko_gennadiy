/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

import 'dart:async';
import 'dart:html';

import 'package:iren_proto/common.pb.dart';
import 'package:web_helpers/web_helpers.dart';

import 'common.dart';

class LoginScreen extends ProgramScreen {
  final Connection _connection;
  @override final DivElement element = new DivElement();
  final TextInputElement _userNameText = new TextInputElement();
  final ButtonElement _logInButton = new ButtonElement();
  final SpanElement _logInFailedLabel = new SpanElement();
  /* nullable */StreamSubscription<ServerMessage> _messageSubscription;

  LoginScreen(ShowLoginScreen message, Connection this._connection) {
    element
        ..append(new FormElement()
            ..appendText("${tr("User Name:")} ")
            ..append(_userNameText
                ..maxLength = message.maxUserNameLength
                ..onInput.listen((_) => _logInFailedLabel.text = ""))
            ..append(_logInButton
                ..text = tr("Log In")
                ..onClick.listen(_onLogInClick))
            ..append(_logInFailedLabel));

    _messageSubscription = _connection.onMessage.listen(_onServerMessage);
  }

  @override void onDismiss() {
    _messageSubscription?.cancel();
    _messageSubscription = null;
  }

  @override void onShow() {
    _userNameText.focus();
  }

  void _onLogInClick(MouseEvent e) {
    e.preventDefault();

    _userNameText.disabled = true;
    _logInButton.disabled = true;
    _logInFailedLabel.text = "";

    _connection.send(new ClientMessage()
        ..type = ClientMessageType.LOG_IN
        ..logIn = (new LogIn()
            ..userName = _userNameText.value));
  }

  void _onServerMessage(ServerMessage m) {
    switch (m.type) {
      case ServerMessageType.LOG_IN_FAILED:
        _handleLogInFailed(m.logInFailed);
        break;
    }
  }

  void _handleLogInFailed(LogInFailed m) {
    _userNameText.disabled = false;
    _logInButton.disabled = false;

    check(m.reason == LogInFailed_Reason.INCORRECT_USER_NAME);
    _logInFailedLabel.text = tr("Incorrect user name.");
  }
}
