/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

import 'dart:async';
import 'dart:html';
import 'dart:math' as math;

import 'package:iren_proto/common.pb.dart';

import 'common.dart';
import 'selector_bar.dart';

class ScoreScreen extends ProgramScreen {
  static const String _PAGE_HEADER = "ru-irenproject-scoreScreen-pageHeader";
  static const String _SELECTED = "ru-irenproject-scoreScreen-pageHeader-selected";

  @override final DivElement element = new DivElement();
  /* nullable */ShowScore _showScore;
  final Connection _connection;
  final DivElement _pageContainer = new DivElement();
  /* nullable */OverallScorePage _overallPage;
  /* nullable */QuestionScorePage _questionPage;

  ScoreScreen(ServerMessage message, Connection this._connection) {
    element.classes.add("ru-irenproject-scoreScreen");

    if (message.hasShowScore()) {
      _showScore = message.showScore;

      DivElement pageSelector = new DivElement()
          ..classes.add("ru-irenproject-scoreScreen-pageSelector");
      element.append(pageSelector);

      pageSelector.append(makeKeyboardAccessible(new DivElement())
          ..classes.addAll([_PAGE_HEADER, _SELECTED])
          ..text = tr("Overall Results")
          ..onClick.listen(_onOverallScoreClick));

      if (_showScore.hasQuestionScore()) {
        pageSelector.append(makeKeyboardAccessible(new DivElement())
            ..classes.add(_PAGE_HEADER)
            ..text = tr("Questions")
            ..onClick.listen(_onQuestionScoreClick));
      }

      element.append(_pageContainer
          ..classes.add("ru-irenproject-scoreScreen-pageContainer"));

      _overallPage = new OverallScorePage(_showScore);
      _pageContainer.children = [_overallPage.element];
    } else {
      element.text = tr("The work is finished.");
    }
  }

  @override void onDismiss() {
    _questionPage?.dismiss();
  }

  @override void onResize() {
    if ((_questionPage != null) && isAttached(_questionPage.element)) {
      _questionPage.layOut();
    }
  }

  void _onOverallScoreClick(MouseEvent e) {
    if (_selectHeader(e.currentTarget)) {
      _pageContainer.children = [_overallPage.element];
    }
  }

  void _onQuestionScoreClick(MouseEvent e) {
    if (_selectHeader(e.currentTarget)) {
      _questionPage ??= new QuestionScorePage(_showScore, _connection);
      _pageContainer.children = [_questionPage.element];
      _questionPage.layOut();
    }
  }

  bool _selectHeader(Element header) {
    bool res = !header.classes.contains(_SELECTED);
    if (res) {
      for (Element e in header.parent.children) {
        e.classes.toggle(_SELECTED, identical(e, header));
      }
    }
    return res;
  }
}

class OverallScorePage {
  final Element element = new DivElement();

  OverallScorePage(ShowScore showScore) {
    element.classes.add("ru-irenproject-testScore-overall");

    TableElement table = new TableElement()
        ..classes.add("ru-irenproject-testScore-table");

    if (showScore.hasScaledResult()) {
      int percent = scaledDecimalToIntegerPercent(showScore.scaledResult);

      element.append(_renderChart(percent));

      table.addRow()
          ..classes.add("ru-irenproject-testScore-table-mainRow")
          ..addCell().text = tr("Result:")
          ..addCell().text = "${percent}%";
    }

    if (showScore.hasMark()) {
      table.addRow()
          ..classes.add("ru-irenproject-testScore-table-mainRow")
          ..addCell().text = tr("Grade:")
          ..addCell().text = showScore.mark;
    }

    table.addRow()
        ..classes.add("ru-irenproject-testScore-table-divider")
        ..addCell().text = tr("Questions offered:")
        ..addCell().text = "${showScore.questionCount}";

    if (showScore.hasScaledScore()) {
      table.addRow()
          ..addCell().text = tr("Points earned:")
          ..addCell().text = tr("POINTS")(formatScaledDecimal(showScore.scaledScore), showScore.totalAchievableScore);
    }

    element.append(table);
  }

  Element _renderChart(int percent) {
    const int SIZE = 200;
    const String BACKGROUND_COLOR = "#c0c0c0";
    const String CORRECT_COLOR = "#00e000";

    CanvasElement res = new CanvasElement(width: SIZE, height: SIZE)
        ..classes.add("ru-irenproject-testScore-chart");
    CanvasRenderingContext2D ctx = res.context2D;

    num xc = SIZE/2 + 0.5;
    num yc = SIZE/2 + 0.5;
    num r = SIZE/2 - 1;

    if ((percent == 0) || (percent == 100)) {
      ctx
          ..beginPath()
          ..arc(xc, yc, r, 0, 2*math.PI)
          ..fillStyle = (percent == 0) ? BACKGROUND_COLOR : CORRECT_COLOR
          ..fill()
          ..stroke();
    } else {
      num angle = -math.PI/2 + 2*math.PI/100*percent;

      ctx
          ..beginPath()
          ..arc(xc, yc, r, 0, 2*math.PI)
          ..fillStyle = BACKGROUND_COLOR
          ..fill()

          ..beginPath()
          ..moveTo(xc, yc)
          ..arc(xc, yc, r, -math.PI/2, angle)
          ..closePath()
          ..fillStyle = CORRECT_COLOR
          ..fill()
          ..stroke()

          ..beginPath()
          ..arc(xc, yc, r, angle, -math.PI/2)
          ..stroke();
    }

    return res;
  }
}

class QuestionScorePage {
  static const String _RESPONSE_HEADER = "ru-irenproject-questionScore-responseHeader";
  static const String _SELECTED = "ru-irenproject-questionScore-responseHeader-selected";

  final Element element = new DivElement();
  final DivElement _questionContainer = new DivElement();
  final DivElement _detailsPanel = new DivElement();
  final ButtonElement _previousButton = new ButtonElement();
  final ButtonElement _nextButton = new ButtonElement();

  final Connection _connection;
  final ShowScore _showScore;
  /* nullable */StreamSubscription<ServerMessage> _messageSubscription;

  int _questionIndex = 0;
  final SelectorBar _selectorBar;
  /* nullable */DialogWidget _dialogWidget;
  bool _showingCorrectResponse = false;

  /* nullable */Dialog _dialog;
  /* nullable */DialogResponse _response;
  /* nullable */DialogResponse _correctResponse;

  QuestionScorePage(ShowScore showScore, Connection this._connection)
      : _showScore = showScore,
        _selectorBar = new SelectorBar(showScore.questionScore.questionDescriptor.length) {
    setUpSelectorBar(_showScore.questionScore.questionDescriptor, _selectorBar);
    _selectorBar
        ..selectionEnabled = false
        ..onSelectItem.listen((SelectItemEvent e) => _sendQueryQuestionDetails(e.itemIndex));

    element
        ..classes.add("ru-irenproject-questionScore")
        ..append(new DivElement()
            ..classes.add("ru-irenproject-questionScore-topPanel")
            ..append(_selectorBar.element));

    if (_showScore.questionScore.correctResponseAvailable) {
      element.append(new DivElement()
          ..classes.add("ru-irenproject-questionScore-responseSelector"))
          ..append(makeKeyboardAccessible(new DivElement())
              ..classes.addAll([_RESPONSE_HEADER, _SELECTED])
              ..text = tr("Your Answer")
              ..onClick.listen((MouseEvent e) => _onHeaderClick(e, false)))
          ..append(makeKeyboardAccessible(new DivElement())
              ..classes.add(_RESPONSE_HEADER)
              ..text = tr("Correct Answer")
              ..onClick.listen((MouseEvent e) => _onHeaderClick(e, true)));
    }

    element
        ..append(_questionContainer
            ..classes.add("ru-irenproject-questionScore-questionContainer"))
        ..append(_detailsPanel
            ..classes.add("ru-irenproject-questionScore-detailsPanel"))
        ..append(new DivElement()
            ..classes.add("ru-irenproject-questionScore-bottomPanel")
            ..append(_previousButton
                ..classes.add("ru-irenproject-questionScore-previousButton")
                ..text = tr("Previous")
                ..disabled = true
                ..onClick.listen((_) => _sendQueryQuestionDetails(_questionIndex - 1)))
            ..append(_nextButton
                ..classes.add("ru-irenproject-questionScore-nextButton")
                ..text = tr("Next")
                ..disabled = true
                ..onClick.listen((_) => _sendQueryQuestionDetails(_questionIndex + 1))));

    _messageSubscription = _connection.onMessage.listen(_onServerMessage);
    _sendQueryQuestionDetails(_questionIndex);
  }

  void dismiss() {
    _messageSubscription?.cancel();
    _messageSubscription = null;
  }

  void _onServerMessage(ServerMessage m) {
    switch (m.type) {
      case ServerMessageType.SHOW_QUESTION_DETAILS:
        _showQuestionDetails(m.showQuestionDetails);
        break;
    }
  }

  void _showQuestionDetails(ShowQuestionDetails m) {
    _dialog = m.dialog;
    _response = m.response;
    _correctResponse = m.hasCorrectResponse() ? m.correctResponse : null;

    _questionIndex = m.questionIndex;
    _selectorBar.setCurrentItem(_questionIndex);

    _selectorBar.selectionEnabled = true;
    _previousButton.disabled = (_questionIndex == 0);
    _nextButton.disabled = (_questionIndex == _showScore.questionCount - 1);

    _updateDetailsPanel(m);
    _showCurrentQuestion();
  }

  void _updateDetailsPanel(ShowQuestionDetails m) {
    _detailsPanel.nodes.clear();

    if (m.hasScaledResult()) {
      _detailsPanel
          ..appendText("${tr("QUESTION_RESULT")} ")
          ..append(new SpanElement()
              ..classes.add("ru-irenproject-questionScore-result")
              ..text = "${scaledDecimalToIntegerPercent(m.scaledResult)}%");
    }

    if (m.hasWeight()) {
      _detailsPanel
          ..appendText("${tr("Question weight:")} ")
          ..append(new SpanElement()
              ..classes.add("ru-irenproject-questionScore-weight")
              ..text = "${m.weight}");
    }

    if (m.hasScaledScore()) {
      _detailsPanel
          ..appendText("${tr("QUESTION_SCORE")} ")
          ..append(new SpanElement()
              ..text = "${formatScaledDecimal(m.scaledScore)}");
    }

    _detailsPanel.hidden = !_detailsPanel.hasChildNodes();
  }

  void _showCurrentQuestion() {
    _dialogWidget = new DialogWidget(_dialog, _showingCorrectResponse ? _correctResponse : _response)
        ..setReadOnly(true);
    _questionContainer.children = [_dialogWidget.element];

    if (isAttached(element)) {
      _dialogWidget.layOut();
    }

    _questionContainer.scrollTop = 0;
  }

  void layOut() {
    _selectorBar.layOut();
    _dialogWidget?.layOut();
  }

  void _sendQueryQuestionDetails(int questionIndex) {
    _connection.send(new ClientMessage()
        ..type = ClientMessageType.QUERY_QUESTION_DETAILS
        ..queryQuestionDetails = (new QueryQuestionDetails()
            ..questionIndex = questionIndex));
  }

  void _onHeaderClick(MouseEvent e, bool showCorrectResponse) {
    if (_selectHeader(e.currentTarget)) {
      _showingCorrectResponse = showCorrectResponse;
      if (_dialog != null) {
        int saved = _questionContainer.scrollTop;
        _showCurrentQuestion();
        _questionContainer.scrollTop = saved;
      }
    }
  }

  bool _selectHeader(Element header) {
    bool res = !header.classes.contains(_SELECTED);
    if (res) {
      for (Element e in header.parent.children) {
        e.classes.toggle(_SELECTED, identical(e, header));
      }
    }
    return res;
  }
}
