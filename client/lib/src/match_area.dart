/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

import 'dart:html';
import 'dart:math' as math;

import 'package:html5_dnd/html5_dnd.dart' as dnd;
import 'package:iren_proto/common.pb.dart';
import 'package:iren_proto/match_area.pb.dart';

import 'common.dart';

class MatchAreaWidget extends AreaWidget {
  static const int _CONTAINER_BORDER_SIZE = 1;
  static const int _CONTAINER_PADDING = 1;
  static const int _CONTAINER_FRAME_SIZE = _CONTAINER_BORDER_SIZE + _CONTAINER_PADDING;
  static const int _CONTAINER_VERTICAL_SPACING = 13;
  static const int _LINK_WIDTH = 20;
  static const int _LINK_BORDER_SIZE = 1;
  static const int _RIGHT_SPACING = 50;
  static const int _DIVIDER_BORDER_SIZE = 1;

  final MatchArea _area;
  final MatchResponse _response;

  @override final DivElement element = new DivElement();
  final List<Element> _left = [];
  final List<Element> _right = [];
  final List<Element> _containers = [];
  final List<Element> _links = [];
  Element _divider;
  final dnd.DraggableGroup _dragGroup = new dnd.DraggableGroup();
  final dnd.DropzoneGroup _dropGroup = new dnd.DropzoneGroup();

  MatchAreaWidget(Area area, AreaResponse response)
      : _area = area.getExtension(MatchArea.matchArea),
        _response = response.getExtension(MatchResponse.matchResponse),
        super(area, response) {
    element.classes.add("ru-irenproject-matchArea");

    for (Flow flow in _area.left) {
      _left.add(_createItem(flow));
      _createContainer();
      _createLink();
    }

    for (Flow flow in _area.right) {
      _right.add(_createItem(flow)
          ..classes.add("ru-irenproject-matchArea-draggableItem"));
    }

    _createDivider();

    _dragGroup.installAll(_right);
    _dropGroup.onDrop.listen(_onDrop);
    _dragGroup.onDragEnd.listen(_onDragEnd);
  }

  Element _createItem(Flow flow) {
    Element res = renderFlow(flow)
        ..classes.add("ru-irenproject-matchArea-item");
    element.append(res);
    return res;
  }

  void _createContainer() {
    Element e = new DivElement()
        ..classes.addAll(["ru-irenproject-matchArea-container", "dnd-rectangle-target"]);
    e.style
        ..borderWidth = "${_CONTAINER_BORDER_SIZE}px"
        ..padding = "${_CONTAINER_PADDING}px";
    element.append(e);
    _dropGroup.install(e);
    _containers.add(e);
  }

  void _createLink() {
    Element e = new DivElement()
        ..classes.add("ru-irenproject-matchArea-link");
    e.style
        ..width = "${_LINK_WIDTH - _LINK_BORDER_SIZE*2}px"
        ..borderWidth = "${_LINK_BORDER_SIZE}px";
    element.append(e);
    _links.add(e);
  }

  void _createDivider() {
    _divider = new DivElement()
        ..classes.add("ru-irenproject-matchArea-divider")
        ..style.borderWidth = "${_DIVIDER_BORDER_SIZE}px";
    element.append(_divider);
  }

  @override void layOut() {
    int areaWidth = element.clientWidth;
    int itemWidth = math.max((areaWidth - _LINK_WIDTH - 2*_CONTAINER_FRAME_SIZE - _RIGHT_SPACING) ~/ 3, 0);
    int containerLeft = itemWidth + _LINK_WIDTH;

    int itemHeight = 0;
    for (Element e in _allItems()) {
      e.style
          ..height = ""
          ..width = "${itemWidth}px";
      itemHeight = math.max(itemHeight, e.offsetHeight);
    }

    for (Element e in _allItems()) {
      e.style.height = "${itemHeight}px";
    }

    int containerHeight = itemHeight + 2*_CONTAINER_FRAME_SIZE;
    int containerStride = containerHeight + _CONTAINER_VERTICAL_SPACING;
    int areaHeight = math.max(_right.length*containerStride - _CONTAINER_VERTICAL_SPACING, 0);
    element.style.height = "${areaHeight}px";

    _left.asMap().forEach((int i, Element item) {
      int containerTop = i * containerStride;
      item.style.top = "${containerTop + _CONTAINER_FRAME_SIZE}px";
      _containers[i].style
          ..top = "${containerTop}px"
          ..left = "${containerLeft}px"
          ..width = "${itemWidth}px"
          ..height = "${itemHeight}px";
      _links[i].style
          ..top = "${containerTop + containerHeight~/2 - _LINK_BORDER_SIZE}px"
          ..left = "${itemWidth}px";
    });

    List</* nullable */int> icm = _itemToContainerMapping();
    _right.asMap().forEach((int i, Element item) {
      int containerIndex = icm[i];
      item.style
          ..left = "${(containerIndex == null) ? areaWidth - itemWidth : containerLeft + _CONTAINER_FRAME_SIZE}px"
          ..top = "${((containerIndex == null) ? i : containerIndex)*containerStride + _CONTAINER_FRAME_SIZE}px";
    });

    _divider.style.right = "${itemWidth + _RIGHT_SPACING~/2 - _DIVIDER_BORDER_SIZE}px";
  }

  List</* nullable */int> _itemToContainerMapping() {
    List</* nullable */int> res = new List(_right.length);
    _response.mapping.asMap().forEach((int containerIndex, int itemIndex) {
      if (itemIndex != -1) {
        res[itemIndex] = containerIndex;
      }
    });
    return res;
  }

  List<Element> _allItems() => []..addAll(_left)..addAll(_right);

  void _onDrop(dnd.DropzoneEvent e) {
    _putToContainer(_right.indexOf(e.draggable), _containers.indexOf(e.dropzone));
  }

  void _onDragEnd(dnd.DraggableEvent e) {
    if (!dnd.dragCanceled && !dnd.droppedOverTarget) {
      _putToContainer(_right.indexOf(e.draggable), null);
    }
  }

  void _putToContainer(int itemIndex, /* nullable */int containerIndex) {
    int oldContainerIndex = _itemToContainerMapping()[itemIndex];
    if (containerIndex != oldContainerIndex) {
      if (oldContainerIndex != null) {
        _response.mapping[oldContainerIndex] = -1;
      }

      if (containerIndex != null) {
        if (oldContainerIndex != null) {
          _response.mapping[oldContainerIndex] = _response.mapping[containerIndex];
        }
        _response.mapping[containerIndex] = itemIndex;
      }

      triggerResponse();
      layOut();
    }
  }

  @override void setReadOnly(bool readOnly) {
    super.setReadOnly(readOnly);
    _dragGroup.cancelDrag();

    _dragGroup.uninstallAll(_right);
    if (!readOnly) {
      _dragGroup.installAll(_right);
    }
  }
}
