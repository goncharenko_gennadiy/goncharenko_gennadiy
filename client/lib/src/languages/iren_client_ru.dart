/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

final Map<String, Map<String, dynamic>> ru = {
  "iren_client": {
    "QUESTION_RESULT": "Оценка ответа:",
    "QUESTION_SCORE": "Набранные баллы:",
    "POINTS": (String earned, int total) => "$earned из $total",
    "ITEM_COUNT": (int itemCount) => "из $itemCount",
    "Iren": "Айрен",
    "User Name:": "Пользователь:",
    "Log In": "Войти",
    "Incorrect user name.": "Недопустимое имя пользователя.",
    "Submit": "Ответить",
    "Answer:": "Ответ:",
    "The connection to the server has been lost.": "Связь с сервером прервана.",
    "Connecting...": "Соединение...",
    "Finish Work": "Завершить работу",
    "Do you want to finish the test?": "Завершить работу?",
    "Finish": "Завершить",
    "Not Now": "Не сейчас",
    "The work is finished.": "Работа завершена.",
    "Overall Results": "Общий итог",
    "Result:": "Итог:",
    "Grade:": "Оценка:",
    "Questions offered:": "Предложено вопросов:",
    "Points earned:": "Набрано баллов:",
    "Questions": "Вопросы",
    "Previous": "Назад",
    "Next": "Вперед",
    "Your Answer": "Ваш ответ",
    "Correct Answer": "Верный ответ",
    "Question": "Вопрос",
    "Question weight:": "Вес вопроса:",
    "Time remaining": "Осталось времени",
    "No tests are available.": "Нет доступных работ.",
  }
};
