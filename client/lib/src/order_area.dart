/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

import 'dart:html';
import 'dart:math' as math;

import 'package:html5_dnd/html5_dnd.dart' as dnd;
import 'package:iren_proto/common.pb.dart';
import 'package:iren_proto/order_area.pb.dart';

import 'common.dart';

class OrderAreaWidget extends AreaWidget {
  static const int _CONTAINER_BORDER_SIZE = 1;
  static const int _CONTAINER_PADDING = 1;
  static const int _CONTAINER_FRAME_SIZE = _CONTAINER_BORDER_SIZE + _CONTAINER_PADDING;
  static const int _CONTAINER_VERTICAL_SPACING = 13;
  static const int _HORIZONTAL_SPACING = 50;
  static const int _DIVIDER_BORDER_SIZE = 1;

  final OrderArea _area;
  final OrderResponse _response;

  @override final DivElement element = new DivElement();
  final List<Element> _offered = [];
  final List<Element> _containers = [];
  final List<Element> _labels = [];
  Element _divider;
  final dnd.DraggableGroup _dragGroup = new dnd.DraggableGroup();
  final dnd.DropzoneGroup _dropGroup = new dnd.DropzoneGroup();

  OrderAreaWidget(Area area, AreaResponse response)
      : _area = area.getExtension(OrderArea.orderArea),
        _response = response.getExtension(OrderResponse.orderResponse),
        super(area, response) {
    element.classes.add("ru-irenproject-orderArea");

    for (int correctCount = _response.mapping.length, i = 0; i < correctCount; ++i) {
      _createContainer();
      _createLabel(i);
    }

    for (Flow flow in _area.offered) {
      _createItem(flow);
    }

    _createDivider();

    _dragGroup.installAll(_offered);
    _dropGroup.onDrop.listen(_onDrop);
    _dragGroup.onDragEnd.listen(_onDragEnd);
  }

  void _createContainer() {
    Element e = new DivElement()
        ..classes.addAll(["ru-irenproject-orderArea-container", "dnd-rectangle-target"]);
    e.style
        ..borderWidth = "${_CONTAINER_BORDER_SIZE}px"
        ..padding = "${_CONTAINER_PADDING}px";
    element.append(e);
    _dropGroup.install(e);
    _containers.add(e);
  }

  void _createLabel(int index) {
    Element e = new DivElement()
        ..classes.add("ru-irenproject-orderArea-label")
        ..text = (index + 1).toString()
        ..style.userSelect = "none";
    element.append(e);
    _labels.add(e);
  }

  void _createItem(Flow flow) {
    Element e = renderFlow(flow)
        ..classes.add("ru-irenproject-orderArea-item");
    element.append(e);
    _offered.add(e);
  }

  void _createDivider() {
    _divider = new DivElement()
        ..classes.add("ru-irenproject-orderArea-divider")
        ..style.borderWidth = "${_DIVIDER_BORDER_SIZE}px";
    element.append(_divider);
  }

  @override void layOut() {
    int areaWidth = element.clientWidth;
    int labelWidth = _computeMaxLabelWidth();
    int itemWidth = math.max((areaWidth - labelWidth - 2*_CONTAINER_FRAME_SIZE - _HORIZONTAL_SPACING) ~/ 2, 0);
    int containerLeft = labelWidth;

    int itemHeight = 0;
    for (Element e in _offered) {
      e.style
          ..height = ""
          ..width = "${itemWidth}px";
      itemHeight = math.max(itemHeight, e.offsetHeight);
    }

    for (Element e in _offered) {
      e.style.height = "${itemHeight}px";
    }

    int containerHeight = itemHeight + 2*_CONTAINER_FRAME_SIZE;
    int containerStride = containerHeight + _CONTAINER_VERTICAL_SPACING;
    int areaHeight = math.max(_offered.length*containerStride - _CONTAINER_VERTICAL_SPACING, 0);
    element.style.height = "${areaHeight}px";

    int correctCount = _response.mapping.length;
    int labelRight = math.max(areaWidth - labelWidth, 0);
    for (int i = 0; i < correctCount; ++i) {
      int containerTop = i * containerStride;
      _containers[i].style
          ..top = "${containerTop}px"
          ..left = "${containerLeft}px"
          ..width = "${itemWidth}px"
          ..height = "${itemHeight}px";
      _labels[i].style
          ..top = "${containerTop}px"
          ..right = "${labelRight}px";
    }

    List</* nullable */int> ocm = _offeredToContainerMapping();
    _offered.asMap().forEach((int i, Element e) {
      int containerIndex = ocm[i];
      e.style
          ..left = "${(containerIndex == null) ? areaWidth - itemWidth : containerLeft + _CONTAINER_FRAME_SIZE}px"
          ..top = "${((containerIndex == null) ? i : containerIndex)*containerStride + _CONTAINER_FRAME_SIZE}px";
    });

    _divider.style.right = "${itemWidth + _HORIZONTAL_SPACING~/2 - _DIVIDER_BORDER_SIZE}px";
  }

  int _computeMaxLabelWidth() => _labels.map((Element e) => e.offsetWidth).fold(0, math.max);

  List</* nullable */int> _offeredToContainerMapping() {
    List</* nullable */int> res = new List(_offered.length);
    _response.mapping.asMap().forEach((int containerIndex, int offeredIndex) {
      if (offeredIndex != -1) {
        res[offeredIndex] = containerIndex;
      }
    });
    return res;
  }

  void _onDrop(dnd.DropzoneEvent e) {
    _putToContainer(_offered.indexOf(e.draggable), _containers.indexOf(e.dropzone));
  }

  void _onDragEnd(dnd.DraggableEvent e) {
    if (!dnd.dragCanceled && !dnd.droppedOverTarget) {
      _putToContainer(_offered.indexOf(e.draggable), null);
    }
  }

  void _putToContainer(int offeredIndex, /* nullable */int containerIndex) {
    int oldContainerIndex = _offeredToContainerMapping()[offeredIndex];
    if (containerIndex != oldContainerIndex) {
      if (oldContainerIndex != null) {
        _response.mapping[oldContainerIndex] = -1;
      }

      if (containerIndex != null) {
        if (oldContainerIndex != null) {
          _response.mapping[oldContainerIndex] = _response.mapping[containerIndex];
        }
        _response.mapping[containerIndex] = offeredIndex;
      }

      triggerResponse();
      layOut();
    }
  }

  @override void setReadOnly(bool readOnly) {
    super.setReadOnly(readOnly);
    _dragGroup.cancelDrag();

    _dragGroup.uninstallAll(_offered);
    if (!readOnly) {
      _dragGroup.installAll(_offered);
    }
  }
}
