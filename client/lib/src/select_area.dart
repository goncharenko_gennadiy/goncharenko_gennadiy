/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

import 'dart:html';

import 'package:iren_proto/common.pb.dart';
import 'package:iren_proto/select_area.pb.dart';

import 'common.dart';

class SelectAreaWidget extends AreaWidget {
  static int _instanceCount = 0;

  final SelectArea _area;
  final SelectResponse _response;

  @override final DivElement element = new DivElement();
  final List<InputElement> _checkboxes = [];
  final List<DivElement> _choices = [];

  SelectAreaWidget(Area area, AreaResponse response)
      : _area = area.getExtension(SelectArea.selectArea),
        _response = response.getExtension(SelectResponse.selectResponse),
        super(area, response) {
    element.classes.add("ru-irenproject-selectArea");

    String name;
    if (_area.multipleSelection) {
      name = null;
    } else {
      ++_instanceCount;
      name = "ru-irenproject-selectArea-$_instanceCount";
    }

    _area.choice.asMap().forEach((int i, Flow choice) {
      DivElement choiceDiv = new DivElement()
          ..classes.add("ru-irenproject-selectArea-choice");

      InputElement checkbox = _area.multipleSelection ?
          new CheckboxInputElement() : new RadioButtonInputElement();
      _checkboxes.add(checkbox);
      if (name != null) {
        checkbox.name = name;
      }
      checkbox
          ..classes.add("ru-irenproject-selectArea-checkbox")
          ..checked = _response.selected[i]
          ..onClick.listen((_) => _updateResponse());
      choiceDiv.append(checkbox);

      DivElement flow = renderFlow(choice)
          ..classes.add("ru-irenproject-selectArea-choice-flow")
          ..onClick.listen((_) => _onChoiceClick(i));
      _choices.add(flow);
      choiceDiv.append(flow);

      element.append(choiceDiv);
    });
  }

  void _onChoiceClick(int choiceIndex) {
    if (!isReadOnly) {
      InputElement cb = _checkboxes[choiceIndex];
      cb.checked = _area.multipleSelection ? !cb.checked : true;
      _updateResponse();
    }
  }

  void _updateResponse() {
    bool changed = false;

    _checkboxes.asMap().forEach((int i, InputElement cb) {
      bool c = cb.checked;
      if (_response.selected[i] != c) {
        changed = true;
        _response.selected[i] = c;
      }
    });

    if (changed) {
      triggerResponse();
    }
  }

  @override void setReadOnly(bool readOnly) {
    super.setReadOnly(readOnly);
    for (InputElement cb in _checkboxes) {
      cb.disabled = readOnly;
    }
  }
}
