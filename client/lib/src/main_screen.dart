/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

import 'dart:async';
import 'dart:html';
import 'dart:math' as math;

import 'package:iren_proto/common.pb.dart';

import 'common.dart';
import 'selector_bar.dart';

class MainScreen extends ProgramScreen {
  static const int _MILLISECONDS_RESERVED = 5000;

  final int _questionCount;
  /* nullable */int _currentQuestion;

  final Connection _connection;
  /* nullable */StreamSubscription<ServerMessage> _messageSubscription;

  @override final DivElement element = new DivElement();
  final DivElement _dialogPanel = new DivElement();

  final ButtonElement _submitButton = new ButtonElement();
  final ButtonElement _previousButton = new ButtonElement();
  final ButtonElement _nextButton = new ButtonElement();
  final ButtonElement _finishWorkButton = new ButtonElement();

  final DivElement _timePanel = new DivElement();
  final DivElement _timeLabel = new DivElement();
  final DivElement _timeProgress = new DivElement();

  final SelectorBar _selectorBar;
  /* nullable */DialogWidget _dialog;

  /* nullable */Timer _setResponseTimer;
  /* nullable */bool _responseChanged;

  /* nullable */bool _waitingForReply;
  /* nullable */bool _setResponseEnqueued;
  /* nullable */bool _submitEnqueued;
  /* nullable */int _questionToChoose;

  bool _finishWorkEnqueued = false;
  bool _finishingWork = false;

  /* nullable */ShowRemainingTime _timing;
  /* nullable */num _timingArrivalMilliseconds;
  /* nullable */Timer _clockTimer;

  MainScreen(MainScreenTurnOn message, Connection this._connection)
      : _questionCount = message.questionDescriptor.length,
        _selectorBar = new SelectorBar(message.questionDescriptor.length) {
    setUpSelectorBar(message.questionDescriptor, _selectorBar);
    _selectorBar.onSelectItem.listen((SelectItemEvent e) => _chooseQuestion(e.itemIndex));

    element
        ..classes.add("ru-irenproject-mainScreen")
        ..append(new DivElement()
            ..classes.add("ru-irenproject-mainScreen-topPanel")
            ..append(_selectorBar.element))
        ..append(_dialogPanel
            ..classes.addAll(["ru-irenproject-mainScreen-dialogPanel", "dnd-scrollable"])
            ..onKeyDown.listen(_onDialogPanelKeyDown))
        ..append(new DivElement()
            ..classes.add("ru-irenproject-mainScreen-bottomPanel")
            ..append(_submitButton
                ..classes.add("ru-irenproject-mainScreen-submitButton")
                ..text = tr("Submit")
                ..disabled = true
                ..onClick.listen((_) => _submit()))
            ..append(_previousButton
                ..classes.add("ru-irenproject-mainScreen-previousButton")
                ..text = tr("Previous")
                ..onClick.listen((_) => _chooseQuestion(_currentQuestion - 1)))
            ..append(_nextButton
                ..classes.add("ru-irenproject-mainScreen-nextButton")
                ..text = tr("Next")
                ..onClick.listen((_) => _chooseQuestion(_currentQuestion + 1)))
            ..append(_timePanel
                ..classes.add("ru-irenproject-mainScreen-timePanel")
                ..title = tr("Time remaining")
                ..style.display = "none"
                ..append(new DivElement()
                    ..classes.add("ru-irenproject-mainScreen-timeIcon"))
                ..append(new DivElement()
                    ..classes.add("ru-irenproject-mainScreen-timeBox")
                    ..append(_timeProgress
                        ..classes.add("ru-irenproject-mainScreen-timeProgress"))
                    ..append(_timeLabel
                        ..classes.add("ru-irenproject-mainScreen-timeLabel"))))
            ..append(_finishWorkButton
                ..classes.add("ru-irenproject-mainScreen-finishWorkButton")
                ..text = tr("Finish Work")
                ..disabled = true
                ..onClick.listen((_) => _onFinishWorkClick())));

    _setNavigationEnabled(false);

    _messageSubscription = _connection.onMessage.listen(_onServerMessage);
  }

  @override void onDismiss() {
    _clearSetResponseTimer();
    _clearClockTimer();
    _messageSubscription?.cancel();
    _messageSubscription = null;
  }

  @override void onShow() {
    _selectorBar.layOut();
  }

  @override void onResize() {
    _dialog?.layOut();
  }

  void _onServerMessage(ServerMessage m) {
    switch (m.type) {
      case ServerMessageType.SHOW_DIALOG:
        _showDialog(m.showDialog);
        break;
      case ServerMessageType.SET_RESPONSE_OK:
        _handleSetResponseOk();
        break;
      case ServerMessageType.SUBMIT_OK:
        _handleSubmitOk(m.submitOk);
        break;
      case ServerMessageType.SHOW_QUESTION_STATUS:
        _showQuestionStatus(m.showQuestionStatus);
        break;
      case ServerMessageType.SHOW_REMAINING_TIME:
        _showRemainingTime(m.showRemainingTime);
        break;
    }
  }

  void _showDialog(ShowDialog m) {
    _clearSetResponseTimer();
    _responseChanged = false;

    _waitingForReply = false;
    _setResponseEnqueued = false;
    _submitEnqueued = false;
    _questionToChoose = null;

    if (_finishWorkEnqueued) {
      _processQueue();
    } else {
      _dialog = new DialogWidget(m.dialog, m.response);
      _dialog
          ..setReadOnly(m.readOnly)
          ..onResponse.listen(_onDialogResponse);

      _dialogPanel.children = [_dialog.element];
      _dialog.layOut();
      _dialogPanel.scrollTop = 0;

      _currentQuestion = m.questionIndex;
      _selectorBar.setCurrentItem(_currentQuestion);

      if (!m.readOnly && ![_previousButton, _nextButton].contains(document.activeElement)) {
        _dialog.setFocus();
      }

      _submitButton.disabled = !m.canSubmit;
      _setNavigationEnabled(!m.canSubmit);

      _finishWorkButton.disabled = false;
    }
  }

  void _handleSetResponseOk() {
    _waitingForReply = false;
    _processQueue();
  }

  void _handleSubmitOk(SubmitOk m) {
    _waitingForReply = false;

    if (!_finishingWork) {
      _dialog.setReadOnly(m.makeReadOnly);
      _setNavigationEnabled(true);
    }

    _processQueue();
  }

  void _showQuestionStatus(ShowQuestionStatus m) {
    _selectorBar.setItemColor(m.questionIndex, QUESTION_COLORS[m.status]);
  }

  void _clearSetResponseTimer() {
    _setResponseTimer?.cancel();
    _setResponseTimer = null;
  }

  void _processQueue() {
    _processSetResponse();
    _processFinishWork();
    _processSubmit();
    _processChooseQuestion();
  }

  void _processSetResponse() {
    if (!_waitingForReply && _setResponseEnqueued) {
      _connection.send(new ClientMessage()
          ..type = ClientMessageType.SET_RESPONSE
          ..setResponse = (new SetResponse()
              ..response = _dialog.response));
      _waitingForReply = true;

      _responseChanged = false;
      _setResponseEnqueued = false;
    }
  }

  void _processFinishWork() {
    if (!_waitingForReply && _finishWorkEnqueued) {
      _connection.send(new ClientMessage()
          ..type = ClientMessageType.FINISH_WORK);
      _waitingForReply = true;

      _finishWorkEnqueued = false;
    }
  }

  void _processSubmit() {
    if (!_waitingForReply && _submitEnqueued) {
      _connection.send(new ClientMessage()
          ..type = ClientMessageType.SUBMIT);
      _waitingForReply = true;

      _submitEnqueued = false;
    }
  }

  void _processChooseQuestion() {
    if (!_waitingForReply && (_questionToChoose != null)) {
      _connection.send(new ClientMessage()
          ..type = ClientMessageType.CHOOSE_QUESTION
          ..chooseQuestion = (new ChooseQuestion()
              ..questionIndex = _questionToChoose));
      _waitingForReply = true;

      _questionToChoose = null;
    }
  }

  void _onDialogResponse(DialogEvent e) {
    if (identical(e.source, _dialog)) {
      _responseChanged = true;
      _submitButton.disabled = false;
      if ((document.activeElement == null) || !_dialogPanel.contains(document.activeElement)) {
        _submitButton.focus();
      }
      _setNavigationEnabled(false);

      _clearSetResponseTimer();
      _setResponseTimer = new Timer(const Duration(seconds: 1), () {
        _clearSetResponseTimer();
        _setResponseEnqueued = true;
        _processQueue();
      });
    }
  }

  void _submit() {
    _submitButton.disabled = true;
    _dialog.setReadOnly(true);

    _enqueueSetResponseIfChanged();

    _submitEnqueued = true;
    _processQueue();
  }

  void _enqueueSetResponseIfChanged() {
    _clearSetResponseTimer();
    if (_responseChanged) {
      _setResponseEnqueued = true;
    }
  }

  void _onFinishWorkClick() {
    new YesNoDialog(tr("Do you want to finish the test?"), tr("Finish"), tr("Not Now"),
        "ru-irenproject-finishWorkDialog", _finishWork);
  }

  void _finishWork() {
    if (!_finishingWork) {
      _finishingWork = true;

      _finishWorkButton.disabled = true;
      _submitButton.disabled = true;
      _dialog.setReadOnly(true);
      _setNavigationEnabled(false);

      _enqueueSetResponseIfChanged();

      _finishWorkEnqueued = true;
      _processQueue();
    }
  }

  void _chooseQuestion(int questionIndex) {
    _dialog.setReadOnly(true);

    _questionToChoose = questionIndex;
    _processQueue();
  }

  void _setNavigationEnabled(bool enabled) {
    _selectorBar.selectionEnabled = enabled;

    _previousButton.disabled = !(enabled && (_currentQuestion > 0));
    _nextButton.disabled = !(enabled && (_currentQuestion < _questionCount - 1));
  }

  void _showRemainingTime(ShowRemainingTime m) {
    _timing = m;
    _timingArrivalMilliseconds = window.performance.now();

    _timePanel.style.display = "";
    _updateTimePanel(_millisecondsRemaining(), _millisecondsTotalIfLimited());

    _clearClockTimer();
    _clockTimer = new Timer.periodic(const Duration(seconds: 1), (_) {
      num remaining = _millisecondsRemaining();
      _updateTimePanel(remaining, _millisecondsTotalIfLimited());
      if (remaining == 0) {
        _finishWork();
      }
    });
  }

  num _millisecondsRemaining() => math.max(_timing.millisecondsRemaining
      - (window.performance.now() - _timingArrivalMilliseconds) - _MILLISECONDS_RESERVED, 0);

  /* nullable */num _millisecondsTotalIfLimited() => _timing.hasMillisecondsTotal() ?
      math.max(_timing.millisecondsTotal - _MILLISECONDS_RESERVED, 0) : null;

  void _updateTimePanel(num millisecondsRemaining, /* nullable */num millisecondsTotal) {
    _timeLabel.text = formatRemainingTime(millisecondsRemaining, showLastSeconds: true);

    num partRemaining;
    if (millisecondsTotal == null) {
      partRemaining = 0;
    } else {
      partRemaining = (millisecondsRemaining < millisecondsTotal) ? millisecondsRemaining / millisecondsTotal : 1;
    }
    _timeProgress.style.right = "${formatCssNumber((1 - partRemaining) * 100)}%";
  }

  void _clearClockTimer() {
    _clockTimer?.cancel();
    _clockTimer = null;
  }

  void _onDialogPanelKeyDown(KeyboardEvent e) {
    if (!keyboardInputBlocked() && (e.keyCode == KeyCode.ENTER) && !_submitButton.disabled) {
      e
          ..preventDefault()
          ..stopPropagation();
      _submit();
    }
  }
}
