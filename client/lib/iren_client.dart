/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

import 'dart:async';
import 'dart:html';

import 'package:iren_proto/classify_area.pb.dart';
import 'package:iren_proto/common.pb.dart';
import 'package:iren_proto/input_area.pb.dart';
import 'package:iren_proto/match_area.pb.dart';
import 'package:iren_proto/order_area.pb.dart';
import 'package:iren_proto/select_area.pb.dart';
import 'package:protobuf/protobuf.dart';

import 'src/classify_area.dart';
import 'src/common.dart';
import 'src/input_area.dart';
import 'src/match_area.dart';
import 'src/order_area.dart';
import 'src/select_area.dart';

export 'src/common.dart' show Connection, DialogWidget, ProgramScreen, formatCssNumber, formatRemainingTime,
    formatScaledDecimal, isAttached, makeKeyboardAccessible, scaledDecimalToIntegerPercent, scaledDecimalToPercent,
    showScreen, sortWorksByTitle;
export 'src/languages.dart' show registerClientDictionaries;
export 'src/selector_bar.dart';

class WebSocketConnection extends Connection {
  final WebSocket socket;
  bool disconnectExpected = false;

  final StreamController<ServerMessage> messageController = new StreamController.broadcast(sync: true);
  @override Stream<ServerMessage> get onMessage => messageController.stream;

  WebSocketConnection(this.socket);

  @override void send(ClientMessage message) {
    socket.sendTypedData(message.writeToBuffer());
  }

  @override void expectDisconnect() {
    disconnectExpected = true;
  }

  @override void close() {
    expectDisconnect();
    socket.close();
  }
}

final ExtensionRegistry protoExtensionRegistry = new ExtensionRegistry();

void registerExtensions() {
  [
      FlowArea.flowArea,
      SelectArea.selectArea, SelectResponse.selectResponse,
      InputArea.inputArea, InputResponse.inputResponse,
      MatchArea.matchArea, MatchResponse.matchResponse,
      OrderArea.orderArea, OrderResponse.orderResponse,
      ClassifyArea.classifyArea, ClassifyResponse.classifyResponse
  ].forEach(protoExtensionRegistry.add);
}

void registerAreaWidgets() {
  registerAreaWidget(FlowArea.flowArea, (a, r) => new FlowAreaWidget(a, r));
  registerAreaWidget(SelectArea.selectArea, (a, r) => new SelectAreaWidget(a, r));
  registerAreaWidget(InputArea.inputArea, (a, r) => new InputAreaWidget(a, r));
  registerAreaWidget(MatchArea.matchArea, (a, r) => new MatchAreaWidget(a, r));
  registerAreaWidget(OrderArea.orderArea, (a, r) => new OrderAreaWidget(a, r));
  registerAreaWidget(ClassifyArea.classifyArea, (a, r) => new ClassifyAreaWidget(a, r));
}
