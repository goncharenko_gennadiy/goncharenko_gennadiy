/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

import 'dart:async';
import 'dart:html';
import 'dart:typed_data';

import 'package:async/async.dart';
import 'package:iren_client/iren_client.dart';
import 'package:iren_client/src/common.dart';
import 'package:iren_client/src/login_screen.dart';
import 'package:iren_client/src/main_screen.dart';
import 'package:iren_client/src/score_screen.dart';
import 'package:iren_client/src/select_work_screen.dart';
import 'package:iren_proto/common.pb.dart';
import 'package:web_helpers/translator.dart';

class _LogScreen extends ProgramScreen {
  @override final DivElement element = new DivElement();

  void log(String message) {
    element.append(new ParagraphElement()
        ..text = message);
  }
}

Map<String, /* nullable */String> _programArguments;

Future<Null> main() async {
  await window.onLoad.first;

  registerClientDictionaries();
  selectDictionaryForCurrentLanguage();

  _parseProgramArguments();
  registerExtensions();
  registerAreaWidgets();

  document.title = tr("Iren");
  return _connect();
}

void _parseProgramArguments() {
  _programArguments = {};
  if (window.location.hash.startsWith("#/")) {
    for (String s in window.location.hash.substring(2).split(",")) {
      int n = s.indexOf("=");
      if (n == -1) {
        _programArguments[s] = null;
      } else {
        _programArguments[s.substring(0, n)] = s.substring(n + 1);
      }
    }
  }
}

Future<Null> _connect({bool connectionLost: false}) async {
  _LogScreen screen = new _LogScreen();
  showScreen(screen);

  WebSocket socket = await _openSocket(screen.log, connectionLost: connectionLost);
  WebSocketConnection connection = new WebSocketConnection(socket);
  socket
      ..onClose.listen((_) => _onSocketClose(connection))
      ..onMessage.listen((MessageEvent e) => _onSocketMessage(e, connection));

  if (Keeper.hasWorkId && Keeper.hasSessionKey) {
    connection.send(new ClientMessage()
        ..type = ClientMessageType.SELECT_WORK
        ..selectWork = (new SelectWork()
            ..id = Keeper.workId
            ..sessionKey = Keeper.sessionKey));
  } else {
    connection.send(new ClientMessage()
        ..type = ClientMessageType.LIST_WORKS);
  }
}

Future<WebSocket> _openSocket(void log(String message), {bool connectionLost: false}) async {
  const Duration RECONNECT_DELAY = const Duration(seconds: 3);
  const Duration CONNECTING_MESSAGE_DELAY = const Duration(seconds: 3);

  if (connectionLost) {
    log(tr("The connection to the server has been lost."));
    await new Future.delayed(RECONNECT_DELAY);
  }

  Timer showConnectingMessage = new Timer(connectionLost ? Duration.ZERO : CONNECTING_MESSAGE_DELAY,
      () => log(tr("Connecting...")));

  WebSocket res;
  do {
    res = new WebSocket(_webSocketUri())
        ..binaryType = "arraybuffer";
    await StreamGroup.merge([res.onOpen, res.onClose]).first;
    if (res.readyState != WebSocket.OPEN) {
      res = null;
      await new Future.delayed(RECONNECT_DELAY);
    }
  } while (res == null);

  showConnectingMessage.cancel();
  return res;
}

String _webSocketUri() {
  String protocol = (window.location.protocol == "https:") ? "wss" : "ws";
  String customPort = _programArguments["port"];
  String host = (customPort == null) ? window.location.host : "${window.location.hostname}:$customPort";
  return "$protocol://$host${window.location.pathname}websocket/";
}

void _onSocketClose(WebSocketConnection connection) {
  _connect(connectionLost: !connection.disconnectExpected);
}

void _onSocketMessage(MessageEvent e, WebSocketConnection connection) {
  ByteBuffer b = e.data;
  ServerMessage m = new ServerMessage.fromBuffer(b.asUint8List(), protoExtensionRegistry);

  switch (m.type) {
    case ServerMessageType.SHOW_SELECT_WORK_SCREEN:
      showScreen(new SelectWorkScreen(connection));
      break;
    case ServerMessageType.SHOW_LOGIN_SCREEN:
      showScreen(new LoginScreen(m.showLoginScreen, connection));
      break;
    case ServerMessageType.MAIN_SCREEN_TURN_ON:
      showScreen(new MainScreen(m.mainScreenTurnOn, connection));
      break;
    case ServerMessageType.SESSION_KEY:
      Keeper.setPersistentSessionKey(m.sessionKey.sessionKey);
      break;
    case ServerMessageType.SHOW_SCORE:
      if (Keeper.hasSessionKey) {
        Keeper.setTransientSessionKey(Keeper.sessionKey);
      }
      showScreen(new ScoreScreen(m, connection));
      break;
    default:
      connection.messageController.add(m);
  }
}
