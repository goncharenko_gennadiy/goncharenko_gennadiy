{----------------------------------------------------------------------------}
{ RemObjects Pascal Script                                                   }
{                                                                            }
{ compiler: Delphi 2 and up, Kylix 3 and up                                  }
{ platform: Win32, Linux                                                     }
{                                                                            }
{ (c)opyright RemObjects Software. all rights reserved.                      }
{ Modified by Sergey Ostanin.                                                }
{                                                                            }
{----------------------------------------------------------------------------}

{$INCLUDE eDefines.inc}

{$IFDEF FPC}
{$MODE DELPHI}{$H+}
{$DEFINE PS_HAVEVARIANT}
{$DEFINE DELPHI6UP}
{$ENDIF}

{$R-}{$Q-}

{$UNDEF DEBUG}
