{
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
}

unit SecureRandom;

interface

uses
  Classes, SysUtils;

procedure GenerateSecureRandom(out Buffer; BufferSize: Integer);
function SecureRandomInteger(Range: Integer): Integer;
function SecureRandomFloat: Extended;

implementation

{$IFDEF WINDOWS}
function RtlGenRandom(out RandomBuffer; RandomBufferLength: Cardinal): LongBool; stdcall;
  external 'advapi32.dll' name 'SystemFunction036';
{$ELSE}
const
  RANDOM_DEVICE = '/dev/urandom';
var
  RandomDevice: THandle = -1;
{$ENDIF}

procedure GenerateSecureRandom(out Buffer; BufferSize: Integer);
{$IFNDEF WINDOWS}
var
  p: PByte;
  Remaining, n: Integer;
{$ENDIF}
begin
  Assert( BufferSize >= 0 );
  if BufferSize > 0 then
  begin
    {$IFDEF WINDOWS}
    if not RtlGenRandom(Buffer, BufferSize) then
      raise Exception.Create('RtlGenRandom failed.');
    {$ELSE}
    Assert( RandomDevice <> -1 );
    p := @Buffer;
    Remaining := BufferSize;

    while Remaining > 0 do
    begin
      n := FileRead(RandomDevice, p^, Remaining);
      if n <= 0 then
        raise Exception.CreateFmt('Can''t read "%s".', [RANDOM_DEVICE]);
      Inc(p, n);
      Dec(Remaining, n);
    end;
    {$ENDIF}
  end;
end;

function SecureRandomInteger(Range: Integer): Integer;
var
  n: Integer;
begin
  if Range >= 2 then
  begin
    repeat
      GenerateSecureRandom(n, SizeOf(n));
      n := n and MaxInt;
      Result := n mod Range;
    until n - Result <= MaxInt - Range + 1;
  end
  else
    Result := 0;
end;

function SecureRandomFloat: Extended;
var
  n: Cardinal;
begin
  GenerateSecureRandom(n, SizeOf(n));
  Result := n / $100000000;
end;

{$IFNDEF WINDOWS}
initialization

  RandomDevice := FileOpen(RANDOM_DEVICE, fmOpenRead or fmShareDenyNone);
  if RandomDevice = -1 then
    raise Exception.CreateFmt('Can''t open "%s".', [RANDOM_DEVICE]);

finalization

  if RandomDevice <> -1 then
  begin
    FileClose(RandomDevice);
    RandomDevice := -1;
  end;

{$ENDIF}
end.
