{
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
}

unit FormatUtils;

interface

uses
  Classes, SysUtils, Math;

function GetUiDecimalSeparator: Char;
function GetEmptyFormatSettings: TFormatSettings;
function ConvertFloatToString(const Value: Extended; DigitsAfterDecimalPoint: Integer;
  DecimalSeparator: Char; OutputTrailingZeros: Boolean = FALSE): String;

implementation

resourcestring
  SUiDecimalSeparator = '.';

var
  EmptyFormatSettings: TFormatSettings;

function GetUiDecimalSeparator: Char;
var
  s: String;
begin
  s := SUiDecimalSeparator;
  Assert( Length(s) = 1 );
  Result := s[1];
end;

function GetEmptyFormatSettings: TFormatSettings;
begin
  Result := EmptyFormatSettings;
end;

function ConvertFloatToString(const Value: Extended; DigitsAfterDecimalPoint: Integer;
  DecimalSeparator: Char; OutputTrailingZeros: Boolean = FALSE): String;
var
  FormatSettings: TFormatSettings;
  c: Char;
  FractionFormat: String;
begin
  FormatSettings := GetEmptyFormatSettings;
  FormatSettings.DecimalSeparator := DecimalSeparator;
  if OutputTrailingZeros then
    c := '0'
  else
    c := '#';
  FractionFormat := StringOfChar(c, Max(Min(DigitsAfterDecimalPoint, 10), 0));
  if FractionFormat <> '' then
    FractionFormat := '.' + FractionFormat;

  Result := FormatFloat('0' + FractionFormat, Value, FormatSettings);

  if Result = '-0' then
    Result := '0';
end;

end.
