{
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
}

program irenScriptRunner;

uses
  {$IFNDEF WINDOWS}FileDescriptorCloser,{$ENDIF}
  Classes, SysUtils, Script, iostream, StreamUtils{$IFNDEF WINDOWS}, BaseUnix, syscall{$ENDIF};

type
  TRetryingOutputStream = class(TIOStream)
  public
    function Write(const Buffer; Count: Integer): Integer; override;
  end;

function TRetryingOutputStream.Write(const Buffer; Count: Integer): Integer;
var
  p: PByte;
  Remaining, n: Integer;
begin
  p := @Buffer;
  Remaining := Count;

  while Remaining > 0 do
  begin
    n := inherited Write(p^, Remaining);
    if n = 0 then
      raise Exception.Create('Can''t write to stream.');
    Inc(p, n);
    Dec(Remaining, n);
  end;

  Result := Count;
end;

procedure WriteVariableList(List: TVariableList; Output: TStream);
var
  v: TVariable;
begin
  WriteStreamInteger(Output, List.Count);
  for v in List do
  begin
    WriteStreamSizedString(Output, v.Name);
    WriteStreamSizedString(Output, v.Text);
  end;
end;

procedure HandleException(e: Exception);
var
  StderrStream: TStream;
begin
  ExitCode := 1;
  FileClose(StdInputHandle);

  StderrStream := TRetryingOutputStream.Create(iosError);
  try
    WriteStreamString(StderrStream, Format('%s at %p: %s', [e.ClassName, ExceptAddr, e.Message]));
  finally
    StderrStream.Free;
  end;
end;

{$IFNDEF WINDOWS}
procedure SetResourceLimits;
const
  CPU_LIMIT_SECONDS = 5;
  CPU_LIMIT: TRLimit = (
    rlim_cur: CPU_LIMIT_SECONDS;
    rlim_max: CPU_LIMIT_SECONDS + 1
  );

  MEMORY_LIMIT_BYTES = 10 * 1024 * 1024;
  MEMORY_LIMIT: TRLimit = (
    rlim_cur: MEMORY_LIMIT_BYTES;
    rlim_max: MEMORY_LIMIT_BYTES
  );
begin
  if FpSetRLimit(RLIMIT_CPU, @CPU_LIMIT) <> 0 then
    raise Exception.Create('Can''t set RLIMIT_CPU.');

  if FpSetRLimit(RLIMIT_AS, @MEMORY_LIMIT) <> 0 then
    raise Exception.Create('Can''t set RLIMIT_AS.');
end;

procedure EnableSeccomp;
const
  PR_SET_NO_NEW_PRIVS = 38;
  PR_SET_SECCOMP = 22;
  SECCOMP_MODE_FILTER = 2;
  SECCOMP_RET_KILL = 0;
  SECCOMP_RET_ALLOW = $7fff0000;
  AUDIT_ARCH_I386 = $40000003;
  BPF_LD = $00;
  BPF_JMP = $05;
  BPF_RET = $06;
  BPF_K = $00;
  BPF_W = $00;
  BPF_ABS = $20;
  BPF_JEQ = $10;
type
  TSockFilter = packed record
    code: Word;
    jt: Byte;
    jf: Byte;
    k: Cardinal;
  end;
  PSockFilter = ^TSockFilter;

  TSockFprog = packed record
    len: Word;
    padding: Word;
    filter: PSockFilter;
  end;
const
  FILTER: array [0..20] of TSockFilter = (
    (code: BPF_LD + BPF_W + BPF_ABS; jt: 0; jf: 0; k: 4{ seccomp_data.arch }),
    (code: BPF_JMP + BPF_JEQ + BPF_K; jt: 1; jf: 0; k: AUDIT_ARCH_I386),
    (code: BPF_RET + BPF_K; jt: 0; jf: 0; k: SECCOMP_RET_KILL),

    (code: BPF_LD + BPF_W + BPF_ABS; jt: 0; jf: 0; k: 0{ seccomp_data.nr }),

    (code: BPF_JMP + BPF_JEQ + BPF_K; jt: 0; jf: 1; k: syscall_nr_mmap),
    (code: BPF_RET + BPF_K; jt: 0; jf: 0; k: SECCOMP_RET_ALLOW),

    (code: BPF_JMP + BPF_JEQ + BPF_K; jt: 0; jf: 1; k: syscall_nr_munmap),
    (code: BPF_RET + BPF_K; jt: 0; jf: 0; k: SECCOMP_RET_ALLOW),

    (code: BPF_JMP + BPF_JEQ + BPF_K; jt: 0; jf: 1; k: syscall_nr_read),
    (code: BPF_RET + BPF_K; jt: 0; jf: 0; k: SECCOMP_RET_ALLOW),

    (code: BPF_JMP + BPF_JEQ + BPF_K; jt: 0; jf: 1; k: syscall_nr_write),
    (code: BPF_RET + BPF_K; jt: 0; jf: 0; k: SECCOMP_RET_ALLOW),

    (code: BPF_JMP + BPF_JEQ + BPF_K; jt: 0; jf: 1; k: syscall_nr_close),
    (code: BPF_RET + BPF_K; jt: 0; jf: 0; k: SECCOMP_RET_ALLOW),

    (code: BPF_JMP + BPF_JEQ + BPF_K; jt: 0; jf: 1; k: syscall_nr_rt_sigreturn),
    (code: BPF_RET + BPF_K; jt: 0; jf: 0; k: SECCOMP_RET_ALLOW),

    (code: BPF_JMP + BPF_JEQ + BPF_K; jt: 0; jf: 1; k: syscall_nr_rt_sigprocmask),
    (code: BPF_RET + BPF_K; jt: 0; jf: 0; k: SECCOMP_RET_ALLOW),

    (code: BPF_JMP + BPF_JEQ + BPF_K; jt: 0; jf: 1; k: syscall_nr_exit_group),
    (code: BPF_RET + BPF_K; jt: 0; jf: 0; k: SECCOMP_RET_ALLOW),

    (code: BPF_RET + BPF_K; jt: 0; jf: 0; k: SECCOMP_RET_KILL)
  );

  FILTER_PROGRAM: TSockFprog = (
    len: Length(FILTER);
    padding: 0;
    filter: @FILTER;
  );
var
  p: TSysParam;
begin
  if Do_SysCall(syscall_nr_prctl, PR_SET_NO_NEW_PRIVS, 1, 0, 0, 0) <> 0 then
    raise Exception.Create('PR_SET_NO_NEW_PRIVS failed.');

  {$HINTS OFF}
  p := PtrInt(@FILTER_PROGRAM);
  {$HINTS ON}
  if Do_SysCall(syscall_nr_prctl, PR_SET_SECCOMP, SECCOMP_MODE_FILTER, p) <> 0 then
    raise Exception.Create('PR_SET_SECCOMP failed.');
end;
{$ENDIF}

procedure Run;
var
  Script: TStringList;
  StdinStream, StdoutStream: TStream;
  MemoryStream: TMemoryStream;
  Compiled: TCompiledPascalScript;
  vl: TVariableList;
  s: String;
begin
  try
    {$IFNDEF WINDOWS}
    SetResourceLimits;
    EnableSeccomp;
    {$ENDIF}

    FormatSettings.DecimalSeparator := '.';
    FormatSettings.ThousandSeparator := ' ';

    s := GetEnvironmentVariable('RU_IRENPROJECT_SCRIPT_DECIMAL_SEPARATOR');
    if Length(s) <> 1 then
      raise Exception.Create('Script decimal separator is not specified.');
    ScriptDecimalSeparator := s[1];

    Script := TStringList.Create;
    try
      StdinStream := TIOStream.Create(iosInput);
      try
        MemoryStream := TMemoryStream.Create;
        try
          CopyStreamTail(StdinStream, MemoryStream);
          MemoryStream.Seek(0, soFromBeginning);
          Script.LoadFromStream(MemoryStream);
        finally
          MemoryStream.Free;
        end;
      finally
        StdinStream.Free;
      end;

      Compiled := CompilePascalScript(Script);
      try
        vl := TVariableList.Create;
        try
          ExecutePascalScript(Compiled, vl);

          StdoutStream := TRetryingOutputStream.Create(iosOutput);
          try
            WriteVariableList(vl, StdoutStream);
          finally
            StdoutStream.Free;
          end;
        finally
          vl.Free;
        end;
      finally
        Compiled.Free;
      end;
    finally
      Script.Free;
    end;
  except on E: Exception do
    HandleException(E);
  end;
end;

begin
  Run;
end.
