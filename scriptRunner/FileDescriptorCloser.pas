{
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
}

unit FileDescriptorCloser;

interface

uses
  Classes, SysUtils, BaseUnix;

implementation

procedure CloseFileDescriptorsGreaterThan(Handle: THandle);
var
  Dir: pDir;
  Entry: pDirent;
  s: String;
  h: THandle;
begin
  Dir := FpOpendir('/proc/self/fd');
  if Dir = nil then
    raise Exception.Create('Can''t open "/proc/self/fd".');
  try
    repeat
      Entry := FpReaddir(Dir^);
      if Entry <> nil then
      begin
        s := Entry.d_name;
        if (s <> '.') and (s <> '..') then
        begin
          h := StrToInt(s);
          if (h <> Dir.dd_fd) and (h > Handle) then
            FpClose(h);
        end;
      end;
    until Entry = nil;
  finally
    FpClosedir(Dir^);
  end;
end;

initialization

  CloseFileDescriptorsGreaterThan(StdErrorHandle);

end.
