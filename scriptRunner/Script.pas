{
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
}

unit Script;

interface

uses
  {$IFDEF WINDOWS}Windows, TimeUtils, {$ENDIF}Classes, SysUtils, fgl, uPSCompiler, uPSDebugger, uPSRuntime, uPSUtils,
  Math, SecureRandom, FormatUtils;

type
  TCompiledPascalScript = class
  private
    FByteCode: String;
    FDebugInfo: String;
  end;

  TVariableType = (vrtString, vrtInt64, vrtDouble);

  TVariable = class
  private
    FName: String;
    FVariableType: TVariableType;
    FStringValue: String;
    FInt64Value: Int64;
    FDoubleValue: Double;
    procedure Clear;
    function GetStringValue: String;
    procedure SetStringValue(const Value: String);
    procedure CheckType(RequiredType: TVariableType);
    function GetInt64Value: Int64;
    procedure SetInt64Value(const Value: Int64);
    function GetDoubleValue: Double;
    procedure SetDoubleValue(const Value: Double);
    function GetText: String;
  public
    constructor Create;

    property DoubleValue: Double read GetDoubleValue write SetDoubleValue;
    property Int64Value: Int64 read GetInt64Value write SetInt64Value;
    property Name: String read FName write FName;
    property StringValue: String read GetStringValue write SetStringValue;
    property Text: String read GetText;
    property VariableType: TVariableType read FVariableType;
  end;

  TVariableList = class(TFPGObjectList<TVariable>)
  public
    function AddSafely(Item: TVariable): TVariable;
  end;

var
  ScriptDecimalSeparator: Char;

function CompilePascalScript(Script: TStrings): TCompiledPascalScript;
procedure ExecutePascalScript(CompiledScript: TCompiledPascalScript; VariableList: TVariableList);

implementation

resourcestring
  SCompileError = 'Script compilation error: %s';
  SExecutionError = 'Script execution error: %s';
  {$IFDEF WINDOWS}
  SScriptTimedOut = 'Script timed out.';
  {$ENDIF}

{$IFDEF WINDOWS}
const
  SCRIPT_TIMEOUT = 5000;
{$ENDIF}

function RandomInt(Range: Integer): Integer;
begin
  Result := SecureRandomInteger(Range);
end;

function RandomIntWrapper(Caller: TPSExec; p: TPSExternalProcRec; Global, Stack: TPSStack): Boolean;
begin
  Stack.SetInt(-1, RandomInt(Stack.GetInt(-2)));
  Result := TRUE;
end;

function RandomFloat: Extended;
begin
  Result := SecureRandomFloat;
end;

function RandomFloatWrapper(Caller: TPSExec; p: TPSExternalProcRec; Global, Stack: TPSStack): Boolean;
begin
  Stack.SetReal(-1, RandomFloat);
  Result := TRUE;
end;

procedure Error(const Msg: UnicodeString);
begin
  raise Exception.Create(UTF8Encode(Msg));
end;

function ErrorWrapper(Caller: TPSExec; p: TPSExternalProcRec; Global, Stack: TPSStack): Boolean;
begin
  Error(Stack.GetUnicodeString(-1));
  Result := TRUE;
end;

function CalcPower(const Base, Exponent: Extended): Extended;
begin
  Result := power(Base, Exponent);
end;

function CalcPowerWrapper(Caller: TPSExec; p: TPSExternalProcRec; Global, Stack: TPSStack): Boolean;
begin
  Stack.SetReal(-1, CalcPower(Stack.GetReal(-2), Stack.GetReal(-3)));
  Result := TRUE;
end;

function CalcExp(x: Extended): Extended;
begin
  Result := Exp(x);
end;

function CalcExpWrapper(Caller: TPSExec; p: TPSExternalProcRec; Global, Stack: TPSStack): Boolean;
begin
  Stack.SetReal(-1, CalcExp(Stack.GetReal(-2)));
  Result := TRUE;
end;

function CalcLn(x: Extended): Extended;
begin
  Result := Ln(x);
end;

function CalcLnWrapper(Caller: TPSExec; p: TPSExternalProcRec; Global, Stack: TPSStack): Boolean;
begin
  Stack.SetReal(-1, CalcLn(Stack.GetReal(-2)));
  Result := TRUE;
end;

function CalcRound(x: Extended): Longint;
{ Round to nearest, round half away from zero. }
begin
  if x > 0 then
    Result := Trunc(x+0.5)
  else
    Result := Trunc(x-0.5);
end;

function CalcRoundWrapper(Caller: TPSExec; p: TPSExternalProcRec; Global, Stack: TPSStack): Boolean;
begin
  Stack.SetInt(-1, CalcRound(Stack.GetReal(-2)));
  Result := TRUE;
end;

function FloatToString(Value: Extended; DecimalDigits: Integer): UnicodeString;
begin
  Result := ConvertFloatToString(Value, DecimalDigits, ScriptDecimalSeparator);
end;

function FloatToStringWrapper(Caller: TPSExec; p: TPSExternalProcRec; Global, Stack: TPSStack): Boolean;
begin
  Stack.SetUnicodeString(-1, FloatToString(Stack.GetReal(-2), Stack.GetInt(-3)));
  Result := TRUE;
end;

function ScriptAnsiUpperCase(const s: UnicodeString): UnicodeString;
{ Support English and Russian for backward compatibility. }
var
  i: Integer;
  w: Word;
begin
  Result := AsciiUpperCase(s);
  for i := 1 to Length(Result) do
  begin
    w := Ord(Result[i]);
    if (w >= $430) and (w <= $44f) then
      Result[i] := WideChar(w - $20)
    else if w = $451 then
      Result[i] := #$401;
  end;
end;

function ScriptAnsiUpperCaseWrapper(Caller: TPSExec; p: TPSExternalProcRec; Global, Stack: TPSStack): Boolean;
begin
  Stack.SetUnicodeString(-1, ScriptAnsiUpperCase(Stack.GetUnicodeString(-2)));
  Result := TRUE;
end;

function ScriptAnsiLowerCase(const s: UnicodeString): UnicodeString;
{ Support English and Russian for backward compatibility. }
var
  i: Integer;
  w: Word;
begin
  Result := AsciiLowerCase(s);
  for i := 1 to Length(Result) do
  begin
    w := Ord(Result[i]);
    if (w >= $410) and (w <= $42f) then
      Result[i] := WideChar(w + $20)
    else if w = $401 then
      Result[i] := #$451;
  end;
end;

function ScriptAnsiLowerCaseWrapper(Caller: TPSExec; p: TPSExternalProcRec; Global, Stack: TPSStack): Boolean;
begin
  Stack.SetUnicodeString(-1, ScriptAnsiLowerCase(Stack.GetUnicodeString(-2)));
  Result := TRUE;
end;

function CompilerUses(Sender: TPSPascalCompiler;
  const Name: String): Boolean;
var
  DoubleType: TPSType;
begin
  if Name = 'SYSTEM' then
  begin
    DoubleType := Sender.FindType('Double');
    Assert( DoubleType <> nil );
    Sender.AddTypeCopy('Real', DoubleType);

    Sender.AddFunction('function Random(Range: Integer): Integer;');
    Sender.AddFunction('function RandomFloat: Extended;');
    Sender.AddFunction('function FloatToString(Value: Extended; DecimalDigits: Integer): String;');
    Sender.AddFunction('procedure Error(const Msg: String);');
    Sender.AddFunction('function Power(const Base, Exponent: Extended): Extended;');
    Sender.AddFunction('function Exp(x: Extended): Extended;');
    Sender.AddFunction('function Ln(x: Extended): Extended;');

    Result := TRUE;
  end
  else
    raise Exception.CreateFmt('Unit "%s" not found.', [Name]);
end;

function CompilePascalScript(Script: TStrings): TCompiledPascalScript;
var
  Compiler: TPSPascalCompiler;
  Msg: String;
begin
  Result := TCompiledPascalScript.Create;
  try
    Compiler := TPSPascalCompiler.Create;
    try
      Compiler.BooleanShortCircuit := TRUE;
      Compiler.OnUses := CompilerUses;
      if not Compiler.Compile(Script.Text) then
      begin
        if Compiler.MsgCount > 0 then
          Msg := Compiler.Msg[Compiler.MsgCount-1].MessageToString
        else
          Msg := '<unknown error>';
        raise Exception.CreateFmt(SCompileError, [Msg]);
      end;
      if not Compiler.GetOutput(Result.FByteCode) then
        raise Exception.Create('Can''t get compiler output.');
      if not Compiler.GetDebugOutput(Result.FDebugInfo) then
        raise Exception.Create('Can''t get compiler debug output.');
    finally
      Compiler.Free;
    end;
  except
    Result.Free;
    raise;
  end;
end;

procedure ExecRunLine(Sender: TPSExec);
begin
  {$IFDEF WINDOWS}
  {$HINTS OFF}
  if GetIntervalSince(Cardinal(Sender.Id)) > SCRIPT_TIMEOUT then
  {$HINTS ON}
    raise Exception.Create(SScriptTimedOut);
  {$ENDIF}
end;

procedure ExecutePascalScript(CompiledScript: TCompiledPascalScript; VariableList: TVariableList);
var
  Exec: TPSDebugExec;
  i: Integer;
  p: PPSVariant;
  v: TVariable;
  ValueAssigned: Boolean;
begin
  VariableList.Clear;
  Exec := TPSDebugExec.Create;
  try
    Exec.RegisterFunctionName('Random', @RandomIntWrapper, nil, nil);
    Exec.RegisterFunctionName('RandomFloat', @RandomFloatWrapper, nil, nil);
    Exec.RegisterFunctionName('FloatToString', @FloatToStringWrapper, nil, nil);
    Exec.RegisterFunctionName('Error', @ErrorWrapper, nil, nil);
    Exec.RegisterFunctionName('Power', @CalcPowerWrapper, nil, nil);
    Exec.RegisterFunctionName('Exp', @CalcExpWrapper, nil, nil);
    Exec.RegisterFunctionName('Ln', @CalcLnWrapper, nil, nil);
    Exec.RegisterFunctionName('Round', @CalcRoundWrapper, nil, nil); { supersede }
    Exec.RegisterFunctionName('AnsiUpperCase', @ScriptAnsiUpperCaseWrapper, nil, nil); { supersede }
    Exec.RegisterFunctionName('AnsiLowerCase', @ScriptAnsiLowerCaseWrapper, nil, nil); { supersede }

    if not Exec.LoadData(CompiledScript.FByteCode) then
      raise Exception.Create('Can''t load compiled bytecode.');
    Exec.LoadDebugData(CompiledScript.FDebugInfo);
    Exec.OnRunLine := ExecRunLine;
    {$IFDEF WINDOWS}
    {$HINTS OFF}
    Exec.Id := Pointer(GetTickCount);
    {$HINTS ON}
    {$ENDIF}
    if not Exec.RunScript then
      raise Exception.CreateFmt(SExecutionError, [PSErrorToString(Exec.ExceptionCode, Exec.ExceptionString)]);

    Assert( Exec.GetVarCount = Exec.GlobalVarNames.Count );
    for i := 0 to Exec.GetVarCount-1 do
    begin
      p := Exec.GetVarNo(i);
      if (p <> nil) and (p.FType.ClassType = TPSTypeRec) then
      begin
        v := TVariable.Create;
        try
          v.Name := Exec.GlobalVarNames[i];
          ValueAssigned := TRUE;
          case p.FType.BaseType of
            btU8: v.Int64Value := PPSVariantU8(p).Data;
            btS8: v.Int64Value := PPSVariantS8(p).Data;
            btU16: v.Int64Value := PPSVariantU16(p).Data;
            btS16: v.Int64Value := PPSVariantS16(p).Data;
            btU32: v.Int64Value := PPSVariantU32(p).Data;
            btS32: v.Int64Value := PPSVariantS32(p).Data;
            btSingle: v.DoubleValue := PPSVariantSingle(p).Data;
            btDouble: v.DoubleValue := PPSVariantDouble(p).Data;
            btExtended: v.DoubleValue := PPSVariantExtended(p).Data;
            btUnicodeString: v.StringValue := UTF8Encode(PPSVariantUString(p).Data);
            btS64: v.Int64Value := PPSVariantS64(p).Data;
            btWideChar: v.StringValue := UTF8Encode(UnicodeString(PPSVariantWChar(p).Data));
            else
              ValueAssigned := FALSE;
          end;
        except
          v.Free;
          raise;
        end;
        if ValueAssigned then
          VariableList.AddSafely(v)
        else
          v.Free;
      end;
    end;
  finally
    Exec.Free;
  end;
end;

{ TVariable }

procedure TVariable.CheckType(RequiredType: TVariableType);
begin
  if RequiredType <> FVariableType then
    raise Exception.CreateFmt('Variable type mismatch: expected %d, found %d.',
      [Ord(RequiredType), Ord(FVariableType)]);
end;

procedure TVariable.Clear;
begin
  FStringValue := '';
  FDoubleValue := 0;
  FInt64Value := 0;
end;

constructor TVariable.Create;
begin
  inherited;
  FVariableType := vrtString;
end;

function TVariable.GetDoubleValue: Double;
begin
  CheckType(vrtDouble);
  Result := FDoubleValue;
end;

function TVariable.GetInt64Value: Int64;
begin
  CheckType(vrtInt64);
  Result := FInt64Value;
end;

function TVariable.GetStringValue: String;
begin
  CheckType(vrtString);
  Result := FStringValue;
end;

function TVariable.GetText: String;
begin
  case FVariableType of
    vrtString: Result := FStringValue;
    vrtInt64: Result := IntToStr(FInt64Value);
    vrtDouble: Result := ConvertFloatToString(FDoubleValue, 3, ScriptDecimalSeparator);
    else
      raise Exception.CreateFmt('Unknown variable type %d.', [Ord(FVariableType)]);
  end;
end;

procedure TVariable.SetDoubleValue(const Value: Double);
begin
  Clear;
  FVariableType := vrtDouble;
  FDoubleValue := Value;
end;

procedure TVariable.SetInt64Value(const Value: Int64);
begin
  Clear;
  FVariableType := vrtInt64;
  FInt64Value := Value;
end;

procedure TVariable.SetStringValue(const Value: String);
begin
  Clear;
  FVariableType := vrtString;
  FStringValue := Value;
end;

{ TVariableList }

function TVariableList.AddSafely(Item: TVariable): TVariable;
begin
  try
    Add(Item);
    Result := Item;
  except
    Item.Free;
    raise;
  end;
end;

initialization

  ScriptDecimalSeparator := GetUiDecimalSeparator;

end.
