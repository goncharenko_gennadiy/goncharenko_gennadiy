/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.common;

option java_outer_classname = "Proto";

message Flow {
  repeated Block block = 1;
}

message Block {
  required BlockType type = 1;
  optional TextBlock text_block = 2;
  optional ImageBlock image_block = 3;
}

enum BlockType {
  TEXT = 1;
  IMAGE = 2;
  LINE_FEED = 3;
}

message TextBlock {
  required string text = 1;
}

message ImageBlock {
  required string mime_type = 1;
  required bytes data = 2;
}

message Dialog {
  repeated Area area = 1;
}

message Area {
  required int32 kind = 1;
  optional string id = 2;
  extensions 1000 to max;
}

message FlowArea {
  extend Area {
    optional FlowArea flow_area = 1000;
  }

  required Flow flow = 1;
}

message DialogResponse {
  repeated AreaResponse area_response = 1;
}

message AreaResponse {
  required string area_id = 1;
  extensions 1000 to max;
}

message ClientMessage {
  required ClientMessageType type = 1;
  optional SetResponse set_response = 2;
  optional SelectWork select_work = 3;
  optional LogIn log_in = 4;
  optional QueryQuestionDetails query_question_details = 5;
  optional ChooseQuestion choose_question = 6;
  optional WatchSession watch_session = 7;
  optional WatchQuestion watch_question = 8;
  optional Supervise supervise = 9;
  optional WatchWork watch_work = 10;
  optional CreateWork create_work = 11;
}

enum ClientMessageType {
  SET_RESPONSE = 1;
  SUBMIT = 2;
  LIST_WORKS = 3;
  SELECT_WORK = 4;
  LOG_IN = 5;
  FINISH_WORK = 6;
  QUERY_QUESTION_DETAILS = 7;
  CHOOSE_QUESTION = 8;
  SUPERVISE = 9;
  WATCH_WORK = 10;
  WATCH_SESSION = 11;
  WATCH_QUESTION = 12;
  CREATE_WORK = 13;
  DELETE_WORK = 14;
}

message SetResponse {
  required DialogResponse response = 1;
}

message ServerMessage {
  required ServerMessageType type = 1;
  optional ShowDialog show_dialog = 2;
  optional ShowLoginScreen show_login_screen = 4;
  optional LogInFailed log_in_failed = 5;
  repeated SessionScore session_score = 6;
  optional SessionKey session_key = 7;
  optional ShowScore show_score = 8;
  optional ShowQuestionDetails show_question_details = 9;
  optional MainScreenTurnOn main_screen_turn_on = 10;
  optional ShowQuestionStatus show_question_status = 11;
  optional SubmitOk submit_ok = 12;
  optional ShowRemainingTime show_remaining_time = 13;
  optional ServerTime server_time = 14;
  optional ShowWatchedSession show_watched_session = 15;
  optional ShowWatchedQuestion show_watched_question = 16;
  optional UpdateWatchedQuestion update_watched_question = 17;
  optional CreateWorkComplete create_work_complete = 19;
  optional WorkList work_list = 20;
  optional ShowSupervisorScreen show_supervisor_screen = 21;
}

enum ServerMessageType {
  SHOW_DIALOG = 1;
  SHOW_SELECT_WORK_SCREEN = 2;
  SHOW_LOGIN_SCREEN = 3;
  MAIN_SCREEN_TURN_ON = 4;
  LOG_IN_FAILED = 5;
  SESSION_SCORE = 6;
  SESSION_KEY = 7;
  SET_RESPONSE_OK = 8;
  SUBMIT_OK = 9;
  SHOW_SCORE = 10;
  SHOW_QUESTION_DETAILS = 11;
  SHOW_QUESTION_STATUS = 12;
  SHOW_REMAINING_TIME = 13;
  SERVER_TIME = 14;
  SHOW_WATCHED_SESSION = 15;
  SHOW_WATCHED_QUESTION = 16;
  UPDATE_WATCHED_QUESTION = 17;
  SHOW_SUPERVISOR_SCREEN = 18;
  CREATE_WORK_COMPLETE = 19;
  WORK_LIST = 20;
  INCORRECT_SUPERVISOR_KEY = 21;
}

message MainScreenTurnOn {
  repeated QuestionDescriptor question_descriptor = 1;
}

message QuestionDescriptor {
  required int32 weight = 1;
  required QuestionStatus status = 2;
}

enum QuestionStatus {
  UNANSWERED = 1;
  ANSWERED = 2;
  CORRECT = 3;
  INCORRECT = 4;
  PARTIALLY_CORRECT = 5;
}

message ShowDialog {
  required Dialog dialog = 1;
  required DialogResponse response = 2;
  required bool can_submit = 3;
  required int32 question_index = 4;
  required bool read_only = 5;
}

message WorkDescriptor {
  required string id = 1;
  required string title = 2;
}

message SelectWork {
  required string id = 1;
  optional string session_key = 2;
}

message ShowLoginScreen {
  required int32 max_user_name_length = 1;
}

message LogIn {
  required string user_name = 1;
}

message LogInFailed {
  required Reason reason = 1;

  enum Reason {
    INCORRECT_USER_NAME = 1;
  }
}

message SessionScore {
  required int64 id = 1;
  required string user_name = 2;
  required int64 scaled_score = 3;
  required int32 scaled_result = 4;
  required int32 total_achievable_score = 5;
  required int32 current_achievable_score = 6;
  required bool finished = 7;
  optional double deadline_milliseconds = 8;
  required string mark = 9;
}

message SessionKey {
  required string session_key = 1;
}

message ShowScore {
  required int32 question_count = 1;
  optional int32 scaled_result = 2;
  optional string mark = 3;
  optional int64 scaled_score = 4;
  optional int32 total_achievable_score = 5;
  optional QuestionScore question_score = 6;
}

message QuestionScore {
  required bool correct_response_available = 1;
  repeated QuestionDescriptor question_descriptor = 2;
}

message QueryQuestionDetails {
  required int32 question_index = 1;
}

message ShowQuestionDetails {
  required int32 question_index = 1;
  required Dialog dialog = 2;
  required DialogResponse response = 3;
  optional DialogResponse correct_response = 4;
  optional int32 scaled_result = 5;
  optional int32 weight = 6;
  optional int64 scaled_score = 7;
}

message ShowQuestionStatus {
  required int32 question_index = 1;
  required QuestionStatus status = 2;
}

message ChooseQuestion {
  required int32 question_index = 1;
}

message SubmitOk {
  required bool make_read_only = 1;
}

message ShowRemainingTime {
  required double milliseconds_remaining = 1;
  optional double milliseconds_total = 2;
}

message ServerTime {
  required double server_time_milliseconds = 1;
}

message Supervise {
  required bytes supervisor_key = 1;
}

message WatchWork {
  required string work_id = 1;
}

message WatchSession {
  optional int64 session_id = 1;
}

message ShowWatchedSession {
  optional int64 session_id = 1;
  repeated QuestionDescriptor question_descriptor = 2;
  optional ShowWatchedQuestion question = 3;
}

message ShowWatchedQuestion {
  required int32 question_index = 1;
  required Dialog dialog = 2;
  required DialogResponse correct_response = 3;
  required int32 weight = 4;
  required UpdateWatchedQuestion update = 5;
}

message UpdateWatchedQuestion {
  required DialogResponse response = 1;
  required int32 scaled_result = 2;
  required int64 scaled_score = 3;
}

message WatchQuestion {
  required int32 question_index = 1;
}

message CreateWork {
  required bytes test = 1;
  required string title = 2;
  required string language = 3;
}

message CreateWorkComplete {
  required Result result = 1;

  enum Result {
    OK = 1;
    INCORRECT_TEST_FILE = 2;
  }
}

message WorkList {
  repeated WorkDescriptor work_descriptor = 1;
}

message ShowSupervisorScreen {
  required string server_address = 1;
}
