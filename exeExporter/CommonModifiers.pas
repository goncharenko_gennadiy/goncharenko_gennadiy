{
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
}

unit CommonModifiers;

interface

uses
  Classes, SysUtils, TestCore, Script;

type
  TSetEvaluationModelModifier = class(TModifier)
  private
    FLaxEvaluation: Boolean;
    FQuestionClasses: TQuestionClassList;
  public
    constructor Create; override;
    destructor Destroy; override;
    procedure Apply(var Question: TQuestion; Alterants: TAlterantList); override;
    procedure Assign(Source: TModifier); override;

    property LaxEvaluation: Boolean read FLaxEvaluation write FLaxEvaluation;
    property QuestionClasses: TQuestionClassList read FQuestionClasses;
  end;

  TSetEvaluationModelAlterant = class(TAlterant)
  private
    FLaxEvaluation: Boolean;
  public
    procedure Apply(var Question: TQuestion); override;

    property LaxEvaluation: Boolean read FLaxEvaluation write FLaxEvaluation;
  end;

  TSetWeightModifier = class(TModifier)
  private
    FNewWeight: Integer;
    procedure SetNewWeight(Value: Integer);
  public
    constructor Create; override;
    procedure Apply(var Question: TQuestion; Alterants: TAlterantList); override;
    procedure Assign(Source: TModifier); override;

    property NewWeight: Integer read FNewWeight write SetNewWeight;
  end;

  TSetWeightAlterant = class(TAlterant)
  private
    FNewWeight: Integer;
    procedure SetNewWeight(Value: Integer);
  public
    constructor Create; override;
    procedure Apply(var Question: TQuestion); override;

    property NewWeight: Integer read FNewWeight write SetNewWeight;
  end;

  TScriptModifier = class(TModifier)
  private
    FScript: TStringList;
    FCompiledScript: TCompiledPascalScript;
  public
    constructor Create; override;
    destructor Destroy; override;
    procedure GetScript(Script: TStrings);
    procedure SetScript(Script: TStrings);
    procedure Apply(var Question: TQuestion; Alterants: TAlterantList); override;
    procedure Assign(Source: TModifier); override;
  end;

  TExpandVariablesAlterant = class(TAlterant)
  private
    FVariableList: TVariableList;
  public
    constructor Create; override;
    destructor Destroy; override;
    procedure Apply(var Question: TQuestion); override;

    property VariableList: TVariableList read FVariableList;
  end;

implementation

{ TSetEvaluationModelModifier }

procedure TSetEvaluationModelModifier.Apply(var Question: TQuestion;
  Alterants: TAlterantList);
var
  Alterant: TSetEvaluationModelAlterant;
begin
  if (Question.LaxEvaluation <> FLaxEvaluation)
    and ((FQuestionClasses.Count = 0)
    or (FQuestionClasses.IndexOf(TQuestionClass(Question.ClassType)) <> -1)) then
  begin
    Alterant := TSetEvaluationModelAlterant.Create;
    Alterants.Add(Alterant);

    Alterant.LaxEvaluation := FLaxEvaluation;
    Alterant.Apply(Question);
  end;
end;

procedure TSetEvaluationModelModifier.Assign(Source: TModifier);
var
  m: TSetEvaluationModelModifier;
begin
  inherited;
  m := Source as TSetEvaluationModelModifier;
  FLaxEvaluation := m.FLaxEvaluation;
  FQuestionClasses.Assign(m.FQuestionClasses);
end;

constructor TSetEvaluationModelModifier.Create;
begin
  inherited;
  FLaxEvaluation := TRUE;
  FQuestionClasses := TQuestionClassList.Create;
end;

destructor TSetEvaluationModelModifier.Destroy;
begin
  FreeAndNil(FQuestionClasses);
  inherited;
end;

{ TSetEvaluationModelAlterant }

procedure TSetEvaluationModelAlterant.Apply(var Question: TQuestion);
begin
  Question.LaxEvaluation := FLaxEvaluation;
end;

{ TSetWeightModifier }

procedure TSetWeightModifier.Apply(var Question: TQuestion; Alterants: TAlterantList);
var
  Alterant: TSetWeightAlterant;
begin
  if Question.Weight <> FNewWeight then
  begin
    Alterant := TSetWeightAlterant.Create;
    Alterants.Add(Alterant);

    Alterant.NewWeight := FNewWeight;
    Alterant.Apply(Question);
  end;
end;

procedure TSetWeightModifier.Assign(Source: TModifier);
begin
  inherited;
  FNewWeight := (Source as TSetWeightModifier).FNewWeight;
end;

constructor TSetWeightModifier.Create;
begin
  inherited;
  FNewWeight := 1;
end;

procedure TSetWeightModifier.SetNewWeight(Value: Integer);
begin
  Assert( Value >= 1 );
  FNewWeight := Value;
end;

{ TSetWeightAlterant }

procedure TSetWeightAlterant.Apply(var Question: TQuestion);
begin
  Question.Weight := FNewWeight;
end;

procedure TSetWeightAlterant.SetNewWeight(Value: Integer);
begin
  Assert( Value >= 1 );
  FNewWeight := Value;
end;

constructor TSetWeightAlterant.Create;
begin
  inherited;
  FNewWeight := 1;
end;

{ TScriptModifier }

procedure TScriptModifier.Apply(var Question: TQuestion; Alterants: TAlterantList);
var
  Alterant: TExpandVariablesAlterant;
  i, ReplaceCount: Integer;
  v: TVariable;
  VariableList: TVariableList;
begin
  VariableList := TVariableList.Create;
  try
    if FCompiledScript = nil then
      FCompiledScript := CompilePascalScript(FScript);

    ExecutePascalScript(FCompiledScript, VariableList);

    Alterant := TExpandVariablesAlterant.Create;
    try
      i := 0;
      while i < VariableList.Count do
      begin
        v := VariableList[i];
        ReplaceCount := Question.ReplaceString(Format('$(%s)', [v.Name]), v.Text);
        if ReplaceCount > 0 then
        begin
          VariableList.Extract(v);
          Alterant.VariableList.AddSafely(v);
        end
        else
          Inc(i);
      end;
    except
      Alterant.Free;
      raise;
    end;

    if Alterant.VariableList.Count > 0 then
      Alterants.Add(Alterant)
    else
      Alterant.Free;
  finally
    VariableList.Free;
  end;
end;

procedure TScriptModifier.Assign(Source: TModifier);
begin
  inherited;
  FreeAndNil(FCompiledScript);
  FScript.Assign((Source as TScriptModifier).FScript);
end;

constructor TScriptModifier.Create;
begin
  inherited;
  FScript := TStringList.Create;
  FScript.Add('begin');
  FScript.Add('');
  FScript.Add('end.');
end;

destructor TScriptModifier.Destroy;
begin
  FreeAndNil(FScript);
  FreeAndNil(FCompiledScript);
  inherited;
end;

procedure TScriptModifier.GetScript(Script: TStrings);
begin
  Script.AddStrings(FScript);
end;

procedure TScriptModifier.SetScript(Script: TStrings);
begin
  FreeAndNil(FCompiledScript);
  FScript.Assign(Script);
end;

{ TExpandVariablesAlterant }

procedure TExpandVariablesAlterant.Apply(var Question: TQuestion);
var
  v: TVariable;
begin
  for v in FVariableList do
    Question.ReplaceString(Format('$(%s)', [v.Name]), v.Text);
end;

constructor TExpandVariablesAlterant.Create;
begin
  inherited;
  FVariableList := TVariableList.Create;
end;

destructor TExpandVariablesAlterant.Destroy;
begin
  FreeAndNil(FVariableList);
  inherited;
end;

end.
