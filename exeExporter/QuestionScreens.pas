{
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
}

unit QuestionScreens;

interface

uses
  Classes, SysUtils, VisualUtils, TestCore, MiscUtils;

type
  TQuestionScreenRegistry = class
  private type
    TEntry = class
    private
      FQuestionScreenClass: TFrameClass;
      FQuestionClass: TQuestionClass;
    public
      constructor Create(QuestionScreenClass: TFrameClass; QuestionClass: TQuestionClass);
    end;
    TEntryList = TGenericObjectList<TEntry>;
  private
    FEntries: TEntryList;
  public
    constructor Create;
    destructor Destroy; override;
    procedure Add(QuestionScreenClass: TFrameClass; QuestionClass: TQuestionClass);
    function Find(QuestionClass: TQuestionClass): TFrameClass;
  end;

function QuestionScreenRegistry: TQuestionScreenRegistry;

implementation

var
  Registry: TQuestionScreenRegistry;

function QuestionScreenRegistry: TQuestionScreenRegistry;
begin
  Result := Registry;
end;

{ TQuestionScreenRegistry.TEntry }

constructor TQuestionScreenRegistry.TEntry.Create(
  QuestionScreenClass: TFrameClass; QuestionClass: TQuestionClass);
begin
  inherited Create;
  FQuestionScreenClass := QuestionScreenClass;
  FQuestionClass := QuestionClass;
end;

{ TQuestionScreenRegistry }

constructor TQuestionScreenRegistry.Create;
begin
  inherited;
  FEntries := TEntryList.Create;
end;

destructor TQuestionScreenRegistry.Destroy;
begin
  FreeAndNil(FEntries);
  inherited;
end;

procedure TQuestionScreenRegistry.Add(QuestionScreenClass: TFrameClass;
  QuestionClass: TQuestionClass);
begin
  FEntries.AddSafely(TEntry.Create(QuestionScreenClass, QuestionClass));
end;

function TQuestionScreenRegistry.Find(QuestionClass: TQuestionClass): TFrameClass;
var
  e: TEntry;
begin
  for e in FEntries do
    if e.FQuestionClass = QuestionClass then
      Exit(e.FQuestionScreenClass);
  raise Exception.CreateFmt('No question screen for %s.', [QuestionClass.ClassName]);
end;

initialization

  Registry := TQuestionScreenRegistry.Create;

finalization

  FreeAndNil(Registry);

end.
