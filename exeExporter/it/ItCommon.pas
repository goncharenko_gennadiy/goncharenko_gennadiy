{
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
}

unit ItCommon;

interface

uses
  Classes, SysUtils;

const
  ELEM_APPLY_TO = 'applyTo';
  ELEM_BASE_ITEM = 'baseItem';
  ELEM_BR = 'br';
  ELEM_CATEGORIES = 'categories';
  ELEM_CATEGORY = 'category';
  ELEM_CATEGORY_TITLE = 'categoryTitle';
  ELEM_CATEGORY_ITEM = 'categoryItem';
  ELEM_CATEGORY_ITEMS = 'categoryItems';
  ELEM_CHOICE = 'choice';
  ELEM_CHOICES = 'choices';
  ELEM_CLASSIFY_OPTIONS = 'classifyOptions';
  ELEM_CONTENT = 'content';
  ELEM_DISTRACTOR = 'distractor';
  ELEM_DISTRACTORS = 'distractors';
  ELEM_IMG = 'img';
  ELEM_INSTANT_FEEDBACK = 'instantFeedback';
  ELEM_IREN_TEST = 'irenTest';
  ELEM_LABEL = 'label';
  ELEM_LABELS = 'labels';
  ELEM_MARK = 'mark';
  ELEM_MARKS = 'marks';
  ELEM_MARK_SCALE = 'markScale';
  ELEM_MATCH_OPTIONS = 'matchOptions';
  ELEM_MATCHING_ITEM = 'matchingItem';
  ELEM_MODIFIER = 'modifier';
  ELEM_MODIFIERS = 'modifiers';
  ELEM_ORDER_OPTIONS = 'orderOptions';
  ELEM_PAIR = 'pair';
  ELEM_PAIRS = 'pairs';
  ELEM_PATTERN = 'pattern';
  ELEM_PATTERNS = 'patterns';
  ELEM_PROFILE = 'profile';
  ELEM_PROFILES = 'profiles';
  ELEM_QUESTION = 'question';
  ELEM_QUESTION_RESULTS = 'questionResults';
  ELEM_QUESTION_SELECTION = 'questionSelection';
  ELEM_QUESTIONS = 'questions';
  ELEM_QUESTION_TYPE = 'questionType';
  ELEM_SCRIPT = 'script';
  ELEM_SECTION = 'section';
  ELEM_SECTIONS = 'sections';
  ELEM_SECTION_RESULTS = 'sectionResults';
  ELEM_SEQUENCE = 'sequence';
  ELEM_SEQUENCE_ITEM = 'sequenceItem';
  ELEM_SESSION_OPTIONS = 'sessionOptions';
  ELEM_TEST_RESULTS = 'testResults';
  ELEM_TEXT = 'text';

  ATTR_ANSWER_CORRECTNESS = 'answerCorrectness';
  ATTR_BASE_ITEMS_USED = 'baseItemsUsed';
  ATTR_BROWSABLE_QUESTIONS = 'browsableQuestions';
  ATTR_CASE_SENSITIVE = 'caseSensitive';
  ATTR_CLIPBOARD_DATA = 'clipboardData';
  ATTR_CLIPBOARD_FORMAT = 'clipboardFormat';
  ATTR_CORRECT = 'correct';
  ATTR_CORRECT_ANSWER = 'correctAnswer';
  ATTR_DISTRACTORS_USED = 'distractorsUsed';
  ATTR_DURATION_MINUTES = 'durationMinutes';
  ATTR_EDITABLE_ANSWERS = 'editableAnswers';
  ATTR_ENABLED = 'enabled';
  ATTR_FIXED = 'fixed';
  ATTR_ITEMS_USED = 'itemsUsed';
  ATTR_LABEL_FILTER = 'labelFilter';
  ATTR_LINE = 'line';
  ATTR_LOWER_BOUND = 'lowerBound';
  ATTR_MARK = 'mark';
  ATTR_MIN_ITEMS_PER_CATEGORY_USED = 'minItemsPerCategoryUsed';
  ATTR_NEGATIVE = 'negative';
  ATTR_NEW_EVALUATION_MODEL = 'newEvaluationModel';
  ATTR_NEW_WEIGHT = 'newWeight';
  ATTR_NUMERIC_AWARE = 'numericAware';
  ATTR_PERCENT_CORRECT = 'percentCorrect';
  ATTR_POINTS = 'points';
  ATTR_PRECISION = 'precision';
  ATTR_QUALITY = 'quality';
  ATTR_QUESTIONS_PER_SECTION = 'questionsPerSection';
  ATTR_QUESTION_COUNT = 'questionCount';
  ATTR_QUESTION_LIST = 'questionList';
  ATTR_SEQUENCE_ITEMS_USED = 'sequenceItemsUsed';
  ATTR_SHUFFLE_QUESTIONS = 'shuffleQuestions';
  ATTR_SPACES = 'spaces';
  ATTR_SRC = 'src';
  ATTR_TITLE = 'title';
  ATTR_TOTAL_PERCENT_CORRECT = 'totalPercentCorrect';
  ATTR_TYPE = 'type';
  ATTR_VALUE = 'value';
  ATTR_WEIGHT = 'weight';
  ATTR_WEIGHT_CUES = 'weightCues';
  ATTR_WILDCARD = 'wildcard';

  VAL_TRUE = 'true';
  VAL_FALSE = 'false';
  VAL_ALL = 'all';
  VAL_UNLIMITED = 'unlimited';

  VAL_QUESTION_TYPE_SELECT = 'select';
  VAL_QUESTION_TYPE_INPUT = 'input';
  VAL_QUESTION_TYPE_MATCH = 'match';
  VAL_QUESTION_TYPE_ORDER = 'order';
  VAL_QUESTION_TYPE_CLASSIFY = 'classify';

  VAL_MODIFIER_TYPE_SUPPRESS_SINGLE_CHOICE_HINT = 'suppressSingleChoiceHint';
  VAL_MODIFIER_TYPE_ADD_NEGATIVE_CHOICE = 'addNegativeChoice';
  VAL_MODIFIER_TYPE_SET_NEGATIVE_CHOICE_CONTENT = 'setNegativeChoiceContent';
  VAL_MODIFIER_TYPE_SHUFFLE_CHOICES = 'shuffleChoices';
  VAL_MODIFIER_TYPE_SET_EVALUATION_MODEL = 'setEvaluationModel';
  VAL_MODIFIER_TYPE_SET_WEIGHT = 'setWeight';
  VAL_MODIFIER_TYPE_SCRIPT = 'script';

  VAL_EVALUATION_MODEL_TYPE_DICHOTOMIC = 'dichotomic';
  VAL_EVALUATION_MODEL_TYPE_LAX = 'lax';

  VAL_PATTERN_TYPE_TEXT = 'text';
  VAL_PATTERN_TYPE_REGEXP = 'regexp';

  VAL_PATTERN_SPACES_NORMALIZE = 'normalize';
  VAL_PATTERN_SPACES_IGNORE = 'ignore';
  VAL_PATTERN_SPACES_EXACT = 'exact';

  IT_TEST_FILE_NAME = 'test.xml';
  IT_IMAGE_DIRECTORY_NAME = 'images';
  IT_SOURCE_DATA_FILE_EXTENSION = 'dat';

  IT_IMAGE_PNG_EXTENSION = 'png';
  IT_IMAGE_JPG_EXTENSION = 'jpg';

implementation

end.
