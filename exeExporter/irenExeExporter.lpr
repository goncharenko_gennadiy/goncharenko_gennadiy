{
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
}

program irenExeExporter;

uses
  Interfaces, Classes, SysUtils, Dialogs, MiscUtils, ShellApi, Windows, JwaWinBase, FileUtil, TestUtils, zlibStream,
  CrcUtils, ExeTestUtils, StreamUtils;

type
  TOutputFile = class
  private
    FStream: TStream;
    FFileName: String;
    FTempFileName: String;
    FCommitted: Boolean;
  public
    constructor Create(const FileName: String);
    destructor Destroy; override;
    procedure Commit;

    property Stream: TStream read FStream;
  end;

var
  PauseBeforeExit: Boolean;

procedure SafeMoveFile(const OldName, NewName: String);
var
  OldUnicodeName, NewUnicodeName: UnicodeString;
begin
  OldUnicodeName := UTF8Decode(OldName);
  NewUnicodeName := UTF8Decode(NewName);
  if not MoveFileExW(PWideChar(OldUnicodeName), PWideChar(NewUnicodeName), MOVEFILE_REPLACE_EXISTING) then
    raise Exception.CreateFmt('Could not rename "%s" to "%s": error %d.', [OldName, NewName, GetLastError]);
end;

{ TOutputFile }

procedure TOutputFile.Commit;
begin
  Assert( not FCommitted );
  FreeAndNil(FStream);
  SafeMoveFile(FTempFileName, FFileName);
  FCommitted := TRUE;
end;

constructor TOutputFile.Create(const FileName: String);
begin
  inherited Create;
  FFileName := FileName;
  FTempFileName := FileName + '~';
  FStream := TUnicodeFileStream.CreateNew(FTempFileName);
end;

destructor TOutputFile.Destroy;
begin
  FreeAndNil(FStream);
  if not FCommitted then
    DeleteFileUTF8(FTempFileName);
  inherited;
end;

function GetExeDirectory: String;
begin
  Result := ExtractFileDir(GetExeFileName);
end;

function MakeAbsoluteFileName(const FileName: String): String;
var
  u: UnicodeString;
  Buffer: array [0..MAX_PATH] of UnicodeChar;
  Dummy: PWideChar;
  n: Integer;
begin
  u := UTF8Decode(FileName);
  {$HINTS OFF}
  n := GetFullPathNameW(PWideChar(u), MAX_PATH, Buffer, Dummy);
  {$HINTS ON}
  if (n = 0) or (n > MAX_PATH) then
    raise Exception.Create('GetFullPathNameW failed.');
  Result := UTF8Encode(UnicodeString(Buffer));
end;

procedure WriteLnUtf8(const s: String);
begin
  WriteLn(UTF8ToConsole(s));
end;

function RunProcess(const ExeName, CommandLineArguments: String): Cardinal;
var
  si: TStartupInfoW;
  pi: TProcessInformation;
  CommandLine: UnicodeString;
begin
  {$HINTS OFF}
  FillChar(si, SizeOf(si), 0);
  {$HINTS ON}
  si.cb := SizeOf(si);

  CommandLine := UTF8Decode(Format('"%s" %s', [ExeName, CommandLineArguments]));

  UniqueString(CommandLine); { CreateProcessW wants writable lpCommandLine buffer }
  {$HINTS OFF}
  if not CreateProcessW(PWideChar(UTF8Decode(ExeName)),
    PWideChar(CommandLine), nil, nil, FALSE, 0, nil, nil, si, pi) then
    raise Exception.CreateFmt('Could not run "%s".', [ExeName]);
  {$HINTS ON}

  try
    if WaitForSingleObject(pi.hProcess, INFINITE) <> WAIT_OBJECT_0 then
      raise Exception.Create('WaitForSingleObject failed.');
    {$HINTS OFF}
    if not GetExitCodeProcess(pi.hProcess, Result) then
      raise Exception.Create('GetExitCodeProcess failed.');
    {$HINTS ON}
  finally
    CloseHandle(pi.hThread);
    CloseHandle(pi.hProcess);
  end;
end;

procedure ConvertToExe(const FileName: String);
var
  TargetPath, TestName, TargetFileName, FilteredItxFileName: String;
  Output: TOutputFile;
  Player, FilteredItx: TUnicodeFileStream;
  p: Int64;
  CompressionStream: TCompressionStream;
begin
  TargetPath := ExtractFilePath(FileName) + 'converted-exe\';
  CreateDirUTF8(TargetPath);

  TestName := ExtractFileNameOnly(FileName);
  TargetFileName := TargetPath + TestName + '.exe';
  FilteredItxFileName := TargetPath + TestName + '~';

  try
    if RunProcess(Format('%s\..\jre\bin\irenEditorServer.exe', [GetExeDirectory]),
      Format('-classpath "%s\..\lib\*" ru.irenproject.export.ExportFilter "%s" "%s"',
      [GetExeDirectory, FileName, FilteredItxFileName])) <> 0 then
      raise Exception.Create('Conversion failed.');

    WriteLnUtf8(TargetFileName);
    Output := TOutputFile.Create(TargetFileName);
    try
      Player := TUnicodeFileStream.OpenForRead(GetExeDirectory + '\irenPlayer.bin');
      try
        Output.Stream.CopyFrom(Player, 0);
      finally
        Player.Free;
      end;

      p := Output.Stream.Position;
      WriteStreamString(Output.Stream, ITX_EMBEDDED_MIME_TYPE);
      WriteStreamSizedString(Output.Stream, TestName);

      CompressionStream := TCompressionStream.Create(clMax, Output.Stream, TRUE, FALSE);
      try
        FilteredItx := TUnicodeFileStream.OpenForRead(FilteredItxFileName);
        try
          CompressionStream.CopyFrom(FilteredItx, 0);
        finally
          FilteredItx.Free;
        end;
        CompressionStream.Flush;
      finally
        CompressionStream.Free;
      end;

      WriteStreamInt64(Output.Stream, Output.Stream.Position - p);

      Output.Stream.Seek(0, soFromBeginning);
      WriteStreamCardinal(Output.Stream, ComputeStreamChunkCrc32(Output.Stream, Output.Stream.Size));

      Output.Commit;
    finally
      Output.Free;
    end;
  finally
    DeleteFileUTF8(FilteredItxFileName);
  end;
end;

procedure ExtractFromExe(const ExeName: String);
var
  TargetPath, TestTitleIgnored, TestMimeType, TargetFileName, Ext: String;
  Extracted: TMemoryStream;
  Output: TOutputFile;
begin
  TargetPath := ExtractFilePath(ExeName) + 'converted-itx\';
  CreateDirUTF8(TargetPath);

  Extracted := TMemoryStream.Create;
  try
    ExtractItStreamFromExe(ExeName, Extracted, TestTitleIgnored, TestMimeType);
    if TestMimeType = IT3_EMBEDDED_MIME_TYPE then
      Ext := 'it3'
    else
      Ext := 'itx';

    TargetFileName := TargetPath + ExtractFileNameOnly(ExeName) + '.' + Ext;
    WriteLnUtf8(TargetFileName);

    Output := TOutputFile.Create(TargetFileName);
    try
      Output.Stream.CopyFrom(Extracted, 0);
      Output.Commit;
    finally
      Output.Free;
    end;
  finally
    Extracted.Free;
  end;
end;

procedure ProcessFile(const FileName: String);
var
  Ext: String;
begin
  WriteLnUtf8(FileName + ' ->');
  Ext := UpperCase(RightStr(FileName, 4));
  if (Ext = '.ITX') or (Ext = '.IT3') or (Ext = '.IT2') then
    ConvertToExe(FileName)
  else if Ext = '.EXE' then
    ExtractFromExe(FileName)
  else
    WriteLn('(skipped)');
  WriteLn;
end;

procedure ParseCommandLine(Arguments: TStrings);
var
  ArgumentCount, i: Integer;
  p: PPWideChar;
begin
  p := CommandLineToArgvW(GetCommandLineW, @ArgumentCount);
  if p <> nil then
  begin
    try
      for i := 0 to ArgumentCount-1 do
        Arguments.Add(UTF8Encode(UnicodeString(p[i])));
    finally
      {$HINTS OFF}
      LocalFree(HLOCAL(p));
      {$HINTS ON}
    end;
  end;
end;

procedure Run;
var
  CommandLineArguments: TStringList;
  i: Integer;
  d: TOpenDialog;
  FileName: String;
begin
  CommandLineArguments := TStringList.Create;
  try
    ParseCommandLine(CommandLineArguments);
    if CommandLineArguments.Count > 1 then
    begin
      for i := 1 to CommandLineArguments.Count-1 do
        ProcessFile(MakeAbsoluteFileName(CommandLineArguments[i]));
    end
    else
    begin
      d := TOpenDialog.Create(nil);
      try
        d.Options := d.Options + [ofHideReadOnly, ofNoChangeDir, ofAllowMultiSelect];
        d.Filter := 'Tests (*.itx; *.it3; *.it2; *.exe)|*.itx;*.it3;*.it2;*.exe';
        d.Title := '';

        if d.Execute then
        begin
          PauseBeforeExit := TRUE;
          for FileName in d.Files do
            ProcessFile(FileName);
        end;
      finally
        d.Free;
      end;
    end;
  finally
    CommandLineArguments.Free;;
  end;
end;

begin
  try
    Run;
  except on E: Exception do
    begin
      ExitCode := 1;
      WriteLnUtf8(E.Message);
    end;
  end;

  if PauseBeforeExit then
  begin
    WriteLn;
    WriteLn('Press <Enter>...');
    ReadLn;
  end;
end.
