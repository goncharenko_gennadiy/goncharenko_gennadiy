{
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
}

unit MatchQuestionScreen;

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, ExtCtrls, TestUiUtils,
  MatchQuestion, TestCore, PadWidget, VisualUtils, DragSpace, MiscUtils,
  Math, Graphics, QuestionScreens, types;

type
  TMatchQuestionScreenFrame = class(TFrame, IQuestionScreen, IDragSpaceHost{ internal })
    pnlQuestion: TPanel;
    sbxQuestion: TScrollBox;
    procedure sbxQuestionMouseWheel(Sender: TObject; Shift: TShiftState;
      WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
  private
    FQuestion: TMatchQuestion;
    FFormulation: TPadWidget;
    FDragSpace: TDragSpaceFrame;
    FLeft: TPadWidgetList;
    FRight: TPadWidgetList;
    FDivider: TShape;
    FOldContainerIndex: Integer;
    function GetContainerIndexUnderItem(Widget: TPadWidget): Integer;
    { private declarations }
  public
    destructor Destroy; override;
    procedure SetUp(Question: TQuestion);
    procedure GoReadOnly;
    function GetQuestion: TQuestion;

    { IDragSpaceHost (internal) }
    procedure LayOut;
    procedure NotifyBeginDrag;
    procedure NotifyDragging;
    procedure NotifyEndDrag;

    { public declarations }
  end;

implementation

{$R *.lfm}

const
  VSPACING = 13;
  CONTAINER_MARGIN = 2;
  HSPACING = 10;
  DIVIDER_WIDTH = 2;
  LINK_WIDTH = 15;
  LINK_HEIGHT = 2;
  EMPTY_CONTAINER_COLOR = $a0a0a0;
  OCCUPIED_CONTAINER_COLOR = $ffcc99;
  HOT_TRACK_CONTAINER_COLOR = $80ffb3;

{ TMatchQuestionScreenFrame }

procedure TMatchQuestionScreenFrame.sbxQuestionMouseWheel(Sender: TObject;
  Shift: TShiftState; WheelDelta: Integer; MousePos: TPoint;
  var Handled: Boolean);
begin
  FDragSpace.NotifyScroll(ScrollVerticallyByWheel(sbxQuestion, WheelDelta));
  Handled := TRUE;
end;

function TMatchQuestionScreenFrame.GetContainerIndexUnderItem(Widget: TPadWidget): Integer;
var
  n, p1, p2: Integer;
  Container: TShape;

  function GetCommonVerticalPixels(ContainerIndex: Integer): Integer;
  var
    c: TShape;
    y1, y2: Integer;
  begin
    if (ContainerIndex >= 0) and (ContainerIndex < FLeft.Count) then
    begin
      c := TShape(FLeft[ContainerIndex].Tag);
      if IntersectSegments(Widget.Top, Widget.Top + Widget.Height,
        c.Top, c.Top + c.Height, y1, y2) then
        Result := y2 - y1
      else
        Result := 0;
    end
    else
      Result := 0;
  end;

begin
  if FLeft.Count > 0 then
    Container := TShape(FLeft[0].Tag)
  else
    Container := nil;
  if (Container <> nil) and SegmentsOverlap(Widget.Left, Widget.Left + Widget.Width,
    Container.Left, Container.Left + Container.Width) then
  begin
    n := Widget.Top div (Container.Height + VSPACING);
    p1 := GetCommonVerticalPixels(n);
    p2 := GetCommonVerticalPixels(n + 1);
    if (p1 > 0) and (p1 >= p2) then
      Result := n
    else if (p2 > 0) and (p2 >= p1) then
      Result := n + 1
    else
      Result := -1;
  end
  else
    Result := -1;
end;

destructor TMatchQuestionScreenFrame.Destroy;
begin
  FreeAndNil(FLeft);
  FreeAndNil(FRight);
  FreeAndNil(FDivider);
  FreeAndNil(FDragSpace);
  FreeAndNil(FFormulation);
  FreeAndNil(FQuestion);
  inherited;
end;

procedure TMatchQuestionScreenFrame.SetUp(Question: TQuestion);
var
  i: Integer;
  Widget: TPadWidget;
  ContainerShape, LinkShape: TShape;
begin
  FQuestion := (Question as TMatchQuestion).Clone as TMatchQuestion;
  MakeVerticallyAutoScrollable(sbxQuestion);

  DisableAutoSizing;
  try
    FFormulation := TPadWidget.Create(nil);
    FFormulation.Border := TRUE;
    FFormulation.Parent := pnlQuestion;
    FFormulation.ReadOnly := TRUE;
    FFormulation.Model.Assign(FQuestion.Formulation);

    FDragSpace := TDragSpaceFrame.Create(nil);
    FDragSpace.Parent := pnlQuestion;

    Assert( Length(FQuestion.Response.GetAnswer) = FQuestion.Left.Count );

    FLeft := TPadWidgetList.Create;
    for i := 0 to FQuestion.Left.Count-1 do
    begin
      Widget := FLeft.AddSafely(TPadWidget.Create(nil));
      Widget.Model.Assign(FQuestion.Left[i]);
      Widget.Border := TRUE;
      Widget.ReadOnly := TRUE;
      Widget.Parent := FDragSpace;

      ContainerShape := TShape.Create(Self);
      ContainerShape.Parent := FDragSpace;
      Widget.Tag := PtrInt(ContainerShape);

      LinkShape := TShape.Create(Self);
      LinkShape.Width := LINK_WIDTH;
      LinkShape.Height := LINK_HEIGHT;
      LinkShape.Pen.Color := clGrayText;
      LinkShape.Parent := FDragSpace;
      ContainerShape.Tag := PtrInt(LinkShape);
    end;

    FRight := TPadWidgetList.Create;
    for i := 0 to FQuestion.Right.Count-1 do
    begin
      Widget := FRight.AddSafely(TPadWidget.Create(nil));
      Widget.Model.Assign(FQuestion.Right[i]);
      Widget.Border := TRUE;
      Widget.ReadOnly := TRUE;
      Widget.Parent := FDragSpace;
      Widget.AddMouseListener(FDragSpace);
      Widget.Cursor := crSizeAll;
    end;

    FDivider := TShape.Create(nil);
    FDivider.Parent := FDragSpace;
    FDivider.Width := DIVIDER_WIDTH;
    FDivider.Pen.Color := clGrayText;
    FDivider.AnchorParallel(akTop, 0, FDragSpace);
    FDivider.AnchorParallel(akBottom, 0, FDragSpace);

    FDragSpace.SetUp(Self);
  finally
    EnableAutoSizing;
  end;
end;

procedure TMatchQuestionScreenFrame.GoReadOnly;
var
  w: TPadWidget;
begin
  FDragSpace.DraggingEnabled := FALSE;
  for w in FRight do
    w.Cursor := crDefault;
end;

function TMatchQuestionScreenFrame.GetQuestion: TQuestion;
begin
  Result := FQuestion;
end;

procedure TMatchQuestionScreenFrame.LayOut;
var
  Answer: TIntegerArray;
  ItemWidth, ContainerWidth, i, ItemHeight, ContainerHeight, y: Integer;
  ContainerIndex: Integer;
  Widget: TPadWidget;
  LinkShape, ContainerShape: TShape;
begin
  Answer := FQuestion.Response.GetAnswer;
  ItemWidth := Max((FDragSpace.ClientWidth - LINK_WIDTH - 2*CONTAINER_MARGIN
    - HSPACING - DIVIDER_WIDTH - HSPACING) div 3, 0);
  ContainerWidth := ItemWidth + 2*CONTAINER_MARGIN;
  FDivider.Left := ItemWidth + LINK_WIDTH + ContainerWidth + HSPACING;

  ItemHeight := 0;
  for Widget in FLeft do
  begin
    Widget.Width := ItemWidth;
    ItemHeight := Max(ItemHeight, GetPreferredControlSize(Widget).cy);
  end;
  for Widget in FRight do
  begin
    Widget.Width := ItemWidth;
    ItemHeight := Max(ItemHeight, GetPreferredControlSize(Widget).cy);
  end;
  ContainerHeight := ItemHeight + 2*CONTAINER_MARGIN;

  { Place left items and containers. }
  for i := 0 to FLeft.Count-1 do
  begin
    y := i*(ContainerHeight + VSPACING);
    Widget := FLeft[i];
    Widget.Height := ItemHeight;
    Widget.Top := y + CONTAINER_MARGIN;

    ContainerShape := TShape(Widget.Tag);
    ContainerShape.Width := ContainerWidth;
    ContainerShape.Height := ContainerHeight;
    ContainerShape.Left := Widget.Left + ItemWidth + LINK_WIDTH;
    ContainerShape.Top := y;
    if Answer[i] = -1 then
      ContainerShape.Brush.Color := EMPTY_CONTAINER_COLOR
    else
      ContainerShape.Brush.Color := OCCUPIED_CONTAINER_COLOR;

    LinkShape := TShape(ContainerShape.Tag);
    LinkShape.Left := ItemWidth;
    LinkShape.Top := y + ContainerHeight div 2 - 1;
  end;

  { Place right items. }
  for i := 0 to FRight.Count-1 do
  begin
    Widget := FRight[i];
    Widget.Height := ItemHeight;
    ContainerIndex := FindArrayInteger(Answer, i);
    if ContainerIndex = -1 then
    begin
      Widget.Left := ItemWidth + LINK_WIDTH + ContainerWidth + HSPACING + DIVIDER_WIDTH + HSPACING;
      Widget.Top := i*(ContainerHeight + VSPACING) + CONTAINER_MARGIN;
    end
    else
    begin
      ContainerShape := TShape(FLeft[ContainerIndex].Tag);
      Widget.Left := ContainerShape.Left + CONTAINER_MARGIN;
      Widget.Top := ContainerShape.Top + CONTAINER_MARGIN;
    end;
  end;
end;

procedure TMatchQuestionScreenFrame.NotifyBeginDrag;
begin
  FOldContainerIndex := -1;
end;

procedure TMatchQuestionScreenFrame.NotifyDragging;
var
  ContainerIndex, Element, DraggingElement: Integer;
  Color: TColor;
  Answer: TIntegerArray;
begin
  DraggingElement := FRight.IndexOf(FDragSpace.DragWidget);
  ContainerIndex := GetContainerIndexUnderItem(FDragSpace.DragWidget);
  Answer := FQuestion.Response.GetAnswer;

  if ContainerIndex <> FOldContainerIndex then
  begin
    if FOldContainerIndex <> -1 then
    begin
      Element := Answer[FOldContainerIndex];
      if (Element = -1) or (Element = DraggingElement) then
        Color := EMPTY_CONTAINER_COLOR
      else
        Color := OCCUPIED_CONTAINER_COLOR;
      TShape(FLeft[FOldContainerIndex].Tag).Brush.Color := Color;
    end;

    FOldContainerIndex := ContainerIndex;
    if ContainerIndex <> -1 then
      TShape(FLeft[ContainerIndex].Tag).Brush.Color := HOT_TRACK_CONTAINER_COLOR;
  end;
end;

procedure TMatchQuestionScreenFrame.NotifyEndDrag;
var
  ContainerIndex, ItemIndex, OldContainerIndex: Integer;
  Answer: TIntegerArray;
begin
  ContainerIndex := GetContainerIndexUnderItem(FDragSpace.DragWidget);
  ItemIndex := FRight.IndexOf(FDragSpace.DragWidget);
  Answer := FQuestion.Response.GetAnswer;
  OldContainerIndex := FindArrayInteger(Answer, ItemIndex);

  if OldContainerIndex <> ContainerIndex then
  begin
    if OldContainerIndex <> -1 then
      Answer[OldContainerIndex] := -1;
    if ContainerIndex <> -1 then
    begin
      if OldContainerIndex <> -1 then
        Answer[OldContainerIndex] := Answer[ContainerIndex];
      Answer[ContainerIndex] := ItemIndex;
    end;
    FQuestion.Response.SetAnswer(Answer);
  end;
end;

initialization

  QuestionScreenRegistry.Add(TMatchQuestionScreenFrame, TMatchQuestion);

end.
