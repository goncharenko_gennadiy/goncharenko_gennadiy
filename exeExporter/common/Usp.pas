{
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
}

unit Usp;

interface

uses
  Windows;

const
  USP10_DLL = 'usp10.dll';
  SSA_BREAK = $40;

  fSoftBreak = $01;
  fCharStop = $04;

type
  SCRIPT_STRING_ANALYSIS = Pointer;

  SCRIPT_LOGATTR = Byte;
  PSCRIPT_LOGATTR = ^SCRIPT_LOGATTR;

function ScriptStringAnalyse(
  hdc: HDC;
  pString: Pointer;
  cString: Integer;
  cGlyphs: Integer;
  iCharset: Integer;
  dwFlags: DWORD;
  iReqWidth: Integer;
  psControl: Pointer;
  psState: Pointer;
  piDx: PInteger;
  pTabdef: Pointer;
  pbInClass: PByte;
  out pssa: SCRIPT_STRING_ANALYSIS
): HRESULT; stdcall; external USP10_DLL;

function ScriptStringFree(
  var pssa: SCRIPT_STRING_ANALYSIS
): HRESULT; stdcall; external USP10_DLL;

function ScriptString_pLogAttr(
  ssa: SCRIPT_STRING_ANALYSIS
): PSCRIPT_LOGATTR; stdcall; external USP10_DLL;

implementation

end.
