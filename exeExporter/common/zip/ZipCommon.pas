{
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
}

unit ZipCommon;

interface

const
  ZIP_LOCAL_FILE_HEADER_SIGNATURE = $04034b50;
  ZIP_CENTRAL_FILE_HEADER_SIGNATURE = $02014b50;
  ZIP_END_DIRECTORY_RECORD_SIGNATURE = $06054b50;
  ZIP_METHOD_STORE = 0;
  ZIP_METHOD_DEFLATE = 8;
  ZIP_VERSION_2_0 = 20;

type
  TZipLocalFileHeader = packed record
    Signature: Cardinal;
    VersionToExtract: Word;
    Flags: Word;
    CompressionMethod: Word;
    LastModFileTime: Word;
    LastModFileDate: Word;
    Crc32: Cardinal;
    CompressedSize: Cardinal;
    UncompressedSize: Cardinal;
    FileNameLength: Word;
    ExtraFieldLength: Word;
  end;

  TZipCentralFileHeader = packed record
    Signature: Cardinal;
    VersionMadeBy: Word;
    VersionToExtract: Word;
    Flags: Word;
    CompressionMethod: Word;
    LastModFileTime: Word;
    LastModFileDate: Word;
    Crc32: Cardinal;
    CompressedSize: Cardinal;
    UncompressedSize: Cardinal;
    FileNameLength: Word;
    ExtraFieldLength: Word;
    FileCommentLength: Word;
    DiskNumberStart: Word;
    InternalFileAttributes: Word;
    ExternalFileAttributes: Cardinal;
    LocalHeaderOffset: Cardinal;
  end;

  TZipEndDirectoryRecord = packed record
    Signature: Cardinal;
    DiskNumber: Word;
    StartDirDiskNumber: Word;
    DiskEntryCount: Word;
    TotalEntryCount: Word;
    DirSize: Cardinal;
    DirOffset: Cardinal;
    CommentLength: Word;
  end;

implementation

end.
