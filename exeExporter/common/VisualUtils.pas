{
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
}

unit VisualUtils;

interface

uses
  LCLProc, LCLType, SysUtils, Windows, Classes, Graphics, ComCtrls,
  Math, Forms, MiscUtils, Controls, ActnList, CommCtrl, StdCtrls, ImgList, PngCustom;

type
  {$INTERFACES CORBA}
  IMouseListener = interface
    ['2b40ae480b66ef88']
    procedure NotifyMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure NotifyMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure NotifyMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
  end;
  {$INTERFACES DEFAULT}
  TMouseListenerList = TGenericList<IMouseListener>;

  {$INTERFACES CORBA}
  ICloseQueryListener = interface
    ['c182fac8e5e6920b']
    function CanClose: Boolean;
  end;
  {$INTERFACES DEFAULT}

  TFrameClass = class of TFrame;

procedure InitializeProgram;

procedure TrySetFocus(WinControl: TWinControl);
procedure SelectFirstChild(Parent: TWinControl);

function GetAncestorOfType(Control: TControl; AncestorClass: TWinControlClass): TWinControl;
function FindParentForm(Control: TControl): TCustomForm;
function HasVisibleChildControls(Parent: TWinControl): Boolean;

procedure SaveFormPosition(Form: TForm);
function RestoreFormPosition(Form: TForm): Boolean;
function ShowModalForm(Form: TForm): Integer;

procedure MakeVerticallyAutoScrollable(ScrollBox: TScrollBox);
procedure ScrollControlPartIntoView(Control: TControl; PartTop, PartHeight: Integer);
procedure ScrollControlIntoView(Control: TControl);
function IsVerticalScrollBarVisible(Control: TScrollingWinControl): Boolean;
function ScrollVerticallyByWheel(Control: TScrollingWinControl; WheelDelta: Integer): Integer;
function DragScrollVertically(Control: TControl): Integer;

procedure LockRedraw(Control: TWinControl);
procedure UnlockRedraw(Control: TWinControl);

procedure LoadActionImages(ActionList: TActionList);
function AddPngResourceToImageList(const ResourceName: String; ImageList: TCustomImageList): Integer;
procedure LoadPngResourceToPicture(const ResourceName: String; Target: TPicture);

procedure SetFixedWidthConstraint(Control: TControl; Width: Integer);
procedure SetFixedHeightConstraint(Control: TControl; Height: Integer);
procedure SetFixedSizeConstraints(Control: TControl; Width, Height: Integer);
function GetPreferredControlSize(Control: TControl): TSize;
procedure SetFontSize(const Controls: array of TControl; FontSize: Integer);
procedure SetFontColor(const Controls: array of TControl; FontColor: TColor);
procedure StretchToWidest(const Controls: array of TControl);
procedure SetRightBorderSpacing(const Controls: array of TControl; LogicalPixels: Integer);
function ScalePixels(Value: Integer): Integer;

procedure SetEditTextWithoutOnChange(Edit: TEdit; const Text: String);

procedure DisplayListViewSortIndicator(ListView: TListView; ColumnIndex: Integer; SortBackwards: Boolean);
procedure MakeListViewDoubleBuffered(ListView: TListView);
procedure AutoSizeListViewColumn(ListView: TListView; ColumnIndex: Integer);
procedure AutoSizeListViewColumns(ListView: TListView);

implementation

type
  TSavedFormPosition = class
  private
    FFormClass: TFormClass;
    FWasMaximized: Boolean;
    FLastBoundsRect: TRect;
  end;
  TSavedFormPositionList = TGenericObjectList<TSavedFormPosition>;

var
  SavedFormPositions: TSavedFormPositionList;

procedure TrySetFocus(WinControl: TWinControl);
begin
  try
    WinControl.SetFocus;
  except
  end;
end;

function GetSavedFormPosition(FormClass: TFormClass): TSavedFormPosition;
begin
  if SavedFormPositions <> nil then
    for Result in SavedFormPositions do
      if Result.FFormClass = FormClass then
        Exit;
  Result := nil;
end;

procedure SetEditTextWithoutOnChange(Edit: TEdit; const Text: String);
var
  SavedHandler: TNotifyEvent;
begin
  SavedHandler := Edit.OnChange;
  Edit.OnChange := nil;
  try
    Edit.Text := Text;
  finally
    Edit.OnChange := SavedHandler;
  end;
end;

procedure SaveFormPosition(Form: TForm);
var
  p: TSavedFormPosition;
begin
  p := GetSavedFormPosition(TFormClass(Form.ClassType));
  if p = nil then
  begin
    if SavedFormPositions = nil then
      SavedFormPositions := TSavedFormPositionList.Create;
    p := SavedFormPositions.AddSafely(TSavedFormPosition.Create);
  end;

  p.FFormClass := TFormClass(Form.ClassType);
  p.FWasMaximized := Form.WindowState = wsMaximized;
  if p.FWasMaximized then
    p.FLastBoundsRect := Rect(0, 0, 0, 0)
  else
    p.FLastBoundsRect := Form.BoundsRect;
end;

function RestoreFormPosition(Form: TForm): Boolean;
var
  p: TSavedFormPosition;
begin
  p := GetSavedFormPosition(TFormClass(Form.ClassType));
  Result := p <> nil;
  if Result then
  begin
    if p.FWasMaximized then
    begin
      Form.Position := poDefaultPosOnly;
      Form.WindowState := wsMaximized;
    end
    else
    begin
      Form.Position := poDesigned;
      if Form.BorderStyle in [bsSizeable, bsSizeToolWin] then
        Form.BoundsRect := p.FLastBoundsRect
      else
      begin
        Form.Top := p.FLastBoundsRect.Top;
        Form.Left := p.FLastBoundsRect.Left;
      end;
    end;
  end;
end;

function ShowModalForm(Form: TForm): Integer;
begin
  Result := Form.ShowModal;
  SaveFormPosition(Form);
end;

procedure AutoSizeListViewColumn(ListView: TListView; ColumnIndex: Integer);
var
  w, i: Integer;
begin
  w := 0;
  for i := 0 to ListView.Columns.Count-1 do
  begin
    if i <> ColumnIndex then
      Inc(w, ListView.Columns[i].Width);
  end;
  ListView.Columns[ColumnIndex].Width := Max(
    ListView.Width - GetSystemMetrics(SM_CXVSCROLL) - 2*GetSystemMetrics(SM_CXEDGE) - w,
    ListView.Columns[ColumnIndex].MinWidth
  );
end;

procedure AutoSizeListViewColumns(ListView: TListView);
var
  i, w, Padding: Integer;
  c: TListColumn;
begin
  ListView.Columns.Add; { Create a temporary column to prevent the rightmost
    of the existing columns from occupying the entire remaining width. }
  try
    Padding := ListView.Canvas.TextWidth(StringOfChar('_', 6));
    for i := 0 to ListView.Columns.Count-2 do
    begin
      c := ListView.Columns[i];
      c.AutoSize := TRUE;
      w := c.Width;
      c.AutoSize := FALSE;
      c.Width := w + Padding;
    end;
  finally
    ListView.Columns.Delete(ListView.Columns.Count-1);
  end;
end;

function GetAncestorOfType(Control: TControl; AncestorClass: TWinControlClass): TWinControl;
begin
  Result := Control.Parent;
  while (Result <> nil) and not (Result is AncestorClass) do
    Result := Result.Parent;
end;

type
  TScrollHelper = class
    class procedure ScrollBoxResize(Sender: TObject);
    class procedure ScrollBoxMouseWheel(Sender: TObject;
      Shift: TShiftState; WheelDelta: Integer; MousePos: TPoint;
      var Handled: Boolean);
    class procedure ContentResize(Sender: TObject);
  end;

procedure EnsureScrollPositionCorrect(ScrollBar: TControlScrollBar);
begin
  if ScrollBar.Page >= ScrollBar.Range then
    ScrollBar.Position := 0;
end;

class procedure TScrollHelper.ScrollBoxResize(Sender: TObject);
var
  s: TScrollBox;
  c: TControl;
begin
  s := Sender as TScrollBox;
  if s.ControlCount = 1 then
  begin
    c := s.Controls[0];
    c.Constraints.MaxWidth := Max(s.Width - GetSystemMetrics(SM_CXVSCROLL), 0);
    s.VertScrollBar.Page := s.ClientHeight;
    EnsureScrollPositionCorrect(s.VertScrollBar);
  end;
end;

class procedure TScrollHelper.ScrollBoxMouseWheel(Sender: TObject;
  Shift: TShiftState; WheelDelta: Integer; MousePos: TPoint;
  var Handled: Boolean);
begin
  ScrollVerticallyByWheel((Sender as TScrollBox), WheelDelta);
  Handled := TRUE;
end;

class procedure TScrollHelper.ContentResize(Sender: TObject);
var
  s: TScrollBox;
  c: TControl;
begin
  c := Sender as TControl;
  if (c.Parent is TScrollBox) and (c.Parent.ControlCount = 1) then
  begin
    s := TScrollBox(c.Parent);
    s.VertScrollBar.Range := c.Height;
    EnsureScrollPositionCorrect(s.VertScrollBar);
  end;
end;

procedure MakeVerticallyAutoScrollable(ScrollBox: TScrollBox);
var
  c: TControl;
begin
  Assert( not ScrollBox.AutoScroll );
  Assert( not Assigned(ScrollBox.OnChangeBounds) );
  Assert( ScrollBox.ControlCount = 1 );
  c := ScrollBox.Controls[0];
  Assert( not Assigned(c.OnResize) );

  ScrollBox.OnChangeBounds := TScrollHelper.ScrollBoxResize;
  ScrollBox.VertScrollBar.Tracking := TRUE;
  c.OnResize := TScrollHelper.ContentResize;

  TScrollHelper.ScrollBoxResize(ScrollBox);

  if not Assigned(ScrollBox.OnMouseWheel) then
    ScrollBox.OnMouseWheel := TScrollHelper.ScrollBoxMouseWheel;
end;

procedure ScrollControlPartIntoView(Control: TControl; PartTop,
  PartHeight: Integer);
var
  s: TScrollingWinControl;
  y1, y2, t: Integer;
  InView: Boolean;
begin
  s := TScrollingWinControl(GetAncestorOfType(Control, TScrollBox));
  if s <> nil then
  begin
    y1 := s.VertScrollBar.Position;
    y2 := y1 + s.ClientHeight;
    t := s.ScreenToClient(Control.ControlOrigin).y + PartTop;

    InView := (t >= y1) and ((t + PartHeight) <= y2);
    if not InView then
      s.VertScrollBar.Position := t - (s.ClientHeight - PartHeight) div 2;
  end;
end;

procedure ScrollControlIntoView(Control: TControl);
begin
  ScrollControlPartIntoView(Control, 0, Control.Height);
end;

procedure LockRedraw(Control: TWinControl);
begin
  SendMessage(Control.Handle, WM_SETREDRAW, 0, 0);
end;

procedure UnlockRedraw(Control: TWinControl);
begin
  SendMessage(Control.Handle, WM_SETREDRAW, 1, 0);
  Control.Invalidate;
end;

function FindParentForm(Control: TControl): TCustomForm;
begin
  Result := GetParentForm(Control);
  if Result = nil then
    raise Exception.CreateFmt('Control "%s" ("%s") does not have a parent form.', [Control.ClassName, Control.Name]);
end;

function LoadPngResource(const ResourceName: String): TPngImage;
var
  Stream: TResourceStream;
begin
  Result := TPngImage.Create;
  try
    Stream := TResourceStream.Create(HINSTANCE, ResourceName, RT_RCDATA);
    try
      Result.LoadFromStream(Stream);
    finally
      Stream.Free;
    end;
  except
    Result.Free;
    raise;
  end;
end;

procedure LoadActionImages(ActionList: TActionList);
var
  a: TContainedAction;
  Action: TAction;
begin
  for a in ActionList do
  begin
    Action := a as TAction;
    if Action.HelpKeyword <> '' then
      Action.ImageIndex := AddPngResourceToImageList(Action.HelpKeyword, ActionList.Images);
  end;
end;

function AddPngResourceToImageList(const ResourceName: String; ImageList: TCustomImageList): Integer;
var
  Png: TPngImage;
begin
  Png := LoadPngResource(ResourceName);
  try
    Result := ImageList.Add(Png, nil);
  finally
    Png.Free;
  end;
end;

procedure LoadPngResourceToPicture(const ResourceName: String; Target: TPicture);
var
  Png: TPngImage;
begin
  Png := LoadPngResource(ResourceName);
  try
    Target.Graphic := Png;
  finally
    Png.Free;
  end;
end;

procedure InitializeProgram;
begin
  Application.HintHidePause := 120000;
  Application.HintHidePausePerChar := 0;
  Application.UpdateFormatSettings := FALSE;
  FormatSettings.DecimalSeparator := '.';
  FormatSettings.ThousandSeparator := ' ';
end;

procedure SetFixedWidthConstraint(Control: TControl; Width: Integer);
begin
  Control.Constraints.MinWidth := Width;
  Control.Constraints.MaxWidth := Width;
end;

procedure SetFixedHeightConstraint(Control: TControl; Height: Integer);
begin
  Control.Constraints.MinHeight := Height;
  Control.Constraints.MaxHeight := Height;
end;

procedure SetFixedSizeConstraints(Control: TControl; Width, Height: Integer);
begin
  SetFixedWidthConstraint(Control, Width);
  SetFixedHeightConstraint(Control, Height);
end;

type
  THackedWinControl = class(TWinControl) end;

procedure SelectFirstChild(Parent: TWinControl);
begin
  THackedWinControl(Parent).SelectFirst;
end;

function DragScrollVertically(Control: TControl): Integer;
{ Returns amount scrolled in pixels. }
var
  p: TPoint;
  Scrollable: TScrollingWinControl;
  d, t, y1, ScrollableScreenTop, ScrollableScreenBottom, OldPosition: Integer;
begin
  Scrollable := TScrollingWinControl(GetAncestorOfType(Control, TScrollBox));
  {$HINTS OFF}
  if (Scrollable <> nil) and GetCursorPos(p) then
  {$HINTS ON}
  begin
    ScrollableScreenTop := Scrollable.ControlOrigin.y;
    ScrollableScreenBottom := ScrollableScreenTop + Scrollable.Height;
    y1 := Scrollable.VertScrollBar.Position;
    t := Scrollable.ScreenToClient(Control.ControlOrigin).y;

    if (t < y1) and (p.y < ScrollableScreenTop) then
      d := p.y - ScrollableScreenTop
    else if ((t + Control.Height) > (y1 + Scrollable.ClientHeight)) and (p.y > ScrollableScreenBottom) then
      d := p.y - ScrollableScreenBottom
    else
      d := 0;

    if d = 0 then
      Result := 0
    else
    begin
      OldPosition := Scrollable.VertScrollBar.Position;
      Scrollable.VertScrollBar.Position := Scrollable.VertScrollBar.Position
        + Sign(d) * Min(Abs(d), 80);
      Result := Scrollable.VertScrollBar.Position - OldPosition;
    end;
  end
  else
    Result := 0;
end;

function GetPreferredControlSize(Control: TControl): TSize;
begin
  Result.cx := 0;
  Result.cy := 0;
  Control.GetPreferredSize(Result.cx, Result.cy);
end;

function ScalePixels(Value: Integer): Integer;
begin
  Result := ScaleX(Value, 96);
end;

procedure SetFontSize(const Controls: array of TControl; FontSize: Integer);
var
  c: TControl;
begin
  for c in Controls do
    c.Font.Size := FontSize;
end;

procedure SetFontColor(const Controls: array of TControl; FontColor: TColor);
var
  c: TControl;
begin
  for c in Controls do
    c.Font.Color := FontColor;
end;

procedure StretchToWidest(const Controls: array of TControl);
var
  w: Integer;
  c: TControl;
begin
  w := 0;
  for c in Controls do
    w := Max(w, GetPreferredControlSize(c).cx);
  for c in Controls do
    SetFixedWidthConstraint(c, w);
end;

procedure SetRightBorderSpacing(const Controls: array of TControl;
  LogicalPixels: Integer);
var
  c: TControl;
  p: Integer;
begin
  p := ScalePixels(LogicalPixels);
  for c in Controls do
    c.BorderSpacing.Right := p;
end;

function HasVisibleChildControls(Parent: TWinControl): Boolean;
var
  i: Integer;
begin
  Result := FALSE;
  for i := 0 to Parent.ControlCount-1 do
    if Parent.Controls[i].Visible then
    begin
      Result := TRUE;
      Break;
    end;
end;

procedure DisplayListViewSortIndicator(ListView: TListView; ColumnIndex: Integer;
  SortBackwards: Boolean);
var
  i: Integer;
  h: HWND;
  Item: HDITEM;
begin
  h := ListView_GetHeader(ListView.Handle);
  for i := 0 to ListView.Columns.Count-1 do
  begin
    {$HINTS OFF}
    FillChar(Item, SizeOf(Item), 0);
    {$HINTS ON}
    Item.mask := HDI_FORMAT;
    Header_GetItem(h, i, Item);
    Item.fmt := Item.fmt and not (HDF_SORTUP or HDF_SORTDOWN);
    if i = ColumnIndex then
    begin
      if SortBackwards then
        Item.fmt := Item.fmt or HDF_SORTDOWN
      else
        Item.fmt := Item.fmt or HDF_SORTUP;
    end;
    Header_SetItem(h, i, Item);
  end;
end;

procedure MakeListViewDoubleBuffered(ListView: TListView);
begin
  ListView_SetExtendedListViewStyleEx(ListView.Handle,
    LVS_EX_DOUBLEBUFFER, LVS_EX_DOUBLEBUFFER);
end;

function IsVerticalScrollBarVisible(Control: TScrollingWinControl): Boolean;
begin
  Result := GetWindowLong(Control.Handle, GWL_STYLE) and WS_VSCROLL <> 0;
end;

function ScrollVerticallyByWheel(Control: TScrollingWinControl;
  WheelDelta: Integer): Integer;
{ Returns amount scrolled in pixels. }
var
  Old: Integer;
begin
  if IsVerticalScrollBarVisible(Control) then
  begin
    Old := Control.VertScrollBar.Position;
    Control.VertScrollBar.Position := Old - WheelDelta div 4;
    Result := Control.VertScrollBar.Position - Old;
  end
  else
    Result := 0;
end;

finalization

  FreeAndNil(SavedFormPositions);

end.
