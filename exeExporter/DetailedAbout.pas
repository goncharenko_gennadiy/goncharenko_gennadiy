{
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
}

unit DetailedAbout;

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  ExtCtrls, VisualUtils, Windows;

type
  TDetailedAboutForm = class(TForm)
    btnClose: TButton;
    memAbout: TMemo;
    pnlBottom: TPanel;
  private
    procedure Prepare;
    { private declarations }
  public
    class procedure OpenModal;
    { public declarations }
  end;

implementation

{$R *.lfm}
{$R *.rc}

{ TDetailedAboutForm }

procedure TDetailedAboutForm.Prepare;
var
  Stream: TResourceStream;
begin
  Stream := TResourceStream.Create(HINSTANCE, 'DetailedAbout/CREDITS', RT_RCDATA);
  try
    memAbout.Lines.LoadFromStream(Stream);
  finally
    Stream.Free;
  end;
end;

class procedure TDetailedAboutForm.OpenModal;
var
  Form: TDetailedAboutForm;
begin
  Form := TDetailedAboutForm.Create(nil);
  try
    Form.Width := ScalePixels(600);
    Form.Height := ScalePixels(400);
    RestoreFormPosition(Form);

    Form.Prepare;
    ShowModalForm(Form);
  finally
    Form.Free;
  end;
end;

end.
