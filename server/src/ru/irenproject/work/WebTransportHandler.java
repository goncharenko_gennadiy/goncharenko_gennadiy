/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.work;

import ru.irenproject.BadInputException;
import ru.irenproject.Check;
import ru.irenproject.ClosingChannelException;
import ru.irenproject.Utils;
import ru.irenproject.common.Proto.ClientMessage;
import ru.irenproject.common.Proto.ServerMessage;

import com.google.common.base.Ascii;
import com.google.common.collect.ImmutableMap;
import com.google.common.net.MediaType;
import com.google.inject.assistedinject.Assisted;
import com.google.protobuf.InvalidProtocolBufferException;
import io.netty.buffer.ByteBufInputStream;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelDuplexHandler;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPromise;
import io.netty.handler.codec.http.DefaultFullHttpResponse;
import io.netty.handler.codec.http.DefaultHttpResponse;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.HttpHeaders;
import io.netty.handler.codec.http.HttpMethod;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.netty.handler.codec.http.HttpVersion;
import io.netty.handler.codec.http.LastHttpContent;
import io.netty.handler.codec.http.websocketx.BinaryWebSocketFrame;
import io.netty.handler.codec.http.websocketx.CloseWebSocketFrame;
import io.netty.handler.codec.http.websocketx.PingWebSocketFrame;
import io.netty.handler.codec.http.websocketx.PongWebSocketFrame;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import io.netty.handler.codec.http.websocketx.WebSocketServerHandshaker13;
import io.netty.handler.stream.ChunkedStream;
import io.netty.handler.stream.ChunkedWriteHandler;
import io.netty.util.ReferenceCountUtil;
import org.apache.commons.lang3.StringUtils;

import javax.inject.Inject;
import java.io.IOException;

public final class WebTransportHandler extends ChannelDuplexHandler {
  public interface Factory {
    WebTransportHandler create(ResourceDepot resourceDepot);
  }

  public static final int MAX_FRAME_SIZE = 32_000_000;

  private static final ImmutableMap<String, MediaType> MEDIA_TYPES = ImmutableMap.of(
      "html", MediaType.HTML_UTF_8,
      "css", MediaType.CSS_UTF_8,
      "js", MediaType.JAVASCRIPT_UTF_8);

  private final ResourceDepot fResourceDepot;
  private final ProtoExtensionRegistry fProtoExtensionRegistry;

  private boolean fChunkedWriteHandlerAdded;
  private boolean fWebSocketUpgradeReceived;
  private boolean fCloseFrameSent;

  @Inject private WebTransportHandler(
      @Assisted ResourceDepot resourceDepot,
      ProtoExtensionRegistry protoExtensionRegistry) {
    fResourceDepot = resourceDepot;
    fProtoExtensionRegistry = protoExtensionRegistry;
  }

  @Override public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
    boolean release = true;
    try {
      if (msg instanceof FullHttpRequest) {
        handleHttpRequest((FullHttpRequest) msg, ctx);
      } else if (msg instanceof BinaryWebSocketFrame) {
        handleBinaryFrame((BinaryWebSocketFrame) msg, ctx);
      } else if (msg instanceof TextWebSocketFrame) {
        throw new BadInputException();
      } else if (msg instanceof CloseWebSocketFrame) {
        handleCloseFrame(ctx);
      } else if (msg instanceof PingWebSocketFrame) {
        handlePingFrame((PingWebSocketFrame) msg, ctx);
      } else if (msg instanceof PongWebSocketFrame) {
        // do nothing
      } else {
        release = false;
        super.channelRead(ctx, msg);
      }
    } finally {
      if (release) {
        ReferenceCountUtil.release(msg);
      }
    }
  }

  private void handleHttpRequest(FullHttpRequest request, ChannelHandlerContext ctx) {
    Check.input(!fWebSocketUpgradeReceived);

    Check.input(request.getDecoderResult().isSuccess());
    Check.input(request.getMethod().equals(HttpMethod.GET));
    Check.input(request.getProtocolVersion().equals(HttpVersion.HTTP_1_1));

    String upgrade = request.headers().get(HttpHeaders.Names.UPGRADE);
    if ((upgrade != null) && Ascii.equalsIgnoreCase(upgrade, "websocket")) {
      fWebSocketUpgradeReceived = true;
      new WebSocketServerHandshaker13(null, "", false, MAX_FRAME_SIZE)
          .handshake(ctx.channel(), request)
          .addListener(Utils.CLOSE_ON_ERROR);
    } else {
      String resourceName = request.getUri().equals("/") ?
          "index.html" : StringUtils.removeStart(request.getUri(), "/");
      ResourceDepot.Resource resource = fResourceDepot.getIfExists(resourceName);

      if (resource == null) {
        sendNotFoundResponse(ctx);
      } else if (resource.etag().equals(request.headers().get(HttpHeaders.Names.IF_NONE_MATCH))) {
        sendNotModifiedResponse(resource.etag(), ctx);
      } else {
        DefaultHttpResponse response = new DefaultHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.OK);
        response.headers().set(HttpHeaders.Names.CONTENT_ENCODING, HttpHeaders.Values.GZIP);
        response.headers().set(HttpHeaders.Names.CONTENT_LENGTH, resource.gzippedData().size());

        MediaType mediaType = MEDIA_TYPES.get(StringUtils.substringAfterLast(resourceName, "."));
        if (mediaType != null) {
          response.headers().set(HttpHeaders.Names.CONTENT_TYPE, mediaType);
        }

        response.headers().set(HttpHeaders.Names.ETAG, resource.etag());

        if (!fChunkedWriteHandlerAdded) {
          ctx.pipeline().addBefore(ctx.name(), "chunkedWriteHandler", new ChunkedWriteHandler());
          fChunkedWriteHandlerAdded = true;
        }

        ctx.write(response).addListener(Utils.CLOSE_ON_ERROR);
        ctx.write(new ChunkedStream(resource.gzippedData().newInput())).addListener(Utils.CLOSE_ON_ERROR);
        ctx.writeAndFlush(LastHttpContent.EMPTY_LAST_CONTENT).addListener(Utils.CLOSE_ON_ERROR);
      }
    }
  }

  private static void sendNotFoundResponse(ChannelHandlerContext ctx) {
    DefaultFullHttpResponse r = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.NOT_FOUND);
    r.headers().set(HttpHeaders.Names.CONTENT_LENGTH, 0);

    ctx.writeAndFlush(r).addListener(Utils.CLOSE_ON_ERROR);
  }

  private static void sendNotModifiedResponse(String etag, ChannelHandlerContext ctx) {
    DefaultFullHttpResponse r = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.NOT_MODIFIED);
    r.headers().set(HttpHeaders.Names.ETAG, etag);

    ctx.writeAndFlush(r).addListener(Utils.CLOSE_ON_ERROR);
  }

  private void handleBinaryFrame(BinaryWebSocketFrame frame, ChannelHandlerContext ctx) {
    try {
      ClientMessage m;
      try {
        m = ClientMessage.parseFrom(new ByteBufInputStream(frame.content()), fProtoExtensionRegistry.get());
      } catch (InvalidProtocolBufferException e) {
        throw new BadInputException(e);
      }

      ctx.fireChannelRead(m);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  private void handleCloseFrame(ChannelHandlerContext ctx) {
    ctx.writeAndFlush(new CloseWebSocketFrame()).addListener(ChannelFutureListener.CLOSE);
    fCloseFrameSent = true;
  }

  private void handlePingFrame(PingWebSocketFrame frame, ChannelHandlerContext ctx) {
    PongWebSocketFrame reply = new PongWebSocketFrame(frame.content().retain());
    ctx.writeAndFlush(reply).addListener(Utils.CLOSE_ON_ERROR);
  }

  @Override public void write(ChannelHandlerContext ctx, Object msg, ChannelPromise promise) throws Exception {
    if (fCloseFrameSent) {
      ReferenceCountUtil.release(msg);
      promise.setFailure(new ClosingChannelException());
    } else if (msg instanceof ServerMessage) {
      ctx.write(new BinaryWebSocketFrame(Unpooled.wrappedBuffer(((ServerMessage) msg).toByteArray())), promise);
    } else {
      super.write(ctx, msg, promise);
    }
  }
}
