/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.work;

import com.google.protobuf.ExtensionRegistry;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton public final class ProtoExtensionRegistry {
  private final ExtensionRegistry fRegistry;

  @SuppressWarnings("UnnecessaryFullyQualifiedName")
  @Inject private ProtoExtensionRegistry() {
    ExtensionRegistry r = ExtensionRegistry.newInstance();

    ru.irenproject.common.Proto.registerAllExtensions(r);

    ru.irenproject.common.select.Proto.registerAllExtensions(r);
    ru.irenproject.common.input.Proto.registerAllExtensions(r);
    ru.irenproject.common.match.Proto.registerAllExtensions(r);
    ru.irenproject.common.order.Proto.registerAllExtensions(r);
    ru.irenproject.common.classify.Proto.registerAllExtensions(r);

    ru.irenproject.live.selectQuestion.Proto.registerAllExtensions(r);
    ru.irenproject.live.inputQuestion.Proto.registerAllExtensions(r);
    ru.irenproject.live.matchQuestion.Proto.registerAllExtensions(r);
    ru.irenproject.live.orderQuestion.Proto.registerAllExtensions(r);
    ru.irenproject.live.classifyQuestion.Proto.registerAllExtensions(r);

    fRegistry = r.getUnmodifiable();
  }

  public ExtensionRegistry get() {
    return fRegistry;
  }
}
