/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.work;

import ru.irenproject.Check;
import ru.irenproject.common.Proto.ClientMessage;
import ru.irenproject.common.Proto.CreateWork;
import ru.irenproject.common.Proto.CreateWorkComplete;
import ru.irenproject.common.Proto.ServerMessage;
import ru.irenproject.common.Proto.ServerMessageType;
import ru.irenproject.itx.ItxInputException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.io.Closeable;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Locale;
import java.util.concurrent.Executor;

@Singleton public final class GlobalProcessor {
  private static final Logger fLogger = LoggerFactory.getLogger(GlobalProcessor.class);

  private final WorkManager fWorkManager;
  private final Executor fExecutor;

  @Inject private GlobalProcessor(WorkManager workManager, WorkDispatcher workDispatcher) {
    fWorkManager = workManager;
    fExecutor = workDispatcher.executor();
  }

  public void process(ClientMessage message, Connection connection) {
    fExecutor.execute(() -> {
      boolean ok = false;
      try {
        handleMessage(message, connection);
        ok = true;
      } catch (RuntimeException e) {
        fLogger.warn("", e);
      } finally {
        if (!ok) {
          connection.close();
        }
      }
    });
  }

  private void handleMessage(ClientMessage m, Connection connection) {
    //noinspection EnumSwitchStatementWhichMissesCases
    switch (m.getType()) {
      case CREATE_WORK: {
        Check.input(m.hasCreateWork());
        handleCreateWork(m.getCreateWork(), connection);
        break;
      }
      default: {
        throw new RuntimeException();
      }
    }
  }

  private void handleCreateWork(CreateWork m, Connection connection) {
    try {
      Check.input(connection.supervising());

      CreateWorkComplete.Result result;
      Path temp = Files.createTempFile(getClass().getName() + ".", "");
      try (Closeable ignored = () -> Files.delete(temp)) {
        try (OutputStream out = Files.newOutputStream(temp)) {
          m.getTest().writeTo(out);
        }

        try {
          fWorkManager.createWork(temp, m.getTitle(), Locale.forLanguageTag(m.getLanguage()));
          result = CreateWorkComplete.Result.OK;
        } catch (ItxInputException ignore) {
          result = CreateWorkComplete.Result.INCORRECT_TEST_FILE;
        }
      }

      connection.send(ServerMessage.newBuilder()
          .setType(ServerMessageType.CREATE_WORK_COMPLETE)
          .setCreateWorkComplete(CreateWorkComplete.newBuilder()
              .setResult(result))
          .build());
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }
}
