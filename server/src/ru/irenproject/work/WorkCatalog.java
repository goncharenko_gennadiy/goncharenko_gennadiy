/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.work;

import ru.irenproject.DataDirectory;
import ru.irenproject.Utils;
import ru.irenproject.common.Proto.WorkDescriptor;

import com.google.protobuf.ByteString;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import javax.inject.Inject;
import javax.inject.Singleton;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;

@Singleton public final class WorkCatalog {
  public interface Listener {
    void changed();
  }

  private static final Logger fLogger = LoggerFactory.getLogger(WorkCatalog.class);

  private static final String TITLE_FILE = "title";
  private static final String LANGUAGE_FILE = "language";
  private static final String AVAILABLE_FILE = "available";

  private final ConcurrentSkipListMap<ByteString, Work> fWorks = new ConcurrentSkipListMap<>(Ids.COMPARATOR);
  private final CopyOnWriteArrayList<Listener> fListeners = new CopyOnWriteArrayList<>();
  private final Object fChangeAndListenSerializer = new Object();

  @Inject private WorkCatalog() {
    load();
  }

  private void load() {
    try {
      for (Path dir : Utils.listDirectory(workDir())) {
        if (Files.isDirectory(dir)) {
          ByteString id = Ids.fromHexString(dir.getFileName().toString());

          if (Files.exists(dir.resolve(AVAILABLE_FILE))) {
            String title = new String(Files.readAllBytes(dir.resolve(TITLE_FILE)), StandardCharsets.UTF_8);
            Locale locale = Locale.forLanguageTag(
                new String(Files.readAllBytes(dir.resolve(LANGUAGE_FILE)), StandardCharsets.UTF_8));
            fWorks.put(id, new Work(id, title, locale));
          } else {
            WorkManager.deleteWorkDirectory(dir);
          }
        }
      }
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  private Path workDir() {
    return DataDirectory.get().resolve("works");
  }

  public @Nullable Work getIfExists(ByteString workId) {
    return fWorks.get(workId);
  }

  public Path getDirectory(ByteString workId) {
    return workDir().resolve(Ids.toHexString(workId));
  }

  public void write(Work work) {
    try {
      Path dir = getDirectory(work.id());
      Files.write(dir.resolve(TITLE_FILE), work.title().getBytes(StandardCharsets.UTF_8));
      Files.write(dir.resolve(LANGUAGE_FILE), work.locale().toLanguageTag().getBytes(StandardCharsets.UTF_8));
      Files.createFile(dir.resolve(AVAILABLE_FILE));
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  public void markForDeletion(Work work) {
    try {
      Files.deleteIfExists(getDirectory(work.id()).resolve(AVAILABLE_FILE));
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  public void add(Work work) {
    synchronized (fChangeAndListenSerializer) {
      fWorks.put(work.id(), work);
    }
    triggerChanged();
  }

  public void remove(Work work) {
    boolean removed;
    synchronized (fChangeAndListenSerializer) {
      removed = (fWorks.remove(work.id()) != null);
    }

    if (removed) {
      triggerChanged();
    }
  }

  private void triggerChanged() {
    for (Listener listener : fListeners) {
      try {
        listener.changed();
      } catch (RuntimeException e) {
        fLogger.warn("", e);
      }
    }
  }

  public void addListener(Listener listener) {
    synchronized (fChangeAndListenSerializer) {
      fListeners.add(listener);
    }
  }

  public void removeListener(Listener listener) {
    fListeners.remove(listener);
  }

  public List<WorkDescriptor> descriptors() {
    return fWorks.values().stream()
        .map(work -> WorkDescriptor.newBuilder()
            .setId(Ids.toHexString(work.id()))
            .setTitle(work.title())
            .build())
        .collect(Collectors.toList());
  }

  public List<Work> works() {
    return new ArrayList<>(fWorks.values());
  }
}
