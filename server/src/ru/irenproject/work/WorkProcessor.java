/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.work;

import ru.irenproject.BadInputException;
import ru.irenproject.Check;
import ru.irenproject.Modifier;
import ru.irenproject.ModifierList;
import ru.irenproject.Oops;
import ru.irenproject.Question;
import ru.irenproject.QuestionItem;
import ru.irenproject.Section;
import ru.irenproject.common.Proto.ChooseQuestion;
import ru.irenproject.common.Proto.ClientMessage;
import ru.irenproject.common.Proto.DialogResponse;
import ru.irenproject.common.Proto.LogIn;
import ru.irenproject.common.Proto.LogInFailed;
import ru.irenproject.common.Proto.MainScreenTurnOn;
import ru.irenproject.common.Proto.QueryQuestionDetails;
import ru.irenproject.common.Proto.QuestionDescriptor;
import ru.irenproject.common.Proto.QuestionStatus;
import ru.irenproject.common.Proto.SelectWork;
import ru.irenproject.common.Proto.ServerMessage;
import ru.irenproject.common.Proto.ServerMessageType;
import ru.irenproject.common.Proto.ServerTime;
import ru.irenproject.common.Proto.SessionKey;
import ru.irenproject.common.Proto.SessionScore;
import ru.irenproject.common.Proto.SetResponse;
import ru.irenproject.common.Proto.ShowDialog;
import ru.irenproject.common.Proto.ShowLoginScreen;
import ru.irenproject.common.Proto.ShowQuestionDetails;
import ru.irenproject.common.Proto.ShowQuestionStatus;
import ru.irenproject.common.Proto.ShowRemainingTime;
import ru.irenproject.common.Proto.ShowScore;
import ru.irenproject.common.Proto.ShowWatchedQuestion;
import ru.irenproject.common.Proto.ShowWatchedSession;
import ru.irenproject.common.Proto.SubmitOk;
import ru.irenproject.common.Proto.UpdateWatchedQuestion;
import ru.irenproject.common.Proto.WatchQuestion;
import ru.irenproject.common.Proto.WatchSession;
import ru.irenproject.live.IssueOrderList;
import ru.irenproject.live.LiveQuestion;
import ru.irenproject.live.LiveQuestionLoader;
import ru.irenproject.live.ModifierApplier;
import ru.irenproject.live.Proto._LiveQuestion;
import ru.irenproject.live.QuestionSpawner;
import ru.irenproject.live.SetLocaleIssueOrder;
import ru.irenproject.live.SetWeightIssueOrder;
import ru.irenproject.profile.MarkScale;
import ru.irenproject.profile.Profile;
import ru.irenproject.profile.Proto.AvailableQuestionScore;
import ru.irenproject.profile.Proto.AvailableScore;
import ru.irenproject.work.Proto.Session;
import ru.irenproject.work.Proto.SessionQuestion;

import com.almworks.sqlite4java.SQLiteException;
import com.almworks.sqlite4java.SQLiteStatement;
import com.google.common.base.CharMatcher;
import com.google.common.collect.ImmutableMap;
import com.google.common.io.BaseEncoding;
import com.google.common.math.IntMath;
import com.google.common.primitives.Ints;
import com.google.inject.assistedinject.Assisted;
import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import com.ibm.icu.text.Normalizer2;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import javax.inject.Inject;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public final class WorkProcessor {
  public interface Factory {
    WorkProcessor create(Work work);
  }

  public interface Listener {
    void responseSubmitted(long sessionId, int questionIndex, SessionQuestion sessionQuestion, DialogResponse response);
    void scoreChanged(long sessionId, Session session);
  }

  private static final int MAX_USER_NAME_LENGTH = 100;
  private static final CharMatcher USER_NAME_CHAR_MATCHER = new UserNameCharMatcher().precomputed();
  private static final int SESSION_KEY_SIZE = 32;
  private static final int SCALED_ONE = Ints.checkedCast(scaleScore(BigDecimal.ONE));

  public static QuestionStatus getQuestionStatus(SessionQuestion q, boolean revealCorrectness) {
    QuestionStatus res;

    if (q.hasScaledResult()) {
      if (revealCorrectness) {
        if (q.getScaledResult() == 0) {
          res = QuestionStatus.INCORRECT;
        } else if (q.getScaledResult() == SCALED_ONE) {
          res = QuestionStatus.CORRECT;
        } else {
          res = QuestionStatus.PARTIALLY_CORRECT;
        }
      } else {
        res = QuestionStatus.ANSWERED;
      }
    } else {
      res = QuestionStatus.UNANSWERED;
    }

    return res;
  }

  public static UpdateWatchedQuestion buildWatchedQuestionUpdate(SessionQuestion q, DialogResponse response) {
    return UpdateWatchedQuestion.newBuilder()
        .setResponse(response)
        .setScaledResult(q.hasScaledResult() ? q.getScaledResult() : 0)
        .setScaledScore(getScaledScore(q))
        .build();
  }

  public static SessionScore getSessionScore(long sessionId, Session session, MarkScale markScale) {
    BigDecimal sessionResult = computeSessionResult(session);
    SessionScore.Builder b = SessionScore.newBuilder()
        .setId(sessionId)
        .setUserName(session.getUserName())
        .setScaledScore(session.getScaledScore())
        .setScaledResult(Ints.checkedCast(scaleScore(sessionResult)))
        .setTotalAchievableScore(session.getTotalAchievableScore())
        .setCurrentAchievableScore(session.getCurrentAchievableScore())
        .setFinished(session.getFinished())
        .setMark(markScale.getTitleForResult(sessionResult));
    if (session.hasDeadline()) {
      b.setDeadlineMilliseconds(session.getDeadline());
    }
    return b.build();
  }

  private static String sessionKeyToHexString(ByteString sessionKey) {
    return BaseEncoding.base16().lowerCase().encode(sessionKey.toByteArray());
  }

  private static ByteString hexStringToSessionKey(String s) {
    byte[] ba;
    try {
      ba = BaseEncoding.base16().lowerCase().decode(s);
    } catch (IllegalArgumentException e) {
      throw new BadInputException(e);
    }

    Check.input(ba.length == SESSION_KEY_SIZE);
    return ByteString.copyFrom(ba);
  }

  private final Work fWork;

  private final ImmutableMap<Class<? extends Question>, QuestionSpawner> fQuestionSpawners;
  private final ImmutableMap<Class<? extends Modifier>, ModifierApplier> fModifierAppliers;
  private final LiveQuestionLoader fLiveQuestionLoader;
  private final WorkDispatcher fWorkDispatcher;
  private final WorkManager fWorkManager;
  private final ProtoExtensionRegistry fProtoExtensionRegistry;

  private final ThreadLocal<SecureRandom> fSecureRandom = ThreadLocal.withInitial(SecureRandom::new);
  private final Logger fLogger;
  private final SequentialExecutor fSequentialExecutor;
  private final CopyOnWriteArrayList<Listener> fListeners = new CopyOnWriteArrayList<>();

  @Inject private WorkProcessor(
      @Assisted Work work,
      Map<Class<? extends Question>, QuestionSpawner> questionSpawners,
      Map<Class<? extends Modifier>, ModifierApplier> modifierAppliers,
      LiveQuestionLoader liveQuestionLoader,
      WorkDispatcher workDispatcher,
      WorkManager workManager,
      ProtoExtensionRegistry protoExtensionRegistry) {
    fWork = work;
    fQuestionSpawners = ImmutableMap.copyOf(questionSpawners);
    fModifierAppliers = ImmutableMap.copyOf(modifierAppliers);
    fLiveQuestionLoader = liveQuestionLoader;
    fWorkDispatcher = workDispatcher;
    fWorkManager = workManager;
    fProtoExtensionRegistry = protoExtensionRegistry;

    fLogger = LoggerFactory.getLogger(String.format("%s.%s", getClass().getName(), Ids.toHexString(fWork.id())));
    fSequentialExecutor = new SequentialExecutor(fWorkDispatcher.executor());
  }

  public void process(ClientMessage message, Connection connection) {
    executeConcurrently(connection, dbr -> handleMessage(message, connection, dbr));
  }

  private void handleMessage(ClientMessage m, Connection connection, DbReader dbr) {
    switch (m.getType()) {
      case SELECT_WORK: {
        Check.input(m.hasSelectWork());
        handleSelectWork(m.getSelectWork(), connection, dbr);
        break;
      }
      case LOG_IN: {
        Check.input(m.hasLogIn());
        handleLogIn(m.getLogIn(), connection, dbr);
        break;
      }
      case SET_RESPONSE: {
        Check.input(m.hasSetResponse());
        handleSetResponse(m.getSetResponse(), connection, dbr);
        break;
      }
      case SUBMIT: {
        handleSubmit(connection);
        break;
      }
      case FINISH_WORK: {
        handleFinishWork(connection);
        break;
      }
      case QUERY_QUESTION_DETAILS: {
        Check.input(m.hasQueryQuestionDetails());
        handleQueryQuestionDetails(m.getQueryQuestionDetails(), connection, dbr);
        break;
      }
      case CHOOSE_QUESTION: {
        Check.input(m.hasChooseQuestion());
        handleChooseQuestion(m.getChooseQuestion(), connection);
        break;
      }
      case WATCH_WORK: {
        handleWatchWork(connection);
        break;
      }
      case WATCH_SESSION: {
        Check.input(m.hasWatchSession());
        handleWatchSession(m.getWatchSession(), connection);
        break;
      }
      case WATCH_QUESTION: {
        Check.input(m.hasWatchQuestion());
        handleWatchQuestion(m.getWatchQuestion(), connection);
        break;
      }
      case DELETE_WORK: {
        handleDeleteWork(connection);
        break;
      }
      case LIST_WORKS:
      case SUPERVISE:
      case CREATE_WORK: {
        throw new RuntimeException();
      }
    }
  }

  private void executeConcurrently(Connection connection, Consumer<DbReader> task) {
    fWorkDispatcher.execute(fWorkDispatcher.executor(), fWork, connection, db -> {
      try (DbReader dbr = db.beginRead()) {
        task.accept(dbr);
        dbr.commit();
      }
    });
  }

  private void executeSequentially(Connection connection, Consumer<Database> task) {
    fWorkDispatcher.execute(fSequentialExecutor, fWork, connection, task);
  }

  // called at most once per connection
  private void handleSelectWork(SelectWork m, Connection connection, DbReader dbr) {
    try {
      Long sessionId = null;

      if (m.hasSessionKey()) {
        ByteString sessionKey = hexStringToSessionKey(m.getSessionKey());
        SQLiteStatement s = dbr.prepare("SELECT ID FROM SESSIONS WHERE KEY = ?1");
        try {
          s.bind(1, sessionKey.toByteArray());
          if (s.step()) {
            sessionId = s.columnLong(0);
          }
        } finally {
          s.dispose();
        }
      }

      if (sessionId == null) {
        connection.allowLogIn();
        connection.send(ServerMessage.newBuilder()
            .setType(ServerMessageType.SHOW_LOGIN_SCREEN)
            .setShowLoginScreen(ShowLoginScreen.newBuilder().setMaxUserNameLength(MAX_USER_NAME_LENGTH))
            .build());
      } else {
        connection.initializeSessionId(sessionId);
        sendInitialSessionMessage(connection, dbr);
      }
    } catch (SQLiteException e) {
      throw new RuntimeException(e);
    }
  }

  private void sendInitialSessionMessage(Connection connection, DbReader dbr) {
    if (isActive(loadSession(connection.sessionId(), dbr))) {
      sendDialog(true, connection, dbr);
    } else {
      sendFinalScore(connection, dbr);
    }
  }

  private void handleLogIn(LogIn m, Connection connection, DbReader dbr) {
    Check.input(connection.logInAllowed());

    String userName;
    try {
      userName = parseUserName(m.getUserName());
    } catch (BadInputException ignored) {
      userName = null;
    }

    if (userName == null) {
      connection.send(ServerMessage.newBuilder()
          .setType(ServerMessageType.LOG_IN_FAILED)
          .setLogInFailed(LogInFailed.newBuilder().setReason(LogInFailed.Reason.INCORRECT_USER_NAME))
          .build());
    } else {
      Check.input(connection.recordLogInAttempt());
      fLogger.info("Login: '{}' {}.", userName, connection);

      String canonicalUserName = canonicalizeUserName(userName);
      Long existingSessionId = getSessionIdByCanonicalUserName(canonicalUserName, dbr);

      if (existingSessionId == null) {
        List<_LiveQuestion> liveQuestions = issueQuestions(dbr);
        if (liveQuestions.isEmpty()) {
          throw new RuntimeException("No questions are available.");
        }

        int totalWeight = 0;
        for (_LiveQuestion lq : liveQuestions) {
          totalWeight = IntMath.checkedAdd(totalWeight, lq.getWeight());
        }

        Session.Builder session = Session.newBuilder()
            .setUserName(userName)
            .setCurrentQuestionIndex(0)
            .setScaledScore(0)
            .setTotalAchievableScore(totalWeight)
            .setCurrentAchievableScore(0)
            .setFinished(false);

        Profile profile = getProfile(dbr);
        if (profile.options().hasDurationMinutes()) {
          session.setDeadline(System.currentTimeMillis() + profile.options().getDurationMinutes()*60_000L);
        }

        executeSequentially(connection, db -> createSession(session, liveQuestions, canonicalUserName, connection, db));
      } else {
        executeSequentially(connection, db -> setNewSessionKey(existingSessionId, connection, db));
      }
    }
  }

  private static String parseUserName(String s) {
    Check.input(s.length() <= MAX_USER_NAME_LENGTH);
    String res = CharMatcher.whitespace().trimAndCollapseFrom(s, ' ');
    Check.input(!res.isEmpty());
    Check.input(USER_NAME_CHAR_MATCHER.matchesAllOf(res));
    return res;
  }

  private static final class UserNameCharMatcher extends CharMatcher {
    @Override public boolean matches(char c) {
      switch (Character.getType(c)) {
        case Character.UPPERCASE_LETTER:
        case Character.LOWERCASE_LETTER:
        case Character.TITLECASE_LETTER:
        case Character.MODIFIER_LETTER:
        case Character.OTHER_LETTER:
        case Character.DECIMAL_DIGIT_NUMBER:
        case Character.NON_SPACING_MARK:
        case Character.COMBINING_SPACING_MARK:
          return true;
        default:
          return c == ' ';
      }
    }
  }

  private static String canonicalizeUserName(String userName) {
    return Normalizer2.getNFKCCasefoldInstance().normalize(userName);
  }

  private @Nullable Long getSessionIdByCanonicalUserName(String canonicalUserName, DbReader dbr) {
    try {
      SQLiteStatement s = dbr.prepare("SELECT ID FROM SESSIONS WHERE CANONICAL_USER_NAME = ?1");
      try {
        s.bind(1, canonicalUserName);
        return s.step() ? s.columnLong(0) : null;
      } finally {
        s.dispose();
      }
    } catch (SQLiteException e) {
      throw new RuntimeException(e);
    }
  }

  private void createSession(Session.Builder sessionBuilder, List<_LiveQuestion> liveQuestions,
      String canonicalUserName, Connection connection, Database db) {
    try {
      long sessionId;
      Session session;
      ByteString key = generateSessionKey();

      try (DbWriter dbw = db.beginWrite()) {
        SQLiteStatement s = dbw.prepare("INSERT INTO SCREENS (LIVE_QUESTION) VALUES (?1)");
        try {
          for (_LiveQuestion lq : liveQuestions) {
            s.reset();
            s.bind(1, lq.toByteArray());
            s.step();
            sessionBuilder.addQuestionBuilder()
                .setScreenId(dbw.connection().getLastInsertId())
                .setWeight(lq.getWeight());
          }
        } finally {
          s.dispose();
        }

        session = sessionBuilder.build();

        s = dbw.prepare("INSERT INTO SESSIONS (DATA, CANONICAL_USER_NAME, KEY) VALUES (?1, ?2, ?3)");
        try {
          s.bind(1, session.toByteArray());
          s.bind(2, canonicalUserName);
          s.bind(3, key.toByteArray());
          s.step();
          sessionId = dbw.connection().getLastInsertId();
        } finally {
          s.dispose();
        }

        dbw.commit();
      }

      connection.initializeSessionId(sessionId);

      triggerScoreChanged(sessionId, session);
      sendSessionKey(key, connection);

      executeConcurrently(connection, dbr -> sendDialog(true, connection, dbr));
    } catch (SQLiteException e) {
      throw new RuntimeException(e);
    }
  }

  private ByteString generateSessionKey() {
    byte[] key = new byte[SESSION_KEY_SIZE];
    fSecureRandom.get().nextBytes(key);
    return ByteString.copyFrom(key);
  }

  private void sendSessionKey(ByteString sessionKey, Connection connection) {
    connection.send(ServerMessage.newBuilder()
        .setType(ServerMessageType.SESSION_KEY)
        .setSessionKey(SessionKey.newBuilder().setSessionKey(sessionKeyToHexString(sessionKey)))
        .build());
  }

  private void setNewSessionKey(long sessionId, Connection connection, Database db) {
    try {
      ByteString key = generateSessionKey();

      try (DbWriter dbw = db.beginWrite()) {
        SQLiteStatement s = dbw.prepare("UPDATE SESSIONS SET KEY = ?1 WHERE ID = ?2 AND KEY IS NULL");
        try {
          s.bind(1, key.toByteArray());
          s.bind(2, sessionId);
          s.step();
          if (dbw.connection().getChanges() != 1) {
            throw new BadInputException(); //TODO inform client
          }
        } finally {
          s.dispose();
        }

        dbw.commit();
      }

      connection.initializeSessionId(sessionId);

      sendSessionKey(key, connection);
      executeConcurrently(connection, dbr -> sendInitialSessionMessage(connection, dbr));
    } catch (SQLiteException e) {
      throw new RuntimeException(e);
    }
  }

  private void sendDialog(boolean firstDialog, Connection connection, DbReader dbr) {
    Session session = loadSession(connection.sessionId(), dbr);

    Profile profile;
    if (firstDialog) {
      profile = getProfile(dbr);
      List<QuestionDescriptor> descriptors = getQuestionDescriptors(
          session, profile.options().getWeightCues(), profile.options().getInstantAnswerCorrectness());

      connection.enqueue(ServerMessage.newBuilder()
          .setType(ServerMessageType.MAIN_SCREEN_TURN_ON)
          .setMainScreenTurnOn(MainScreenTurnOn.newBuilder()
              .addAllQuestionDescriptor(descriptors))
          .build());
    } else {
      profile = null;
    }

    connection.enqueue(buildDialog(session, dbr));

    if (firstDialog && session.hasDeadline()) {
      double millisecondsRemaining = Long.max(session.getDeadline() - System.currentTimeMillis(), 0);

      ShowRemainingTime.Builder b = ShowRemainingTime.newBuilder()
          .setMillisecondsRemaining(millisecondsRemaining);
      if (profile.options().hasDurationMinutes()) {
        b.setMillisecondsTotal(profile.options().getDurationMinutes() * 60_000L);
      }

      connection.enqueue(ServerMessage.newBuilder()
          .setType(ServerMessageType.SHOW_REMAINING_TIME)
          .setShowRemainingTime(b)
          .build());
    }

    connection.flush();
  }

  private ServerMessage buildDialog(Session session, DbReader dbr) {
    Screen screen = loadScreen(getCurrentScreenId(session), dbr);

    return ServerMessage.newBuilder()
        .setType(ServerMessageType.SHOW_DIALOG)
        .setShowDialog(ShowDialog.newBuilder()
            .setDialog(screen.fLiveQuestion.render())
            .setResponse(session.hasNewResponse() ? session.getNewResponse() : screen.userOrEmptyResponse())
            .setCanSubmit(session.hasNewResponse())
            .setQuestionIndex(session.getCurrentQuestionIndex())
            .setReadOnly(getCurrentQuestion(session).hasScaledResult()
                && !getProfile(dbr).options().getEditableAnswers()))
        .build();
  }

  private static long getCurrentScreenId(Session session) {
    return getCurrentQuestion(session).getScreenId();
  }

  private static SessionQuestion getCurrentQuestion(Session session) {
    return session.getQuestion(session.getCurrentQuestionIndex());
  }

  private List<QuestionDescriptor> getQuestionDescriptors(Session session, boolean revealWeight,
      boolean revealCorrectness) {
    return session.getQuestionList().stream()
        .map(q -> QuestionDescriptor.newBuilder()
            .setWeight(revealWeight ? q.getWeight() : 1)
            .setStatus(getQuestionStatus(q, revealCorrectness))
            .build())
        .collect(Collectors.toList());
  }

  private List<_LiveQuestion> issueQuestions(DbReader dbr) {
    Profile profile = getProfile(dbr);

    //TODO
    if (profile.options().hasLabelFilter()) {
      throw new RuntimeException("Labels are not supported yet.");
    }

    ArrayList<_LiveQuestion> res = new ArrayList<>();
    issueFromTree(dbr.source().test().root(), profile, res);

    if (profile.options().getShuffleQuestions()) {
      Collections.shuffle(res, fSecureRandom.get());
    }

    return res;
  }

  private Profile getProfile(DbReader dbr) {
    return dbr.source().test().profileList().profiles().get(0);
  }

  private void issueFromTree(Section section, Profile profile, List<_LiveQuestion> out) {
    ArrayList<QuestionItem> candidates = section.questionList().items().stream()
        .filter(QuestionItem::enabled)
        .collect(Collectors.toCollection(ArrayList::new));
    int candidateCount = candidates.size();

    int questionsToChoose;
    if (profile.options().hasQuestionsPerSection()) {
      questionsToChoose = Integer.min(profile.options().getQuestionsPerSection(), candidateCount);
      for (int i = candidateCount; i > candidateCount - questionsToChoose; --i) {
        Collections.swap(candidates, i - 1, fSecureRandom.get().nextInt(i));
      }
    } else {
      questionsToChoose = candidateCount;
    }

    for (QuestionItem questionItem : candidates.subList(candidateCount - questionsToChoose, candidateCount)) {
      out.add(issueQuestion(questionItem, section, profile));
    }

    for (Section child : section.sections()) {
      issueFromTree(child, profile, out);
    }
  }

  private _LiveQuestion issueQuestion(QuestionItem questionItem, Section section, Profile profile) {
    Question question = questionItem.question();
    QuestionSpawner spawner = Check.notNull(fQuestionSpawners.get(question.getClass()),
        () -> Oops.format("No question spawner for %s.", question.getClass()));

    IssueOrderList issueOrderList = new IssueOrderList();
    issueOrderList.add(new SetLocaleIssueOrder(fWork.locale()));
    issueOrderList.add(new SetWeightIssueOrder(questionItem.weight()));

    spawner.prepare(question, issueOrderList);
    applyModifiers(questionItem.modifierList(), question, issueOrderList);

    for (Section s = section; s != null; s = s.parent()) {
      applyModifiers(s.modifierList(), question, issueOrderList);
    }

    applyModifiers(profile.modifierList(), question, issueOrderList);

    return spawner.spawn(question, issueOrderList);
  }

  private void applyModifiers(ModifierList modifierList, Question question, IssueOrderList issueOrderList) {
    for (Modifier m : modifierList.modifiers()) {
      ModifierApplier modifierApplier = Check.notNull(fModifierAppliers.get(m.getClass()),
          () -> Oops.format("Don't know how to apply modifier %s.", m.getClass().getName()));
      modifierApplier.apply(m, question, issueOrderList);
    }
  }

  private Session loadSession(long sessionId, DbReader dbr) {
    return Check.notNull(loadSessionIfExists(sessionId, dbr));
  }

  private @Nullable Session loadSessionIfExists(long sessionId, DbReader dbr) {
    try {
      SQLiteStatement s = dbr.prepare("SELECT DATA FROM SESSIONS WHERE ID = ?1");
      try {
        s.bind(1, sessionId);
        return s.step() ? Session.parseFrom(s.columnBlob(0), fProtoExtensionRegistry.get()) : null;
      } finally {
        s.dispose();
      }
    } catch (SQLiteException | InvalidProtocolBufferException e) {
      throw new RuntimeException(e);
    }
  }

  private Screen loadScreen(long screenId, DbReader dbr) {
    try {
      SQLiteStatement s = dbr.prepare("SELECT LIVE_QUESTION, RESPONSE FROM SCREENS WHERE ID = ?1");
      try {
        s.bind(1, screenId);
        Check.that(s.step());

        LiveQuestion liveQuestion = fLiveQuestionLoader.load(_LiveQuestion.parseFrom(
            ByteString.copyFrom(s.columnBlob(0)), fProtoExtensionRegistry.get()), dbr.source()::getQuestion);

        byte[] responseBlob = s.columnBlob(1);
        DialogResponse response = (responseBlob == null) ? null : DialogResponse.parseFrom(
            responseBlob, fProtoExtensionRegistry.get());

        return new Screen(liveQuestion, response);
      } finally {
        s.dispose();
      }
    } catch (SQLiteException | InvalidProtocolBufferException e) {
      throw new RuntimeException(e);
    }
  }

  private static final class Screen {
    final LiveQuestion fLiveQuestion;
    final @Nullable DialogResponse fResponse;

    Screen(LiveQuestion liveQuestion, @Nullable DialogResponse response) {
      fLiveQuestion = liveQuestion;
      fResponse = response;
    }

    DialogResponse userOrEmptyResponse() {
      return (fResponse == null) ? fLiveQuestion.getEmptyResponse() : fResponse;
    }
  }

  private void handleSetResponse(SetResponse m, Connection connection, DbReader dbr) {
    Check.input(connection.hasSessionId());

    Session session = loadSession(connection.sessionId(), dbr);
    checkCanSetResponse(session, dbr);

    Screen screen = loadScreen(getCurrentScreenId(session), dbr);

    DialogResponse response = m.getResponse();
    BigDecimal result = screen.fLiveQuestion.evaluate(response);

    int questionIndex = session.getCurrentQuestionIndex();
    executeSequentially(connection, db -> setResponse(questionIndex, response, result, connection, db));
  }

  private void checkCanSetResponse(Session session, DbReader dbr) {
    Check.input(isActive(session));
    Check.input(!getCurrentQuestion(session).hasScaledResult() || getProfile(dbr).options().getEditableAnswers());
  }

  private void setResponse(int questionIndex, DialogResponse response, BigDecimal result,
      Connection connection, Database db) {
    try (DbWriter dbw = db.beginWrite()) {
      Session session = loadSession(connection.sessionId(), dbw);

      Check.input(session.getCurrentQuestionIndex() == questionIndex);
      checkCanSetResponse(session, dbw);

      session = session.toBuilder()
          .setNewResponse(response)
          .setNewScaledResult(Ints.checkedCast(scaleScore(result)))
          .buildPartial();

      saveSession(connection.sessionId(), session, dbw);
      dbw.commit();
    }

    connection.send(ServerMessage.newBuilder()
        .setType(ServerMessageType.SET_RESPONSE_OK)
        .build());
  }

  private static long scaleScore(BigDecimal score) {
    return score.scaleByPowerOfTen(LiveQuestion.SCORE_FRACTION_DIGITS).longValueExact();
  }

  private static BigDecimal unscaleScore(long value) {
    return BigDecimal.valueOf(value, LiveQuestion.SCORE_FRACTION_DIGITS);
  }

  private void handleSubmit(Connection connection) {
    Check.input(connection.hasSessionId());
    executeSequentially(connection, db -> submit(connection, db));
  }

  private void submit(Connection connection, Database db) {
    Session session;
    DialogResponse response;
    ServerMessage statusMessage;
    int oldQuestionIndex;
    boolean editableAnswers;

    try (DbWriter dbw = db.beginWrite()) {
      session = loadSession(connection.sessionId(), dbw);

      Check.input(isActive(session));
      Check.input(session.hasNewResponse());
      response = session.getNewResponse();

      boolean wasPreviouslyAnswered = getCurrentQuestion(session).hasScaledResult();
      session = submitCurrentResponse(session, dbw);

      Profile profile = getProfile(dbw);
      editableAnswers = profile.options().getEditableAnswers();

      statusMessage = ServerMessage.newBuilder()
          .setType(ServerMessageType.SHOW_QUESTION_STATUS)
          .setShowQuestionStatus(ShowQuestionStatus.newBuilder()
              .setQuestionIndex(session.getCurrentQuestionIndex())
              .setStatus(getQuestionStatus(getCurrentQuestion(session),
                  profile.options().getInstantAnswerCorrectness())))
          .build();

      oldQuestionIndex = session.getCurrentQuestionIndex();
      if (!wasPreviouslyAnswered) {
        session = selectNextUnansweredQuestion(session);
      }

      saveSession(connection.sessionId(), session, dbw);
      dbw.commit();
    }

    connection.enqueue(statusMessage);

    if (oldQuestionIndex == session.getCurrentQuestionIndex()) {
      connection.send(ServerMessage.newBuilder()
          .setType(ServerMessageType.SUBMIT_OK)
          .setSubmitOk(SubmitOk.newBuilder()
              .setMakeReadOnly(!editableAnswers))
          .build());
    } else {
      executeConcurrently(connection, dbr -> sendDialog(false, connection, dbr));
    }

    triggerResponseSubmitted(connection.sessionId(), oldQuestionIndex, session.getQuestion(oldQuestionIndex), response);
    triggerScoreChanged(connection.sessionId(), session);
  }

  private Session submitCurrentResponse(Session session, DbWriter dbw) {
    try {
      Check.that(session.hasNewResponse());

      SQLiteStatement s = dbw.prepare("UPDATE SCREENS SET RESPONSE = ?1 WHERE ID = ?2");
      try {
        s.bind(1, session.getNewResponse().toByteArray());
        s.bind(2, getCurrentScreenId(session));
        s.step();
        Check.that(dbw.connection().getChanges() == 1);
      } finally {
        s.dispose();
      }

      SessionQuestion q = getCurrentQuestion(session);

      BigDecimal resultDelta = unscaleScore(session.getNewScaledResult()).subtract(
          q.hasScaledResult() ? unscaleScore(q.getScaledResult()) : BigDecimal.ZERO);

      BigDecimal score = unscaleScore(session.getScaledScore()).add(
          resultDelta.multiply(BigDecimal.valueOf(q.getWeight())));

      int currentAchievableScore = IntMath.checkedAdd(session.getCurrentAchievableScore(),
          q.hasScaledResult() ? 0 : q.getWeight());

      return session.toBuilder()
          .clearNewResponse()
          .clearNewScaledResult()
          .setScaledScore(scaleScore(score))
          .setCurrentAchievableScore(currentAchievableScore)
          .setQuestion(session.getCurrentQuestionIndex(), q.toBuilder()
              .setScaledResult(session.getNewScaledResult()))
          .buildPartial();
    } catch (SQLiteException e) {
      throw new RuntimeException(e);
    }
  }

  private Session selectNextUnansweredQuestion(Session session) {
    int currentQuestionIndex = session.getCurrentQuestionIndex();
    int questionCount = session.getQuestionCount();

    int n = currentQuestionIndex;
    do {
      n = (n + 1) % questionCount;
    } while ((n != currentQuestionIndex) && session.getQuestion(n).hasScaledResult());

    return (n == currentQuestionIndex) ? session : session.toBuilder().setCurrentQuestionIndex(n).buildPartial();
  }

  private void saveSession(long sessionId, Session session, DbWriter dbw) {
    try {
      SQLiteStatement s = dbw.prepare("UPDATE SESSIONS SET DATA = ?1 WHERE ID = ?2");
      try {
        s.bind(1, session.toByteArray());
        s.bind(2, sessionId);
        s.step();
        Check.that(dbw.connection().getChanges() == 1);
      } finally {
        s.dispose();
      }
    } catch (SQLiteException e) {
      throw new RuntimeException(e);
    }
  }

  private void handleWatchWork(Connection connection) {
    executeSequentially(connection, db -> watchWork(connection, db));
  }

  // called at most once per connection
  private void watchWork(Connection connection, Database db) {
    Check.that(connection.supervising());
    WatchBuffer watchBuffer;

    try (DbReader dbr = db.beginRead()) {
      watchBuffer = new WatchBuffer(connection, getProfile(dbr).markScale());

      connection.enqueue(ServerMessage.newBuilder()
          .setType(ServerMessageType.SERVER_TIME)
          .setServerTime(ServerTime.newBuilder()
              .setServerTimeMilliseconds(System.currentTimeMillis()))
          .build());

      connection.send(ServerMessage.newBuilder()
          .setType(ServerMessageType.SESSION_SCORE)
          .addAllSessionScore(listSessionScores(dbr))
          .build());

      dbr.commit();
    }

    fListeners.add(watchBuffer);
    connection.closeFuture().addListener(future -> fListeners.remove(watchBuffer));

    connection.initializeWatchBuffer(watchBuffer);
  }

  private List<SessionScore> listSessionScores(DbReader dbr) {
    try {
      ArrayList<SessionScore> res = new ArrayList<>();
      MarkScale markScale = getProfile(dbr).markScale();

      SQLiteStatement s = dbr.prepare("SELECT ID, DATA FROM SESSIONS ORDER BY ID");
      try {
        while (s.step()) {
          long sessionId = s.columnLong(0);
          Session session = Session.parseFrom(s.columnBlob(1), fProtoExtensionRegistry.get());
          res.add(getSessionScore(sessionId, session, markScale));
        }
      } finally {
        s.dispose();
      }

      return res;
    } catch (SQLiteException | InvalidProtocolBufferException e) {
      throw new RuntimeException(e);
    }
  }

  private void triggerResponseSubmitted(long sessionId, int questionIndex, SessionQuestion sessionQuestion,
      DialogResponse response) {
    for (Listener listener : fListeners) {
      try {
        listener.responseSubmitted(sessionId, questionIndex, sessionQuestion, response);
      } catch (RuntimeException e) {
        fLogger.warn("", e);
      }
    }
  }

  private void triggerScoreChanged(long sessionId, Session session) {
    for (Listener listener : fListeners) {
      try {
        listener.scoreChanged(sessionId, session);
      } catch (RuntimeException e) {
        fLogger.warn("", e);
      }
    }
  }

  private static BigDecimal computeSessionResult(Session session) {
    return unscaleScore(session.getScaledScore()).divide(
        BigDecimal.valueOf(session.getTotalAchievableScore()), LiveQuestion.SCORE_FRACTION_DIGITS, RoundingMode.DOWN);
  }

  private void handleFinishWork(Connection connection) {
    Check.input(connection.hasSessionId());
    executeSequentially(connection, db -> finishWork(connection, db));
  }

  private void finishWork(Connection connection, Database db) {
    Session session;
    boolean submit;
    DialogResponse response = null;

    try (DbWriter dbw = db.beginWrite()) {
      session = loadSession(connection.sessionId(), dbw);
      Check.input(!session.getFinished());

      submit = session.hasNewResponse() && !isTimedOut(session);
      if (submit) {
        response = session.getNewResponse();
        session = submitCurrentResponse(session, dbw);
      }

      session = session.toBuilder().setFinished(true).buildPartial();

      saveSession(connection.sessionId(), session, dbw);
      dbw.commit();
    }

    if (submit) {
      triggerResponseSubmitted(connection.sessionId(), session.getCurrentQuestionIndex(), getCurrentQuestion(session),
          response);
    }

    triggerScoreChanged(connection.sessionId(), session);
    executeConcurrently(connection, dbr -> sendFinalScore(connection, dbr));
  }

  private void sendFinalScore(Connection connection, DbReader dbr) {
    Session session = loadSession(connection.sessionId(), dbr);
    Check.input(!isActive(session));

    ServerMessage.Builder m = ServerMessage.newBuilder().setType(ServerMessageType.SHOW_SCORE);

    Profile profile = getProfile(dbr);
    AvailableScore a = profile.availableScore();
    if (a != null) {
      ShowScore.Builder showScore = m.getShowScoreBuilder()
          .setQuestionCount(session.getQuestionCount());
      BigDecimal result = computeSessionResult(session);

      if (a.getPercentCorrect()) {
        showScore.setScaledResult(Ints.checkedCast(scaleScore(result)));
      }

      if (a.getPoints()) {
        showScore
            .setScaledScore(session.getScaledScore())
            .setTotalAchievableScore(session.getTotalAchievableScore());
      }

      if (a.getMark()) {
        String mark = profile.markScale().getTitleForResult(result);
        if (!mark.isEmpty()) {
          showScore.setMark(mark);
        }
      }

      if (a.hasForQuestions()) {
        boolean revealWeight = profile.options().getWeightCues() || a.getForQuestions().getPoints();
        boolean revealCorrectness = a.getForQuestions().getPercentCorrect();
        List<QuestionDescriptor> descriptors = getQuestionDescriptors(session, revealWeight, revealCorrectness);

        showScore.getQuestionScoreBuilder()
            .setCorrectResponseAvailable(a.getForQuestions().getCorrectAnswer())
            .addAllQuestionDescriptor(descriptors);
      }
    }

    connection.send(m.build());
  }

  private void handleQueryQuestionDetails(QueryQuestionDetails m, Connection connection, DbReader dbr) {
    Check.input(connection.hasSessionId());

    Session session = loadSession(connection.sessionId(), dbr);
    Check.input(!isActive(session));

    Profile profile = getProfile(dbr);
    AvailableScore a = Check.inputNotNull(profile.availableScore());
    Check.input(a.hasForQuestions());
    AvailableQuestionScore qs = a.getForQuestions();

    checkQuestionIndex(m.getQuestionIndex(), session);
    SessionQuestion q = session.getQuestion(m.getQuestionIndex());

    Screen screen = loadScreen(q.getScreenId(), dbr);

    ShowQuestionDetails.Builder showQuestionDetails = ShowQuestionDetails.newBuilder()
        .setQuestionIndex(m.getQuestionIndex())
        .setDialog(screen.fLiveQuestion.render())
        .setResponse(screen.userOrEmptyResponse());

    if (qs.getCorrectAnswer()) {
      showQuestionDetails.setCorrectResponse(screen.fLiveQuestion.getCorrectResponse());
    }

    if (qs.getPercentCorrect()) {
      showQuestionDetails.setScaledResult(q.hasScaledResult() ? q.getScaledResult() : 0);
    }

    if (qs.getPoints()) {
      showQuestionDetails.setWeight(q.getWeight());
    }

    if (qs.getPercentCorrect() && qs.getPoints()) {
      showQuestionDetails.setScaledScore(getScaledScore(q));
    }

    connection.send(ServerMessage.newBuilder()
        .setType(ServerMessageType.SHOW_QUESTION_DETAILS)
        .setShowQuestionDetails(showQuestionDetails)
        .build());
  }

  private static void checkQuestionIndex(int questionIndex, Session session) {
    Check.input(questionIndex >= 0);
    Check.input(questionIndex < session.getQuestionCount());
  }

  private static long getScaledScore(SessionQuestion q) {
    BigDecimal score = q.hasScaledResult() ?
        unscaleScore(q.getScaledResult()).multiply(BigDecimal.valueOf(q.getWeight())) : BigDecimal.ZERO;
    return scaleScore(score);
  }

  private void handleChooseQuestion(ChooseQuestion m, Connection connection) {
    Check.input(connection.hasSessionId());
    executeSequentially(connection, db -> chooseQuestion(m, connection, db));
  }

  private void chooseQuestion(ChooseQuestion m, Connection connection, Database db) {
    try (DbWriter dbw = db.beginWrite()) {
      Session session = loadSession(connection.sessionId(), dbw);

      Check.input(isActive(session));
      Check.input(!session.hasNewResponse());
      checkQuestionIndex(m.getQuestionIndex(), session);

      session = session.toBuilder()
          .setCurrentQuestionIndex(m.getQuestionIndex())
          .buildPartial();

      saveSession(connection.sessionId(), session, dbw);
      dbw.commit();
    }

    executeConcurrently(connection, dbr -> sendDialog(false, connection, dbr));
  }

  private static boolean isActive(Session session) {
    return !(session.getFinished() || isTimedOut(session));
  }

  private static boolean isTimedOut(Session session) {
    return session.hasDeadline() && (System.currentTimeMillis() > session.getDeadline());
  }

  private void handleWatchSession(WatchSession m, Connection connection) {
    executeSequentially(connection, db -> watchSession(m, connection, db));
  }

  private void watchSession(WatchSession m, Connection connection, Database db) {
    WatchBuffer watchBuffer = Check.inputNotNull(connection.watchBuffer());
    ShowWatchedSession.Builder reply = ShowWatchedSession.newBuilder();

    if (m.hasSessionId()) {
      reply.setSessionId(m.getSessionId());

      try (DbReader dbr = db.beginRead()) {
        Session session = Check.inputNotNull(loadSessionIfExists(m.getSessionId(), dbr));

        reply
            .addAllQuestionDescriptor(getQuestionDescriptors(session, true, true))
            .setQuestion(buildWatchedQuestion(session, session.getCurrentQuestionIndex(), dbr));

        watchBuffer.watchSession(m.getSessionId(), session.getCurrentQuestionIndex());
        dbr.commit();
      }
    } else {
      watchBuffer.watchSession(null, 0);
    }

    connection.send(ServerMessage.newBuilder()
        .setType(ServerMessageType.SHOW_WATCHED_SESSION)
        .setShowWatchedSession(reply)
        .build());
  }

  private ShowWatchedQuestion buildWatchedQuestion(Session session, int questionIndex, DbReader dbr) {
    SessionQuestion q = session.getQuestion(questionIndex);
    Screen screen = loadScreen(q.getScreenId(), dbr);

    return ShowWatchedQuestion.newBuilder()
        .setQuestionIndex(questionIndex)
        .setDialog(screen.fLiveQuestion.render())
        .setCorrectResponse(screen.fLiveQuestion.getCorrectResponse())
        .setWeight(q.getWeight())
        .setUpdate(buildWatchedQuestionUpdate(q, screen.userOrEmptyResponse()))
        .build();
  }

  private void handleWatchQuestion(WatchQuestion m, Connection connection) {
    executeSequentially(connection, db -> watchQuestion(m, connection, db));
  }

  private void watchQuestion(WatchQuestion m, Connection connection, Database db) {
    WatchBuffer watchBuffer = Check.inputNotNull(connection.watchBuffer());
    long watchedSessionId = Check.inputNotNull(watchBuffer.watchedSessionId());

    try (DbReader dbr = db.beginRead()) {
      Session session = loadSession(watchedSessionId, dbr);
      checkQuestionIndex(m.getQuestionIndex(), session);

      watchBuffer.watchQuestion(m.getQuestionIndex());

      connection.send(ServerMessage.newBuilder()
          .setType(ServerMessageType.SHOW_WATCHED_QUESTION)
          .setShowWatchedQuestion(buildWatchedQuestion(session, m.getQuestionIndex(), dbr))
          .build());

      dbr.commit();
    }
  }

  private void handleDeleteWork(Connection connection) {
    Check.input(connection.supervising());
    fWorkManager.deleteWork(fWork);
    connection.close();
  }
}
