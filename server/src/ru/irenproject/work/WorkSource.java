/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.work;

import ru.irenproject.Check;
import ru.irenproject.Question;
import ru.irenproject.QuestionItem;
import ru.irenproject.Test;

import java.util.HashMap;

public final class WorkSource {
  private final Test fTest;
  private final HashMap<Long, Question> fQuestionsById = new HashMap<>();

  public WorkSource(Test test) {
    fTest = test;
    for (QuestionItem item : fTest.root().listTreeQuestions()) {
      fQuestionsById.put(item.question().id(), item.question());
    }
  }

  public Test test() {
    return fTest;
  }

  public Question getQuestion(long id) {
    return Check.notNull(fQuestionsById.get(id));
  }
}
