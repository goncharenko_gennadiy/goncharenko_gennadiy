/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.work;

import ru.irenproject.common.Proto.DialogResponse;
import ru.irenproject.common.Proto.QuestionStatus;
import ru.irenproject.common.Proto.ServerMessage;
import ru.irenproject.common.Proto.ServerMessageType;
import ru.irenproject.common.Proto.SessionScore;
import ru.irenproject.common.Proto.ShowQuestionStatus;
import ru.irenproject.common.Proto.UpdateWatchedQuestion;
import ru.irenproject.profile.MarkScale;
import ru.irenproject.work.Proto.Session;
import ru.irenproject.work.Proto.SessionQuestion;

import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;

import javax.annotation.Nullable;
import java.util.Map;
import java.util.TreeMap;

public final class WatchBuffer implements WorkProcessor.Listener {
  private final Connection fConnection;
  private final MarkScale fMarkScale;

  private final Object fLock = new Object();
  private boolean fSending;

  private final TreeMap<Long, SessionScore> fScores = new TreeMap<>();

  private Long fWatchedSessionId;
  private final TreeMap<Integer, QuestionStatus> fStatusBuffer = new TreeMap<>();

  private int fWatchedQuestionIndex;
  private UpdateWatchedQuestion fQuestionUpdate;

  public WatchBuffer(Connection connection, MarkScale markScale) {
    fConnection = connection;
    fMarkScale = markScale;
  }

  private void send() {
    synchronized (fLock) {
      if (!fSending) {
        ChannelFuture f = null;

        if (!fScores.isEmpty()) {
          f = fConnection.enqueue(ServerMessage.newBuilder()
              .setType(ServerMessageType.SESSION_SCORE)
              .addAllSessionScore(fScores.values())
              .build());
          fScores.clear();
        }

        if (!fStatusBuffer.isEmpty()) {
          for (Map.Entry<Integer, QuestionStatus> e : fStatusBuffer.entrySet()) {
            f = fConnection.enqueue(ServerMessage.newBuilder()
                .setType(ServerMessageType.SHOW_QUESTION_STATUS)
                .setShowQuestionStatus(ShowQuestionStatus.newBuilder()
                    .setQuestionIndex(e.getKey())
                    .setStatus(e.getValue()))
                .build());
          }
          fStatusBuffer.clear();
        }

        if (fQuestionUpdate != null) {
          f = fConnection.enqueue(ServerMessage.newBuilder()
              .setType(ServerMessageType.UPDATE_WATCHED_QUESTION)
              .setUpdateWatchedQuestion(fQuestionUpdate)
              .build());
          fQuestionUpdate = null;
        }

        if (f != null) {
          fSending = true;
          f.addListener((ChannelFutureListener) this::sendComplete);
          fConnection.flush();
        }
      }
    }
  }

  private void sendComplete(ChannelFuture future) {
    synchronized (fLock) {
      if (future.isSuccess()) {
        fSending = false;
        send();
      }
    }
  }

  public void watchSession(@Nullable Long sessionId, int questionIndex) {
    synchronized (fLock) {
      fWatchedSessionId = sessionId;
      fStatusBuffer.clear();
      watchQuestion(questionIndex);
    }
  }

  public @Nullable Long watchedSessionId() {
    synchronized (fLock) {
      return fWatchedSessionId;
    }
  }

  public void watchQuestion(int questionIndex) {
    synchronized (fLock) {
      fWatchedQuestionIndex = questionIndex;
      fQuestionUpdate = null;
    }
  }

  @Override public void scoreChanged(long sessionId, Session session) {
    synchronized (fLock) {
      fScores.put(sessionId, WorkProcessor.getSessionScore(sessionId, session, fMarkScale));
      send();
    }
  }

  @Override public void responseSubmitted(long sessionId, int questionIndex,
      SessionQuestion sessionQuestion, DialogResponse response) {
    synchronized (fLock) {
      if ((fWatchedSessionId != null) && (fWatchedSessionId == sessionId)) {
        fStatusBuffer.put(questionIndex, WorkProcessor.getQuestionStatus(sessionQuestion, true));

        if (fWatchedQuestionIndex == questionIndex) {
          fQuestionUpdate = WorkProcessor.buildWatchedQuestionUpdate(sessionQuestion, response);
        }

        send();
      }
    }
  }
}
