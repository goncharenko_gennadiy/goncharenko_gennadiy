/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.work;

import ru.irenproject.Utils;

import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@ChannelHandler.Sharable public final class ExceptionHandler extends ChannelInboundHandlerAdapter {
  private static final Logger fLogger = LoggerFactory.getLogger(ExceptionHandler.class);
  private static final ExceptionHandler fInstance = new ExceptionHandler();

  public static ExceptionHandler get() {
    return fInstance;
  }

  private ExceptionHandler() {}

  @SuppressWarnings("RefusedBequest")
  @Override public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
    fLogger.warn("Error in {}:", Utils.channelToString(ctx.channel()), cause);
    ctx.close();
  }
}
