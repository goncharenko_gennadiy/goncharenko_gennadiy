/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.work;

import ru.irenproject.Check;

import com.almworks.sqlite4java.SQLiteConnection;
import com.almworks.sqlite4java.SQLiteException;
import com.almworks.sqlite4java.SQLiteStatement;

public abstract class DbTransaction implements AutoCloseable {
  private /* @Nullable */SQLiteConnection fConnection;
  private final WorkSource fSource;

  protected DbTransaction(SQLiteConnection connection, WorkSource source) {
    fConnection = connection;
    fSource = source;
  }

  public final void open() {
    boolean ok = false;
    try {
      beforeOpen();
      Check.that(!inTransaction());
      fConnection.exec("BEGIN TRANSACTION");
      ok = true;
    } catch (SQLiteException e) {
      throw new RuntimeException(e);
    } finally {
      if (!ok) {
        close();
      }
    }
  }

  @Override public final void close() {
    try {
      if (fConnection != null) {
        if (inTransaction()) {
          fConnection.exec("ROLLBACK");
        }
        afterClose();
        fConnection = null;
      }
    } catch (SQLiteException e) {
      throw new RuntimeException(e);
    }
  }

  private boolean inTransaction() {
    try {
      return !fConnection.getAutoCommit();
    } catch (SQLiteException e) {
      throw new RuntimeException(e);
    }
  }

  public final void commit() {
    try {
      fConnection.exec("COMMIT");
      afterClose();
      fConnection = null;
    } catch (SQLiteException e) {
      throw new RuntimeException(e);
    }
  }

  public final SQLiteConnection connection() {
    return Check.notNull(fConnection);
  }

  public final SQLiteStatement prepare(String sql) {
    try {
      return fConnection.prepare(sql);
    } catch (SQLiteException e) {
      throw new RuntimeException(e);
    }
  }

  protected void beforeOpen() {}

  protected void afterClose() {}

  protected final int changeCount() {
    try {
      return fConnection.getTotalChanges();
    } catch (SQLiteException e) {
      throw new RuntimeException(e);
    }
  }

  public WorkSource source() {
    return fSource;
  }
}
