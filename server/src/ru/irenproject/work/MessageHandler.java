/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.work;

import ru.irenproject.Check;
import ru.irenproject.common.Proto.ClientMessage;
import ru.irenproject.common.Proto.ServerMessage;
import ru.irenproject.common.Proto.ServerMessageType;
import ru.irenproject.common.Proto.ShowSupervisorScreen;
import ru.irenproject.common.Proto.Supervise;

import com.google.inject.assistedinject.Assisted;
import com.google.protobuf.ByteString;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import javax.inject.Inject;
import java.security.MessageDigest;

public final class MessageHandler extends ChannelInboundHandlerAdapter {
  public interface Factory {
    MessageHandler create(byte[] supervisorKey, String serverAddress);
  }

  private static final Logger fLogger = LoggerFactory.getLogger(MessageHandler.class);

  private final byte[] fSupervisorKey;
  private final String fServerAddress;
  private final WorkCatalog fWorkCatalog;
  private final WorkDispatcher fWorkDispatcher;
  private final GlobalProcessor fGlobalProcessor;
  private final WorkListSender.Factory fWorkListSenderFactory;

  private Connection fConnection;
  private @Nullable Work fWork;

  @Inject private MessageHandler(
      @Assisted byte[] supervisorKey,
      @Assisted String serverAddress,
      WorkCatalog workCatalog,
      WorkDispatcher workDispatcher,
      GlobalProcessor globalProcessor,
      WorkListSender.Factory workListSenderFactory) {
    fSupervisorKey = supervisorKey.clone();
    fServerAddress = serverAddress;
    fWorkCatalog = workCatalog;
    fWorkDispatcher = workDispatcher;
    fGlobalProcessor = globalProcessor;
    fWorkListSenderFactory = workListSenderFactory;
  }

  @Override public void channelActive(ChannelHandlerContext ctx) throws Exception {
    fConnection = new Connection(ctx.channel());
    fLogger.info("Accepted {}.", fConnection);
    super.channelActive(ctx);
  }

  @Override public void channelInactive(ChannelHandlerContext ctx) throws Exception {
    fLogger.info("Closed {}.", fConnection);
    super.channelInactive(ctx);
  }

  @Override public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
    if (msg instanceof ClientMessage) {
      handleMessage((ClientMessage) msg);
    } else {
      super.channelRead(ctx, msg);
    }
  }

  private void handleMessage(ClientMessage m) {
    //noinspection EnumSwitchStatementWhichMissesCases
    switch (m.getType()) {
      case LIST_WORKS: {
        handleListWorks();
        break;
      }
      case SELECT_WORK: {
        handleSelectWork(m);
        break;
      }
      case SUPERVISE: {
        Check.input(m.hasSupervise());
        handleSupervise(m.getSupervise());
        break;
      }
      case WATCH_WORK: {
        handleWatchWork(m);
        break;
      }
      case CREATE_WORK: {
        fGlobalProcessor.process(m, fConnection);
        break;
      }
      default: {
        process(Check.inputNotNull(fWork), m);
      }
    }
  }

  private void process(Work work, ClientMessage m) {
    WorkProcessor p = fWorkDispatcher.getWorkProcessorIfExists(work);
    if (p == null) {
      fConnection.close();
    } else {
      p.process(m, fConnection);
    }
  }

  private void handleListWorks() {
    Check.input(!fConnection.supervising());
    Check.input(fWork == null);

    uninstallWorkListSender();

    fConnection.enqueue(ServerMessage.newBuilder()
        .setType(ServerMessageType.SHOW_SELECT_WORK_SCREEN)
        .build());
    installWorkListSender();
  }

  private void handleSelectWork(ClientMessage m) {
    Check.input(m.hasSelectWork());
    Check.input(!fConnection.supervising());
    Check.input(fWork == null);

    ByteString id = Ids.fromAlienHexString(m.getSelectWork().getId());

    Work work = fWorkCatalog.getIfExists(id);
    if (work == null) {
      handleListWorks();
    } else {
      uninstallWorkListSender();
      fWork = work;
      process(fWork, m);
    }
  }

  private void handleSupervise(Supervise m) {
    Check.input(!fConnection.supervising());
    Check.input(fWork == null);

    if (MessageDigest.isEqual(m.getSupervisorKey().toByteArray(), fSupervisorKey)) {
      fConnection.makeSupervising();
      fLogger.info("Supervisor login {}.", fConnection);

      fConnection.enqueue(ServerMessage.newBuilder()
          .setType(ServerMessageType.SHOW_SUPERVISOR_SCREEN)
          .setShowSupervisorScreen(ShowSupervisorScreen.newBuilder()
              .setServerAddress(fServerAddress)
              .build())
          .build());
      installWorkListSender();
    } else {
      fConnection.send(ServerMessage.newBuilder()
          .setType(ServerMessageType.INCORRECT_SUPERVISOR_KEY)
          .build());
    }
  }

  private void handleWatchWork(ClientMessage m) {
    Check.input(m.hasWatchWork());
    Check.input(fConnection.supervising());
    Check.input(fWork == null);

    ByteString workId = Ids.fromAlienHexString(m.getWatchWork().getWorkId());
    fWork = Check.inputNotNull(fWorkCatalog.getIfExists(workId));

    uninstallWorkListSender();
    process(fWork, m);
  }

  private void installWorkListSender() {
    WorkListSender sender = fWorkListSenderFactory.create(fConnection);
    fConnection.initializeWorkListSender(sender);
    sender.start();
  }

  private void uninstallWorkListSender() {
    WorkListSender sender = fConnection.removeWorkListSender();
    if (sender != null) {
      sender.stop();
    }
  }
}
