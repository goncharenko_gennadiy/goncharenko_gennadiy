/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.work;

import ru.irenproject.Check;
import ru.irenproject.common.Proto.ServerMessage;
import ru.irenproject.common.Proto.ServerMessageType;
import ru.irenproject.common.Proto.WorkList;

import com.google.inject.assistedinject.Assisted;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;

import javax.inject.Inject;

public final class WorkListSender {
  public interface Factory {
    WorkListSender create(Connection connection);
  }

  private enum State { CREATED, ACTIVE, STOPPED }

  private final Connection fConnection;
  private final WorkCatalog fWorkCatalog;
  private final Object fLock = new Object();

  private final WorkCatalog.Listener fCatalogListener = this::catalogChanged;
  private final ChannelFutureListener fCloseListener = future -> stop();

  private State fState = State.CREATED;
  private boolean fHasNewData = true;
  private boolean fSending;

  @Inject private WorkListSender(@Assisted Connection connection, WorkCatalog workCatalog) {
    fConnection = connection;
    fWorkCatalog = workCatalog;
  }

  public void start() {
    synchronized (fLock) {
      Check.that(fState == State.CREATED);
      fState = State.ACTIVE;

      fWorkCatalog.addListener(fCatalogListener);
      fConnection.closeFuture().addListener(fCloseListener);

      send();
    }
  }

  private void send() {
    synchronized (fLock) {
      if (!fSending && fHasNewData && (fState == State.ACTIVE)) {
        ChannelFuture f = fConnection.send(ServerMessage.newBuilder()
            .setType(ServerMessageType.WORK_LIST)
            .setWorkList(WorkList.newBuilder()
                .addAllWorkDescriptor(fWorkCatalog.descriptors()))
            .build());

        fSending = true;
        fHasNewData = false;

        f.addListener((ChannelFutureListener) this::sendComplete);
      }
    }
  }

  private void sendComplete(ChannelFuture future) {
    synchronized (fLock) {
      if (future.isSuccess()) {
        fSending = false;
        send();
      }
    }
  }

  private void catalogChanged() {
    synchronized (fLock) {
      fHasNewData = true;
      send();
    }
  }

  public void stop() {
    synchronized (fLock) {
      fState = State.STOPPED;
      fWorkCatalog.removeListener(fCatalogListener);
      fConnection.closeFuture().removeListener(fCloseListener);
    }
  }
}
