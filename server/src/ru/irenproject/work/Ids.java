/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.work;

import ru.irenproject.BadInputException;
import ru.irenproject.Check;

import com.google.common.io.BaseEncoding;
import com.google.common.primitives.UnsignedBytes;
import com.google.protobuf.ByteString;

import java.util.Comparator;

public final class Ids {
  public static final int SIZE = 16;

  public static final Comparator<ByteString> COMPARATOR = new IdComparator();

  public static ByteString fromBytes(byte[] value) {
    Check.that(value.length == SIZE);
    return ByteString.copyFrom(value);
  }

  public static ByteString fromHexString(String s) {
    return fromBytes(BaseEncoding.base16().lowerCase().decode(s));
  }

  public static ByteString fromAlienHexString(String s) {
    Check.inputNotNull(s);

    byte[] ba;
    try {
      ba = BaseEncoding.base16().lowerCase().decode(s);
    } catch (IllegalArgumentException e) {
      throw new BadInputException(e);
    }

    Check.input(ba.length == SIZE);
    return fromBytes(ba);
  }

  public static String toHexString(ByteString id) {
    checkValid(id);
    return BaseEncoding.base16().lowerCase().encode(id.toByteArray());
  }

  public static boolean valid(ByteString id) {
    return id.size() == SIZE;
  }

  public static void checkValid(ByteString id) {
    Check.that(valid(id));
  }

  private static final class IdComparator implements Comparator<ByteString> {
    @Override public int compare(ByteString left, ByteString right) {
      checkValid(left);
      checkValid(right);

      for (int i = 0; i < SIZE; ++i) {
        int d = UnsignedBytes.compare(left.byteAt(i), right.byteAt(i));
        if (d != 0) {
          return d;
        }
      }
      return 0;
    }
  }

  private Ids() {}
}
