/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.work;

import java.util.ArrayDeque;
import java.util.Queue;
import java.util.concurrent.Executor;

public final class SequentialExecutor implements Executor {
  private final Executor fExecutor;
  private final Queue<Runnable> fTasks = new ArrayDeque<>();
  private boolean fBusy;
  private final Object fLock = new Object();

  public SequentialExecutor(Executor executor) {
    fExecutor = executor;
  }

  @Override public void execute(Runnable command) {
    synchronized (fLock) {
      fTasks.add(command);
      if (!fBusy) {
        executeNext();
      }
    }
  }

  private void executeNext() {
    synchronized (fLock) {
      Runnable task = fTasks.poll();
      fBusy = (task != null);
      if (fBusy) {
        fExecutor.execute(() -> {
          try {
            task.run();
          } finally {
            executeNext();
          }
        });
      }
    }
  }
}
