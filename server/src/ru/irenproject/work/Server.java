/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.work;

import ru.irenproject.DataDirectory;
import ru.irenproject.Utils;

import com.google.common.io.BaseEncoding;
import com.google.common.net.InetAddresses;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpRequestDecoder;
import io.netty.handler.codec.http.HttpResponseEncoder;
import io.netty.handler.codec.http.websocketx.WebSocketFrameAggregator;
import org.apache.commons.lang3.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.bridge.SLF4JBridgeHandler;

import javax.inject.Singleton;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Enumeration;
import java.util.Locale;

@Singleton public final class Server {
  private static final Logger fLogger = LoggerFactory.getLogger(Server.class);

  public static final int DEFAULT_PORT = 9981;

  public static void main(String[] args) {
    Thread.setDefaultUncaughtExceptionHandler((t, e) -> fLogger.error("Exception in thread {}:", t.getName(), e));

    SLF4JBridgeHandler.removeHandlersForRootLogger();
    SLF4JBridgeHandler.install();

    boolean companion = ArrayUtils.contains(args, "--companion");
    if (companion) {
      Utils.enableLoggingToFile(DataDirectory.get().resolve("server.log"));
    }

    Injector injector = Guice.createInjector(new WorkModule());
    injector.getInstance(Server.class).run(companion, DEFAULT_PORT, Utils::readFromSystemIn);
  }

  private final WebTransportHandler.Factory fWebTransportHandlerFactory;
  private final MessageHandler.Factory fMessageHandlerFactory;
  private final WorkDispatcher fWorkDispatcher;

  @Inject private Server(
      WebTransportHandler.Factory webTransportHandlerFactory,
      MessageHandler.Factory messageHandlerFactory,
      WorkDispatcher workDispatcher,
      WorkCatalog workCatalog) {
    fWebTransportHandlerFactory = webTransportHandlerFactory;
    fMessageHandlerFactory = messageHandlerFactory;
    fWorkDispatcher = workDispatcher;

    workCatalog.works().forEach(fWorkDispatcher::register);
  }

  public void run(boolean companion, int port, Runnable shutdownWaiter) {
    byte[] supervisorKey = Utils.readOrGenerateKey(companion ? null : DataDirectory.get().resolve("supervisorKey"));
    String serverAddress = obtainServerAddress(port);

    try {
      NioEventLoopGroup childGroup = new NioEventLoopGroup(Runtime.getRuntime().availableProcessors());
      try {
        NioEventLoopGroup parentGroup = new NioEventLoopGroup(1);
        try {
          ServerBootstrap bootstrap = new ServerBootstrap();
          ResourceDepot resourceDepot = new ResourceDepot(Utils.externalResourcePath().resolve("irenClient.zip"));

          bootstrap
              .group(parentGroup, childGroup)
              .channel(NioServerSocketChannel.class)
              .childHandler(new ChannelInitializer<SocketChannel>() {
                @Override protected void initChannel(SocketChannel ch) {
                  ch.pipeline().addLast(
                      NetworkExceptionHandler.get(),
                      new HttpRequestDecoder(),
                      new HttpResponseEncoder(),
                      new HttpObjectAggregator(1000),
                      new WebSocketFrameAggregator(WebTransportHandler.MAX_FRAME_SIZE),
                      fWebTransportHandlerFactory.create(resourceDepot));
                  MessageLogger.addIfLoggingEnabled(ch.pipeline());
                  ch.pipeline().addLast(
                      fMessageHandlerFactory.create(supervisorKey, serverAddress),
                      ExceptionHandler.get());
                }
              });
          bootstrap.bind(port).syncUninterruptibly();

          if (companion) {
            System.out.format((Locale) null, "{\"port\": %d, \"supervisorKey\": \"%s\"}\n", port,
                BaseEncoding.base16().lowerCase().encode(supervisorKey));
            System.out.close();
          }

          shutdownWaiter.run();
        } finally {
          Utils.stopExecutorGroup(parentGroup);
        }
      } finally {
        Utils.stopExecutorGroup(childGroup);
      }
    } finally {
      fWorkDispatcher.shutDown();
    }

    fLogger.info("Stopped.");
  }

  private String obtainServerAddress(int port) {
    return String.format("http://%s%s/", InetAddresses.toUriString(someAddressOfTheHost()),
        (port == 80) ? "" : ":" + port);
  }

  private InetAddress someAddressOfTheHost() {
    try {
      InetAddress res = null;
      Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();

      outer:
      while (interfaces.hasMoreElements()) {
        Enumeration<InetAddress> addresses = interfaces.nextElement().getInetAddresses();
        while (addresses.hasMoreElements()) {
          InetAddress address = addresses.nextElement();
          if (!address.isLoopbackAddress()) {
            if (address instanceof Inet4Address) {
              res = address;
              break outer;
            } else if (res == null) {
              res = address;
            }
          }
        }
      }

      if (res == null) {
        res = InetAddress.getLocalHost();
      }

      return res;
    } catch (SocketException | UnknownHostException e) {
      throw new RuntimeException(e);
    }
  }
}
