/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.work;

import ru.irenproject.Check;
import ru.irenproject.Utils;
import ru.irenproject.common.Proto.ServerMessage;

import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;

import javax.annotation.Nullable;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

public final class Connection {
  private final Channel fChannel;

  private final AtomicBoolean fLogInAllowed = new AtomicBoolean();
  private final AtomicBoolean fLogInAttempted = new AtomicBoolean();
  private final AtomicReference</* @Nullable */Long> fSessionId = new AtomicReference<>();

  private final AtomicBoolean fSupervising = new AtomicBoolean();
  private final AtomicReference</* @Nullable */WatchBuffer> fWatchBuffer = new AtomicReference<>();

  private final AtomicReference</* @Nullable */WorkListSender> fWorkListSender = new AtomicReference<>();

  public Connection(Channel channel) {
    fChannel = channel;
  }

  @Override public String toString() {
    return Utils.channelToString(fChannel);
  }

  public boolean logInAllowed() {
    return fLogInAllowed.get();
  }

  public void allowLogIn() {
    Check.that(fLogInAllowed.compareAndSet(false, true));
  }

  public boolean recordLogInAttempt() {
    return fLogInAttempted.compareAndSet(false, true);
  }

  public boolean hasSessionId() {
    return fSessionId.get() != null;
  }

  public long sessionId() {
    return Check.notNull(fSessionId.get());
  }

  public void initializeSessionId(long sessionId) {
    Check.that(fSessionId.compareAndSet(null, sessionId));
  }

  public boolean supervising() {
    return fSupervising.get();
  }

  public void makeSupervising() {
    Check.that(fSupervising.compareAndSet(false, true));
  }

  public @Nullable WatchBuffer watchBuffer() {
    return fWatchBuffer.get();
  }

  public void initializeWatchBuffer(WatchBuffer watchBuffer) {
    Check.that(fWatchBuffer.compareAndSet(null, watchBuffer));
  }

  public void initializeWorkListSender(WorkListSender workListSender) {
    Check.that(fWorkListSender.compareAndSet(null, workListSender));
  }

  public @Nullable WorkListSender removeWorkListSender() {
    return fWorkListSender.getAndSet(null);
  }

  public ChannelFuture enqueue(ServerMessage message) {
    return write(message, false);
  }

  public ChannelFuture send(ServerMessage message) {
    return write(message, true);
  }

  private ChannelFuture write(ServerMessage message, boolean flush) {
    ChannelFuture res = flush ? fChannel.writeAndFlush(message) : fChannel.write(message);
    res.addListener(Utils.CLOSE_ON_ERROR);
    return res;
  }

  public void flush() {
    fChannel.flush();
  }

  public void close() {
    fChannel.close();
  }

  public ChannelFuture closeFuture() {
    return fChannel.closeFuture();
  }
}
