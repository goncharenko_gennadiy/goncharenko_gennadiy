/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.work;

import com.google.protobuf.ByteString;

import javax.annotation.concurrent.Immutable;
import java.util.Locale;

@Immutable public final class Work {
  private final ByteString fId;
  private final String fTitle;
  private final Locale fLocale;

  public Work(ByteString id, String title, Locale locale) {
    Ids.checkValid(id);
    fId = id;
    fTitle = title;
    fLocale = locale;
  }

  public ByteString id() {
    return fId;
  }

  public String title() {
    return fTitle;
  }

  public Locale locale() {
    return fLocale;
  }
}
