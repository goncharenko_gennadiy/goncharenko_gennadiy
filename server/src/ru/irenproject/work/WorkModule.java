/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.work;

import ru.irenproject.Modifier;
import ru.irenproject.TestModule;
import ru.irenproject.addNegativeChoiceModifier.AddNegativeChoiceModifier;
import ru.irenproject.live.AddNegativeChoiceModifierApplier;
import ru.irenproject.live.ModifierApplier;
import ru.irenproject.live.ScriptModifierApplier;
import ru.irenproject.live.SetEvaluationModelModifierApplier;
import ru.irenproject.live.SetNegativeChoiceContentModifierApplier;
import ru.irenproject.live.SetWeightModifierApplier;
import ru.irenproject.live.ShuffleChoicesModifierApplier;
import ru.irenproject.live.SuppressSingleChoiceHintModifierApplier;
import ru.irenproject.live.classifyQuestion.LiveClassifyQuestionModule;
import ru.irenproject.live.inputQuestion.LiveInputQuestionModule;
import ru.irenproject.live.matchQuestion.LiveMatchQuestionModule;
import ru.irenproject.live.orderQuestion.LiveOrderQuestionModule;
import ru.irenproject.live.selectQuestion.LiveSelectQuestionModule;
import ru.irenproject.scriptModifier.ScriptModifier;
import ru.irenproject.setEvaluationModelModifier.SetEvaluationModelModifier;
import ru.irenproject.setNegativeChoiceContentModifier.SetNegativeChoiceContentModifier;
import ru.irenproject.setWeightModifier.SetWeightModifier;
import ru.irenproject.shuffleChoicesModifier.ShuffleChoicesModifier;
import ru.irenproject.suppressSingleChoiceHintModifier.SuppressSingleChoiceHintModifier;

import com.google.inject.TypeLiteral;
import com.google.inject.assistedinject.FactoryModuleBuilder;
import com.google.inject.multibindings.MapBinder;

public final class WorkModule extends TestModule {
  @Override protected void configure() {
    super.configure();

    install(new FactoryModuleBuilder().build(WorkProcessor.Factory.class));
    install(new FactoryModuleBuilder().build(WorkListSender.Factory.class));
    install(new FactoryModuleBuilder().build(WebTransportHandler.Factory.class));
    install(new FactoryModuleBuilder().build(MessageHandler.Factory.class));

    install(new LiveSelectQuestionModule());
    install(new LiveInputQuestionModule());
    install(new LiveMatchQuestionModule());
    install(new LiveOrderQuestionModule());
    install(new LiveClassifyQuestionModule());

    registerModifierAppliers();
  }

  private void registerModifierAppliers() {
    MapBinder<Class<? extends Modifier>, ModifierApplier> b = MapBinder.newMapBinder(binder(),
        new TypeLiteral<Class<? extends Modifier>>() {},
        new TypeLiteral<ModifierApplier>() {});
    b.addBinding(AddNegativeChoiceModifier.class).to(AddNegativeChoiceModifierApplier.class);
    b.addBinding(ScriptModifier.class).to(ScriptModifierApplier.class);
    b.addBinding(SetEvaluationModelModifier.class).to(SetEvaluationModelModifierApplier.class);
    b.addBinding(SetNegativeChoiceContentModifier.class).to(SetNegativeChoiceContentModifierApplier.class);
    b.addBinding(SetWeightModifier.class).to(SetWeightModifierApplier.class);
    b.addBinding(ShuffleChoicesModifier.class).to(ShuffleChoicesModifierApplier.class);
    b.addBinding(SuppressSingleChoiceHintModifier.class).to(SuppressSingleChoiceHintModifierApplier.class);
  }
}
