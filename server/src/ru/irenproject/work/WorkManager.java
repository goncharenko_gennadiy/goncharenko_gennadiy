/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.work;

import ru.irenproject.Test;
import ru.irenproject.Utils;
import ru.irenproject.infra.FileBlobStore;
import ru.irenproject.infra.KryoFactory;
import ru.irenproject.infra.Realm;
import ru.irenproject.itx.ItxInput;
import ru.irenproject.profile.Profile;

import com.almworks.sqlite4java.SQLiteConnection;
import com.almworks.sqlite4java.SQLiteException;
import com.esotericsoftware.kryo.io.Output;
import com.google.protobuf.ByteString;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.SecureRandom;
import java.util.Locale;

@Singleton public final class WorkManager {
  public static void deleteWorkDirectory(Path workDirectory) {
    Utils.deleteShallowDirectory(workDirectory.resolve(WorkDispatcher.BLOB_STORE_DIRECTORY));
    Utils.deleteShallowDirectory(workDirectory);
  }

  private final WorkDispatcher fWorkDispatcher;
  private final WorkCatalog fWorkCatalog;
  private final ItxInput fItxInput;
  private final KryoFactory fKryoFactory;

  @Inject private WorkManager(
      WorkDispatcher workDispatcher,
      WorkCatalog workCatalog,
      ItxInput itxInput,
      KryoFactory kryoFactory) {
    fWorkDispatcher = workDispatcher;
    fWorkCatalog = workCatalog;
    fItxInput = itxInput;
    fKryoFactory = kryoFactory;
  }

  public void createWork(Path testFile, String rawTitle, Locale locale) {
    try {
      ByteString workId = generateWorkId();
      Path workDir = fWorkCatalog.getDirectory(workId);
      Work work;

      boolean retainDir = false;
      Files.createDirectories(workDir);
      try {
        createFrozenTest(testFile, workDir);
        createDatabase(workDir);

        work = new Work(workId, Utils.shortenString(rawTitle, 200, 215), locale);
        fWorkCatalog.write(work);

        retainDir = true;
      } finally {
        if (!retainDir) {
          deleteWorkDirectory(workDir);
        }
      }

      fWorkDispatcher.register(work);
      fWorkCatalog.add(work);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  private ByteString generateWorkId() {
    byte[] ba = new byte[Ids.SIZE];
    new SecureRandom().nextBytes(ba);
    return Ids.fromBytes(ba);
  }

  private void createFrozenTest(Path testFile, Path workDir) {
    try {
      Realm realm = new Realm(Utils::logUnexpectedEvent, null, new FileBlobStore(workDir.resolve(
          WorkDispatcher.BLOB_STORE_DIRECTORY)));
      Test test = fItxInput.readTest(testFile, realm);

      if (test.profileList().isEmpty()) {
        test.profileList().add(Profile.createDefault(realm));
      }

      try (
          OutputStream outputStream = Files.newOutputStream(workDir.resolve(WorkDispatcher.FROZEN_TEST_FILE));
          Output kryoOutput = new Output(outputStream)) {
        fKryoFactory.create(realm).writeObject(kryoOutput, new FrozenTest(test, realm.lastResidentId()));
      }
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  private void createDatabase(Path workDir) {
    try {
      SQLiteConnection connection = new SQLiteConnection(workDir.resolve(WorkDispatcher.DATABASE_FILE).toFile());
      connection.open();
      try {
        connection.exec("PRAGMA journal_mode = WAL");

        connection.exec("CREATE TABLE SESSIONS ("
            + "  ID INTEGER NOT NULL PRIMARY KEY"
            + ", DATA BLOB NOT NULL"
            + ", CANONICAL_USER_NAME TEXT NOT NULL UNIQUE"
            + ", KEY BLOB UNIQUE"
            + ")");
        connection.exec("CREATE TABLE SCREENS ("
            + "  ID INTEGER NOT NULL PRIMARY KEY"
            + ", LIVE_QUESTION BLOB NOT NULL"
            + ", RESPONSE BLOB"
            + ")");
      } finally {
        connection.dispose();
      }
    } catch (SQLiteException e) {
      throw new RuntimeException(e);
    }
  }

  public void deleteWork(Work work) {
    fWorkCatalog.markForDeletion(work);
    fWorkCatalog.remove(work);
    fWorkDispatcher.unregister(work);
  }
}
