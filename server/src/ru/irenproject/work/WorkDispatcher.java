/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.work;

import ru.irenproject.Check;
import ru.irenproject.Utils;
import ru.irenproject.infra.FileBlobStore;
import ru.irenproject.infra.KryoFactory;
import ru.irenproject.infra.Realm;

import com.almworks.sqlite4java.SQLiteConnection;
import com.almworks.sqlite4java.SQLiteException;
import com.almworks.sqlite4java.SQLiteStatement;
import com.esotericsoftware.kryo.io.Input;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.cache.RemovalNotification;
import org.apache.commons.lang3.concurrent.BasicThreadFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import javax.inject.Inject;
import javax.inject.Singleton;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.function.Consumer;

@Singleton public final class WorkDispatcher {
  private static final Logger fLogger = LoggerFactory.getLogger(WorkDispatcher.class);

  public static final String DATABASE_FILE = "db";
  public static final String FROZEN_TEST_FILE = "test.frozen";
  public static final String BLOB_STORE_DIRECTORY = "blobStore";

  private final WorkProcessor.Factory fWorkProcessorFactory;
  private final WorkCatalog fWorkCatalog;
  private final KryoFactory fKryoFactory;

  private final ExecutorService fExecutor;

  private final ThreadLocal<LoadingCache<Work, Database>> fDatabases =
      ThreadLocal.withInitial(this::createDatabaseCache);

  private final LoadingCache<Work, Object> fConnectLocks = CacheBuilder.newBuilder()
      .weakKeys()
      .build(CacheLoader.from(Object::new));

  private final ConcurrentHashMap<Work, WorkProcessor> fWorkProcessors = new ConcurrentHashMap<>();

  @Inject private WorkDispatcher(
      WorkProcessor.Factory workProcessorFactory,
      WorkCatalog workCatalog,
      KryoFactory kryoFactory) {
    fWorkProcessorFactory = workProcessorFactory;
    fWorkCatalog = workCatalog;
    fKryoFactory = kryoFactory;

    fExecutor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors(),
        new WorkThreadFactory());
  }

  private final class WorkThreadFactory implements ThreadFactory {
    final ThreadFactory fBaseFactory = new BasicThreadFactory.Builder().namingPattern("work-%d").build();

    @Override public Thread newThread(Runnable r) {
      return fBaseFactory.newThread(() -> {
        try {
          r.run();
        } finally {
          fDatabases.get().invalidateAll();
        }
      });
    }
  }

  public Executor executor() {
    return fExecutor;
  }

  public void execute(Executor executor, Work work, Connection connection, Consumer<Database> task) {
    executor.execute(() -> runTask(work, connection, task));
  }

  private void runTask(Work work, Connection connection, Consumer<Database> task) {
    boolean ok = false;
    try {
      task.accept(fDatabases.get().getUnchecked(work));
      ok = true;
    } catch (RuntimeException e) {
      fLogger.warn("Processing error in work {}:", Ids.toHexString(work.id()), e);
    } finally {
      if (!ok) {
        connection.close();
      }
    }
  }

  private LoadingCache<Work, Database> createDatabaseCache() {
    return CacheBuilder.newBuilder()
        .weakKeys()
        .maximumSize(20)
        .concurrencyLevel(1)
        .removalListener((RemovalNotification<Work, Database> n) -> Check.notNull(n.getValue()).dispose())
        .build(CacheLoader.from(this::openDatabase));
  }

  private Database openDatabase(Work work) {
    WorkSource source = getWorkSource(work);
    return new Database(openDbConnection(work), source);
  }

  private WorkSource getWorkSource(Work work) {
    try {
      Realm realm = new Realm(Utils::logUnexpectedEvent, null,
          new FileBlobStore(fWorkCatalog.getDirectory(work.id()).resolve(BLOB_STORE_DIRECTORY)));
      FrozenTest frozenTest;

      try (
          InputStream inputStream = Files.newInputStream(fWorkCatalog.getDirectory(work.id())
              .resolve(FROZEN_TEST_FILE));
          Input kryoInput = new Input(inputStream)) {
        frozenTest = fKryoFactory.create(realm).readObject(kryoInput, FrozenTest.class);
      }

      realm.setLastResidentId(frozenTest.lastResidentId());
      return new WorkSource(frozenTest.test());
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  private SQLiteConnection openDbConnection(Work work) {
    boolean ok = false;
    Path dbFile = fWorkCatalog.getDirectory(work.id()).resolve(DATABASE_FILE);

    SQLiteConnection res = new SQLiteConnection(dbFile.toFile());
    try {
      // SQLite needs exclusive access to the database in WAL mode during the initial activity
      // in the first connection.
      synchronized (fConnectLocks.getUnchecked(work)) {
        res.open();
        SQLiteStatement s = res.prepare("SELECT * FROM SQLITE_MASTER LIMIT 1");
        try {
          s.step();
        } finally {
          s.dispose();
        }
      }

      ok = true;
    } catch (SQLiteException e) {
      throw new RuntimeException(e);
    } finally {
      if (!ok) {
        res.dispose();
      }
    }

    return res;
  }

  public void register(Work work) {
    Check.that(fWorkProcessors.put(work, fWorkProcessorFactory.create(work)) == null);
  }

  public void unregister(Work work) {
    fWorkProcessors.remove(work);
  }

  public @Nullable WorkProcessor getWorkProcessorIfExists(Work work) {
    return fWorkProcessors.get(work);
  }

  public void shutDown() {
    fExecutor.shutdown();
    Utils.awaitTerminationUninterruptibly(fExecutor);
  }
}
