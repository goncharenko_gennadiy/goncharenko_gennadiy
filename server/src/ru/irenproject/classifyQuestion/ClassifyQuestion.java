/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.classifyQuestion;

import ru.irenproject.Question;
import ru.irenproject.TestCheck;
import ru.irenproject.classifyQuestion.Proto.ClassifyQuestionType;
import ru.irenproject.infra.Freezable;
import ru.irenproject.infra.Realm;
import ru.irenproject.pad.Pad;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class ClassifyQuestion extends Question {
  public static final int MAX_CATEGORIES = 10;
  public static final int MAX_ITEMS = 100;

  private final Pad fFormulation;
  private final ArrayList<Category> fCategories = new ArrayList<>();
  private @Nullable Integer fItemLimit;
  private @Nullable Integer fMinItemsPerCategory;

  public ClassifyQuestion(Realm realm) {
    super(realm);
    fFormulation = new Pad(realm);
  }

  @Override public String type() {
    return ClassifyQuestionType.classify.name();
  }

  @Freezable private static final class Category {
    final Pad fTitle;
    final ArrayList<Pad> fItems = new ArrayList<>();

    Category(Pad title) {
      fTitle = title;
    }
  }

  @Override public Pad formulation() {
    return fFormulation;
  }

  public int categoryCount() {
    return fCategories.size();
  }

  public void addCategory(Pad title) {
    TestCheck.input(categoryCount() < MAX_CATEGORIES);

    fCategories.add(new Category(title));
    post(ChangeEvent.INSTANCE);
  }

  public Pad getCategoryTitle(int categoryIndex) {
    checkCategoryIndex(categoryIndex);
    return fCategories.get(categoryIndex).fTitle;
  }

  private void checkCategoryIndex(int index) {
    TestCheck.input(index >= 0);
    TestCheck.input(index < categoryCount());
  }

  public void addItem(int categoryIndex, Pad item) {
    checkCategoryIndex(categoryIndex);
    TestCheck.input(totalItemCount() < MAX_ITEMS);

    fCategories.get(categoryIndex).fItems.add(item);
    post(ChangeEvent.INSTANCE);
  }

  public int totalItemCount() {
    return fCategories.stream().mapToInt(c -> c.fItems.size()).sum();
  }

  public List<Pad> getCategoryItems(int categoryIndex) {
    checkCategoryIndex(categoryIndex);
    return Collections.unmodifiableList(fCategories.get(categoryIndex).fItems);
  }

  public void deleteCategory(int index) {
    checkCategoryIndex(index);

    fCategories.remove(index);
    post(ChangeEvent.INSTANCE);
  }

  public void deleteItem(int categoryIndex, int itemIndex) {
    checkItemIndex(categoryIndex, itemIndex);

    fCategories.get(categoryIndex).fItems.remove(itemIndex);
    post(ChangeEvent.INSTANCE);
  }

  private void checkItemIndex(int categoryIndex, int itemIndex) {
    checkCategoryIndex(categoryIndex);
    TestCheck.input(itemIndex >= 0);
    TestCheck.input(itemIndex < fCategories.get(categoryIndex).fItems.size());
  }

  public void moveCategory(int index, boolean forward) {
    checkCategoryIndex(index);
    int newIndex = index + (forward ? 1 : -1);
    checkCategoryIndex(newIndex);

    Collections.swap(fCategories, index, newIndex);
    post(ChangeEvent.INSTANCE);
  }

  public void moveItem(int categoryIndex, int itemIndex, boolean forward) {
    checkItemIndex(categoryIndex, itemIndex);
    Category category = fCategories.get(categoryIndex);

    if (forward && (itemIndex == category.fItems.size() - 1)) {
      int next = categoryIndex + 1;
      checkCategoryIndex(next);
      fCategories.get(next).fItems.add(0, category.fItems.remove(itemIndex));
    } else if (!forward && (itemIndex == 0)) {
      int previous = categoryIndex - 1;
      checkCategoryIndex(previous);
      fCategories.get(previous).fItems.add(category.fItems.remove(itemIndex));
    } else {
      Collections.swap(category.fItems, itemIndex, itemIndex + (forward ? 1 : -1));
    }

    post(ChangeEvent.INSTANCE);
  }

  public @Nullable Integer itemLimit() {
    return fItemLimit;
  }

  public @Nullable Integer minItemsPerCategory() {
    return fMinItemsPerCategory;
  }

  public void setLimits(int itemLimit, int minItemsPerCategory) {
    TestCheck.input(itemLimit >= 1);
    TestCheck.input(itemLimit <= MAX_ITEMS);

    TestCheck.input(minItemsPerCategory >= 0);
    TestCheck.input(minItemsPerCategory <= MAX_ITEMS);

    fItemLimit = itemLimit;
    fMinItemsPerCategory = minItemsPerCategory;

    post(ChangeEvent.INSTANCE);
  }

  public void clearLimits() {
    fItemLimit = null;
    fMinItemsPerCategory = null;
    post(ChangeEvent.INSTANCE);
  }

  public boolean hasDefaultOptions() {
    return fItemLimit == null;
  }

  @Override public void setDefaults(String initialText) {
    formulation().appendText(initialText);
  }
}
