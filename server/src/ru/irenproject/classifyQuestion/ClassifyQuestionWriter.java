/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.classifyQuestion;

import ru.irenproject.Question;
import ru.irenproject.itx.Itx;
import ru.irenproject.itx.ItxWriter;
import ru.irenproject.itx.QuestionWriter;
import ru.irenproject.pad.Pad;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.List;

@Singleton public final class ClassifyQuestionWriter implements QuestionWriter {
  @Inject private ClassifyQuestionWriter() {}

  @Override public void write(Question question, ItxWriter out) {
    ClassifyQuestion q = (ClassifyQuestion) question;
    out.writePad(q.formulation());

    out.begin(Itx.ELEM_CLASSIFY_OPTIONS);
    Integer itemLimit = q.itemLimit();
    out.add(Itx.ATTR_ITEMS_USED, (itemLimit == null) ? Itx.VAL_ALL : Integer.toString(itemLimit));
    Integer minItemsPerCategory = q.minItemsPerCategory();
    out.add(Itx.ATTR_MIN_ITEMS_PER_CATEGORY_USED, (minItemsPerCategory == null) ?
        Itx.VAL_ALL : Integer.toString(minItemsPerCategory));
    out.end();

    int categoryCount = q.categoryCount();
    if (categoryCount > 0) {
      out.begin(Itx.ELEM_CATEGORIES);
      for (int i = 0; i < categoryCount; ++i) {
        out.begin(Itx.ELEM_CATEGORY);

        out.begin(Itx.ELEM_CATEGORY_TITLE);
        out.writePad(q.getCategoryTitle(i));
        out.end();

        List<Pad> categoryItems = q.getCategoryItems(i);
        if (!categoryItems.isEmpty()) {
          out.begin(Itx.ELEM_CATEGORY_ITEMS);
          for (Pad item : categoryItems) {
            out.begin(Itx.ELEM_CATEGORY_ITEM);
            out.writePad(item);
            out.end();
          }
          out.end();
        }

        out.end();
      }
      out.end();
    }
  }
}
