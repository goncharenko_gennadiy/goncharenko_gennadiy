/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.classifyQuestion;

import ru.irenproject.itx.Itx;
import ru.irenproject.itx.ItxReader;
import ru.irenproject.itx.QuestionReader;

import org.w3c.dom.Element;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton public final class ClassifyQuestionReader implements QuestionReader {
  @Inject private ClassifyQuestionReader() {}

  @Override public ClassifyQuestion read(ItxReader in, Element e) {
    ClassifyQuestion res = new ClassifyQuestion(in.realm());
    in.readPad(Itx.getChild(e, Itx.ELEM_CONTENT), res.formulation());

    Element xmlOptions = Itx.getChild(e, Itx.ELEM_CLASSIFY_OPTIONS);
    String itemLimit = Itx.find(xmlOptions, Itx.ATTR_ITEMS_USED);
    if (!itemLimit.equals(Itx.VAL_ALL)) {
      res.setLimits(
          Itx.toInteger(itemLimit),
          Itx.toInteger(Itx.find(xmlOptions, Itx.ATTR_MIN_ITEMS_PER_CATEGORY_USED)));
    }

    Element xmlCategories = Itx.getChildIfExists(e, Itx.ELEM_CATEGORIES);
    if (xmlCategories != null) {
      for (Element xmlCategory : Itx.getChildren(xmlCategories, Itx.ELEM_CATEGORY)) {
        readCategory(in, xmlCategory, res);
      }
    }

    return res;
  }

  private void readCategory(ItxReader in, Element e, ClassifyQuestion out) {
    out.addCategory(in.readNewPad(Itx.getChild(Itx.getChild(e, Itx.ELEM_CATEGORY_TITLE), Itx.ELEM_CONTENT)));

    Element xmlCategoryItems = Itx.getChildIfExists(e, Itx.ELEM_CATEGORY_ITEMS);
    if (xmlCategoryItems != null) {
      for (Element xmlCategoryItem : Itx.getChildren(xmlCategoryItems, Itx.ELEM_CATEGORY_ITEM)) {
        out.addItem(out.categoryCount() - 1, in.readNewPad(Itx.getChild(xmlCategoryItem, Itx.ELEM_CONTENT)));
      }
    }
  }
}
