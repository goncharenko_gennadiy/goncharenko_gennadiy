/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.matchQuestion;

import ru.irenproject.Question;
import ru.irenproject.itx.Itx;
import ru.irenproject.itx.ItxWriter;
import ru.irenproject.itx.QuestionWriter;
import ru.irenproject.pad.Pad;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.List;

@Singleton public final class MatchQuestionWriter implements QuestionWriter {
  @Inject private MatchQuestionWriter() {}

  @Override public void write(Question question, ItxWriter out) {
    MatchQuestion q = (MatchQuestion) question;
    out.writePad(q.formulation());

    out.begin(Itx.ELEM_MATCH_OPTIONS);
    Integer pairLimit = q.pairLimit();
    out.add(Itx.ATTR_BASE_ITEMS_USED, (pairLimit == null) ? Itx.VAL_ALL : Integer.toString(pairLimit));
    Integer distractorLimit = q.distractorLimit();
    out.add(Itx.ATTR_DISTRACTORS_USED, (distractorLimit == null) ? Itx.VAL_ALL : Integer.toString(distractorLimit));
    out.end();

    List<Pad> left = q.left();
    List<Pad> right = q.right();

    if (!left.isEmpty()) {
      out.begin(Itx.ELEM_PAIRS);
      for (int i = 0; i < left.size(); ++i) {
        out.begin(Itx.ELEM_PAIR);

        out.begin(Itx.ELEM_BASE_ITEM);
        out.writePad(left.get(i));
        out.end();

        out.begin(Itx.ELEM_MATCHING_ITEM);
        out.writePad(right.get(i));
        out.end();

        out.end();
      }
      out.end();
    }

    if (right.size() > left.size()) {
      out.begin(Itx.ELEM_DISTRACTORS);
      for (Pad distractor : right.subList(left.size(), right.size())) {
        out.begin(Itx.ELEM_DISTRACTOR);
        out.writePad(distractor);
        out.end();
      }
      out.end();
    }
  }
}
