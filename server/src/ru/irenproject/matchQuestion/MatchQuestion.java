/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.matchQuestion;

import ru.irenproject.Question;
import ru.irenproject.TestCheck;
import ru.irenproject.infra.Realm;
import ru.irenproject.matchQuestion.Proto.MatchQuestionType;
import ru.irenproject.pad.Pad;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class MatchQuestion extends Question {
  public static final int MAX_ROWS = 50;

  private final Pad fFormulation;
  private final ArrayList<Pad> fLeft = new ArrayList<>();
  private final ArrayList<Pad> fRight = new ArrayList<>();
  private @Nullable Integer fPairLimit;
  private @Nullable Integer fDistractorLimit;

  public MatchQuestion(Realm realm) {
    super(realm);
    fFormulation = new Pad(realm);
  }

  @Override public String type() {
    return MatchQuestionType.match.name();
  }

  @Override public Pad formulation() {
    return fFormulation;
  }

  public List<Pad> left() {
    return Collections.unmodifiableList(fLeft);
  }

  public List<Pad> right() {
    return Collections.unmodifiableList(fRight);
  }

  public void addPair(Pad left, Pad right) {
    TestCheck.input(fRight.size() < MAX_ROWS);

    fRight.add(fLeft.size(), right);
    fLeft.add(left);
    post(ChangeEvent.INSTANCE);
  }

  public void addDistractor(Pad pad) {
    TestCheck.input(fRight.size() < MAX_ROWS);

    fRight.add(pad);
    post(ChangeEvent.INSTANCE);
  }

  public void deleteRow(int index) {
    checkRowIndex(index);

    if (!isDistractorRow(index)) {
      fLeft.remove(index);
    }
    fRight.remove(index);

    post(ChangeEvent.INSTANCE);
  }

  private void checkRowIndex(int index) {
    TestCheck.input(index >= 0);
    TestCheck.input(index < fRight.size());
  }

  private boolean isDistractorRow(int index) {
    return index >= fLeft.size();
  }

  public void moveRow(int index, boolean forward) {
    checkRowIndex(index);
    int newIndex = index + (forward ? 1 : -1);
    checkRowIndex(newIndex);
    TestCheck.input(isDistractorRow(index) == isDistractorRow(newIndex));

    if (!isDistractorRow(index)) {
      Collections.swap(fLeft, index, newIndex);
    }
    Collections.swap(fRight, index, newIndex);

    post(ChangeEvent.INSTANCE);
  }

  public @Nullable Integer pairLimit() {
    return fPairLimit;
  }

  public void setPairLimit(@Nullable Integer value) {
    if (value != null) {
      TestCheck.input(value >= 1);
      TestCheck.input(value <= MAX_ROWS);
    }

    fPairLimit = value;
    post(ChangeEvent.INSTANCE);
  }

  public @Nullable Integer distractorLimit() {
    return fDistractorLimit;
  }

  public void setDistractorLimit(@Nullable Integer value) {
    if (value != null) {
      TestCheck.input(value >= 0);
      TestCheck.input(value <= MAX_ROWS);
    }

    fDistractorLimit = value;
    post(ChangeEvent.INSTANCE);
  }

  public boolean hasDefaultOptions() {
    return (fPairLimit == null) && (fDistractorLimit == null);
  }

  @Override public void setDefaults(String initialText) {
    formulation().appendText(initialText);
  }
}
