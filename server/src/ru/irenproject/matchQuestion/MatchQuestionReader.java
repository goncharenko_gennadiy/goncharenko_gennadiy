/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.matchQuestion;

import ru.irenproject.itx.Itx;
import ru.irenproject.itx.ItxReader;
import ru.irenproject.itx.QuestionReader;

import org.w3c.dom.Element;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton public final class MatchQuestionReader implements QuestionReader {
  @Inject private MatchQuestionReader() {}

  @Override public MatchQuestion read(ItxReader in, Element e) {
    MatchQuestion res = new MatchQuestion(in.realm());
    in.readPad(Itx.getChild(e, Itx.ELEM_CONTENT), res.formulation());

    Element xmlOptions = Itx.getChild(e, Itx.ELEM_MATCH_OPTIONS);
    String pairLimit = Itx.find(xmlOptions, Itx.ATTR_BASE_ITEMS_USED);
    if (!pairLimit.equals(Itx.VAL_ALL)) {
      res.setPairLimit(Itx.toInteger(pairLimit));
    }

    String distractorLimit = Itx.find(xmlOptions, Itx.ATTR_DISTRACTORS_USED);
    if (!distractorLimit.equals(Itx.VAL_ALL)) {
      res.setDistractorLimit(Itx.toInteger(distractorLimit));
    }

    Element xmlPairs = Itx.getChildIfExists(e, Itx.ELEM_PAIRS);
    if (xmlPairs != null) {
      for (Element xmlPair : Itx.getChildren(xmlPairs, Itx.ELEM_PAIR)) {
        readPair(in, xmlPair, res);
      }
    }

    Element xmlDistractors = Itx.getChildIfExists(e, Itx.ELEM_DISTRACTORS);
    if (xmlDistractors != null) {
      for (Element xmlDistractor : Itx.getChildren(xmlDistractors, Itx.ELEM_DISTRACTOR)) {
        readDistractor(in, xmlDistractor, res);
      }
    }

    return res;
  }

  private void readPair(ItxReader in, Element e, MatchQuestion out) {
    out.addPair(
        in.readNewPad(Itx.getChild(Itx.getChild(e, Itx.ELEM_BASE_ITEM), Itx.ELEM_CONTENT)),
        in.readNewPad(Itx.getChild(Itx.getChild(e, Itx.ELEM_MATCHING_ITEM), Itx.ELEM_CONTENT)));
  }

  private void readDistractor(ItxReader in, Element e, MatchQuestion out) {
    out.addDistractor(in.readNewPad(Itx.getChild(e, Itx.ELEM_CONTENT)));
  }
}
