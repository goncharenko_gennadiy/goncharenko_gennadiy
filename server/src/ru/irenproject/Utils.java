/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject;

import ru.irenproject.infra.Event;
import ru.irenproject.infra.Resident;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.Appender;
import ch.qos.logback.core.FileAppender;
import ch.qos.logback.core.OutputStreamAppender;
import ch.qos.logback.core.encoder.Encoder;
import com.google.common.base.CharMatcher;
import com.google.common.io.ByteSource;
import com.google.protobuf.ByteString;
import com.google.protobuf.GeneratedMessage.ExtendableMessage;
import com.google.protobuf.GeneratedMessage.GeneratedExtension;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFutureListener;
import io.netty.util.concurrent.AbstractEventExecutorGroup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.net.URISyntaxException;
import java.nio.channels.ClosedChannelException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.SecureRandom;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public final class Utils {
  private static final Logger fLogger = LoggerFactory.getLogger(Utils.class);

  public static final ChannelFutureListener CLOSE_ON_ERROR = future -> {
    if (!future.isSuccess()
        && !(future.cause() instanceof ClosingChannelException)
        && !(future.cause() instanceof ClosedChannelException)) {
      fLogger.warn("Closing connection {} due to an error:", channelToString(future.channel()), future.cause());
      future.channel().close();
    }
  };

  public static String shortenString(String s, int softLimit, int hardLimit) {
    String res;
    if (s.length() <= softLimit) {
      res = s;
    } else {
      String ellipsis = "...";

      int k = softLimit;
      int e = Integer.min(s.length(), hardLimit);
      while ((k < e) && !Character.isWhitespace(s.charAt(k))) {
        ++k;
      }
      if (k < e) {
        res = s.substring(0, k) + ellipsis;
      } else if (s.length() <= hardLimit) {
        res = s;
      } else {
        int hardBreak = hardLimit;
        if (Character.isLowSurrogate(s.charAt(hardLimit))) {
          --hardBreak;
        }
        res = s.substring(0, hardBreak) + ellipsis;
      }
    }

    return res;
  }

  public static BigDecimal parseNormalizedDecimal(String s) {
    TestCheck.input(s.length() == 6);
    TestCheck.input(s.charAt(1) == '.');
    TestCheck.input(CharMatcher.inRange('0', '9').countIn(s) == s.length() - 1);

    BigDecimal res = new BigDecimal(s);
    TestCheck.input(res.compareTo(BigDecimal.ONE) <= 0);
    return res;
  }

  public static int waitForProcessUninterruptibly(Process process) {
    boolean interrupted = false;
    try {
      while (true) {
        try {
          return process.waitFor();
        } catch (InterruptedException ignored) {
          interrupted = true;
        }
      }
    } finally {
      if (interrupted) {
        Thread.currentThread().interrupt();
      }
    }
  }

  public static void awaitTerminationUninterruptibly(ExecutorService executorService) {
    boolean interrupted = false;
    try {
      while (true) {
        try {
          if (executorService.awaitTermination(5, TimeUnit.SECONDS)) {
            break;
          }
          fLogger.warn("Still waiting for {} to terminate.", executorService, new StackTrace());
        } catch (InterruptedException ignored) {
          interrupted = true;
        }
      }
    } finally {
      if (interrupted) {
        Thread.currentThread().interrupt();
      }
    }
  }

  public static void stopExecutorGroup(AbstractEventExecutorGroup group) {
    group.shutdownGracefully(200, 1000, TimeUnit.MILLISECONDS);

    while (!group.terminationFuture().awaitUninterruptibly(5, TimeUnit.SECONDS)) {
      fLogger.warn("Still waiting for {} to terminate.", group, new StackTrace());
    }
  }

  public static @Nullable String replaceAllIfFound(String source, Pattern pattern, String replacement) {
    Matcher m = pattern.matcher(source);
    String res;
    if (m.find()) {
      StringBuffer sb = new StringBuffer();

      do {
        m.appendReplacement(sb, replacement);
      } while (m.find());

      m.appendTail(sb);
      res = sb.toString();
    } else {
      res = null;
    }
    return res;
  }

  public static <M extends ExtendableMessage<M>, T> T getExistingExtension(
      M message, GeneratedExtension<M, T> extension) {
    Check.that(message.hasExtension(extension));
    return message.getExtension(extension);
  }

  public static String channelToString(Channel channel) {
    return String.format("[%s #%08x]", channelRemoteAddressToString(channel), channel.hashCode());
  }

  public static String channelRemoteAddressToString(Channel channel) {
    SocketAddress remoteAddress = channel.remoteAddress();
    return (remoteAddress == null) ? "(closed)" : socketAddressToString(remoteAddress);
  }

  public static String socketAddressToString(SocketAddress address) {
    String res;
    if (address instanceof InetSocketAddress) {
      InetSocketAddress a = (InetSocketAddress) address;
      res = String.format("%s:%d", a.getAddress().getHostAddress(), a.getPort());
    } else {
      res = address.toString();
    }
    return res;
  }

  public static void deleteShallowDirectory(Path directory) {
    try {
      for (Path path : listDirectory(directory)) {
        Files.deleteIfExists(path);
      }
      Files.deleteIfExists(directory);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  public static List<Path> listDirectory(Path directory) {
    List<Path> res;

    try (Stream<Path> stream = Files.list(directory)) {
      res = stream.collect(Collectors.toList());
    } catch (NoSuchFileException ignored) {
      res = Collections.emptyList();
    } catch (IOException e) {
      throw new RuntimeException(e);
    }

    return res;
  }

  public static Path externalResourcePath() {
    try {
      Path jarFileOrClassDir = Paths.get(Utils.class.getProtectionDomain().getCodeSource().getLocation().toURI());
      return jarFileOrClassDir.resolve(Files.isDirectory(jarFileOrClassDir) ? "../.." : "..").normalize();
    } catch (URISyntaxException e) {
      throw new RuntimeException(e);
    }
  }

  public static ByteSource byteStringAsByteSource(ByteString byteString) {
    return new ByteStringBasedByteSource(byteString);
  }

  private static final class ByteStringBasedByteSource extends ByteSource {
    final ByteString fByteString;

    ByteStringBasedByteSource(ByteString byteString) {
      fByteString = byteString;
    }

    @Override public InputStream openStream() {
      return fByteString.newInput();
    }

    @SuppressWarnings("RefusedBequest")
    @Override public long size() {
      return fByteString.size();
    }
  }

  public static ByteString readByteSourceToByteString(ByteSource byteSource) {
    try {
      ByteString res;
      if (byteSource instanceof ByteStringBasedByteSource) {
        res = ((ByteStringBasedByteSource) byteSource).fByteString;
      } else {
        try (InputStream inputStream = byteSource.openStream()) {
          res = ByteString.readFrom(inputStream, (int) Long.min(byteSource.size(), 100 * 1024 * 1024));
        }
      }
      return res;
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  public static void enableLoggingToFile(Path path) {
    LoggerContext context = (LoggerContext) LoggerFactory.getILoggerFactory();
    ch.qos.logback.classic.Logger rootLogger = context.getLogger(Logger.ROOT_LOGGER_NAME);

    Appender consoleAppender = rootLogger.getAppender("console");
    if (consoleAppender instanceof OutputStreamAppender) {
      Encoder consoleEncoder = ((OutputStreamAppender) consoleAppender).getEncoder();
      if (consoleEncoder instanceof PatternLayoutEncoder) {
        PatternLayoutEncoder encoder = new PatternLayoutEncoder();
        encoder.setContext(context);
        encoder.setPattern(((PatternLayoutEncoder) consoleEncoder).getPattern());
        encoder.start();

        FileAppender<ILoggingEvent> appender = new FileAppender<>();
        appender.setContext(context);
        appender.setName(path.toString());
        appender.setFile(path.toString());
        appender.setAppend(false);
        appender.setEncoder(encoder);
        appender.start();

        rootLogger.addAppender(appender);
      }
    }
  }

  public static int readFromSystemIn() {
    try {
      return System.in.read();
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  public static byte[] readOrGenerateKey(@Nullable Path keyFile) {
    try {
      byte[] res;
      if (keyFile == null) {
        res = new byte[32];
        new SecureRandom().nextBytes(res);
      } else {
        res = Files.readAllBytes(keyFile);
        Check.that(res.length > 0, () -> Oops.format("Key file '%s' is empty.", keyFile));
      }
      return res;
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  public static void logUnexpectedEvent(Resident source, Resident target, Event event) {
    fLogger.warn("Unexpected event {} ({} -> {}).", event.getClass().getName(), source, target);
  }

  private Utils() {}
}
