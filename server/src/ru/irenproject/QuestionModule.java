/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject;

import ru.irenproject.itx.QuestionReader;
import ru.irenproject.itx.QuestionWriter;

import com.google.inject.AbstractModule;
import com.google.inject.TypeLiteral;
import com.google.inject.multibindings.MapBinder;

public abstract class QuestionModule extends AbstractModule {
  private final Class<? extends Question> fQuestionClass;
  private final String fType;
  private final Question.Factory fFactory;
  private final Class<? extends QuestionWriter> fWriterClass;
  private final Class<? extends QuestionReader> fReaderClass;

  protected QuestionModule(
      Class<? extends Question> questionClass,
      String type,
      Question.Factory factory,
      Class<? extends QuestionWriter> writerClass,
      Class<? extends QuestionReader> readerClass) {
    fQuestionClass = questionClass;
    fType = type;
    fFactory = factory;
    fWriterClass = writerClass;
    fReaderClass = readerClass;
  }

  @Override protected void configure() {
    MapBinder.newMapBinder(binder(), String.class, Question.Factory.class).addBinding(fType).toInstance(fFactory);

    MapBinder.newMapBinder(binder(),
        new TypeLiteral<Class<? extends Question>>() {},
        new TypeLiteral<QuestionWriter>() {}).addBinding(fQuestionClass).to(fWriterClass);

    MapBinder.newMapBinder(binder(), String.class, QuestionReader.class).addBinding(fType).to(fReaderClass);
  }
}
