/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.itx;

import ru.irenproject.Check;
import ru.irenproject.Modifier;
import ru.irenproject.ModifierList;
import ru.irenproject.Question;
import ru.irenproject.QuestionItem;
import ru.irenproject.QuestionList;
import ru.irenproject.Section;
import ru.irenproject.Test;
import ru.irenproject.infra.BlobStore;
import ru.irenproject.pad.Block;
import ru.irenproject.pad.ImageBlock;
import ru.irenproject.pad.ImageSource;
import ru.irenproject.pad.LineFeedBlock;
import ru.irenproject.pad.Pad;
import ru.irenproject.pad.TextBlock;
import ru.irenproject.profile.MarkScale;
import ru.irenproject.profile.Profile;
import ru.irenproject.profile.ProfileList;
import ru.irenproject.profile.Proto.AvailableQuestionScore;
import ru.irenproject.profile.Proto.AvailableScore;
import ru.irenproject.profile.Proto.AvailableSectionScore;

import com.google.common.collect.ImmutableMap;
import com.google.common.hash.Hashing;
import com.google.common.io.ByteSource;
import com.google.common.math.IntMath;
import com.google.common.net.MediaType;
import com.google.inject.assistedinject.Assisted;

import javax.inject.Inject;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import java.io.IOException;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public final class ItxWriter {
  public interface Factory {
    ItxWriter create(XMLStreamWriter out, ZipOutputStream zip, boolean skipImageSource);
  }

  private static final long ZIP_ENTRY_TIME = new GregorianCalendar(2000, 0, 1).getTimeInMillis();

  private static final ImmutableMap<String, String> IMAGE_EXTENSIONS_BY_MIME_TYPE = ImmutableMap.of(
      MediaType.PNG.toString(), Itx.IMAGE_PNG_EXTENSION,
      MediaType.JPEG.toString(), Itx.IMAGE_JPG_EXTENSION);

  public static ZipEntry createZipEntryWithFixedTime(String name) {
    ZipEntry res = new ZipEntry(name);
    res.setTime(ZIP_ENTRY_TIME);
    return res;
  }

  private final XMLStreamWriter fOut;
  private final ZipOutputStream fZip;
  private final boolean fSkipImageSource;
  private final Map<Class<? extends Question>, QuestionWriter> fQuestionWriters;
  private final Map<Class<? extends Modifier>, ModifierWriter> fModifierWriters;

  private final HashMap<ImageBlock, Integer> fImages = new HashMap<>();

  @Inject private ItxWriter(
      @Assisted XMLStreamWriter out,
      @Assisted ZipOutputStream zip,
      @Assisted boolean skipImageSource,
      Map<Class<? extends Question>, QuestionWriter> questionWriters,
      Map<Class<? extends Modifier>, ModifierWriter> modifierWriters) {
    fOut = out;
    fZip = zip;
    fSkipImageSource = skipImageSource;
    fQuestionWriters = questionWriters;
    fModifierWriters = modifierWriters;
  }

  public void begin(String elementName) {
    try {
      fOut.writeStartElement(elementName);
    } catch (XMLStreamException e) {
      throw new RuntimeException(e);
    }
  }

  public void end() {
    try {
      fOut.writeEndElement();
    } catch (XMLStreamException e) {
      throw new RuntimeException(e);
    }
  }

  public void add(String attributeName, String attributeValue) {
    try {
      fOut.writeAttribute(attributeName, attributeValue);
    } catch (XMLStreamException e) {
      throw new RuntimeException(e);
    }
  }

  public void addBoolean(String attributeName, boolean value) {
    add(attributeName, Boolean.toString(value));
  }

  public void writeQuestionItem(QuestionItem v) {
    Question q = v.question();
    QuestionWriter qw = Check.notNull(fQuestionWriters.get(q.getClass()));
    begin(Itx.ELEM_QUESTION);
    add(Itx.ATTR_TYPE, q.type());
    add(Itx.ATTR_WEIGHT, Integer.toString(v.weight()));
    addBoolean(Itx.ATTR_ENABLED, v.enabled());
    qw.write(q, this);

    List<String> labels = v.labels();
    if (!labels.isEmpty()) {
      begin(Itx.ELEM_LABELS);
      for (String label : labels) {
        begin(Itx.ELEM_LABEL);
        add(Itx.ATTR_VALUE, label);
        end();
      }
      end();
    }

    writeModifierList(v.modifierList());
    end();
  }

  public void writePad(Pad pad) {
    begin(Itx.ELEM_CONTENT);
    for (Block block : pad.blocks()) {
      if (block instanceof TextBlock) {
        writeText((TextBlock) block);
      } else if (block instanceof ImageBlock) {
        writeImage((ImageBlock) block, pad.realm().blobStore());
      } else if (block instanceof LineFeedBlock) {
        writeLineFeed();
      } else {
        throw new RuntimeException();
      }
    }
    end();
  }

  private void writeText(TextBlock b) {
    begin(Itx.ELEM_TEXT);
    add(Itx.ATTR_VALUE, b.text());
    end();
  }

  private void writeImage(ImageBlock b, BlobStore blobStore) {
    try {
      ImageBlock block = fSkipImageSource ? new ImageBlock(b.mimeType(), b.data(), null) : b;

      Integer imageNumber = fImages.get(block);
      boolean newImage = (imageNumber == null);

      if (newImage) {
        imageNumber = IntMath.checkedAdd(fImages.size(), 1);
        fImages.put(block, imageNumber);
      }

      String imageFileName = String.format((Locale) null, "%s/%d.%s", Itx.IMAGE_DIRECTORY_NAME,
          imageNumber, Check.notNull(IMAGE_EXTENSIONS_BY_MIME_TYPE.get(block.mimeType())));

      if (newImage) {
        ByteSource data = blobStore.get(block.data());

        ZipEntry entry = createZipEntryWithFixedTime(imageFileName);
        entry.setMethod(ZipEntry.STORED);
        entry.setSize(data.size());
        entry.setCrc(data.hash(Hashing.crc32()).padToLong());

        fZip.putNextEntry(entry);
        data.copyTo(fZip);
        fZip.closeEntry();
      }

      begin(Itx.ELEM_IMG);
      add(Itx.ATTR_SRC, imageFileName);

      ImageSource source = block.source();
      if (source != null) {
        String sourceFileName = String.format((Locale) null, "%s/%d.%s", Itx.IMAGE_DIRECTORY_NAME,
            imageNumber, Itx.SOURCE_DATA_FILE_EXTENSION);

        if (newImage) {
          fZip.putNextEntry(createZipEntryWithFixedTime(sourceFileName));
          blobStore.get(source.data()).copyTo(fZip);
          fZip.closeEntry();
        }

        add(Itx.ATTR_CLIPBOARD_FORMAT, source.format());
        add(Itx.ATTR_CLIPBOARD_DATA, sourceFileName);
      }

      end();
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  private void writeLineFeed() {
    begin(Itx.ELEM_BR);
    end();
  }

  public void writeModifierList(ModifierList list) {
    if (!list.isEmpty()) {
      begin(Itx.ELEM_MODIFIERS);
      for (Modifier m : list.modifiers()) {
        begin(Itx.ELEM_MODIFIER);
        ModifierWriter writer = Check.notNull(fModifierWriters.get(m.getClass()));
        add(Itx.ATTR_TYPE, m.type());
        writer.write(m, this);
        end();
      }
      end();
    }
  }

  public void writeProfile(Profile p) {
    begin(Itx.ELEM_PROFILE);
    add(Itx.ATTR_TITLE, p.title());

    begin(Itx.ELEM_QUESTION_SELECTION);
    add(Itx.ATTR_QUESTIONS_PER_SECTION, p.options().hasQuestionsPerSection() ?
        Integer.toString(p.options().getQuestionsPerSection()) : Itx.VAL_ALL);
    if (p.options().hasLabelFilter()) {
      add(Itx.ATTR_LABEL_FILTER, p.options().getLabelFilter());
    }
    addBoolean(Itx.ATTR_SHUFFLE_QUESTIONS, p.options().getShuffleQuestions());
    end();

    begin(Itx.ELEM_SESSION_OPTIONS);
    add(Itx.ATTR_DURATION_MINUTES, p.options().hasDurationMinutes() ?
        Integer.toString(p.options().getDurationMinutes()) : Itx.VAL_UNLIMITED);
    addBoolean(Itx.ATTR_EDITABLE_ANSWERS, p.options().getEditableAnswers());
    addBoolean(Itx.ATTR_BROWSABLE_QUESTIONS, p.options().getBrowsableQuestions());
    addBoolean(Itx.ATTR_WEIGHT_CUES, p.options().getWeightCues());
    end();

    if (!p.options().getEditableAnswers()) {
      begin(Itx.ELEM_INSTANT_FEEDBACK);
      addBoolean(Itx.ATTR_ANSWER_CORRECTNESS, p.options().getInstantAnswerCorrectness());
      addBoolean(Itx.ATTR_TOTAL_PERCENT_CORRECT, p.options().getInstantTotalPercentCorrect());
      end();
    }

    if (p.availableScore() != null) {
      writeAvailableScore(p.availableScore());
    }

    writeMarkScale(p.markScale());
    writeModifierList(p.modifierList());

    end();
  }

  private void writeAvailableScore(AvailableScore s) {
    begin(Itx.ELEM_TEST_RESULTS);
    addBoolean(Itx.ATTR_PERCENT_CORRECT, s.getPercentCorrect());
    addBoolean(Itx.ATTR_POINTS, s.getPoints());
    addBoolean(Itx.ATTR_MARK, s.getMark());

    if (s.hasForQuestions()) {
      writeAvailableQuestionScore(s.getForQuestions());
    }

    if (s.hasForSections()) {
      writeAvailableSectionScore(s.getForSections());
    }

    end();
  }

  private void writeAvailableQuestionScore(AvailableQuestionScore s) {
    begin(Itx.ELEM_QUESTION_RESULTS);
    addBoolean(Itx.ATTR_PERCENT_CORRECT, s.getPercentCorrect());
    addBoolean(Itx.ATTR_POINTS, s.getPoints());
    addBoolean(Itx.ATTR_CORRECT_ANSWER, s.getCorrectAnswer());
    end();
  }

  private void writeAvailableSectionScore(AvailableSectionScore s) {
    begin(Itx.ELEM_SECTION_RESULTS);
    addBoolean(Itx.ATTR_PERCENT_CORRECT, s.getPercentCorrect());
    addBoolean(Itx.ATTR_POINTS, s.getPoints());
    addBoolean(Itx.ATTR_QUESTION_COUNT, s.getQuestionCount());
    addBoolean(Itx.ATTR_QUESTION_LIST, s.getQuestionList());
    end();
  }

  private void writeMarkScale(MarkScale s) {
    if (!s.isEmpty()) {
      begin(Itx.ELEM_MARK_SCALE);
      begin(Itx.ELEM_MARKS);
      for (int i = 0; i < s.markCount(); ++i) {
        begin(Itx.ELEM_MARK);
        add(Itx.ATTR_TITLE, s.getTitle(i));
        add(Itx.ATTR_LOWER_BOUND, String.format(Locale.ENGLISH, "%.4f", s.getLowerBound(i)));
        end();
      }
      end();
      end();
    }
  }

  public void writeTest(Test test) {
    writeSection(test.root());
    writeProfileList(test.profileList());
  }

  private void writeSection(Section section) {
    begin(Itx.ELEM_SECTION);
    add(Itx.ATTR_TITLE, section.name());
    writeQuestionList(section.questionList());

    if (!section.sections().isEmpty()) {
      begin(Itx.ELEM_SECTIONS);
      section.sections().forEach(this::writeSection);
      end();
    }

    writeModifierList(section.modifierList());
    end();
  }

  private void writeQuestionList(QuestionList list) {
    if (!list.isEmpty()) {
      begin(Itx.ELEM_QUESTIONS);
      list.items().forEach(this::writeQuestionItem);
      end();
    }
  }

  private void writeProfileList(ProfileList list) {
    writeProfiles(list.profiles());
  }

  public void writeProfiles(Collection<Profile> profiles) {
    if (!profiles.isEmpty()) {
      begin(Itx.ELEM_PROFILES);
      profiles.forEach(this::writeProfile);
      end();
    }
  }
}
