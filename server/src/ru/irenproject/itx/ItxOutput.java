/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.itx;

import ru.irenproject.Test;

import javanet.staxutils.IndentingXMLStreamWriter;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.commons.lang3.mutable.MutableObject;
import org.codehaus.stax2.XMLStreamWriter2;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.util.function.Consumer;
import java.util.zip.ZipOutputStream;

@Singleton public final class ItxOutput {
  private final ItxWriter.Factory fItxWriterFactory;

  @Inject private ItxOutput(ItxWriter.Factory itxWriterFactory) {
    fItxWriterFactory = itxWriterFactory;
  }

  public void writeFragment(Consumer<ItxWriter> writeHandler, OutputStream out) {
    write(writeHandler, Itx.ELEM_TEST_FRAGMENT, out, false);
  }

  public void writeTest(Test test, OutputStream out, boolean skipImageSource) {
    write(writer -> writer.writeTest(test), Itx.ELEM_IREN_TEST, out, skipImageSource);
  }

  private void write(Consumer<ItxWriter> writeHandler, String rootElementName, OutputStream out,
      boolean skipImageSource) {
    try (ZipOutputStream zipOut = new ZipOutputStream(out)) {
      ByteArrayOutputStream xmlOut = new ByteArrayOutputStream();

      MutableObject<XMLStreamWriter> w = new MutableObject<>(
          XMLOutputFactory.newInstance().createXMLStreamWriter(xmlOut));
      try (ClosableWithXMLStreamException ignored = () -> w.getValue().close()) {
        ((XMLStreamWriter2) w.getValue()).writeRaw("<?xml version=\"1.0\" encoding=\"utf-8\"?>\n\n");

        w.setValue(new IndentingXMLStreamWriter(w.getValue()));
        w.getValue().writeStartElement(rootElementName);

        writeHandler.accept(fItxWriterFactory.create(w.getValue(), zipOut, skipImageSource));

        w.getValue().writeEndElement();
        w.getValue().writeCharacters("\n");
        w.getValue().writeEndDocument();
      }

      zipOut.putNextEntry(ItxWriter.createZipEntryWithFixedTime(Itx.TEST_FILE_NAME));
      xmlOut.writeTo(zipOut);
      zipOut.closeEntry();
    } catch (XMLStreamException | IOException e) {
      throw new RuntimeException(e);
    }
  }

  private interface ClosableWithXMLStreamException extends AutoCloseable {
    @Override void close() throws XMLStreamException;
  }
}
