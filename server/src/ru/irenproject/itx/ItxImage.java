/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.itx;

import javax.annotation.Nullable;
import java.util.Objects;

public final class ItxImage {
  private final String fSrc;
  private final @Nullable String fClipboardFormat;
  private final @Nullable String fClipboardData;

  public ItxImage(String src, @Nullable String clipboardFormat, @Nullable String clipboardData) {
    fSrc = src;
    fClipboardFormat = clipboardFormat;
    fClipboardData = clipboardData;
  }

  @Override public boolean equals(@Nullable Object obj) {
    if (this == obj) {
      return true;
    }

    if ((obj == null) || (getClass() != obj.getClass())) {
      return false;
    }

    ItxImage other = (ItxImage) obj;

    return Objects.equals(fSrc, other.fSrc)
        && Objects.equals(fClipboardFormat, other.fClipboardFormat)
        && Objects.equals(fClipboardData, other.fClipboardData);
  }

  @Override public int hashCode() {
    return Objects.hash(fSrc, fClipboardFormat, fClipboardData);
  }
}
