/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.itx;

import ru.irenproject.Check;
import ru.irenproject.Test;
import ru.irenproject.TestInputException;
import ru.irenproject.infra.Realm;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.util.function.Function;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;

@Singleton public final class ItxInput {
  private final ItxReader.Factory fItxReaderFactory;

  @Inject private ItxInput(ItxReader.Factory itxReaderFactory) {
    fItxReaderFactory = itxReaderFactory;
  }

  public <R> R read(Path in, Realm realm, Function<ItxReader, R> readHandler) {
    try {
      try (ZipFile zip = new ZipFile(in.toFile())) {
        ZipEntry entry = Check.notNull(zip.getEntry(Itx.TEST_FILE_NAME), ItxInputException::new);
        Document document;
        try (InputStream xmlIn = zip.getInputStream(entry)) {
          DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
          dbf.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
          DocumentBuilder db = dbf.newDocumentBuilder();
          document = db.parse(xmlIn);
        }
        return readHandler.apply(fItxReaderFactory.create(zip, document.getDocumentElement(), realm));
      }
    } catch (ZipException | SAXException | TestInputException e) {
      throw new ItxInputException(e);
    } catch (IOException | ParserConfigurationException e) {
      throw new RuntimeException(e);
    }
  }

  public Test readTest(Path in, Realm realm) {
    return read(in, realm, reader -> {
      Itx.checkName(reader.root(), Itx.ELEM_IREN_TEST);
      return reader.readTest(reader.root());
    });
  }
}
