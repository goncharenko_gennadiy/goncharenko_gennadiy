/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.itx;

import ru.irenproject.Check;
import ru.irenproject.ModifierList;
import ru.irenproject.Question;
import ru.irenproject.QuestionItem;
import ru.irenproject.Section;
import ru.irenproject.Test;
import ru.irenproject.Utils;
import ru.irenproject.infra.BlobId;
import ru.irenproject.infra.Realm;
import ru.irenproject.pad.ImageBlock;
import ru.irenproject.pad.ImageSource;
import ru.irenproject.pad.Pad;
import ru.irenproject.profile.MarkScale;
import ru.irenproject.profile.Profile;
import ru.irenproject.profile.ProfileList;
import ru.irenproject.profile.Proto.AvailableQuestionScore;
import ru.irenproject.profile.Proto.AvailableScore;
import ru.irenproject.profile.Proto.AvailableSectionScore;
import ru.irenproject.profile.Proto.MarkScaleData;
import ru.irenproject.profile.Proto.ProfileOptions;

import com.google.common.net.MediaType;
import com.google.inject.assistedinject.Assisted;
import com.google.protobuf.ByteString;
import org.w3c.dom.Element;

import javax.inject.Inject;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;

public final class ItxReader {
  public interface Factory {
    ItxReader create(ZipFile zip, Element root, Realm realm);
  }

  private final Map<String, QuestionReader> fQuestionReaders;
  private final Map<String, ModifierReader> fModifierReaders;
  private final ZipFile fZip;
  private final Element fRoot;
  private final Realm fRealm;
  private final HashMap<ItxImage, ImageBlock> fImageCache = new HashMap<>();

  @Inject private ItxReader(
      @Assisted ZipFile zip,
      @Assisted Element root,
      @Assisted Realm realm,
      Map<String, QuestionReader> questionReaders,
      Map<String, ModifierReader> modifierReaders) {
    fQuestionReaders = questionReaders;
    fModifierReaders = modifierReaders;
    fZip = zip;
    fRoot = root;
    fRealm = realm;
  }

  public Element root() {
    return fRoot;
  }

  public Realm realm() {
    return fRealm;
  }

  public Test readTest(Element e) {
    Test res = new Test(fRealm);
    readSection(Itx.getChild(e, Itx.ELEM_SECTION), res.root());

    Element xmlProfiles = Itx.getChildIfExists(e, Itx.ELEM_PROFILES);
    if (xmlProfiles != null) {
      readProfiles(xmlProfiles, res.profileList());
    }

    return res;
  }

  private void readSection(Element e, Section out) {
    out.setName(Itx.find(e, Itx.ATTR_TITLE));

    Element xmlQuestions = Itx.getChildIfExists(e, Itx.ELEM_QUESTIONS);
    if (xmlQuestions != null) {
      readQuestionList(xmlQuestions, out);
    }

    Element xmlSections = Itx.getChildIfExists(e, Itx.ELEM_SECTIONS);
    if (xmlSections != null) {
      readSections(xmlSections, out);
    }

    Element xmlModifiers = Itx.getChildIfExists(e, Itx.ELEM_MODIFIERS);
    if (xmlModifiers != null) {
      readModifiers(xmlModifiers, out.modifierList());
    }
  }

  private void readQuestionList(Element e, Section out) {
    for (Element xmlQuestion : Itx.getChildren(e, Itx.ELEM_QUESTION)) {
      out.questionList().addItem(readQuestionItem(xmlQuestion));
    }
  }

  private void readSections(Element e, Section out) {
    for (Element xmlSection : Itx.getChildren(e, Itx.ELEM_SECTION)) {
      Section child = new Section(fRealm);
      readSection(xmlSection, child);
      out.addSection(child);
    }
  }

  public QuestionItem readQuestionItem(Element e) {
    QuestionReader reader = Check.notNull(fQuestionReaders.get(Itx.find(e, Itx.ATTR_TYPE)), ItxInputException::new);
    Question q = reader.read(this, e);

    QuestionItem res = new QuestionItem(q);
    res.setWeight(Itx.findInteger(e, Itx.ATTR_WEIGHT));
    res.setEnabled(Itx.findBoolean(e, Itx.ATTR_ENABLED));

    Element xmlLabels = Itx.getChildIfExists(e, Itx.ELEM_LABELS);
    if (xmlLabels != null) {
      for (Element xmlLabel : Itx.getChildren(xmlLabels, Itx.ELEM_LABEL)) {
        res.addLabel(Itx.find(xmlLabel, Itx.ATTR_VALUE));
      }
    }

    Element xmlModifiers = Itx.getChildIfExists(e, Itx.ELEM_MODIFIERS);
    if (xmlModifiers != null) {
      readModifiers(xmlModifiers, res.modifierList());
    }

    return res;
  }

  public void readPad(Element e, Pad out) {
    for (Element child : Itx.getChildren(e, null)) {
      switch (child.getTagName()) {
        case Itx.ELEM_TEXT: {
          readText(child, out);
          break;
        }
        case Itx.ELEM_IMG: {
          readImage(child, out);
          break;
        }
        case Itx.ELEM_BR: {
          readLineFeed(out);
          break;
        }
        default: {
          throw new ItxInputException();
        }
      }
    }
  }

  public Pad readNewPad(Element e) {
    Pad res = new Pad(fRealm);
    readPad(e, res);
    return res;
  }

  private void readText(Element e, Pad out) {
    out.appendText(Itx.find(e, Itx.ATTR_VALUE));
  }

  private void readImage(Element e, Pad out) {
    String src = Itx.find(e, Itx.ATTR_SRC);
    String clipboardFormat = e.hasAttribute(Itx.ATTR_CLIPBOARD_FORMAT) ?
        Itx.find(e, Itx.ATTR_CLIPBOARD_FORMAT) : null;
    String clipboardData = (clipboardFormat == null) ?
        null : Itx.find(e, Itx.ATTR_CLIPBOARD_DATA);
    ItxImage itxImage = new ItxImage(src, clipboardFormat, clipboardData);

    ImageBlock image = fImageCache.get(itxImage);
    if (image == null) {
      String mimeType;
      if (src.endsWith("." + Itx.IMAGE_PNG_EXTENSION)) {
        mimeType = MediaType.PNG.toString();
      } else if (src.endsWith("." + Itx.IMAGE_JPG_EXTENSION)) {
        mimeType = MediaType.JPEG.toString();
      } else {
        throw new ItxInputException();
      }

      ByteString dataBlob = readZipEntry(fZip, src);
      BlobId dataBlobId = fRealm.blobStore().put(Utils.byteStringAsByteSource(dataBlob));

      ImageSource source;
      if (clipboardFormat == null) {
        source = null;
      } else {
        ByteString clipboardDataBlob = readZipEntry(fZip, clipboardData);
        BlobId clipboardDataBlobId = fRealm.blobStore().put(Utils.byteStringAsByteSource(clipboardDataBlob));
        source = new ImageSource(clipboardFormat, clipboardDataBlobId);
      }

      image = new ImageBlock(mimeType, dataBlobId, source);
      fImageCache.put(itxImage, image);
    }

    out.appendImage(image);
  }

  private void readLineFeed(Pad out) {
    out.appendLineFeed();
  }

  private static ByteString readZipEntry(ZipFile f, String entryName) {
    try {
      ZipEntry e = Check.notNull(f.getEntry(entryName), ItxInputException::new);
      try (InputStream in = f.getInputStream(e)) {
        return ByteString.readFrom(in);
      }
    } catch (ZipException ex) {
      throw new ItxInputException(ex);
    } catch (IOException ex) {
      throw new RuntimeException(ex);
    }
  }

  public void readModifiers(Element e, ModifierList out) {
    for (Element xmlModifier : Itx.getChildren(e, Itx.ELEM_MODIFIER)) {
      ModifierReader reader = Check.notNull(fModifierReaders.get(Itx.find(xmlModifier, Itx.ATTR_TYPE)),
          ItxInputException::new);
      out.add(reader.read(this, xmlModifier));
    }
  }

  private void readProfiles(Element e, ProfileList out) {
    for (Element xmlProfile : Itx.getChildren(e, Itx.ELEM_PROFILE)) {
      out.add(readProfile(xmlProfile));
    }
  }

  public Profile readProfile(Element e) {
    Profile res = new Profile(fRealm);
    res.setTitle(Itx.find(e, Itx.ATTR_TITLE));
    res.setOptions(readProfileOptions(e));

    Element xmlTestResults = Itx.getChildIfExists(e, Itx.ELEM_TEST_RESULTS);
    if (xmlTestResults != null) {
      res.setAvailableScore(readAvailableScore(xmlTestResults));
    }

    Element xmlMarkScale = Itx.getChildIfExists(e, Itx.ELEM_MARK_SCALE);
    if (xmlMarkScale != null) {
      res.setMarkScale(readMarkScale(xmlMarkScale));
    }

    Element xmlModifiers = Itx.getChildIfExists(e, Itx.ELEM_MODIFIERS);
    if (xmlModifiers != null) {
      readModifiers(xmlModifiers, res.modifierList());
    }

    return res;
  }

  private ProfileOptions readProfileOptions(Element e) {
    ProfileOptions.Builder options = ProfileOptions.newBuilder();

    Element xmlQuestionSelection = Itx.getChild(e, Itx.ELEM_QUESTION_SELECTION);
    String questionsPerSection = Itx.find(xmlQuestionSelection, Itx.ATTR_QUESTIONS_PER_SECTION);
    if (!questionsPerSection.equals(Itx.VAL_ALL)) {
      options.setQuestionsPerSection(Itx.toInteger(questionsPerSection));
    }

    if (xmlQuestionSelection.hasAttribute(Itx.ATTR_LABEL_FILTER)) {
      options.setLabelFilter(Itx.find(xmlQuestionSelection, Itx.ATTR_LABEL_FILTER));
    }

    options.setShuffleQuestions(Itx.findBoolean(xmlQuestionSelection, Itx.ATTR_SHUFFLE_QUESTIONS));

    Element xmlSessionOptions = Itx.getChild(e, Itx.ELEM_SESSION_OPTIONS);
    String durationMinutes = Itx.find(xmlSessionOptions, Itx.ATTR_DURATION_MINUTES);
    if (!durationMinutes.equals(Itx.VAL_UNLIMITED)) {
      options.setDurationMinutes(Itx.toInteger(durationMinutes));
    }

    Element xmlInstantFeedback = Itx.getChildIfExists(e, Itx.ELEM_INSTANT_FEEDBACK);

    return options
        .setEditableAnswers(Itx.findBoolean(xmlSessionOptions, Itx.ATTR_EDITABLE_ANSWERS))
        .setBrowsableQuestions(Itx.findBoolean(xmlSessionOptions, Itx.ATTR_BROWSABLE_QUESTIONS))
        .setWeightCues(Itx.findBoolean(xmlSessionOptions, Itx.ATTR_WEIGHT_CUES))
        .setInstantAnswerCorrectness(
            (xmlInstantFeedback != null) && Itx.findBoolean(xmlInstantFeedback, Itx.ATTR_ANSWER_CORRECTNESS))
        .setInstantTotalPercentCorrect(
            (xmlInstantFeedback != null) && Itx.findBoolean(xmlInstantFeedback, Itx.ATTR_TOTAL_PERCENT_CORRECT))
        .build();
  }

  private AvailableScore readAvailableScore(Element e) {
    AvailableScore.Builder b = AvailableScore.newBuilder()
        .setPercentCorrect(Itx.findBoolean(e, Itx.ATTR_PERCENT_CORRECT))
        .setPoints(Itx.findBoolean(e, Itx.ATTR_POINTS))
        .setMark(Itx.findBoolean(e, Itx.ATTR_MARK));

    Element xmlQuestionResults = Itx.getChildIfExists(e, Itx.ELEM_QUESTION_RESULTS);
    if (xmlQuestionResults != null) {
      b.setForQuestions(readAvailableQuestionScore(xmlQuestionResults));
    }

    Element xmlSectionResults = Itx.getChildIfExists(e, Itx.ELEM_SECTION_RESULTS);
    if (xmlSectionResults != null) {
      b.setForSections(readAvailableSectionScore(xmlSectionResults));
    }

    return b.build();
  }

  private AvailableQuestionScore readAvailableQuestionScore(Element e) {
    return AvailableQuestionScore.newBuilder()
        .setPercentCorrect(Itx.findBoolean(e, Itx.ATTR_PERCENT_CORRECT))
        .setPoints(Itx.findBoolean(e, Itx.ATTR_POINTS))
        .setCorrectAnswer(Itx.findBoolean(e, Itx.ATTR_CORRECT_ANSWER))
        .build();
  }

  private AvailableSectionScore readAvailableSectionScore(Element e) {
    return AvailableSectionScore.newBuilder()
        .setPercentCorrect(Itx.findBoolean(e, Itx.ATTR_PERCENT_CORRECT))
        .setPoints(Itx.findBoolean(e, Itx.ATTR_POINTS))
        .setQuestionCount(Itx.findBoolean(e, Itx.ATTR_QUESTION_COUNT))
        .setQuestionList(Itx.findBoolean(e, Itx.ATTR_QUESTION_LIST))
        .build();
  }

  private MarkScale readMarkScale(Element e) {
    MarkScaleData.Builder b = MarkScaleData.newBuilder();
    for (Element xmlMark : Itx.getChildren(Itx.getChild(e, Itx.ELEM_MARKS), Itx.ELEM_MARK)) {
      b.addMarkBuilder()
          .setTitle(Itx.find(xmlMark, Itx.ATTR_TITLE))
          .setLowerBound(Itx.find(xmlMark, Itx.ATTR_LOWER_BOUND));
    }
    return new MarkScale(b.build());
  }
}
