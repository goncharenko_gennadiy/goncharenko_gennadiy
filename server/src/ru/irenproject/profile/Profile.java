/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.profile;

import ru.irenproject.ChangeContentEvent;
import ru.irenproject.ModifierList;
import ru.irenproject.TestCheck;
import ru.irenproject.infra.Event;
import ru.irenproject.infra.Realm;
import ru.irenproject.infra.Resident;
import ru.irenproject.profile.Proto.AvailableScore;
import ru.irenproject.profile.Proto.ProfileOptions;
import ru.irenproject.shuffleChoicesModifier.ShuffleChoicesModifier;

import javax.annotation.Nullable;

public final class Profile extends Resident {
  public enum ChangeTitleEvent implements Event { INSTANCE }

  public enum ChangeEvent implements ChangeContentEvent { INSTANCE }

  private static final ProfileOptions EMPTY_OPTIONS = ProfileOptions.newBuilder()
      .setShuffleQuestions(false)
      .setEditableAnswers(false)
      .setBrowsableQuestions(false)
      .setWeightCues(false)
      .setInstantAnswerCorrectness(false)
      .setInstantTotalPercentCorrect(false)
      .build();

  public static Profile createDefault(Realm realm) {
    Profile res = new Profile(realm);
    res.setDefaults();
    return res;
  }

  private String fTitle = "";
  private ProfileOptions fOptions = EMPTY_OPTIONS;
  private @Nullable AvailableScore fAvailableScore;
  private MarkScale fMarkScale = MarkScale.EMPTY;
  private final ModifierList fModifierList;

  public Profile(Realm realm) {
    super(realm);
    fModifierList = new ModifierList(realm);
  }

  public String title() {
    return fTitle;
  }

  public void setTitle(String value) {
    fTitle = value;
    post(ChangeTitleEvent.INSTANCE);
    post(ChangeEvent.INSTANCE);
  }

  public ProfileOptions options() {
    return fOptions;
  }

  public void setOptions(ProfileOptions value) {
    TestCheck.input(value.isInitialized());

    if (value.hasQuestionsPerSection()) {
      TestCheck.input(value.getQuestionsPerSection() >= 1);
    }

    if (value.hasDurationMinutes()) {
      TestCheck.input(value.getDurationMinutes() >= 1);
    }

    if (value.getEditableAnswers()) {
      TestCheck.input(!value.getInstantAnswerCorrectness());
      TestCheck.input(!value.getInstantTotalPercentCorrect());
    }

    fOptions = value;
    post(ChangeEvent.INSTANCE);
  }

  public @Nullable AvailableScore availableScore() {
    return fAvailableScore;
  }

  public void setAvailableScore(@Nullable AvailableScore value) {
    if (value != null) {
      TestCheck.input(value.isInitialized());

      if (value.hasForSections() && value.getForSections().getQuestionList()) {
        TestCheck.input(value.hasForQuestions());
      }
    }

    fAvailableScore = value;
    post(ChangeEvent.INSTANCE);
  }

  public ModifierList modifierList() {
    return fModifierList;
  }

  public MarkScale markScale() {
    return fMarkScale;
  }

  public void setMarkScale(MarkScale value) {
    fMarkScale = value;
    post(ChangeEvent.INSTANCE);
  }

  private void setDefaults() {
    fOptions = EMPTY_OPTIONS.toBuilder()
        .setShuffleQuestions(true)
        .setEditableAnswers(true)
        .setBrowsableQuestions(true)
        .build();

    fAvailableScore = AvailableScore.newBuilder()
        .setPercentCorrect(true)
        .setPoints(false)
        .setMark(true)
        .build();

    fModifierList.add(new ShuffleChoicesModifier(realm()));
  }
}
