/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.addNegativeChoiceModifier;

import ru.irenproject.itx.ItxReader;
import ru.irenproject.itx.ModifierReader;

import org.w3c.dom.Element;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton public final class AddNegativeChoiceModifierReader implements ModifierReader {
  @Inject private AddNegativeChoiceModifierReader() {}

  @Override public AddNegativeChoiceModifier read(ItxReader in, Element e) {
    return new AddNegativeChoiceModifier(in.realm());
  }
}
