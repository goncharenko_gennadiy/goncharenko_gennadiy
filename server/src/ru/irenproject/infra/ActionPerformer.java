/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.infra;

import ru.irenproject.BadInputException;
import ru.irenproject.Check;
import ru.irenproject.editor.Action;
import ru.irenproject.infra.ActionDescriptor.InputParser;

import com.google.common.base.Throwables;
import com.google.common.collect.ImmutableMap;
import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Message;

import javax.annotation.Nullable;
import javax.inject.Inject;
import javax.inject.Singleton;
import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandleProxies;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.concurrent.ConcurrentHashMap;

@Singleton public final class ActionPerformer {
  private static final String PARSE_FROM_METHOD_NAME = "parseFrom";

  private final ConcurrentHashMap<Class<?>, ImmutableMap<String, ActionDescriptor>> fActions =
      new ConcurrentHashMap<>();

  @Inject private ActionPerformer() {}

  public void perform(Object target, String actionName, ByteString input) {
    try {
      ActionDescriptor action = Check.inputNotNull(getActionDescriptor(target.getClass(), actionName));

      Message inputMessage;
      try {
        inputMessage = action.inputParser().parseFrom(input);
      } catch (InvalidProtocolBufferException e) {
        throw new BadInputException(e);
      }

      try {
        action.method().invoke(target, inputMessage);
      } catch (InvocationTargetException e) {
        boolean badInput = Arrays.stream(action.annotation().badInput()).anyMatch(c -> c.isInstance(e.getCause()));
        throw badInput ? new BadInputException(e.getCause()) : Throwables.propagate(e.getCause());
      }
    } catch (IllegalAccessException e) {
      throw new RuntimeException(e);
    }
  }

  private @Nullable ActionDescriptor getActionDescriptor(Class<?> targetClass, String actionName) {
    return fActions.computeIfAbsent(targetClass, this::scanTargetClass).get(actionName);
  }

  private ImmutableMap<String, ActionDescriptor> scanTargetClass(Class<?> targetClass) {
    try {
      ImmutableMap.Builder<String, ActionDescriptor> b = ImmutableMap.builder();

      for (Method m : targetClass.getMethods()) {
        Action annotation = m.getAnnotation(Action.class);
        if (annotation != null) {
          Class<?>[] parameterTypes = m.getParameterTypes();
          Check.that(parameterTypes.length == 1);
          Class<? extends Message> inputClass = parameterTypes[0].asSubclass(Message.class);

          MethodHandle parseFrom = MethodHandles.lookup().findStatic(inputClass, PARSE_FROM_METHOD_NAME,
              MethodType.methodType(inputClass, ByteString.class));
          InputParser inputParser = MethodHandleProxies.asInterfaceInstance(InputParser.class, parseFrom);

          b.put(inputClass.getSimpleName(), new ActionDescriptor(inputParser, m, annotation));
        }
      }

      return b.build();
    } catch (NoSuchMethodException | IllegalAccessException e) {
      throw new RuntimeException(e);
    }
  }
}
