/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.infra;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.Serializer;
import com.esotericsoftware.kryo.factories.SerializerFactory;
import com.esotericsoftware.kryo.io.Input;
import com.google.inject.Injector;
import com.google.inject.MembersInjector;
import de.javakaffee.kryoserializers.FieldAnnotationAwareSerializer;

import javax.inject.Inject;
import java.util.Collections;

public final class InjectingSerializer<T> extends FieldAnnotationAwareSerializer<T> {
  public static final class Factory implements SerializerFactory {
    private final Injector fInjector;

    public Factory(Injector injector) {
      fInjector = injector;
    }

    @Override public Serializer<?> makeSerializer(Kryo kryo, Class<?> type) {
      return new InjectingSerializer<>(kryo, type, fInjector);
    }
  }

  private final MembersInjector<T> fMembersInjector;

  public InjectingSerializer(Kryo kryo, Class<T> type, Injector injector) {
    super(kryo, type, Collections.singleton(Inject.class), true);
    fMembersInjector = injector.getMembersInjector(type);
  }

  @Override public T read(Kryo kryo, Input input, Class<T> type) {
    T res = super.read(kryo, input, type);
    fMembersInjector.injectMembers(res);
    return res;
  }
}
