/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.infra;

import com.google.common.io.ByteSource;
import com.google.protobuf.ByteString;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public final class FileBlobStore implements BlobStore {
  private final Path fDirectory;

  public FileBlobStore(Path directory) {
    try {
      fDirectory = directory;
      Files.createDirectories(fDirectory);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  @Override public BlobId put(ByteSource blob) {
    try {
      BlobId res = new BlobId(ByteString.copyFrom(blob.hash(BlobId.HASH_FUNCTION).asBytes()));

      Path blobPath = getBlobPath(res);
      if (!Files.exists(blobPath)) {
        Path tempPath = getTempBlobPath(res);
        blob.copyTo(com.google.common.io.Files.asByteSink(tempPath.toFile()));
        Files.move(tempPath, blobPath);
      }

      return res;
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  @Override public ByteSource get(BlobId blobId) {
    return com.google.common.io.Files.asByteSource(getBlobPath(blobId).toFile());
  }

  private Path getBlobPath(BlobId blobId) {
    return fDirectory.resolve(blobId.toHexString());
  }

  private Path getTempBlobPath(BlobId blobId) {
    return fDirectory.resolve(blobId.toHexString() + "_");
  }
}
