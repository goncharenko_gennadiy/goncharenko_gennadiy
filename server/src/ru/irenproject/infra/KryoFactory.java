/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.infra;

import com.esotericsoftware.kryo.Kryo;
import com.google.inject.Injector;
import com.google.protobuf.ByteString;
import com.google.protobuf.GeneratedMessage;
import de.javakaffee.kryoserializers.protobuf.ProtobufSerializer;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton public final class KryoFactory {
  private final Injector fInjector;

  @Inject private KryoFactory(Injector injector) {
    fInjector = injector;
  }

  public Kryo create(Realm realm) {
    Kryo res = new Kryo();

    res.setDefaultSerializer(new InjectingSerializer.Factory(fInjector));
    res.setInstantiatorStrategy(new FreezableInstantiatorStrategy());

    res.addDefaultSerializer(ByteString.class, ByteStringSerializer.class);
    res.addDefaultSerializer(Realm.class, new RealmSerializer(realm));

    res.addDefaultSerializer(GeneratedMessage.class, ProtobufSerializer.class);

    return res;
  }
}
