/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.infra;

import ru.irenproject.infra.Proto.XBlob;

import com.google.protobuf.Message;

public final class BlobWidget extends Widget {
  private final BlobId fBlobId;

  public BlobWidget(Realm realm, BlobId blobId) {
    super(realm);
    fBlobId = blobId;
  }

  @Override public Message render(RenderContext context) {
    return XBlob.newBuilder()
        .setBlob(fBlobId.value())
        .build();
  }
}
