/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.infra;

import com.google.protobuf.Message;

public abstract class Widget extends Resident {
  public enum MarkForRenderEvent implements GlobalOnlyEvent { INSTANCE }

  public static final class ShowScreenEvent implements GlobalOnlyEvent {
    private final Widget fScreen;

    public ShowScreenEvent(Widget screen) {
      fScreen = screen;
    }

    public Widget screen() {
      return fScreen;
    }
  }

  public enum DismissScreenEvent implements GlobalOnlyEvent { INSTANCE }

  protected Widget(Realm realm) {
    super(realm);
  }

  public abstract Message render(RenderContext context);

  protected final void markForRender() {
    post(MarkForRenderEvent.INSTANCE);
  }

  protected final void showScreen(Widget screen) {
    post(new ShowScreenEvent(screen));
  }

  protected final void dismissScreen() {
    post(DismissScreenEvent.INSTANCE);
  }
}
