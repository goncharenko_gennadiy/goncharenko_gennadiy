/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.infra;

import com.esotericsoftware.kryo.Kryo.DefaultInstantiatorStrategy;
import org.objenesis.instantiator.ObjectInstantiator;
import org.objenesis.strategy.InstantiatorStrategy;
import org.objenesis.strategy.StdInstantiatorStrategy;

public final class FreezableInstantiatorStrategy implements InstantiatorStrategy {
  private final DefaultInstantiatorStrategy fDefaultInstantiatorStrategy = new DefaultInstantiatorStrategy();
  private final StdInstantiatorStrategy fConstructorlessInstantiatorStrategy = new StdInstantiatorStrategy();

  @Override public <T> ObjectInstantiator<T> newInstantiatorOf(Class<T> type) {
    InstantiatorStrategy strategy = (type.getAnnotation(Freezable.class) == null) ?
        fDefaultInstantiatorStrategy : fConstructorlessInstantiatorStrategy;
    return strategy.newInstantiatorOf(type);
  }
}
