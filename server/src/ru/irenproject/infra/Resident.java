/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.infra;

import javax.annotation.Nullable;
import java.util.ArrayList;

@Freezable public abstract class Resident {
  private final Realm fRealm;
  private final long fId;
  private final ArrayList<Resident> fListeners = new ArrayList<>();

  protected Resident(Realm realm) {
    fRealm = realm;
    fId = fRealm.generateResidentId();
  }

  public final Realm realm() {
    return fRealm;
  }

  public final long id() {
    return fId;
  }

  @SuppressWarnings("EmptyMethod")
  @Override public final boolean equals(@Nullable Object obj) {
    return super.equals(obj);
  }

  @SuppressWarnings("EmptyMethod")
  @Override public final int hashCode() {
    return super.hashCode();
  }

  protected final void post(Event event) {
    if (event instanceof GlobalEvent) {
      fRealm.postGlobal(this, event);
    }

    if (!(event instanceof GlobalOnlyEvent)) {
      for (Resident listener : fListeners) {
        fRealm.post(this, listener, event);
      }
    }
  }

  public final void addListener(Resident listener) {
    fListeners.add(listener);
  }

  protected void handleEvent(Event e, Resident source) {}
}
