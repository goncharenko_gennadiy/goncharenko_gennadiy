/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject;

import ru.irenproject.infra.Resident;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class QuestionItem extends Resident {
  public enum ChangeEvent implements ChangeContentEvent { INSTANCE }

  public static final int MAX_WEIGHT = 1000;

  public static void checkValidWeight(int weight) {
    TestCheck.input(weight >= 1);
    TestCheck.input(weight <= MAX_WEIGHT);
  }

  private final Question fQuestion;
  private final ModifierList fModifierList;
  private int fWeight = 1;
  private boolean fEnabled = true;
  private final ArrayList<String> fLabels = new ArrayList<>();

  public QuestionItem(Question question) {
    super(question.realm());
    fQuestion = question;
    fModifierList = new ModifierList(question.realm());
  }

  public int weight() {
    return fWeight;
  }

  public void setWeight(int weight) {
    checkValidWeight(weight);
    fWeight = weight;
    post(ChangeEvent.INSTANCE);
  }

  public Question question() {
    return fQuestion;
  }

  public boolean enabled() {
    return fEnabled;
  }

  public void setEnabled(boolean value) {
    fEnabled = value;
    post(ChangeEvent.INSTANCE);
  }

  public ModifierList modifierList() {
    return fModifierList;
  }

  public void addLabel(String label) {
    fLabels.add(label);
    post(ChangeEvent.INSTANCE);
  }

  public List<String> labels() {
    return Collections.unmodifiableList(fLabels);
  }

  public void clearLabels() {
    fLabels.clear();
    post(ChangeEvent.INSTANCE);
  }
}
