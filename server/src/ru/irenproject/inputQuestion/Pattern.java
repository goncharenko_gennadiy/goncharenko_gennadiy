/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.inputQuestion;

import ru.irenproject.ChangeContentEvent;
import ru.irenproject.TestCheck;
import ru.irenproject.infra.Realm;
import ru.irenproject.infra.Resident;

public abstract class Pattern extends Resident {
  public enum ChangeEvent implements ChangeContentEvent { INSTANCE }

  public static final int MAX_LENGTH = 200;

  private String fValue = "";
  private int fQualityPercent = 100;

  protected Pattern(Realm realm) {
    super(realm);
  }

  public final String value() {
    return fValue;
  }

  public final void setValue(String value) {
    TestCheck.input(value.length() <= MAX_LENGTH);

    fValue = value;
    post(ChangeEvent.INSTANCE);
  }

  public final int qualityPercent() {
    return fQualityPercent;
  }

  public final void setQualityPercent(int qualityPercent) {
    TestCheck.input(qualityPercent >= 0);
    TestCheck.input(qualityPercent <= 100);

    fQualityPercent = qualityPercent;
    post(ChangeEvent.INSTANCE);
  }

  public abstract boolean hasDefaultOptions();

  public abstract void setOptionsFrom(Pattern source);
}
