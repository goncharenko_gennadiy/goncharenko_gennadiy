/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.inputQuestion;

import ru.irenproject.Utils;
import ru.irenproject.inputQuestion.Proto.SpacePreprocessMode;
import ru.irenproject.itx.Itx;
import ru.irenproject.itx.ItxInputException;
import ru.irenproject.itx.ItxReader;
import ru.irenproject.itx.QuestionReader;

import org.w3c.dom.Element;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton public final class InputQuestionReader implements QuestionReader {
  @Inject private InputQuestionReader() {}

  @Override public InputQuestion read(ItxReader in, Element e) {
    InputQuestion res = new InputQuestion(in.realm());
    in.readPad(Itx.getChild(e, Itx.ELEM_CONTENT), res.formulation());

    Element xmlPatterns = Itx.getChildIfExists(e, Itx.ELEM_PATTERNS);
    if (xmlPatterns != null) {
      for (Element xmlPattern : Itx.getChildren(xmlPatterns, Itx.ELEM_PATTERN)) {
        readPattern(xmlPattern, res);
      }
    }

    return res;
  }

  private void readPattern(Element e, InputQuestion out) {
    switch (Itx.find(e, Itx.ATTR_TYPE)) {
      case Itx.VAL_PATTERN_TYPE_TEXT: {
        readTextPattern(e, out);
        break;
      }
      case Itx.VAL_PATTERN_TYPE_REGEXP: {
        readRegexpPattern(e, out);
        break;
      }
      default: {
        throw new ItxInputException();
      }
    }
  }

  private void readTextPattern(Element e, InputQuestion out) {
    TextPattern p = new TextPattern(out.realm());
    readBasePattern(e, p);

    p.setCaseSensitive(Itx.findBoolean(e, Itx.ATTR_CASE_SENSITIVE));
    p.setSpaceDelimited(xmlToSpaceDelimited(Itx.find(e, Itx.ATTR_SPACES)));
    p.setPrecisionAsString(Itx.findBoolean(e, Itx.ATTR_NUMERIC_AWARE) ? Itx.find(e, Itx.ATTR_PRECISION) : null);
    p.setWildcard(Itx.findBoolean(e, Itx.ATTR_WILDCARD));

    out.addPattern(p);
  }

  private static boolean xmlToSpaceDelimited(String s) {
    switch (s) {
      case Itx.VAL_PATTERN_SPACES_NORMALIZE: return true;
      case Itx.VAL_PATTERN_SPACES_IGNORE: return false;
      default: throw new ItxInputException();
    }
  }

  private void readRegexpPattern(Element e, InputQuestion out) {
    RegexpPattern p = new RegexpPattern(out.realm());
    readBasePattern(e, p);

    p.setCaseSensitive(Itx.findBoolean(e, Itx.ATTR_CASE_SENSITIVE));
    p.setSpacePreprocessMode(xmlToSpacePreprocessMode(Itx.find(e, Itx.ATTR_SPACES)));

    out.addPattern(p);
  }

  private static SpacePreprocessMode xmlToSpacePreprocessMode(String s) {
    switch (s) {
      case Itx.VAL_PATTERN_SPACES_EXACT: return SpacePreprocessMode.KEEP;
      case Itx.VAL_PATTERN_SPACES_NORMALIZE: return SpacePreprocessMode.NORMALIZE;
      case Itx.VAL_PATTERN_SPACES_IGNORE: return SpacePreprocessMode.REMOVE;
      default: throw new ItxInputException();
    }
  }

  private void readBasePattern(Element e, Pattern out) {
    out.setValue(Itx.find(e, Itx.ATTR_VALUE));
    out.setQualityPercent(Utils.parseNormalizedDecimal(Itx.find(e, Itx.ATTR_QUALITY))
        .scaleByPowerOfTen(2).intValue());
  }
}
