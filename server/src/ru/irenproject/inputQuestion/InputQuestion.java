/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.inputQuestion;

import ru.irenproject.Question;
import ru.irenproject.TestCheck;
import ru.irenproject.infra.Realm;
import ru.irenproject.inputQuestion.Proto.InputQuestionType;
import ru.irenproject.pad.Pad;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class InputQuestion extends Question {
  public static final int MAX_PATTERNS = 20;

  private final Pad fFormulation;
  private final ArrayList<Pattern> fPatterns = new ArrayList<>();

  public InputQuestion(Realm realm) {
    super(realm);
    fFormulation = new Pad(realm);
  }

  @Override public String type() {
    return InputQuestionType.input.name();
  }

  @Override public Pad formulation() {
    return fFormulation;
  }

  public void addPattern(Pattern pattern) {
    TestCheck.input(fPatterns.size() < MAX_PATTERNS);

    fPatterns.add(pattern);
    post(ChangeEvent.INSTANCE);
  }

  public List<Pattern> patterns() {
    return Collections.unmodifiableList(fPatterns);
  }

  public void deletePattern(int index) {
    checkPatternIndex(index);

    fPatterns.remove(index);
    post(ChangeEvent.INSTANCE);
  }

  private void checkPatternIndex(int index) {
    TestCheck.input(index >= 0);
    TestCheck.input(index < fPatterns.size());
  }

  public void movePattern(int index, boolean forward) {
    checkPatternIndex(index);
    int newIndex = index + (forward ? 1 : -1);
    checkPatternIndex(newIndex);

    Collections.swap(fPatterns, index, newIndex);
    post(ChangeEvent.INSTANCE);
  }

  @Override public void setDefaults(String initialText) {
    fFormulation.appendText(initialText);
    addPattern(new TextPattern(realm()));
  }
}
