/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject;

import ru.irenproject.itx.ModifierReader;
import ru.irenproject.itx.ModifierWriter;

import com.google.inject.AbstractModule;
import com.google.inject.TypeLiteral;
import com.google.inject.multibindings.MapBinder;

public abstract class ModifierModule extends AbstractModule {
  private final Class<? extends Modifier> fModifierClass;
  private final String fType;
  private final Modifier.Factory fFactory;
  private final Class<? extends ModifierWriter> fWriterClass;
  private final Class<? extends ModifierReader> fReaderClass;

  protected ModifierModule(
      Class<? extends Modifier> modifierClass,
      String type,
      Modifier.Factory factory,
      Class<? extends ModifierWriter> writerClass,
      Class<? extends ModifierReader> readerClass) {
    fModifierClass = modifierClass;
    fType = type;
    fFactory = factory;
    fWriterClass = writerClass;
    fReaderClass = readerClass;
  }

  @Override protected void configure() {
    MapBinder.newMapBinder(binder(), String.class, Modifier.Factory.class).addBinding(fType).toInstance(fFactory);

    MapBinder.newMapBinder(binder(),
        new TypeLiteral<Class<? extends Modifier>>() {},
        new TypeLiteral<ModifierWriter>() {}).addBinding(fModifierClass).to(fWriterClass);

    MapBinder.newMapBinder(binder(), String.class, ModifierReader.class).addBinding(fType).to(fReaderClass);
  }
}
