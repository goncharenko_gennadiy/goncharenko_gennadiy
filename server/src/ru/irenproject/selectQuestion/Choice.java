/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.selectQuestion;

import ru.irenproject.ChangeContentEvent;
import ru.irenproject.infra.Realm;
import ru.irenproject.infra.Resident;
import ru.irenproject.pad.Pad;

public final class Choice extends Resident {
  public enum ChangeEvent implements ChangeContentEvent { INSTANCE }

  private final Pad fPad;
  private boolean fCorrect;
  private boolean fFixed;
  private final boolean fNegative;

  public Choice(Realm realm, boolean negative) {
    super(realm);
    fPad = new Pad(realm);
    fNegative = negative;
  }

  public Pad pad() {
    return fPad;
  }

  public boolean fixed() {
    return fFixed;
  }

  public void setFixed(boolean fixed) {
    fFixed = fixed;
    post(ChangeEvent.INSTANCE);
  }

  public boolean correct() {
    return fCorrect;
  }

  public void setCorrect(boolean correct) {
    fCorrect = correct;
    post(ChangeEvent.INSTANCE);
  }

  public boolean negative() {
    return fNegative;
  }
}
