/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.selectQuestion;

import ru.irenproject.Question;
import ru.irenproject.TestCheck;
import ru.irenproject.infra.Realm;
import ru.irenproject.pad.Pad;
import ru.irenproject.selectQuestion.Proto.SelectQuestionType;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class SelectQuestion extends Question {
  private static final int MAX_CHOICES = 20;

  private final Pad fFormulation;
  private final ArrayList<Choice> fChoices = new ArrayList<>();

  public SelectQuestion(Realm realm) {
    super(realm);
    fFormulation = new Pad(realm);
  }

  @Override public String type() {
    return SelectQuestionType.select.name();
  }

  @Override public Pad formulation() {
    return fFormulation;
  }

  public void addChoice(Choice choice) {
    TestCheck.input(canAddChoice());
    if (choice.negative()) {
      TestCheck.input(!hasNegativeChoice());
    }

    fChoices.add(choice);
    post(ChangeEvent.INSTANCE);
  }

  public boolean canAddChoice() {
    return fChoices.size() < MAX_CHOICES;
  }

  public List<Choice> choices() {
    return Collections.unmodifiableList(fChoices);
  }

  public void deleteChoice(int index) {
    checkChoiceIndex(index);

    fChoices.remove(index);
    post(ChangeEvent.INSTANCE);
  }

  private void checkChoiceIndex(int index) {
    TestCheck.input(validChoiceIndex(index));
  }

  private boolean validChoiceIndex(int index) {
    return (index >= 0) && (index < fChoices.size());
  }

  private boolean canMoveChoice(int index, boolean forward) {
    return validChoiceIndex(index) && validChoiceIndex(index + (forward ? 1 : -1));
  }

  public void moveChoice(int index, boolean forward) {
    TestCheck.input(canMoveChoice(index, forward));

    Collections.swap(fChoices, index, index + (forward ? 1 : -1));
    post(ChangeEvent.INSTANCE);
  }

  public boolean hasNegativeChoice() {
    return fChoices.stream().anyMatch(Choice::negative);
  }

  @Override public void setDefaults(String initialText) {
    formulation().appendText(initialText);
  }
}
