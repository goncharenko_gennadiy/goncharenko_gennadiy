/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.selectQuestion;

import ru.irenproject.itx.Itx;
import ru.irenproject.itx.ItxReader;
import ru.irenproject.itx.QuestionReader;

import org.w3c.dom.Element;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton public final class SelectQuestionReader implements QuestionReader {
  @Inject private SelectQuestionReader() {}

  @Override public SelectQuestion read(ItxReader in, Element e) {
    SelectQuestion res = new SelectQuestion(in.realm());
    in.readPad(Itx.getChild(e, Itx.ELEM_CONTENT), res.formulation());

    Element xmlChoices = Itx.getChildIfExists(e, Itx.ELEM_CHOICES);
    if (xmlChoices != null) {
      for (Element xmlChoice : Itx.getChildren(xmlChoices, Itx.ELEM_CHOICE)) {
        readChoice(in, xmlChoice, res);
      }
    }

    return res;
  }

  private void readChoice(ItxReader in, Element e, SelectQuestion out) {
    Choice choice = new Choice(out.realm(), Itx.findBoolean(e, Itx.ATTR_NEGATIVE));
    choice.setCorrect(Itx.findBoolean(e, Itx.ATTR_CORRECT));
    choice.setFixed(Itx.findBoolean(e, Itx.ATTR_FIXED));
    in.readPad(Itx.getChild(e, Itx.ELEM_CONTENT), choice.pad());
    out.addChoice(choice);
  }
}
