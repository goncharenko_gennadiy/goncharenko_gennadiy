/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.scriptModifier;

import ru.irenproject.Modifier;
import ru.irenproject.itx.Itx;
import ru.irenproject.itx.ItxWriter;
import ru.irenproject.itx.ModifierWriter;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton public final class ScriptModifierWriter implements ModifierWriter {
  @Inject private ScriptModifierWriter() {}

  @Override public void write(Modifier modifier, ItxWriter out) {
    ScriptModifier m = (ScriptModifier) modifier;

    for (String line : m.script()) {
      out.begin(Itx.ELEM_SCRIPT);
      out.add(Itx.ATTR_LINE, line);
      out.end();
    }
  }
}
