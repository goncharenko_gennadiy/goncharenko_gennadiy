/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.scriptModifier;

import ru.irenproject.Modifier;
import ru.irenproject.infra.Realm;
import ru.irenproject.scriptModifier.Proto.ScriptModifierType;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public final class ScriptModifier extends Modifier {
  private final ArrayList<String> fScript = new ArrayList<>();

  public ScriptModifier(Realm realm) {
    super(realm);
  }

  @Override public String type() {
    return ScriptModifierType.script.name();
  }

  public List<String> script() {
    return Collections.unmodifiableList(fScript);
  }

  public void setScript(Collection<String> value) {
    fScript.clear();
    fScript.addAll(value);
  }
}
