/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.scriptModifier;

import ru.irenproject.itx.Itx;
import ru.irenproject.itx.ItxReader;
import ru.irenproject.itx.ModifierReader;

import org.w3c.dom.Element;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.stream.Collectors;

@Singleton public final class ScriptModifierReader implements ModifierReader {
  @Inject private ScriptModifierReader() {}

  @Override public ScriptModifier read(ItxReader in, Element e) {
    ScriptModifier res = new ScriptModifier(in.realm());

    res.setScript(Itx.getChildren(e, Itx.ELEM_SCRIPT).stream()
        .map(xmlScript -> Itx.find(xmlScript, Itx.ATTR_LINE))
        .collect(Collectors.toList()));

    return res;
  }
}
