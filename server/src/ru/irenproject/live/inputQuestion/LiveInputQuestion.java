/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.live.inputQuestion;

import ru.irenproject.Check;
import ru.irenproject.Oops;
import ru.irenproject.common.Proto.Dialog;
import ru.irenproject.common.Proto.DialogResponse;
import ru.irenproject.common.Proto.Flow;
import ru.irenproject.common.input.Proto.InputArea;
import ru.irenproject.common.input.Proto.InputResponse;
import ru.irenproject.inputQuestion.InputQuestion;
import ru.irenproject.inputQuestion.Pattern;
import ru.irenproject.live.LiveQuestion;
import ru.irenproject.live.Proto._LiveQuestion;
import ru.irenproject.live.QuestionUtils;
import ru.irenproject.test.Proto.EvaluationModelType;

import com.google.inject.assistedinject.Assisted;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.Map;

public final class LiveInputQuestion extends LiveQuestion {
  public interface Factory extends LiveQuestion.Factory {
    @Override LiveInputQuestion create(_LiveQuestion message, SourceQuestionResolver resolver);
  }

  public static final int MAX_RESPONSE_SIZE = Pattern.MAX_LENGTH;

  private static final DialogResponse EMPTY_RESPONSE = buildResponse("");

  private static DialogResponse buildResponse(String input) {
    DialogResponse.Builder b = DialogResponse.newBuilder();
    b.addAreaResponseBuilder()
        .setAreaId(DEFAULT_AREA_ID)
        .setExtension(InputResponse.inputResponse, InputResponse.newBuilder()
            .setInput(input)
            .build());
    return b.build();
  }

  private final Map<Class<? extends Pattern>, PatternMatcher> fPatternMatchers;

  @Inject private LiveInputQuestion(@Assisted _LiveQuestion message, @Assisted SourceQuestionResolver resolver,
      Map<Class<? extends Pattern>, PatternMatcher> patternMatchers) {
    super(message, resolver);
    fPatternMatchers = patternMatchers;
  }

  @Override public Dialog render() {
    Dialog.Builder b = Dialog.newBuilder();

    Flow formulation = QuestionUtils.toFlow(source().formulation());
    b.addArea(QuestionUtils.buildFlowArea(contentTransformer().transformFlow(formulation)));

    b.addAreaBuilder()
        .setKind(InputArea.INPUT_AREA_FIELD_NUMBER)
        .setId(DEFAULT_AREA_ID)
        .setExtension(InputArea.inputArea, InputArea.newBuilder()
            .setMaxSize(MAX_RESPONSE_SIZE)
            .build());
    return b.build();
  }

  @Override public InputQuestion source() {
    return (InputQuestion) super.source();
  }

  @Override public DialogResponse getEmptyResponse() {
    return EMPTY_RESPONSE;
  }

  @Override public DialogResponse getCorrectResponse() {
    String correct = source().patterns().isEmpty() ?
        "" : contentTransformer().transformText(source().patterns().get(0).value());
    return buildResponse(correct);
  }

  @Override public BigDecimal evaluate(DialogResponse response) {
    String input = getSingleResponse(response, InputResponse.inputResponse).getInput();
    checkResponse(input.length() <= MAX_RESPONSE_SIZE);

    BigDecimal res = BigDecimal.ZERO;

    for (Pattern p : source().patterns()) {
      PatternMatcher pm = Check.notNull(fPatternMatchers.get(p.getClass()),
          () -> Oops.format("No pattern matcher for %s.", p.getClass()));
      res = pm.match(p, contentTransformer(), input);
      if (res.signum() > 0) {
        break;
      }
    }

    switch (getEvaluationModelOrDefault(EvaluationModelType.LAX)) {
      case DICHOTOMIC: {
        if (res.compareTo(BigDecimal.ONE) < 0) {
          res = BigDecimal.ZERO;
        }
        break;
      }
      case LAX: {
        break; // do nothing
      }
    }

    return res;
  }
}
