/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.live.inputQuestion;

import ru.irenproject.inputQuestion.Pattern;
import ru.irenproject.inputQuestion.RegexpPattern;
import ru.irenproject.live.ContentTransformer;

import com.google.common.base.CharMatcher;
import com.google.re2j.PatternSyntaxException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.math.BigDecimal;

@Singleton public final class RegexpPatternMatcher implements PatternMatcher {
  private static final Logger fLogger = LoggerFactory.getLogger(RegexpPatternMatcher.class);

  @Inject private RegexpPatternMatcher() {}

  @Override public BigDecimal match(Pattern pattern, ContentTransformer contentTransformer, String input) {
    RegexpPattern p = (RegexpPattern) pattern;
    String regexp = contentTransformer.transformText(p.value());

    String preparedInput;
    switch (p.spacePreprocessMode()) {
      case KEEP: {
        preparedInput = input;
        break;
      }
      case NORMALIZE: {
        preparedInput = CharMatcher.whitespace().trimAndCollapseFrom(input, ' ');
        break;
      }
      case REMOVE: {
        preparedInput = CharMatcher.whitespace().removeFrom(input);
        break;
      }
      default: {
        throw new RuntimeException();
      }
    }

    int flags = com.google.re2j.Pattern.DISABLE_UNICODE_GROUPS;
    if (!p.caseSensitive()) {
      flags |= com.google.re2j.Pattern.CASE_INSENSITIVE;
    }

    com.google.re2j.Pattern compiled;
    try {
      compiled = com.google.re2j.Pattern.compile(regexp, flags); //TODO cache
    } catch (PatternSyntaxException e) {
      compiled = null;
      fLogger.warn("", e);
    }

    boolean matches = (compiled != null) && compiled.matches(preparedInput);
    return matches ? BigDecimal.valueOf(p.qualityPercent(), 2) : BigDecimal.ZERO;
  }
}
