/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.live.inputQuestion;

import ru.irenproject.inputQuestion.InputQuestion;
import ru.irenproject.inputQuestion.Pattern;
import ru.irenproject.inputQuestion.RegexpPattern;
import ru.irenproject.inputQuestion.TextPattern;
import ru.irenproject.live.LiveQuestionModule;
import ru.irenproject.live.inputQuestion.Proto._LiveInputQuestion;

import com.google.inject.TypeLiteral;
import com.google.inject.multibindings.MapBinder;

public final class LiveInputQuestionModule extends LiveQuestionModule {
  public LiveInputQuestionModule() {
    super(
        InputQuestion.class,
        InputQuestionSpawner.class,
        LiveInputQuestion.Factory.class,
        _LiveInputQuestion.LIVE_INPUT_QUESTION_FIELD_NUMBER);
  }

  @Override protected void configure() {
    super.configure();
    MapBinder<Class<? extends Pattern>, PatternMatcher> b = MapBinder.newMapBinder(binder(),
        new TypeLiteral<Class<? extends Pattern>>() {},
        new TypeLiteral<PatternMatcher>() {});
    b.addBinding(TextPattern.class).to(TextPatternMatcher.class);
    b.addBinding(RegexpPattern.class).to(RegexpPatternMatcher.class);
  }
}
