/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.live.inputQuestion;

import ru.irenproject.Question;
import ru.irenproject.live.IssueOrderList;
import ru.irenproject.live.LiveQuestion;
import ru.irenproject.live.Proto._LiveQuestion;
import ru.irenproject.live.QuestionSpawner;
import ru.irenproject.live.inputQuestion.Proto._LiveInputQuestion;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton public final class InputQuestionSpawner implements QuestionSpawner {
  @Inject private InputQuestionSpawner() {}

  @Override public void prepare(Question source, IssueOrderList orderList) {}

  @Override public _LiveQuestion spawn(Question source, IssueOrderList orderList) {
    return LiveQuestion.initialize(_LiveInputQuestion.LIVE_INPUT_QUESTION_FIELD_NUMBER, source.id(), orderList)
        .setExtension(_LiveInputQuestion.liveInputQuestion, _LiveInputQuestion.getDefaultInstance())
        .build();
  }
}
