/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.live.classifyQuestion;

import ru.irenproject.Utils;
import ru.irenproject.classifyQuestion.ClassifyQuestion;
import ru.irenproject.common.Proto.Dialog;
import ru.irenproject.common.Proto.DialogResponse;
import ru.irenproject.common.Proto.Flow;
import ru.irenproject.common.classify.Proto.ClassifyArea;
import ru.irenproject.common.classify.Proto.ClassifyResponse;
import ru.irenproject.live.LiveQuestion;
import ru.irenproject.live.Proto._LiveQuestion;
import ru.irenproject.live.QuestionUtils;
import ru.irenproject.live.classifyQuestion.Proto.OfferedItem;
import ru.irenproject.live.classifyQuestion.Proto._LiveClassifyQuestion;
import ru.irenproject.test.Proto.EvaluationModelType;

import com.google.inject.assistedinject.Assisted;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public final class LiveClassifyQuestion extends LiveQuestion {
  public interface Factory extends LiveQuestion.Factory {
    @Override LiveClassifyQuestion create(_LiveQuestion message, SourceQuestionResolver resolver);
  }

  private final _LiveClassifyQuestion $;

  @Inject private LiveClassifyQuestion(@Assisted _LiveQuestion message, @Assisted SourceQuestionResolver resolver) {
    super(message, resolver);
    $ = Utils.getExistingExtension(message, _LiveClassifyQuestion.liveClassifyQuestion);
  }

  @Override public ClassifyQuestion source() {
    return (ClassifyQuestion) super.source();
  }

  @Override public Dialog render() {
    Dialog.Builder b = Dialog.newBuilder();

    Flow formulation = QuestionUtils.toFlow(source().formulation());
    b.addArea(QuestionUtils.buildFlowArea(contentTransformer().transformFlow(formulation)));

    b.addAreaBuilder()
        .setKind(ClassifyArea.CLASSIFY_AREA_FIELD_NUMBER)
        .setId(DEFAULT_AREA_ID)
        .setExtension(ClassifyArea.classifyArea, buildClassifyArea());
    return b.build();
  }

  private ClassifyArea buildClassifyArea() {
    ClassifyArea.Builder b = ClassifyArea.newBuilder();

    for (int categoryCount = source().categoryCount(), i = 0; i < categoryCount; ++i) {
      b.addCategoryTitle(contentTransformer().transformFlow(QuestionUtils.toFlow(source().getCategoryTitle(i))));
    }

    for (OfferedItem offeredItem : $.getOfferedPlacementList()) {
      b.addOffered(contentTransformer().transformFlow(QuestionUtils.toFlow(source()
          .getCategoryItems(offeredItem.getCategoryIndex())
          .get(offeredItem.getItemIndex()))));
    }

    return b.build();
  }

  @Override public DialogResponse getEmptyResponse() {
    return buildResponse(Collections.nCopies($.getOfferedPlacementCount(), -1));
  }

  private static DialogResponse buildResponse(Iterable<Integer> mapping) {
    DialogResponse.Builder b = DialogResponse.newBuilder();
    b.addAreaResponseBuilder()
        .setAreaId(DEFAULT_AREA_ID)
        .setExtension(ClassifyResponse.classifyResponse, ClassifyResponse.newBuilder().addAllMapping(mapping).build());
    return b.build();
  }

  @Override public DialogResponse getCorrectResponse() {
    return buildResponse(getCorrectMapping());
  }

  @Override public BigDecimal evaluate(DialogResponse response) {
    ClassifyResponse r = getSingleResponse(response, ClassifyResponse.classifyResponse);
    check(r);

    List<Integer> answer = r.getMappingList();
    BigDecimal res;

    switch (getEvaluationModelOrDefault(EvaluationModelType.DICHOTOMIC)) {
      case DICHOTOMIC: {
        res = getCorrectMapping().equals(answer) ? BigDecimal.ONE : BigDecimal.ZERO;
        break;
      }
      case LAX: {
        res = QuestionUtils.evaluateMappingLaxly(answer, getCorrectMapping());
        break;
      }
      default: {
        throw new RuntimeException();
      }
    }

    return res;
  }

  private void check(ClassifyResponse response) {
    checkResponse(response.getMappingCount() == $.getOfferedPlacementCount());
    int categoryCount = source().categoryCount();

    for (int categoryIndex : response.getMappingList()) {
      checkResponse((categoryIndex >= -1) && (categoryIndex < categoryCount));
    }
  }

  private List<Integer> getCorrectMapping() {
    return $.getOfferedPlacementList().stream()
        .map(OfferedItem::getCategoryIndex)
        .collect(Collectors.toList());
  }
}
