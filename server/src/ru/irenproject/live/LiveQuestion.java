/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.live;

import ru.irenproject.Question;
import ru.irenproject.common.Proto.AreaResponse;
import ru.irenproject.common.Proto.Dialog;
import ru.irenproject.common.Proto.DialogResponse;
import ru.irenproject.live.Proto._LiveQuestion;
import ru.irenproject.test.Proto.EvaluationModelType;

import com.google.protobuf.GeneratedMessage.GeneratedExtension;

import javax.annotation.Nullable;
import java.math.BigDecimal;

public abstract class LiveQuestion {
  public interface Factory {
    @SuppressWarnings({"EmptyMethod", "UnusedParameters"})
    LiveQuestion create(_LiveQuestion message, SourceQuestionResolver resolver);
  }

  public interface SourceQuestionResolver {
    Question getQuestion(long id);
  }

  public static final int SCORE_FRACTION_DIGITS = 4;
  protected static final String DEFAULT_AREA_ID = "_";

  public static _LiveQuestion.Builder initialize(int kind, long sourceQuestionId, IssueOrderList issueOrders) {
    _LiveQuestion.Builder res = _LiveQuestion.newBuilder()
        .setKind(kind)
        .setSourceQuestionId(sourceQuestionId)
        .setWeight(issueOrders.getInstance(SetWeightIssueOrder.class).weight())
        .setContentTransformer(ContentTransformer.build(issueOrders));

    SetEvaluationModelIssueOrder evaluationModel = issueOrders.getInstanceIfExists(SetEvaluationModelIssueOrder.class);
    if (evaluationModel != null) {
      res.setEvaluationModel(evaluationModel.modelType());
    }

    return res;
  }

  public static void checkResponse(boolean condition) {
    if (!condition) {
      throw new BadResponseException();
    }
  }

  public static <T> T getSingleResponse(DialogResponse response, GeneratedExtension<AreaResponse, T> extension) {
    checkResponse(response.getAreaResponseCount() == 1);
    checkResponse(response.getAreaResponse(0).hasExtension(extension));
    return response.getAreaResponse(0).getExtension(extension);
  }

  private final _LiveQuestion $;
  private final Question fSource;
  private @Nullable ContentTransformer fContentTransformer;

  protected LiveQuestion(_LiveQuestion message, SourceQuestionResolver resolver) {
    $ = message;
    fSource = resolver.getQuestion($.getSourceQuestionId());
  }

  public Question source() {
    return fSource;
  }

  public final ContentTransformer contentTransformer() {
    if (fContentTransformer == null) {
      fContentTransformer = new ContentTransformer($.getContentTransformer());
    }
    return fContentTransformer;
  }

  public final EvaluationModelType getEvaluationModelOrDefault(EvaluationModelType defaultValue) {
    return $.hasEvaluationModel() ? $.getEvaluationModel() : defaultValue;
  }

  public abstract Dialog render();
  public abstract DialogResponse getEmptyResponse();
  public abstract DialogResponse getCorrectResponse();

  public abstract BigDecimal evaluate(DialogResponse response);
}
