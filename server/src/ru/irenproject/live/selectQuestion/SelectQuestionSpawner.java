/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.live.selectQuestion;

import ru.irenproject.Question;
import ru.irenproject.Translate;
import ru.irenproject.common.Proto.Block;
import ru.irenproject.common.Proto.BlockType;
import ru.irenproject.common.Proto.Flow;
import ru.irenproject.common.Proto.TextBlock;
import ru.irenproject.live.IssueOrderList;
import ru.irenproject.live.LiveQuestion;
import ru.irenproject.live.Proto._LiveQuestion;
import ru.irenproject.live.QuestionSpawner;
import ru.irenproject.live.SetLocaleIssueOrder;
import ru.irenproject.live.selectQuestion.Proto._LiveSelectQuestion;
import ru.irenproject.selectQuestion.Choice;
import ru.irenproject.selectQuestion.SelectQuestion;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

@Singleton public final class SelectQuestionSpawner implements QuestionSpawner {
  private final ThreadLocal<SecureRandom> fSecureRandom = ThreadLocal.withInitial(SecureRandom::new);

  @Inject private SelectQuestionSpawner() {}

  @Override public void prepare(Question source, IssueOrderList orderList) {
    orderList.add(new SelectQuestionIssueOrder());
  }

  @Override public _LiveQuestion spawn(Question source, IssueOrderList orderList) {
    SelectQuestion q = (SelectQuestion) source;
    SelectQuestionIssueOrder order = orderList.getInstance(SelectQuestionIssueOrder.class);

    ArrayList<Integer> choiceIndices = new ArrayList<>();
    if (order.shuffleChoices()) {
      shuffleChoices(q, choiceIndices);
    } else {
      for (int choiceCount = q.choices().size(), i = 0; i < choiceCount; ++i) {
        choiceIndices.add(i);
      }
    }

    boolean hasNegativeChoice = q.hasNegativeChoice();

    _LiveSelectQuestion.Builder b = _LiveSelectQuestion.newBuilder()
        .addAllChoiceIndex(choiceIndices)
        .setMultipleSelection(order.suppressSingleChoiceHint() || !hasSingleCorrectChoice(q))
        .setAddNegativeChoice(order.addNegativeChoice() && !hasNegativeChoice);

    Flow negativeChoiceContent = order.negativeChoiceContent();
    if (negativeChoiceContent == null) {
      if (b.getAddNegativeChoice()) {
        b.setNegativeChoiceContent(getDefaultNegativeChoice(orderList.getInstance(SetLocaleIssueOrder.class).locale()));
      }
    } else {
      if (hasNegativeChoice || order.addNegativeChoice()) {
        b.setNegativeChoiceContent(negativeChoiceContent);
      }
    }

    return LiveQuestion.initialize(_LiveSelectQuestion.LIVE_SELECT_QUESTION_FIELD_NUMBER, source.id(), orderList)
        .setExtension(_LiveSelectQuestion.liveSelectQuestion, b.build())
        .build();
  }

  private void shuffleChoices(SelectQuestion question, ArrayList<Integer> choiceIndices) {
    List<Choice> choices = question.choices();

    ArrayList<Integer> movableChoiceIndices = new ArrayList<>();
    for (int i = 0; i < choices.size(); ++i) {
      if (!choices.get(i).fixed()) {
        movableChoiceIndices.add(i);
      }
    }

    Collections.shuffle(movableChoiceIndices, fSecureRandom.get());

    int nextMovable = 0;
    for (int i = 0; i < choices.size(); ++i) {
      if (choices.get(i).fixed()) {
        choiceIndices.add(i);
      } else {
        choiceIndices.add(movableChoiceIndices.get(nextMovable));
        ++nextMovable;
      }
    }
  }

  private boolean hasSingleCorrectChoice(SelectQuestion question) {
    boolean res = false;

    for (Choice choice : question.choices()) {
      if (choice.correct()) {
        if (res) {
          res = false;
          break;
        } else {
          res = true;
        }
      }
    }

    return res;
  }

  private Flow getDefaultNegativeChoice(Locale locale) {
    return Flow.newBuilder()
        .addBlock(Block.newBuilder()
            .setType(BlockType.TEXT)
            .setTextBlock(TextBlock.newBuilder()
                .setText(Translate.message("negativeChoice", locale))))
        .build();
  }
}
