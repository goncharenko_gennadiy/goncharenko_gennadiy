/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.live.selectQuestion;

import ru.irenproject.Check;
import ru.irenproject.Utils;
import ru.irenproject.common.Proto.Dialog;
import ru.irenproject.common.Proto.DialogResponse;
import ru.irenproject.common.Proto.Flow;
import ru.irenproject.common.select.Proto.SelectArea;
import ru.irenproject.common.select.Proto.SelectResponse;
import ru.irenproject.live.LiveQuestion;
import ru.irenproject.live.Proto._LiveQuestion;
import ru.irenproject.live.QuestionUtils;
import ru.irenproject.live.selectQuestion.Proto._LiveSelectQuestion;
import ru.irenproject.selectQuestion.Choice;
import ru.irenproject.selectQuestion.SelectQuestion;
import ru.irenproject.test.Proto.EvaluationModelType;

import com.google.inject.assistedinject.Assisted;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public final class LiveSelectQuestion extends LiveQuestion {
  public interface Factory extends LiveQuestion.Factory {
    @Override LiveSelectQuestion create(_LiveQuestion message, SourceQuestionResolver resolver);
  }

  private final _LiveSelectQuestion $;

  @Inject private LiveSelectQuestion(@Assisted _LiveQuestion message, @Assisted SourceQuestionResolver resolver) {
    super(message, resolver);
    $ = Utils.getExistingExtension(message, _LiveSelectQuestion.liveSelectQuestion);
  }

  @Override public SelectQuestion source() {
    return (SelectQuestion) super.source();
  }

  private int dialogChoiceCount() {
    return source().choices().size() + ($.getAddNegativeChoice() ? 1 : 0);
  }

  @Override public Dialog render() {
    Dialog.Builder b = Dialog.newBuilder();

    Flow formulation = QuestionUtils.toFlow(source().formulation());
    b.addArea(QuestionUtils.buildFlowArea(contentTransformer().transformFlow(formulation)));

    SelectArea.Builder sab = SelectArea.newBuilder();
    sab.setMultipleSelection($.getMultipleSelection());

    for (Choice choice : shuffledChoiceList()) {
      Flow flow = (choice.negative() && $.hasNegativeChoiceContent()) ?
          $.getNegativeChoiceContent() : QuestionUtils.toFlow(choice.pad());
      sab.addChoice(contentTransformer().transformFlow(flow));
    }

    if ($.getAddNegativeChoice()) {
      Check.that($.hasNegativeChoiceContent());
      sab.addChoice(contentTransformer().transformFlow($.getNegativeChoiceContent()));
    }

    b.addAreaBuilder()
        .setKind(SelectArea.SELECT_AREA_FIELD_NUMBER)
        .setId(DEFAULT_AREA_ID)
        .setExtension(SelectArea.selectArea, sab.build());

    return b.build();
  }

  private List<Choice> shuffledChoiceList() {
    List<Choice> choiceList = source().choices();
    return $.getChoiceIndexList().stream()
        .map(choiceList::get)
        .collect(Collectors.toList());
  }

  @Override public DialogResponse getEmptyResponse() {
    DialogResponse.Builder b = DialogResponse.newBuilder();
    b.addAreaResponseBuilder()
        .setAreaId(DEFAULT_AREA_ID)
        .setExtension(SelectResponse.selectResponse, SelectResponse.newBuilder()
            .addAllSelected(Collections.nCopies(dialogChoiceCount(), false)).build());
    return b.build();
  }

  @Override public DialogResponse getCorrectResponse() {
    DialogResponse.Builder b = DialogResponse.newBuilder();
    b.addAreaResponseBuilder()
        .setAreaId(DEFAULT_AREA_ID)
        .setExtension(SelectResponse.selectResponse, SelectResponse.newBuilder()
            .addAllSelected(correctAnswer()).build());
    return b.build();
  }

  private List<Boolean> correctAnswer() {
    ArrayList<Boolean> res = new ArrayList<>();

    for (Choice choice : shuffledChoiceList()) {
      res.add(choice.correct());
    }

    if ($.getAddNegativeChoice()) {
      res.add(false);
    }

    return res;
  }

  @Override public BigDecimal evaluate(DialogResponse response) {
    SelectResponse r = getSingleResponse(response, SelectResponse.selectResponse);
    checkResponse(r.getSelectedCount() == dialogChoiceCount());

    BigDecimal res;
    switch (getEvaluationModelOrDefault(EvaluationModelType.DICHOTOMIC)) {
      case DICHOTOMIC: {
        res = correctAnswer().equals(r.getSelectedList()) ? BigDecimal.ONE : BigDecimal.ZERO;
        break;
      }
      case LAX: {
        res = evaluateLaxly(r);
        break;
      }
      default: {
        throw new RuntimeException();
      }
    }

    return res;
  }

  private BigDecimal evaluateLaxly(SelectResponse r) {
    List<Boolean> correctAnswer = correctAnswer();
    int correctTotal = 0;
    int correctSelected = 0;
    boolean incorrectSelected = false;

    for (int i = 0; i < correctAnswer.size(); ++i) {
      boolean selected = r.getSelected(i);
      if (correctAnswer.get(i)) {
        ++correctTotal;
        if (selected) {
          ++correctSelected;
        }
      } else {
        if (selected) {
          incorrectSelected = true;
          break;
        }
      }
    }

    BigDecimal res;
    if (incorrectSelected) {
      res = BigDecimal.ZERO;
    } else if (correctSelected == correctTotal) {
      res = BigDecimal.ONE;
    } else {
      res = BigDecimal.valueOf(correctSelected).divide(BigDecimal.valueOf(correctTotal),
          SCORE_FRACTION_DIGITS, RoundingMode.DOWN);
    }

    return res;
  }
}
