/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.live;

import ru.irenproject.Check;
import ru.irenproject.Oops;
import ru.irenproject.Translate;
import ru.irenproject.Utils;

import com.google.common.base.Joiner;
import com.google.common.io.ByteStreams;
import com.google.common.io.LittleEndianDataInputStream;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;

@Singleton public final class ScriptRunner {
  private static final Logger fLogger = LoggerFactory.getLogger(ScriptRunner.class);

  private static final Joiner LINE_JOINER = Joiner.on("\r\n");

  private final Path fExecutable;

  @Inject private ScriptRunner() {
    fExecutable = Utils.externalResourcePath().resolve("irenScriptRunner");
    fLogger.debug("Script runner executable: '{}'.", fExecutable);
    Check.that(Files.isRegularFile(fExecutable));
  }

  public Result run(Iterable<String> script, Locale locale) {
    try {
      byte[] input = LINE_JOINER.join(script).getBytes(StandardCharsets.UTF_8);

      ProcessBuilder pb = new ProcessBuilder(fExecutable.toString());
      pb.environment().put("RU_IRENPROJECT_SCRIPT_DECIMAL_SEPARATOR",
          Translate.message("scriptDecimalSeparator", locale));
      pb.redirectErrorStream(true);

      Process process = pb.start();
      OutputStream stdin = process.getOutputStream();
      InputStream stdout = process.getInputStream();
      InputStream stderr = process.getErrorStream();
      try {
        stdin.write(input);
        stdin.close();
        stdin = null;

        byte[] output = ByteStreams.toByteArray(stdout);

        int exitCode = Utils.waitForProcessUninterruptibly(process);
        process = null;

        if (exitCode != 0) {
          throw Oops.format("Script execution failed (exit code %d): %s", exitCode,
              new String(output, StandardCharsets.UTF_8));
        }

        return readResult(output);
      } finally {
        IOUtils.closeQuietly(stdin);
        IOUtils.closeQuietly(stdout);
        IOUtils.closeQuietly(stderr);

        if (process != null) {
          process.destroy();
        }
      }
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  public static final class Result {
    private final LinkedHashMap<String, String> fVariables = new LinkedHashMap<>();

    private Result() {}

    public Map<String, String> variables() {
      return Collections.unmodifiableMap(fVariables);
    }
  }

  private Result readResult(byte[] data) {
    try {
      Result res = new Result();
      LittleEndianDataInputStream in = new LittleEndianDataInputStream(new ByteArrayInputStream(data));
      int variableCount = in.readInt();

      for (int i = 0; i < variableCount; ++i) {
        String name = readString(in);
        String value = readString(in);
        res.fVariables.put(name, value);
      }

      return res;
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  private String readString(LittleEndianDataInputStream in) {
    try {
      int size = in.readInt();
      Check.that(size >= 0);
      Check.that(size <= in.available());

      byte[] buffer = new byte[size];
      in.readFully(buffer);
      return new String(buffer, StandardCharsets.UTF_8);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }
}
