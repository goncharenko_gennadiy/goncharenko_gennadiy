/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.live;

import ru.irenproject.Check;
import ru.irenproject.Oops;
import ru.irenproject.live.LiveQuestion.SourceQuestionResolver;
import ru.irenproject.live.Proto._LiveQuestion;

import com.google.common.collect.ImmutableMap;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.Map;

@Singleton public final class LiveQuestionLoader {
  private final ImmutableMap<Integer, LiveQuestion.Factory> fFactories;

  @Inject private LiveQuestionLoader(Map<Integer, LiveQuestion.Factory> factories) {
    fFactories = ImmutableMap.copyOf(factories);
  }

  public LiveQuestion load(_LiveQuestion message, SourceQuestionResolver resolver) {
    LiveQuestion.Factory factory = Check.notNull(fFactories.get(message.getKind()),
        () -> Oops.format("Unknown question kind: %d.", message.getKind()));
    return factory.create(message, resolver);
  }
}
