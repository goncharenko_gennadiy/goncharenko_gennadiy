/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.live;

import ru.irenproject.Check;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class IssueOrderList {
  private final ArrayList<IssueOrder> fItems = new ArrayList<>();

  public IssueOrderList add(IssueOrder issueOrder) {
    fItems.add(issueOrder);
    return this;
  }

  public @Nullable <T extends IssueOrder> T getInstanceIfExists(Class<T> cls) {
    return fItems.stream()
        .filter(cls::isInstance)
        .findFirst()
        .map(cls::cast)
        .orElse(null);
  }

  public <T extends IssueOrder> T getInstance(Class<T> cls) {
    return Check.notNull(getInstanceIfExists(cls));
  }

  public List<IssueOrder> items() {
    return Collections.unmodifiableList(fItems);
  }
}
