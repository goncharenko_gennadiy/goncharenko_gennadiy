/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.live;

import ru.irenproject.Question;

import com.google.inject.AbstractModule;
import com.google.inject.TypeLiteral;
import com.google.inject.assistedinject.FactoryModuleBuilder;
import com.google.inject.multibindings.MapBinder;

public abstract class LiveQuestionModule extends AbstractModule {
  private final Class<? extends Question> fQuestionClass;
  private final Class<? extends QuestionSpawner> fSpawnerClass;
  private final Class<? extends LiveQuestion.Factory> fLiveQuestionFactoryClass;
  private final int fExtensionNumber;

  protected LiveQuestionModule(
      Class<? extends Question> questionClass,
      Class<? extends QuestionSpawner> spawnerClass,
      Class<? extends LiveQuestion.Factory> liveQuestionFactoryClass,
      int extensionNumber) {
    fQuestionClass = questionClass;
    fSpawnerClass = spawnerClass;
    fLiveQuestionFactoryClass = liveQuestionFactoryClass;
    fExtensionNumber = extensionNumber;
  }

  @Override protected void configure() {
    MapBinder.newMapBinder(binder(),
        new TypeLiteral<Class<? extends Question>>() {},
        new TypeLiteral<QuestionSpawner>() {}).addBinding(fQuestionClass).to(fSpawnerClass);
    MapBinder.newMapBinder(binder(),
        Integer.class,
        LiveQuestion.Factory.class).addBinding(fExtensionNumber).to(fLiveQuestionFactoryClass);
    install(new FactoryModuleBuilder().build(fLiveQuestionFactoryClass));
  }
}
