/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.live.orderQuestion;

import ru.irenproject.Check;
import ru.irenproject.Utils;
import ru.irenproject.common.Proto.Dialog;
import ru.irenproject.common.Proto.DialogResponse;
import ru.irenproject.common.Proto.Flow;
import ru.irenproject.common.order.Proto.OrderArea;
import ru.irenproject.common.order.Proto.OrderResponse;
import ru.irenproject.live.LiveQuestion;
import ru.irenproject.live.Proto._LiveQuestion;
import ru.irenproject.live.QuestionUtils;
import ru.irenproject.live.orderQuestion.Proto._LiveOrderQuestion;
import ru.irenproject.orderQuestion.OrderQuestion;
import ru.irenproject.pad.Pad;
import ru.irenproject.test.Proto.EvaluationModelType;

import com.google.common.primitives.Ints;
import com.google.inject.assistedinject.Assisted;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public final class LiveOrderQuestion extends LiveQuestion {
  public interface Factory extends LiveQuestion.Factory {
    @Override LiveOrderQuestion create(_LiveQuestion message, SourceQuestionResolver resolver);
  }

  private final _LiveOrderQuestion $;

  @Inject private LiveOrderQuestion(@Assisted _LiveQuestion message, @Assisted SourceQuestionResolver resolver) {
    super(message, resolver);
    $ = Utils.getExistingExtension(message, _LiveOrderQuestion.liveOrderQuestion);
  }

  @Override public OrderQuestion source() {
    return (OrderQuestion) super.source();
  }

  @Override public Dialog render() {
    Dialog.Builder b = Dialog.newBuilder();

    Flow formulation = QuestionUtils.toFlow(source().formulation());
    b.addArea(QuestionUtils.buildFlowArea(contentTransformer().transformFlow(formulation)));

    b.addAreaBuilder()
        .setKind(OrderArea.ORDER_AREA_FIELD_NUMBER)
        .setId(DEFAULT_AREA_ID)
        .setExtension(OrderArea.orderArea, buildOrderArea());
    return b.build();
  }

  private OrderArea buildOrderArea() {
    OrderArea.Builder b = OrderArea.newBuilder();

    List<Pad> items = source().items();
    int itemCount = items.size();
    List<Pad> distractors = source().distractors();

    for (int i : $.getOfferedPlacementList()) {
      Pad pad = (i >= itemCount) ? distractors.get(i - itemCount) : items.get(i);
      b.addOffered(contentTransformer().transformFlow(QuestionUtils.toFlow(pad)));
    }

    return b.build();
  }

  @Override public DialogResponse getEmptyResponse() {
    return buildResponse(Collections.nCopies($.getCorrectPlacementCount(), -1));
  }

  private static DialogResponse buildResponse(Iterable<Integer> mapping) {
    DialogResponse.Builder b = DialogResponse.newBuilder();
    b.addAreaResponseBuilder()
        .setAreaId(DEFAULT_AREA_ID)
        .setExtension(OrderResponse.orderResponse, OrderResponse.newBuilder().addAllMapping(mapping).build());
    return b.build();
  }

  @Override public DialogResponse getCorrectResponse() {
    ArrayList<Integer> correctMapping = new ArrayList<>();
    int[] inverse = inverseOfferedPlacement();

    for (int itemIndex : $.getCorrectPlacementList()) {
      int offeredIndex = inverse[itemIndex];
      Check.that(offeredIndex != -1);
      correctMapping.add(offeredIndex);
    }

    return buildResponse(correctMapping);
  }

  @Override public BigDecimal evaluate(DialogResponse response) {
    OrderResponse r = getSingleResponse(response, OrderResponse.orderResponse);
    check(r);

    List<Integer> answer = mappingToSourceIndices(r.getMappingList());
    BigDecimal res;

    switch (getEvaluationModelOrDefault(EvaluationModelType.DICHOTOMIC)) {
      case DICHOTOMIC: {
        res = $.getCorrectPlacementList().equals(answer) ? BigDecimal.ONE : BigDecimal.ZERO;
        break;
      }
      case LAX: {
        res = evaluateLaxly(answer);
        break;
      }
      default: {
        throw new RuntimeException();
      }
    }

    return res;
  }

  private void check(OrderResponse response) {
    checkResponse(response.getMappingCount() == $.getCorrectPlacementCount());
    int offeredCount = $.getOfferedPlacementCount();
    boolean[] chosen = new boolean[offeredCount];

    for (int offeredIndex : response.getMappingList()) {
      checkResponse((offeredIndex >= -1) && (offeredIndex < offeredCount));
      if (offeredIndex != -1) {
        checkResponse(!chosen[offeredIndex]);
        chosen[offeredIndex] = true;
      }
    }
  }

  private List<Integer> mappingToSourceIndices(Iterable<Integer> mapping) {
    ArrayList<Integer> res = new ArrayList<>();
    for (int offeredIndex : mapping) {
      res.add((offeredIndex == -1) ? -1 : $.getOfferedPlacement(offeredIndex));
    }
    return res;
  }

  private int[] inverseOfferedPlacement() {
    int[] res = new int[source().items().size() + source().distractors().size()];
    Arrays.fill(res, -1);

    for (int i = 0; i < $.getOfferedPlacementCount(); ++i) {
      res[$.getOfferedPlacement(i)] = i;
    }

    return res;
  }

  private BigDecimal evaluateLaxly(List<Integer> answer) {
    int itemCount = source().items().size();
    ArrayList<Integer> chosenSequence = new ArrayList<>();
    int distractorsChosen = 0;

    for (int a : answer) {
      if (a != -1) {
        if (a < itemCount) {
          chosenSequence.add(a);
        } else {
          ++distractorsChosen;
        }
      }
    }

    int itemsChosen = chosenSequence.size();
    int itemsTotal = answer.size();
    BigDecimal res;

    if (itemsTotal == 0) {
      res = BigDecimal.ONE;
    } else if (itemsTotal == 1) {
      res = (itemsChosen == 1) ? BigDecimal.ONE : BigDecimal.ZERO;
    } else if ((itemsChosen <= 1) || (distractorsChosen >= itemsChosen)) {
      res = BigDecimal.ZERO;
    } else {
      double k = computeKendallCorrelation(Ints.toArray(chosenSequence));
      double v = Double.max(k, 0) * (itemsChosen - distractorsChosen) / itemsTotal;
      double v2 = v * v;
      res = BigDecimal.valueOf(v2).setScale(SCORE_FRACTION_DIGITS, RoundingMode.DOWN);
    }

    return res;
  }

  private static double computeKendallCorrelation(int[] a) {
    double res;
    int length = a.length;

    if (length >= 2) {
      int totalPairs = Ints.checkedCast(((long) length) * (length - 1) / 2);
      int concordantPairs = 0;

      for (int i = 0; i < length - 1; ++i) {
        int v = a[i];
        for (int j = i + 1; j < length; ++j) {
          if (v < a[j]) {
            ++concordantPairs;
          }
        }
      }

      res = (2.0*concordantPairs - totalPairs) / totalPairs;
    } else {
      res = 0;
    }

    return res;
  }
}
