/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.live;

import ru.irenproject.Utils;
import ru.irenproject.common.Proto.Block;
import ru.irenproject.common.Proto.BlockType;
import ru.irenproject.common.Proto.Flow;
import ru.irenproject.live.Proto._ContentTransformer;

import com.google.common.base.MoreObjects;

import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public final class ContentTransformer {
  public static _ContentTransformer build(IssueOrderList issueOrders) {
    _ContentTransformer.Builder b = _ContentTransformer.newBuilder();

    for (IssueOrder order : issueOrders.items()) {
      if (order instanceof ReplaceTextIssueOrder) {
        ReplaceTextIssueOrder rt = (ReplaceTextIssueOrder) order;

        for (Map.Entry<String, String> e : rt.map().entrySet()) {
          b.addTextReplacementBuilder()
              .setPattern(e.getKey())
              .setReplacement(e.getValue());
        }
      }
    }

    return b.build();
  }

  private final List<Replacement> fReplacements;

  private static final class Replacement {
    final Pattern fPattern;
    final String fReplacement;

    Replacement(Pattern pattern, String replacement) {
      fPattern = pattern;
      fReplacement = replacement;
    }
  }

  public ContentTransformer(_ContentTransformer message) {
    fReplacements = message.getTextReplacementList().stream()
        .map(r -> new Replacement(
            // Pattern.UNICODE_CASE is not currently needed here.
            Pattern.compile(r.getPattern(), Pattern.LITERAL | Pattern.CASE_INSENSITIVE),
            Matcher.quoteReplacement(r.getReplacement())))
        .collect(Collectors.toList());
  }

  public Flow transformFlow(Flow flow) {
    Flow res;

    if (fReplacements.isEmpty()) {
      res = flow;
    } else {
      Flow.Builder b = flow.toBuilder();

      for (Replacement r : fReplacements) {
        replaceInFlow(b, r);
      }

      res = b.buildPartial();
    }

    return res;
  }

  private void replaceInFlow(Flow.Builder flow, Replacement r) {
    int i = 0;
    while (i < flow.getBlockCount()) {
      Block block = flow.getBlock(i);
      if (block.getType() == BlockType.TEXT) {
        String transformed = Utils.replaceAllIfFound(block.getTextBlock().getText(), r.fPattern, r.fReplacement);
        if (transformed == null) {
          ++i;
        } else if (transformed.isEmpty()) {
          flow.removeBlock(i);
        } else {
          flow.getBlockBuilder(i).getTextBlockBuilder().setText(transformed);
          ++i;
        }
      } else {
        ++i;
      }
    }
  }

  public String transformText(String text) {
    String res = text;

    for (Replacement r : fReplacements) {
      res = MoreObjects.firstNonNull(Utils.replaceAllIfFound(res, r.fPattern, r.fReplacement), res);
    }

    return res;
  }
}
