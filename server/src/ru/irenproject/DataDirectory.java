/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public final class DataDirectory {
  private static final Path PATH;

  static {
    try {
      String name = "irenproject.ru";

      String appData = System.getenv("APPDATA");
      if (appData == null) {
        String home = System.getenv("HOME");
        if (home == null) {
          throw new RuntimeException("Neither APPDATA nor HOME is defined.");
        }
        PATH = Paths.get(home, ".local", "share", name);
      } else {
        PATH = Paths.get(appData, name);
      }

      Files.createDirectories(PATH);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  public static Path get() {
    return PATH;
  }

  private DataDirectory() {}
}
