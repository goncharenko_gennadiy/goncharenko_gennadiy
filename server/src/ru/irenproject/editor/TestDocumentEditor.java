/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.editor;

import ru.irenproject.Check;
import ru.irenproject.Test;
import ru.irenproject.editor.Proto.XTestDocumentEditor;
import ru.irenproject.editor.Proto.XTestDocumentEditor.MarkAsUnmodified;
import ru.irenproject.editor.Proto.XTestDocumentEditor.Nop;
import ru.irenproject.infra.RenderContext;
import ru.irenproject.infra.Widget;

import com.google.inject.assistedinject.Assisted;
import com.google.protobuf.Message;

import javax.inject.Inject;
import java.util.ArrayDeque;

public final class TestDocumentEditor extends Widget {
  public interface Factory {
    TestDocumentEditor create(Test test);
  }

  private final Test fTest;
  private final ArrayDeque<Widget> fScreenStack = new ArrayDeque<>();
  private boolean fModified;

  @Inject private TestDocumentEditor(@Assisted Test test, TestScreen.Factory testScreenFactory) {
    super(test.realm());
    fTest = test;
    pushScreen(testScreenFactory.create(fTest));
  }

  public void pushScreen(Widget screen) {
    fScreenStack.push(screen);
    markForRender();
  }

  public void popScreen() {
    Check.that(fScreenStack.size() >= 2);
    fScreenStack.pop();
    markForRender();
  }

  public Test test() {
    return fTest;
  }

  @Override public Message render(RenderContext context) {
    return XTestDocumentEditor.newBuilder()
        .setScreen(context.link(fScreenStack.getFirst()))
        .setModified(fModified)
        .build();
  }

  @SuppressWarnings("EmptyMethod")
  @Action public void doNop(Nop in) {}

  public void setModified(boolean value) {
    if (fModified != value) {
      fModified = value;
      markForRender();
    }
  }

  @Action public void doMarkAsUnmodified(MarkAsUnmodified in) {
    setModified(false);
  }
}
