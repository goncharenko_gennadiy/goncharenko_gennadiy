/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.editor;

import ru.irenproject.QuestionItem;
import ru.irenproject.QuestionList;
import ru.irenproject.editor.Proto.XQuestionListSelection;
import ru.irenproject.infra.RenderContext;
import ru.irenproject.infra.Widget;

import com.google.protobuf.Message;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

final class QuestionListSelection extends Widget {
  private final QuestionList fQuestionList;
  private final ArrayList<QuestionItem> fItems = new ArrayList<>();

  public QuestionListSelection(QuestionList questionList) {
    super(questionList.realm());
    fQuestionList = questionList;
  }

  @Override public Message render(RenderContext context) {
    return XQuestionListSelection.newBuilder()
        .addAllSelectedIndex(ascendingIndices())
        .build();
  }

  public List<Integer> ascendingIndices() {
    return fItems.stream()
        .map(fQuestionList.itemsWithIndices()::get)
        .sorted()
        .collect(Collectors.toList());
  }

  public void add(QuestionItem item) {
    fItems.add(item);
    markForRender();
  }

  public void addAll(Collection<QuestionItem> items) {
    fItems.addAll(items);
    markForRender();
  }

  public void clear() {
    fItems.clear();
    markForRender();
  }

  public boolean remove(QuestionItem item) {
    boolean res = fItems.remove(item);
    if (res) {
      markForRender();
    }
    return res;
  }

  public List<QuestionItem> items() {
    return Collections.unmodifiableList(fItems);
  }

  public void forceRender() {
    markForRender();
  }
}
