/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.editor;

import ru.irenproject.TestModule;
import ru.irenproject.editor.classifyQuestion.ClassifyQuestionEditorModule;
import ru.irenproject.editor.inputQuestion.InputQuestionEditorModule;
import ru.irenproject.editor.matchQuestion.MatchQuestionEditorModule;
import ru.irenproject.editor.modifier.ModifierListEditor;
import ru.irenproject.editor.modifier.ModifierScreen;
import ru.irenproject.editor.orderQuestion.OrderQuestionEditorModule;
import ru.irenproject.editor.pad.PadEditor;
import ru.irenproject.editor.profile.ProfileEditor;
import ru.irenproject.editor.profile.ProfileScreen;
import ru.irenproject.editor.selectQuestion.SelectQuestionEditorModule;

import com.google.inject.assistedinject.FactoryModuleBuilder;

public final class EditorModule extends TestModule {
  @Override protected void configure() {
    super.configure();

    install(new FactoryModuleBuilder().build(FileManager.Factory.class));
    install(new FactoryModuleBuilder().build(EditorProcessor.Factory.class));
    install(new FactoryModuleBuilder().build(EditorMessageHandler.Factory.class));
    install(new FactoryModuleBuilder().build(TestDocumentEditor.Factory.class));
    install(new FactoryModuleBuilder().build(TestScreen.Factory.class));
    install(new FactoryModuleBuilder().build(QuestionListEditor.Factory.class));
    install(new FactoryModuleBuilder().build(QuestionItemEditor.Factory.class));
    install(new FactoryModuleBuilder().build(SectionTreeEditor.Factory.class));
    install(new FactoryModuleBuilder().build(SectionEditor.Factory.class));
    install(new FactoryModuleBuilder().build(ProfileScreen.Factory.class));
    install(new FactoryModuleBuilder().build(ProfileEditor.Factory.class));
    install(new FactoryModuleBuilder().build(ModifierScreen.Factory.class));
    install(new FactoryModuleBuilder().build(ModifierListEditor.Factory.class));
    install(new FactoryModuleBuilder().build(PadEditor.Factory.class));

    install(new SelectQuestionEditorModule());
    install(new InputQuestionEditorModule());
    install(new MatchQuestionEditorModule());
    install(new OrderQuestionEditorModule());
    install(new ClassifyQuestionEditorModule());
  }
}
