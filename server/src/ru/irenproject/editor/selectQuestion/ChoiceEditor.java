/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.editor.selectQuestion;

import ru.irenproject.editor.Action;
import ru.irenproject.editor.pad.PadEditor;
import ru.irenproject.editor.selectQuestion.Proto.XSelectQuestionChoiceEditor;
import ru.irenproject.editor.selectQuestion.Proto.XSelectQuestionChoiceEditor.SetCorrect;
import ru.irenproject.editor.selectQuestion.Proto.XSelectQuestionChoiceEditor.SetFixed;
import ru.irenproject.infra.Event;
import ru.irenproject.infra.RenderContext;
import ru.irenproject.infra.Resident;
import ru.irenproject.infra.Widget;
import ru.irenproject.selectQuestion.Choice;

import com.google.inject.assistedinject.Assisted;
import com.google.protobuf.Message;

import javax.inject.Inject;

public final class ChoiceEditor extends Widget {
  public interface Factory {
    ChoiceEditor create(Choice choice);
  }

  private final Choice fChoice;
  private final PadEditor fPadEditor;

  @Inject private ChoiceEditor(@Assisted Choice choice, PadEditor.Factory padEditorFactory) {
    super(choice.realm());
    fChoice = choice;
    fChoice.addListener(this);
    fPadEditor = padEditorFactory.create(fChoice.pad());
  }

  @Action public void doSetCorrect(SetCorrect in) {
    fChoice.setCorrect(in.getCorrect());
  }

  @Action public void doSetFixed(SetFixed in) {
    fChoice.setFixed(in.getFixed());
  }

  @Override public Message render(RenderContext context) {
    return XSelectQuestionChoiceEditor.newBuilder()
        .setPadEditor(context.link(fPadEditor))
        .setCorrect(fChoice.correct())
        .setFixed(fChoice.fixed())
        .setNegative(fChoice.negative())
        .build();
  }

  @Override protected void handleEvent(Event e, Resident source) {
    if (e instanceof Choice.ChangeEvent) {
      markForRender();
    }
  }
}
