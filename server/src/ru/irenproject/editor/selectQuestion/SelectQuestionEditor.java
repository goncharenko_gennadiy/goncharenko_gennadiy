/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.editor.selectQuestion;

import ru.irenproject.Question;
import ru.irenproject.TestInputException;
import ru.irenproject.editor.Action;
import ru.irenproject.editor.QuestionEditor;
import ru.irenproject.editor.pad.PadEditor;
import ru.irenproject.editor.selectQuestion.Proto.XSelectQuestionEditor;
import ru.irenproject.editor.selectQuestion.Proto.XSelectQuestionEditor.AddChoice;
import ru.irenproject.editor.selectQuestion.Proto.XSelectQuestionEditor.AddNegativeChoice;
import ru.irenproject.editor.selectQuestion.Proto.XSelectQuestionEditor.DeleteChoice;
import ru.irenproject.editor.selectQuestion.Proto.XSelectQuestionEditor.MoveChoice;
import ru.irenproject.infra.Event;
import ru.irenproject.infra.RenderContext;
import ru.irenproject.infra.Resident;
import ru.irenproject.selectQuestion.Choice;
import ru.irenproject.selectQuestion.SelectQuestion;

import com.google.inject.assistedinject.Assisted;
import com.google.protobuf.Message;

import javax.inject.Inject;
import java.util.LinkedHashMap;
import java.util.stream.Collectors;

public final class SelectQuestionEditor extends QuestionEditor<SelectQuestion> {
  public interface Factory extends QuestionEditor.Factory {
    @Override SelectQuestionEditor create(Question question);
  }

  private final PadEditor fFormulationEditor;
  private final LinkedHashMap<Choice, ChoiceEditor> fChoiceEditors = new LinkedHashMap<>();

  @Inject private ChoiceEditor.Factory fChoiceEditorFactory;

  @Inject private SelectQuestionEditor(@Assisted Question question, PadEditor.Factory padEditorFactory) {
    super((SelectQuestion) question);
    fFormulationEditor = padEditorFactory.create(question().formulation());
  }

  @Override public Message render(RenderContext context) {
    return XSelectQuestionEditor.newBuilder()
        .setFormulationEditor(context.link(fFormulationEditor))
        .addAllChoiceEditor(context.linkAll(question().choices().stream()
            .map(choice -> fChoiceEditors.computeIfAbsent(choice, fChoiceEditorFactory::create))
            .collect(Collectors.toList())))
        .setCanAddChoice(question().canAddChoice())
        .setCanAddNegativeChoice(canAddNegativeChoice())
        .build();
  }

  @Override protected void handleEvent(Event e, Resident source) {
    super.handleEvent(e, source);
    if (e instanceof Question.ChangeEvent) {
      fChoiceEditors.keySet().retainAll(question().choices());
    }
  }

  @Action(badInput = TestInputException.class)
  public void doAddChoice(AddChoice in) {
    question().addChoice(new Choice(realm(), false));
  }

  @Action(badInput = TestInputException.class)
  public void doDeleteChoice(DeleteChoice in) {
    question().deleteChoice(in.getIndex());
  }

  @Action(badInput = TestInputException.class)
  public void doMoveChoice(MoveChoice in) {
    question().moveChoice(in.getIndex(), in.getForward());
  }

  private boolean canAddNegativeChoice() {
    return question().canAddChoice() && !question().hasNegativeChoice();
  }

  @Action(badInput = TestInputException.class)
  public void doAddNegativeChoice(AddNegativeChoice in) {
    Choice c = new Choice(realm(), true);
    c.setFixed(true);
    c.pad().appendText(in.getText());
    question().addChoice(c);
  }
}
