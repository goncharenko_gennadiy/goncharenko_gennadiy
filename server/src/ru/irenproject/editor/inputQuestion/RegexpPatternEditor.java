/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.editor.inputQuestion;

import ru.irenproject.TestInputException;
import ru.irenproject.editor.Action;
import ru.irenproject.editor.inputQuestion.Proto.XPatternEditor;
import ru.irenproject.editor.inputQuestion.Proto.XPatternEditor.SetRegexpPatternOptions;
import ru.irenproject.inputQuestion.Pattern;
import ru.irenproject.inputQuestion.RegexpPattern;

import com.google.inject.assistedinject.Assisted;

import javax.inject.Inject;

public final class RegexpPatternEditor extends PatternEditor<RegexpPattern> {
  public interface Factory extends PatternEditor.Factory {
    @Override RegexpPatternEditor create(Pattern pattern);
  }

  @Inject private RegexpPatternEditor(@Assisted Pattern pattern) {
    super((RegexpPattern) pattern);
  }

  @Override protected void renderSupplement(XPatternEditor.Builder b) {
    b.getRegexpSupplementBuilder()
        .setCaseSensitive(pattern().caseSensitive())
        .setSpacePreprocessMode(pattern().spacePreprocessMode());
  }

  @Action(badInput = TestInputException.class)
  public void doSetRegexpPatternOptions(SetRegexpPatternOptions in) {
    pattern().setCaseSensitive(in.getCaseSensitive());
    pattern().setSpacePreprocessMode(in.getSpacePreprocessMode());
  }
}
