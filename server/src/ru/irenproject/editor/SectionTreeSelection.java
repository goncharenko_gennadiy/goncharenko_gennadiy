/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.editor;

import ru.irenproject.editor.Proto.XSectionTreeSelection;
import ru.irenproject.infra.RenderContext;
import ru.irenproject.infra.Widget;

import com.google.protobuf.Message;

final class SectionTreeSelection extends Widget {
  private SectionEditor fSelectedEditor;

  public SectionTreeSelection(SectionEditor selectedEditor) {
    super(selectedEditor.realm());
    fSelectedEditor = selectedEditor;
  }

  @Override public Message render(RenderContext context) {
    return XSectionTreeSelection.newBuilder()
        .setSelectedEditor(context.link(fSelectedEditor))
        .setCanMoveUp(fSelectedEditor.sectionTreeEditor().canMoveUp())
        .setCanMoveDown(fSelectedEditor.sectionTreeEditor().canMoveDown())
        .build();
  }

  public SectionEditor selectedEditor() {
    return fSelectedEditor;
  }

  public void setSelectedEditor(SectionEditor value) {
    fSelectedEditor = value;
    markForRender();
  }

  public void forceRender() {
    markForRender();
  }
}
