/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.editor;

import ru.irenproject.BadInputException;
import ru.irenproject.ChangeContentEvent;
import ru.irenproject.Check;
import ru.irenproject.Test;
import ru.irenproject.Utils;
import ru.irenproject.infra.ActionPerformer;
import ru.irenproject.infra.AsyncEventQueue;
import ru.irenproject.infra.BlobId;
import ru.irenproject.infra.BlobWidget;
import ru.irenproject.infra.Event;
import ru.irenproject.infra.FileBlobStore;
import ru.irenproject.infra.Freezable;
import ru.irenproject.infra.KryoFactory;
import ru.irenproject.infra.Proto.Reply;
import ru.irenproject.infra.Proto.Request;
import ru.irenproject.infra.Proto.SceneDelta;
import ru.irenproject.infra.Proto.Shape;
import ru.irenproject.infra.Proto.XBlob;
import ru.irenproject.infra.Realm;
import ru.irenproject.infra.RenderContext;
import ru.irenproject.infra.RequestProcessor;
import ru.irenproject.infra.Resident;
import ru.irenproject.infra.Widget;

import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import com.google.common.base.Equivalence;
import com.google.common.collect.MapDifference;
import com.google.common.collect.MapDifference.ValueDifference;
import com.google.common.collect.Maps;
import com.google.inject.assistedinject.Assisted;
import com.google.protobuf.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import javax.inject.Inject;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.ArrayDeque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Queue;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.function.Function;

public final class EditorProcessor implements RequestProcessor {
  public interface Factory {
    EditorProcessor create(Path directory, boolean unfreeze);
  }

  private static final Logger fLogger = LoggerFactory.getLogger(EditorProcessor.class);

  private static final String ROOT_WIDGET_NAME = "";

  public static void deleteEditorDirectory(Path editorDirectory) {
    Utils.deleteShallowDirectory(getBlobStoreDirectory(editorDirectory));
    Utils.deleteShallowDirectory(editorDirectory);
  }

  private final Path fDirectory;
  private final ActionPerformer fActionPerformer;
  private final KryoFactory fKryoFactory;
  private final TestDocumentEditor.Factory fTestDocumentEditorFactory;

  private final AsyncEventQueue fEventQueue = new AsyncEventQueue();
  private final Realm fRealm;
  private @Nullable TestDocumentEditor fTestDocumentEditor;

  private long fLastReplyId;
  private boolean fModified;

  private HashMap<String, Widget> fWidgets = new HashMap<>();
  private HashMap<String, Message> fShapes = new HashMap<>();
  private HashMap<Widget, LinkedHashSet<Widget>> fDependencies = new HashMap<>();
  private final HashMap<BlobId, BlobWidget> fBlobWidgets = new HashMap<>();

  private final HashSet<Widget> fMarkedForRender = new HashSet<>();

  @Inject private EditorProcessor(@Assisted Path directory, @Assisted boolean unfreeze, ActionPerformer actionPerformer,
      KryoFactory kryoFactory, TestDocumentEditor.Factory testDocumentEditorFactory) {
    fDirectory = directory;
    fActionPerformer = actionPerformer;
    fKryoFactory = kryoFactory;
    fTestDocumentEditorFactory = testDocumentEditorFactory;
    fRealm = new Realm(fEventQueue, new GlobalEventHandler(), new FileBlobStore(getBlobStoreDirectory(fDirectory)));

    if (unfreeze) {
      fTestDocumentEditor = unfreeze();
    }
  }

  private final class GlobalEventHandler implements Realm.GlobalEventSink {
    @Override public void postGlobal(Resident source, Event event) {
      if (event instanceof Widget.MarkForRenderEvent) {
        fMarkedForRender.add((Widget) source);
      } else if (event instanceof Widget.ShowScreenEvent) {
        Check.notNull(fTestDocumentEditor).pushScreen(((Widget.ShowScreenEvent) event).screen());
      } else if (event instanceof Widget.DismissScreenEvent) {
        Check.notNull(fTestDocumentEditor).popScreen();
      } else if (event instanceof ChangeContentEvent) {
        fModified = true;
        if (fTestDocumentEditor != null) {
          fTestDocumentEditor.setModified(true);
        }
      }
    }
  }

  private static Path getBlobStoreDirectory(Path editorDirectory) {
    return editorDirectory.resolve("blobStore");
  }

  public void createTestDocumentEditor(Test test) {
    Check.that(fTestDocumentEditor == null);
    fTestDocumentEditor = fTestDocumentEditorFactory.create(test);

    fEventQueue.dispatch();
    fMarkedForRender.clear();

    freeze();
    fModified = false;
  }

  public Realm realm() {
    return fRealm;
  }

  public Path directory() {
    return fDirectory;
  }

  public Test test() {
    return Check.notNull(fTestDocumentEditor).test();
  }

  @Override public Reply process(Request request) {
    ++fLastReplyId;
    SceneDelta sceneDelta;
    boolean badInput;

    try {
      Widget target = request.getTargetName().equals(ROOT_WIDGET_NAME) ?
          fTestDocumentEditor : Check.inputNotNull(fWidgets.get(request.getTargetName()));
      fActionPerformer.perform(target, request.getActionName(), request.getInput());
      badInput = false;
    } catch (BadInputException e) {
      badInput = true;
      fLogger.warn("", e);
    } finally {
      fEventQueue.dispatch();
      sceneDelta = render();
    }

    Check.that(fEventQueue.isEmpty());

    return Reply.newBuilder()
        .setChannel(request.getChannel())
        .setId(fLastReplyId)
        .setOk(!badInput)
        .setSceneDelta(sceneDelta)
        .build();
  }

  private SceneDelta render() {
    HashMap<String, Widget> newWidgets = new HashMap<>();
    HashMap<String, Message> newShapes = new HashMap<>();
    HashMap<Widget, LinkedHashSet<Widget>> newDependencies = new HashMap<>();

    Queue<Widget> pending = new ArrayDeque<>();
    pending.add(fTestDocumentEditor);

    while (!pending.isEmpty()) {
      Widget widget = pending.remove();
      boolean alreadyRendered = newDependencies.containsKey(widget);
      if (!alreadyRendered) {
        String name = getWidgetName(widget);
        Message shape;
        LinkedHashSet<Widget> references;

        Message oldShape = fMarkedForRender.contains(widget) ? null : fShapes.get(name);
        if (oldShape == null) {
          DefaultRenderContext renderContext = new DefaultRenderContext(this::getWidgetName, fBlobWidgets, fRealm);
          shape = widget.render(renderContext);
          references = renderContext.fReferences;
        } else {
          shape = oldShape;
          references = Check.notNull(fDependencies.get(widget));
        }

        newWidgets.put(name, widget);
        newShapes.put(name, shape);
        newDependencies.put(widget, references);

        pending.addAll(references);
      }
    }

    SceneDelta res = computeSceneDelta(fShapes, newShapes);

    fWidgets = newWidgets;
    fShapes = newShapes;
    fDependencies = newDependencies;
    fMarkedForRender.clear();
    fBlobWidgets.values().retainAll(fDependencies.keySet());

    return res;
  }

  private static final class DefaultRenderContext implements RenderContext {
    final Function<Widget, String> fNameProvider;
    final Map<BlobId, BlobWidget> fBlobWidgets;
    final Realm fRealm;
    final LinkedHashSet<Widget> fReferences = new LinkedHashSet<>();

    DefaultRenderContext(Function<Widget, String> nameProvider, Map<BlobId, BlobWidget> blobWidgets, Realm realm) {
      fNameProvider = nameProvider;
      fBlobWidgets = blobWidgets;
      fRealm = realm;
    }

    @Override public String link(Widget widget) {
      fReferences.add(widget);
      return fNameProvider.apply(widget);
    }

    @Override public String linkBlob(BlobId blobId) {
      return link(fBlobWidgets.computeIfAbsent(blobId, _blobId -> new BlobWidget(fRealm, _blobId)));
    }
  }

  private String getWidgetName(Widget widget) {
    return (widget == fTestDocumentEditor) ? ROOT_WIDGET_NAME : Long.toString(widget.id());
  }

  private SceneDelta computeSceneDelta(Map<String, Message> old, Map<String, Message> current) {
    MapDifference<String, Message> diff = Maps.difference(old, current, Equivalence.identity());

    TreeMap<String, Message> changed = new TreeMap<>();
    changed.putAll(diff.entriesOnlyOnRight());
    for (Map.Entry<String, ValueDifference<Message>> e : diff.entriesDiffering().entrySet()) {
      changed.put(e.getKey(), e.getValue().rightValue());
    }

    TreeSet<String> deleted = new TreeSet<>();
    deleted.addAll(diff.entriesOnlyOnLeft().keySet());

    SceneDelta.Builder b = SceneDelta.newBuilder();

    for (Map.Entry<String, Message> e : changed.entrySet()) {
      Message m = e.getValue();

      if (m instanceof XBlob) {
        m = replaceBlobIdWithContent((XBlob) m);
      }

      b.addChanged(Shape.newBuilder()
          .setName(e.getKey())
          .setType(m.getDescriptorForType().getName())
          .setData(m.toByteString()));
    }

    return b
        .addAllDeleted(deleted)
        .build();
  }

  private XBlob replaceBlobIdWithContent(XBlob m) {
    return XBlob.newBuilder()
        .setBlob(Utils.readByteSourceToByteString(fRealm.blobStore().get(new BlobId(m.getBlob()))))
        .build();
  }

  public void freeze() {
    try {
      FrozenEditor frozenEditor = new FrozenEditor(fTestDocumentEditor, fRealm.lastResidentId());
      Path tempFile = getFrozenFile("~");

      try (
          OutputStream outputStream = Files.newOutputStream(tempFile);
          Output kryoOutput = new Output(outputStream)) {
        fKryoFactory.create(fRealm).writeObject(kryoOutput, frozenEditor);
      }

      Files.move(tempFile, getFrozenFile(""), StandardCopyOption.REPLACE_EXISTING);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  private TestDocumentEditor unfreeze() {
    try {
      FrozenEditor frozenEditor;
      try (
          InputStream inputStream = Files.newInputStream(getFrozenFile(""));
          Input kryoInput = new Input(inputStream)) {
        frozenEditor = fKryoFactory.create(fRealm).readObject(kryoInput, FrozenEditor.class);
      }

      fRealm.setLastResidentId(frozenEditor.fLastResidentId);
      return frozenEditor.fTestDocumentEditor;
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  private Path getFrozenFile(String suffix) {
    return fDirectory.resolve("testEditor.frozen" + suffix);
  }

  @Freezable private static final class FrozenEditor {
    final TestDocumentEditor fTestDocumentEditor;
    final long fLastResidentId;

    FrozenEditor(TestDocumentEditor testDocumentEditor, long lastResidentId) {
      fTestDocumentEditor = testDocumentEditor;
      fLastResidentId = lastResidentId;
    }
  }

  public boolean modified() {
    return fModified;
  }

  public void setModified(boolean value) {
    fModified = value;
  }
}
