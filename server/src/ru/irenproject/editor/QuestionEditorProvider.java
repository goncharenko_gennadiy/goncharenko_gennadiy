/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.editor;

import ru.irenproject.Check;
import ru.irenproject.Oops;
import ru.irenproject.Question;

import com.google.common.collect.ImmutableMap;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.Map;

@Singleton public final class QuestionEditorProvider {
  private final ImmutableMap<Class<? extends Question>, QuestionEditor.Factory> fMap;

  @Inject private QuestionEditorProvider(Map<Class<? extends Question>, QuestionEditor.Factory> map) {
    fMap = ImmutableMap.copyOf(map);
  }

  public QuestionEditor<?> create(Question question) {
    return Check.notNull(
        fMap.get(question.getClass()),
        () -> Oops.format("No question editor for %s.", question.getClass())
    ).create(question);
  }
}
