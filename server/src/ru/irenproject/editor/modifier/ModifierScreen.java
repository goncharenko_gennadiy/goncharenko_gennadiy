/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.editor.modifier;

import ru.irenproject.ModifierList;
import ru.irenproject.Section;
import ru.irenproject.editor.Action;
import ru.irenproject.editor.modifier.Proto.XModifierScreen;
import ru.irenproject.editor.modifier.Proto.XModifierScreen.Close;
import ru.irenproject.infra.RenderContext;
import ru.irenproject.infra.Widget;

import com.google.inject.assistedinject.Assisted;
import com.google.protobuf.Message;

import javax.annotation.Nullable;
import javax.inject.Inject;

public final class ModifierScreen extends Widget {
  public interface Factory {
    ModifierScreen create(ModifierList modifierList, @Nullable String questionType, @Nullable Section section);
  }

  private final ModifierListEditor fModifierListEditor;

  @Inject private ModifierScreen(
      @Assisted ModifierList modifierList,
      @Assisted @Nullable String questionType,
      @Assisted @Nullable Section section,
      ModifierListEditor.Factory modifierListEditorFactory) {
    super(modifierList.realm());
    fModifierListEditor = modifierListEditorFactory.create(modifierList, questionType, section);
  }

  @Override public Message render(RenderContext context) {
    return XModifierScreen.newBuilder()
        .setModifierListEditor(context.link(fModifierListEditor))
        .build();
  }

  @Action public void doClose(Close in) {
    dismissScreen();
  }
}
