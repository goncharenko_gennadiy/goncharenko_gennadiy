/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.editor.modifier;

import ru.irenproject.Check;
import ru.irenproject.Modifier;
import ru.irenproject.ModifierList;
import ru.irenproject.Section;
import ru.irenproject.TestInputException;
import ru.irenproject.editor.Action;
import ru.irenproject.editor.modifier.Proto.XModifierListEditor;
import ru.irenproject.editor.modifier.Proto.XModifierListEditor.Add;
import ru.irenproject.editor.modifier.Proto.XModifierListEditor.Delete;
import ru.irenproject.editor.modifier.Proto.XModifierListEditor.Move;
import ru.irenproject.editor.modifier.Proto.XModifierListEditor.Select;
import ru.irenproject.infra.Event;
import ru.irenproject.infra.RenderContext;
import ru.irenproject.infra.Resident;
import ru.irenproject.infra.Widget;

import com.google.inject.assistedinject.Assisted;
import com.google.protobuf.Message;

import javax.annotation.Nullable;
import javax.inject.Inject;
import java.util.Map;
import java.util.stream.Collectors;

public final class ModifierListEditor extends Widget {
  public interface Factory {
    ModifierListEditor create(ModifierList modifierList, @Nullable String questionType, @Nullable Section section);
  }

  private final ModifierList fModifierList;
  private final @Nullable String fQuestionType;
  private final @Nullable Section fSection;
  private @Nullable Modifier fSelected;

  @SuppressWarnings("MismatchedQueryAndUpdateOfCollection")
  @Inject private Map<String, Modifier.Factory> fModifierFactoriesByType;

  @Inject private ModifierListEditor(
      @Assisted ModifierList modifierList,
      @Assisted @Nullable String questionType,
      @Assisted @Nullable Section section) {
    super(modifierList.realm());
    fModifierList = modifierList;
    fQuestionType = questionType;
    fSection = section;

    fModifierList.addListener(this);
    if (fSection != null) {
      fSection.addListener(this);
    }

    if (!fModifierList.isEmpty()) {
      fSelected = fModifierList.modifiers().get(0);
    }
  }

  @Override public Message render(RenderContext context) {
    XModifierListEditor.Builder b = XModifierListEditor.newBuilder()
        .addAllType(fModifierList.modifiers().stream().map(Modifier::type).collect(Collectors.toList()));
    if (fSelected != null) {
      b.setSelectedIndex(fModifierList.modifiers().indexOf(fSelected));
    }
    if (fQuestionType != null) {
      b.setQuestionType(fQuestionType);
    }
    if (fSection != null) {
      b.setSectionName(fSection.name());
    }
    return b.build();
  }

  @Override protected void handleEvent(Event e, Resident source) {
    if ((e instanceof ModifierList.ChangeEvent) || (e instanceof Section.ChangeEvent)) {
      markForRender();
    } else if (e instanceof ModifierList.RemoveEvent) {
      if (fSelected == ((ModifierList.RemoveEvent) e).modifier()) {
        fSelected = null;
      }
    }
  }

  @Action public void doSelect(Select in) {
    Check.input(in.getIndex() >= 0);
    Check.input(in.getIndex() < fModifierList.modifiers().size());

    fSelected = fModifierList.modifiers().get(in.getIndex());
    markForRender();
  }

  @Action public void doAdd(Add in) {
    Modifier m = Check.inputNotNull(fModifierFactoriesByType.get(in.getType())).create(realm());
    fModifierList.add(m);
    fSelected = m;
  }

  @Action public void doDelete(Delete in) {
    Check.inputNotNull(fSelected);

    int selectedIndex = fModifierList.modifiers().indexOf(fSelected);
    fModifierList.delete(selectedIndex);

    selectedIndex = Integer.min(selectedIndex, fModifierList.modifiers().size() - 1);
    if (selectedIndex >= 0) {
      fSelected = fModifierList.modifiers().get(selectedIndex);
    }
  }

  @Action(badInput = TestInputException.class)
  public void doMove(Move in) {
    Check.inputNotNull(fSelected);
    fModifierList.move(fModifierList.modifiers().indexOf(fSelected), in.getForward());
  }
}
