/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.editor;

import ru.irenproject.BadInputException;
import ru.irenproject.Check;
import ru.irenproject.Test;
import ru.irenproject.Utils;
import ru.irenproject.editor.Proto.OpenFile;
import ru.irenproject.editor.Proto.XFileManager;
import ru.irenproject.editor.Proto.XFileManager.Close;
import ru.irenproject.editor.Proto.XFileManager.Nop;
import ru.irenproject.editor.Proto.XFileManager.Open;
import ru.irenproject.editor.Proto.XFileManager.Save;
import ru.irenproject.infra.ActionPerformer;
import ru.irenproject.infra.Proto.Reply;
import ru.irenproject.infra.Proto.Request;
import ru.irenproject.infra.Proto.SceneDelta;
import ru.irenproject.infra.Proto.Shape;
import ru.irenproject.infra.RequestProcessor;
import ru.irenproject.itx.ItxInput;
import ru.irenproject.itx.ItxInputException;
import ru.irenproject.itx.ItxOutput;

import com.google.common.io.BaseEncoding;
import com.google.inject.assistedinject.Assisted;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.security.SecureRandom;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;

public final class FileManager implements RequestProcessor {
  public interface Factory {
    FileManager create(Map<String, RequestProcessor> processorsByChannel);
  }

  private static final Logger fLogger = LoggerFactory.getLogger(FileManager.class);

  private static final String AVAILABLE_FILE = "available";
  private static final String NAME_FILE = "name";
  private static final String NUMBER_FILE = "number";

  private final Map<String, RequestProcessor> fProcessorsByChannel;
  private final EditorProcessor.Factory fEditorProcessorFactory;
  private final ActionPerformer fActionPerformer;
  private final ItxInput fItxInput;
  private final ItxOutput fItxOutput;

  private final LinkedHashMap<String, OpenFile> fOpenFilesByChannel = new LinkedHashMap<>();
  private long fLastFileNumber;

  private long fLastReplyId;

  @Inject private FileManager(
      @Assisted Map<String, RequestProcessor> processorsByChannel,
      EditorProcessor.Factory editorProcessorFactory,
      ActionPerformer actionPerformer,
      ItxInput itxInput,
      ItxOutput itxOutput) {
    fProcessorsByChannel = processorsByChannel;
    fEditorProcessorFactory = editorProcessorFactory;
    fActionPerformer = actionPerformer;
    fItxInput = itxInput;
    fItxOutput = itxOutput;

    openEditors();
  }

  private void openEditors() {
    try {
      TreeMap<Long, Path> editorDirsByNumber = new TreeMap<>();

      for (Path dir : Utils.listDirectory(openFilesPath())) {
        if (Files.isDirectory(dir)) {
          if (Files.exists(dir.resolve(AVAILABLE_FILE))) {
            long number = Long.parseLong(new String(Files.readAllBytes(dir.resolve(NUMBER_FILE)),
                StandardCharsets.US_ASCII));
            fLastFileNumber = Long.max(fLastFileNumber, number);
            editorDirsByNumber.put(number, dir);
          } else {
            EditorProcessor.deleteEditorDirectory(dir);
          }
        }
      }

      for (Path dir : editorDirsByNumber.values()) {
        register(dir, fEditorProcessorFactory.create(dir, true));
      }
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  private static Path openFilesPath() {
    return EditorUtils.dataDirectory().resolve("openFiles");
  }

  private void register(Path editorDir, EditorProcessor processor) {
    try {
      String channelName = "testEditor-" + editorDir.getFileName();
      fProcessorsByChannel.put(channelName, processor);
      fOpenFilesByChannel.put(channelName, OpenFile.newBuilder()
          .setChannel(channelName)
          .setName(new String(Files.readAllBytes(editorDir.resolve(NAME_FILE)), StandardCharsets.UTF_8))
          .build());
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  @Override public Reply process(Request request) {
    ++fLastReplyId;

    boolean badInput;
    try {
      fActionPerformer.perform(this, request.getActionName(), request.getInput());
      badInput = false;
    } catch (BadInputException e) {
      badInput = true;
      fLogger.warn("", e);
    }

    XFileManager shape = XFileManager.newBuilder()
        .addAllFile(fOpenFilesByChannel.values())
        .build();

    return Reply.newBuilder()
        .setChannel(request.getChannel())
        .setId(fLastReplyId)
        .setOk(!badInput)
        .setSceneDelta(SceneDelta.newBuilder()
            .addChanged(Shape.newBuilder()
                .setName("")
                .setType(shape.getDescriptorForType().getName())
                .setData(shape.toByteString())))
        .build();
  }

  @SuppressWarnings("EmptyMethod")
  @Action public void doNop(Nop in) {}

  @Action(badInput = ItxInputException.class)
  public void doOpen(Open in) {
    try {
      Path testFile;
      try {
        testFile = in.getCreateNew() ? null : Paths.get(in.getFileName());
      } catch (InvalidPathException e) {
        throw new BadInputException(e);
      }

      if (testFile != null) {
        Check.input(Files.isReadable(testFile));
      }

      byte[] id = new byte[24];
      new SecureRandom().nextBytes(id);
      Path editorDir = openFilesPath().resolve(BaseEncoding.base16().lowerCase().encode(id));

      EditorProcessor processor;
      boolean retainEditorDir = false;
      Files.createDirectories(editorDir);
      try {
        processor = fEditorProcessorFactory.create(editorDir, false);
        Test test = in.getCreateNew() ? new Test(processor.realm()) : fItxInput.readTest(testFile, processor.realm());
        processor.createTestDocumentEditor(test);

        setFileName(editorDir, in.getFileName());
        ++fLastFileNumber;
        Files.write(editorDir.resolve(NUMBER_FILE), Long.toString(fLastFileNumber).getBytes(StandardCharsets.US_ASCII));

        Files.createFile(editorDir.resolve(AVAILABLE_FILE));

        retainEditorDir = true;
      } finally {
        if (!retainEditorDir) {
          EditorProcessor.deleteEditorDirectory(editorDir);
        }
      }

      register(editorDir, processor);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  private static void setFileName(Path editorDir, String fileName) {
    try {
      Files.write(editorDir.resolve(NAME_FILE), fileName.getBytes(StandardCharsets.UTF_8));
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  @Action public void doClose(Close in) {
    if (fOpenFilesByChannel.remove(in.getChannel()) != null) {
      EditorProcessor processor = (EditorProcessor) fProcessorsByChannel.remove(in.getChannel());
      EditorProcessor.deleteEditorDirectory(processor.directory());
    }
  }

  @Action public void doSave(Save in) {
    try {
      try {
        OpenFile f = Check.inputNotNull(fOpenFilesByChannel.get(in.getChannel()));
        EditorProcessor processor = (EditorProcessor) fProcessorsByChannel.get(in.getChannel());

        Path targetFile, tempFile;
        try {
          targetFile = Paths.get(in.getFileName());
          tempFile = Paths.get(in.getFileName() + '~');
        } catch (InvalidPathException e) {
          throw new BadInputException(e);
        }

        try (OutputStream outputStream = Files.newOutputStream(tempFile)) {
          fItxOutput.writeTest(processor.test(), outputStream, false);
        }

        Files.move(tempFile, targetFile, StandardCopyOption.REPLACE_EXISTING);

        setFileName(processor.directory(), in.getFileName());
        fOpenFilesByChannel.put(in.getChannel(), f.toBuilder()
            .setName(in.getFileName())
            .build());
      } catch (IOException e) {
        throw new RuntimeException(e);
      }
    } catch (RuntimeException e) {
      throw (e.getCause() instanceof IOException) ? new BadInputException(e) : e;
    }
  }
}
