/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.editor.pad;

import ru.irenproject.Check;
import ru.irenproject.Utils;
import ru.irenproject.editor.Action;
import ru.irenproject.editor.EditorUtils;
import ru.irenproject.editor.pad.Proto.BlockType;
import ru.irenproject.editor.pad.Proto.XPadEditor;
import ru.irenproject.editor.pad.Proto.XPadEditor.Copy;
import ru.irenproject.editor.pad.Proto.XPadEditor.Cut;
import ru.irenproject.editor.pad.Proto.XPadEditor.Delete;
import ru.irenproject.editor.pad.Proto.XPadEditor.InsertImage;
import ru.irenproject.editor.pad.Proto.XPadEditor.InsertText;
import ru.irenproject.editor.pad.Proto.XPadEditor.Paste;
import ru.irenproject.infra.BlobId;
import ru.irenproject.infra.Event;
import ru.irenproject.infra.RenderContext;
import ru.irenproject.infra.Resident;
import ru.irenproject.infra.Widget;
import ru.irenproject.itx.Itx;
import ru.irenproject.itx.ItxInput;
import ru.irenproject.itx.ItxOutput;
import ru.irenproject.pad.Block;
import ru.irenproject.pad.ImageBlock;
import ru.irenproject.pad.LineFeedBlock;
import ru.irenproject.pad.Pad;
import ru.irenproject.pad.TextBlock;

import com.google.common.base.CharMatcher;
import com.google.common.base.Splitter;
import com.google.common.collect.ImmutableList;
import com.google.common.net.MediaType;
import com.google.inject.assistedinject.Assisted;
import com.google.protobuf.Message;
import org.w3c.dom.Element;

import javax.inject.Inject;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.util.List;

public final class PadEditor extends Widget {
  public interface Factory {
    PadEditor create(Pad pad);
  }

  private static final Proto.Block LINE_FEED_BLOCK = Proto.Block.newBuilder()
      .setType(BlockType.LINE_FEED)
      .build();

  private static final ImmutableList<String> ALLOWED_IMAGE_TYPES = ImmutableList.of(
      MediaType.PNG.toString(), MediaType.JPEG.toString());

  private static final CharMatcher CONTROL_EXCEPT_LF_MATCHER = CharMatcher.javaIsoControl()
      .and(CharMatcher.isNot('\n'));
  private static final Splitter LF_SPLITTER = Splitter.on('\n');

  private final Pad fPad;

  @Inject private ItxOutput fItxOutput;
  @Inject private ItxInput fItxInput;

  @Inject private PadEditor(@Assisted Pad pad) {
    super(pad.realm());
    fPad = pad;
    fPad.addListener(this);
  }

  @Override protected void handleEvent(Event e, Resident source) {
    if (e instanceof Pad.ChangeEvent) {
      markForRender();
    }
  }

  @Override public Message render(RenderContext context) {
    XPadEditor.Builder b = XPadEditor.newBuilder();

    for (Block block : fPad.blocks()) {
      if (block instanceof TextBlock) {
        b.addBlock(Proto.Block.newBuilder()
            .setType(BlockType.TEXT)
            .setTextBlock(Proto.TextBlock.newBuilder()
                .setText(((TextBlock) block).text())));
      } else if (block instanceof ImageBlock) {
        ImageBlock imageBlock = (ImageBlock) block;
        b.addBlock(Proto.Block.newBuilder()
            .setType(BlockType.IMAGE)
            .setImageBlock(Proto.ImageBlock.newBuilder()
                .setMimeType(imageBlock.mimeType())
                .setData(context.linkBlob(imageBlock.data()))));
      } else if (block instanceof LineFeedBlock) {
        b.addBlock(LINE_FEED_BLOCK);
      }
    }

    return b.build();
  }

  @Action public void doInsertText(InsertText in) {
    List<String> lines = LF_SPLITTER.splitToList(CONTROL_EXCEPT_LF_MATCHER.removeFrom(in.getText()));
    Pad fragment = new Pad(realm());
    for (int i = 0; i < lines.size(); ++i) {
      if (i > 0) {
        fragment.appendLineFeed();
      }
      fragment.appendText(lines.get(i));
    }
    fPad.insertFragment(fragment, in.getPosition());
  }

  @Action public void doInsertImage(InsertImage in) {
    Check.input(ALLOWED_IMAGE_TYPES.contains(in.getMimeType()));

    BlobId dataBlobId = realm().blobStore().put(Utils.byteStringAsByteSource(in.getData()));
    fPad.insertBlock(new ImageBlock(in.getMimeType(), dataBlobId, null), in.getPosition());
  }

  @Action public void doDelete(Delete in) {
    fPad.delete(in.getPosition(), in.getLength());
  }

  @Action public void doCopy(Copy in) {
    copy(in.getPosition(), in.getLength());
  }

  private void copy(int position, int length) {
    try (OutputStream clipboard = Files.newOutputStream(EditorUtils.clipboardFile())) {
      fItxOutput.writeFragment(out -> out.writePad(fPad.slice(position, length)), clipboard);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  @Action public void doPaste(Paste in) {
    if (Files.exists(EditorUtils.clipboardFile())) {
      Pad fragment = fItxInput.read(EditorUtils.clipboardFile(), realm(), reader -> {
        Element xmlContent = Itx.getChildIfExists(reader.root(), Itx.ELEM_CONTENT);
        return (xmlContent == null) ? null : reader.readNewPad(xmlContent);
      });

      if (fragment != null) {
        fPad.insertFragment(fragment, in.getPosition());
      }
    }
  }

  @Action public void doCut(Cut in) {
    copy(in.getPosition(), in.getLength());
    fPad.delete(in.getPosition(), in.getLength());
  }
}
