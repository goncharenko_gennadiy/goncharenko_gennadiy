/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.editor.profile;

import ru.irenproject.Check;
import ru.irenproject.TestInputException;
import ru.irenproject.Utils;
import ru.irenproject.editor.Action;
import ru.irenproject.editor.modifier.ModifierListEditor;
import ru.irenproject.editor.profile.Proto.XProfileEditor;
import ru.irenproject.editor.profile.Proto.XProfileEditor.AddMark;
import ru.irenproject.editor.profile.Proto.XProfileEditor.SetOptions;
import ru.irenproject.editor.profile.Proto.XProfileEditor.SetScore;
import ru.irenproject.editor.profile.Proto.XProfileEditor.SetTitle;
import ru.irenproject.editor.profile.Proto.XProfileEditor.UpdateMark;
import ru.irenproject.infra.Event;
import ru.irenproject.infra.RenderContext;
import ru.irenproject.infra.Resident;
import ru.irenproject.infra.Widget;
import ru.irenproject.profile.MarkScale;
import ru.irenproject.profile.Profile;
import ru.irenproject.profile.Proto.Mark;
import ru.irenproject.profile.Proto.MarkScaleData;

import com.google.inject.assistedinject.Assisted;
import com.google.protobuf.Message;

import javax.inject.Inject;

public final class ProfileEditor extends Widget {
  public interface Factory {
    ProfileEditor create(Profile profile);
  }

  private final Profile fProfile;
  private final ModifierListEditor fModifierListEditor;

  @Inject private ProfileEditor(@Assisted Profile profile, ModifierListEditor.Factory modifierListEditorFactory) {
    super(profile.realm());
    fProfile = profile;
    fProfile.addListener(this);
    fModifierListEditor = modifierListEditorFactory.create(fProfile.modifierList(), null, null);
  }

  @Override public Message render(RenderContext context) {
    XProfileEditor.Builder b = XProfileEditor.newBuilder()
        .setTitle(fProfile.title())
        .setOptions(fProfile.options())
        .setMarkScale(fProfile.markScale().data())
        .setMaxMarks(MarkScale.MAX_MARKS)
        .setModifierListEditor(context.link(fModifierListEditor));
    if (fProfile.availableScore() != null) {
      b.setScore(fProfile.availableScore());
    }
    return b.build();
  }

  @Override protected void handleEvent(Event e, Resident source) {
    if (e instanceof Profile.ChangeEvent) {
      markForRender();
    }
  }

  @Action public void doSetTitle(SetTitle in) {
    fProfile.setTitle(in.getTitle());
  }

  @Action(badInput = TestInputException.class)
  public void doSetOptions(SetOptions in) {
    fProfile.setOptions(in.getOptions());
  }

  @Action(badInput = TestInputException.class)
  public void doSetScore(SetScore in) {
    fProfile.setAvailableScore(in.hasScore() ? in.getScore() : null);
  }

  @Action(badInput = TestInputException.class)
  public void doAddMark(AddMark in) {
    fProfile.setMarkScale(addMark(fProfile.markScale(), in.getMark()));
  }

  private static MarkScale addMark(MarkScale markScale, Mark mark) {
    Integer index = markScale.getMarkIndexForResult(Utils.parseNormalizedDecimal(mark.getLowerBound()));
    return new MarkScale(markScale.data().toBuilder()
        .addMark((index == null) ? 0 : index + 1, mark)
        .buildPartial());
  }

  @Action(badInput = TestInputException.class)
  public void doUpdateMark(UpdateMark in) {
    int index = in.getIndex();
    Check.input(index >= 0);
    Check.input(index < fProfile.markScale().markCount());

    MarkScaleData.Builder data = fProfile.markScale().data().toBuilder();
    MarkScale updated;

    if (in.hasMark()) {
      updated = in.getMark().getLowerBound().equals(data.getMark(index).getLowerBound()) ?
          new MarkScale(data.setMark(index, in.getMark()).buildPartial()) :
          addMark(new MarkScale(data.removeMark(index).buildPartial()), in.getMark());
    } else {
      data.removeMark(index);
      if (data.getMarkCount() > 0) {
        data.getMarkBuilder(0).setLowerBound(MarkScale.ZERO_LOWER_BOUND);
      }
      updated = new MarkScale(data.buildPartial());
    }

    fProfile.setMarkScale(updated);
  }
}
