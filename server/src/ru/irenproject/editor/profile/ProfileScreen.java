/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.editor.profile;

import ru.irenproject.editor.Action;
import ru.irenproject.editor.profile.Proto.XProfileScreen;
import ru.irenproject.editor.profile.Proto.XProfileScreen.Close;
import ru.irenproject.infra.Event;
import ru.irenproject.infra.RenderContext;
import ru.irenproject.infra.Resident;
import ru.irenproject.infra.Widget;
import ru.irenproject.profile.Profile;
import ru.irenproject.profile.ProfileList;

import com.google.inject.assistedinject.Assisted;
import com.google.protobuf.Message;

import javax.inject.Inject;
import java.util.LinkedHashMap;

public final class ProfileScreen extends Widget {
  public interface Factory {
    ProfileScreen create(ProfileList profileList);
  }

  private final ProfileList fProfileList;
  private final ProfileListEditor fProfileListEditor;
  private final LinkedHashMap<Profile, ProfileEditor> fProfileEditors = new LinkedHashMap<>();

  @Inject private ProfileEditor.Factory fProfileEditorFactory;

  @Inject private ProfileScreen(@Assisted ProfileList profileList) {
    super(profileList.realm());
    fProfileList = profileList;
    fProfileList.addListener(this);

    fProfileListEditor = new ProfileListEditor(fProfileList);
    fProfileListEditor.addListener(this);
  }

  @Override public Message render(RenderContext context) {
    XProfileScreen.Builder b = XProfileScreen.newBuilder()
        .setProfileListEditor(context.link(fProfileListEditor));

    Profile selected = fProfileListEditor.selected();
    if (selected != null) {
      b.setProfileEditor(context.link(fProfileEditors.computeIfAbsent(selected, fProfileEditorFactory::create)));
    }

    return b.build();
  }

  @Override protected void handleEvent(Event e, Resident source) {
    if (e instanceof ProfileListEditor.SelectEvent) {
      markForRender();
    } else if (e instanceof ProfileList.RemoveEvent) {
      fProfileEditors.remove(((ProfileList.RemoveEvent) e).profile());
    }
  }

  @Action public void doClose(Close in) {
    dismissScreen();
  }
}
