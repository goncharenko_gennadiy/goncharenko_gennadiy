/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.orderQuestion;

import ru.irenproject.Question;
import ru.irenproject.itx.Itx;
import ru.irenproject.itx.ItxWriter;
import ru.irenproject.itx.QuestionWriter;
import ru.irenproject.pad.Pad;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton public final class OrderQuestionWriter implements QuestionWriter {
  @Inject private OrderQuestionWriter() {}

  @Override public void write(Question question, ItxWriter out) {
    OrderQuestion q = (OrderQuestion) question;
    out.writePad(q.formulation());

    out.begin(Itx.ELEM_ORDER_OPTIONS);
    Integer itemLimit = q.itemLimit();
    out.add(Itx.ATTR_SEQUENCE_ITEMS_USED, (itemLimit == null) ? Itx.VAL_ALL : Integer.toString(itemLimit));
    Integer distractorLimit = q.distractorLimit();
    out.add(Itx.ATTR_DISTRACTORS_USED, (distractorLimit == null) ? Itx.VAL_ALL : Integer.toString(distractorLimit));
    out.end();

    if (!q.items().isEmpty()) {
      out.begin(Itx.ELEM_SEQUENCE);
      for (Pad pad : q.items()) {
        out.begin(Itx.ELEM_SEQUENCE_ITEM);
        out.writePad(pad);
        out.end();
      }
      out.end();
    }

    if (!q.distractors().isEmpty()) {
      out.begin(Itx.ELEM_DISTRACTORS);
      for (Pad pad : q.distractors()) {
        out.begin(Itx.ELEM_DISTRACTOR);
        out.writePad(pad);
        out.end();
      }
      out.end();
    }
  }
}
