/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.pad;

import ru.irenproject.ChangeContentEvent;
import ru.irenproject.Utils;
import ru.irenproject.infra.Realm;
import ru.irenproject.infra.Resident;

import com.google.common.collect.Iterables;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public final class Pad extends Resident {
  public enum ChangeEvent implements ChangeContentEvent { INSTANCE }

  private final ArrayList<Block> fBlocks = new ArrayList<>();

  public Pad(Realm realm) {
    super(realm);
  }

  private Address positionToAddress(int position) {
    Address res = new Address();
    if (position > 0) {
      res.fBlockIndex = fBlocks.size();

      int blockStartPosition = 0;
      for (int i = 0; i < fBlocks.size(); ++i) {
        // assert: position >= blockStartPosition

        int nextBlockStartPosition = blockStartPosition + fBlocks.get(i).length();
        if (position < nextBlockStartPosition) {
          res.fBlockIndex = i;
          res.fOffsetInBlock = position - blockStartPosition;
          break;
        }

        blockStartPosition = nextBlockStartPosition;
      }
    }

    return res;
  }

  private static final class Address {
    static final Comparator<Address> COMPARATOR = Comparator
        .comparingInt((Address a) -> a.fBlockIndex)
        .thenComparingInt(a -> a.fOffsetInBlock);

    int fBlockIndex;
    int fOffsetInBlock;
  }

  public void insertFragment(Pad fragment, int position) {
    insertBlocks(fragment.fBlocks, position);
  }

  private void insertBlocks(Collection<? extends Block> blocks, int position) {
    if (!blocks.isEmpty()) {
      Address address = positionToAddress(position);
      int insertBeforeBlockIndex;
      if (address.fOffsetInBlock > 0) {
        String oldText = ((TextBlock) fBlocks.get(address.fBlockIndex)).text();
        TextBlock headBlock = new TextBlock(oldText.substring(0, address.fOffsetInBlock));
        TextBlock tailBlock = new TextBlock(oldText.substring(address.fOffsetInBlock));

        fBlocks.set(address.fBlockIndex, headBlock);
        insertBeforeBlockIndex = address.fBlockIndex + 1;
        fBlocks.add(insertBeforeBlockIndex, tailBlock);
      } else {
        insertBeforeBlockIndex = address.fBlockIndex;
      }

      fBlocks.addAll(insertBeforeBlockIndex, blocks);

      joinTextBlocks(insertBeforeBlockIndex + blocks.size() - 1);
      joinTextBlocks(insertBeforeBlockIndex - 1);

      post(ChangeEvent.INSTANCE);
    }
  }

  public void insertBlock(Block block, int position) {
    insertBlocks(Collections.singleton(block), position);
  }

  private void joinTextBlocks(int leftIndex) {
    Block leftBlock, rightBlock;
    if ((leftIndex >= 0) && (leftIndex < fBlocks.size() - 1)
        && ((leftBlock = fBlocks.get(leftIndex)) instanceof TextBlock)
        && ((rightBlock = fBlocks.get(leftIndex + 1)) instanceof TextBlock)) {
      fBlocks.set(leftIndex, new TextBlock(
          ((TextBlock) leftBlock).text() + ((TextBlock) rightBlock).text()));
      fBlocks.remove(leftIndex + 1);
    }
  }

  public void appendText(String text) {
    if (!text.isEmpty()) {
      if (!fBlocks.isEmpty() && (fBlocks.get(fBlocks.size() - 1) instanceof TextBlock)) {
        String oldText = ((TextBlock) fBlocks.get(fBlocks.size() - 1)).text();
        fBlocks.set(fBlocks.size() - 1, new TextBlock(oldText + text));
      } else {
        fBlocks.add(new TextBlock(text));
      }
      post(ChangeEvent.INSTANCE);
    }
  }

  public void appendLineFeed() {
    fBlocks.add(LineFeedBlock.INSTANCE);
    post(ChangeEvent.INSTANCE);
  }

  public void appendImage(ImageBlock image) {
    fBlocks.add(image);
    post(ChangeEvent.INSTANCE);
  }

  public List<Block> blocks() {
    return Collections.unmodifiableList(fBlocks);
  }

  public void delete(int position, int length) {
    if (length > 0) {
      Address begin = positionToAddress(position);
      Address end = positionToAddress(position + length - 1);

      if ((Address.COMPARATOR.compare(begin, end) <= 0) && (end.fBlockIndex < fBlocks.size())) {
        if (begin.fBlockIndex == end.fBlockIndex) {
          Block b = fBlocks.get(begin.fBlockIndex);
          if (b instanceof TextBlock) {
            String text = ((TextBlock) b).text();
            String newText = text.substring(0, begin.fOffsetInBlock) + text.substring(end.fOffsetInBlock + 1);
            if (newText.isEmpty()) {
              removeBlockAndJoinText(begin.fBlockIndex);
            } else {
              fBlocks.set(begin.fBlockIndex, new TextBlock(newText));
            }
          } else {
            removeBlockAndJoinText(begin.fBlockIndex);
          }
        } else {
          Block e = fBlocks.get(end.fBlockIndex);
          if (e instanceof TextBlock) {
            String newText = ((TextBlock) e).text().substring(end.fOffsetInBlock + 1);
            if (newText.isEmpty()) {
              fBlocks.remove(end.fBlockIndex);
            } else {
              fBlocks.set(end.fBlockIndex, new TextBlock(newText));
            }
          } else {
            fBlocks.remove(end.fBlockIndex);
          }

          fBlocks.subList(begin.fBlockIndex + 1, end.fBlockIndex).clear();

          Block b = fBlocks.get(begin.fBlockIndex);
          if (b instanceof TextBlock) {
            String newText = ((TextBlock) b).text().substring(0, begin.fOffsetInBlock);
            if (newText.isEmpty()) {
              fBlocks.remove(begin.fBlockIndex);
            } else {
              fBlocks.set(begin.fBlockIndex, new TextBlock(newText));
            }
          } else {
            fBlocks.remove(begin.fBlockIndex);
          }

          joinTextBlocks(begin.fBlockIndex);
          joinTextBlocks(begin.fBlockIndex - 1);
        }

        post(ChangeEvent.INSTANCE);
      }
    }
  }

  private void removeBlockAndJoinText(int blockIndex) {
    fBlocks.remove(blockIndex);
    joinTextBlocks(blockIndex - 1);
  }

  public String getSummary(int softLimit, int hardLimit) {
    StringBuilder sb = new StringBuilder();
    Block lastBlock = null;
    for (Block b : fBlocks) {
      if (b instanceof TextBlock) {
        sb.append(((TextBlock) b).text());
      } else if ((lastBlock == null) || (lastBlock.getClass() != b.getClass())) {
        sb.append((b instanceof LineFeedBlock) ? " " : "...");
      }
      if (sb.length() > hardLimit) {
        break;
      }
      lastBlock = b;
    }
    return Utils.shortenString(sb.toString(), softLimit, hardLimit);
  }

  @Override public String toString() {
    return getSummary(Integer.MAX_VALUE, Integer.MAX_VALUE);
  }

  public Pad slice(int position, int length) {
    Pad res = new Pad(realm());

    if (length > 0) {
      Address start = positionToAddress(position);
      Address end = positionToAddress(position + length - 1);

      if ((Address.COMPARATOR.compare(start, end) <= 0) && (end.fBlockIndex < fBlocks.size())) {
        res.fBlocks.addAll(fBlocks.subList(start.fBlockIndex, end.fBlockIndex + 1));

        int removeFromStart = start.fOffsetInBlock;
        if (removeFromStart > 0) {
          String startingText = ((TextBlock) res.fBlocks.get(0)).text();
          res.fBlocks.set(0, new TextBlock(startingText.substring(removeFromStart)));
        }

        int removeFromEnd = fBlocks.get(end.fBlockIndex).length() - 1 - end.fOffsetInBlock;
        if (removeFromEnd > 0) {
          String endingText = ((TextBlock) Iterables.getLast(res.fBlocks)).text();
          res.fBlocks.set(res.fBlocks.size() - 1, new TextBlock(
              endingText.substring(0, endingText.length() - removeFromEnd)));
        }
      }
    }

    return res;
  }
}
