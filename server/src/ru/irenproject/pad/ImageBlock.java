/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.pad;

import ru.irenproject.infra.BlobId;
import ru.irenproject.infra.Freezable;

import javax.annotation.Nullable;
import java.util.Objects;

@Freezable public final class ImageBlock implements Block {
  private final String fMimeType;
  private final BlobId fData;
  private final @Nullable ImageSource fSource;

  public ImageBlock(String mimeType, BlobId data, @Nullable ImageSource source) {
    fMimeType = mimeType;
    fData = data;
    fSource = source;
  }

  public String mimeType() {
    return fMimeType;
  }

  public BlobId data() {
    return fData;
  }

  public @Nullable ImageSource source() {
    return fSource;
  }

  @Override public int length() {
    return 1;
  }

  @Override public boolean equals(@Nullable Object obj) {
    if (this == obj) {
      return true;
    }

    if ((obj == null) || (getClass() != obj.getClass())) {
      return false;
    }

    ImageBlock other = (ImageBlock) obj;

    return Objects.equals(fMimeType, other.fMimeType)
        && Objects.equals(fData, other.fData)
        && Objects.equals(fSource, other.fSource);
  }

  @Override public int hashCode() {
    return Objects.hash(fMimeType, fData, fSource);
  }
}
