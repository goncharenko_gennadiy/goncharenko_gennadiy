/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.pad;

import ru.irenproject.infra.BlobId;
import ru.irenproject.infra.Freezable;

import javax.annotation.Nullable;
import java.util.Objects;

@Freezable public final class ImageSource {
  private final String fFormat;
  private final BlobId fData;

  public ImageSource(String format, BlobId data) {
    fFormat = format;
    fData = data;
  }

  public String format() {
    return fFormat;
  }

  public BlobId data() {
    return fData;
  }

  @Override public boolean equals(@Nullable Object obj) {
    if (this == obj) {
      return true;
    }

    if ((obj == null) || (getClass() != obj.getClass())) {
      return false;
    }

    ImageSource other = (ImageSource) obj;

    return Objects.equals(fFormat, other.fFormat)
        && Objects.equals(fData, other.fData);
  }

  @Override public int hashCode() {
    return Objects.hash(fFormat, fData);
  }
}
