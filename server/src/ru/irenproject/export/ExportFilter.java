/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.export;

import ru.irenproject.BadInputException;
import ru.irenproject.Check;
import ru.irenproject.QuestionItem;
import ru.irenproject.Section;
import ru.irenproject.Test;
import ru.irenproject.TestModule;
import ru.irenproject.Utils;
import ru.irenproject.infra.MemoryBlobStore;
import ru.irenproject.infra.Realm;
import ru.irenproject.inputQuestion.InputQuestion;
import ru.irenproject.inputQuestion.RegexpPattern;
import ru.irenproject.itx.ItxInput;
import ru.irenproject.itx.ItxOutput;
import ru.irenproject.profile.Profile;

import com.google.inject.Guice;
import com.google.inject.Injector;

import javax.inject.Inject;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.stream.Collectors;

public final class ExportFilter {
  public static void main(String[] args) {
    Check.input(args.length == 2);

    Injector injector = Guice.createInjector(new TestModule());
    injector.getInstance(ExportFilter.class).run(Paths.get(args[0]), Paths.get(args[1]));
  }

  private final ItxInput fItxInput;
  private final ItxOutput fItxOutput;

  @Inject private ExportFilter(ItxInput itxInput, ItxOutput itxOutput) {
    fItxInput = itxInput;
    fItxOutput = itxOutput;
  }

  private void run(Path in, Path out) {
    try {
      Test test = fItxInput.readTest(in, new Realm(Utils::logUnexpectedEvent, null, new MemoryBlobStore()));

      deleteDisabledQuestions(test.root());
      deleteEmptySections(test.root());
      rejectRegexpPatterns(test);
      test.root().listTreeQuestions().forEach(QuestionItem::clearLabels);

      if (test.profileList().isEmpty()) {
        test.profileList().add(Profile.createDefault(test.realm()));
      } else {
        for (int i = test.profileList().profiles().size() - 1; i >= 1; --i) {
          test.profileList().delete(i);
        }
      }

      Profile profile = test.profileList().profiles().get(0);

      //TODO
      if (profile.options().hasLabelFilter()) {
        throw new BadInputException("Labels are not supported yet.");
      }

      profile.setTitle("");
      profile.setOptions(profile.options().toBuilder().clearLabelFilter().buildPartial());

      try (OutputStream outputStream = Files.newOutputStream(out)) {
        fItxOutput.writeTest(test, outputStream, true);
      }
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  private static void deleteDisabledQuestions(Section section) {
    section.questionList().removeItems(section.questionList().items().stream()
        .filter(item -> !item.enabled())
        .collect(Collectors.toList()));
    section.sections().forEach(ExportFilter::deleteDisabledQuestions);
  }

  private static void deleteEmptySections(Section section) {
    ArrayList<Section> empty = new ArrayList<>();

    for (Section s : section.sections()) {
      deleteEmptySections(s);
      if (s.questionList().isEmpty() && s.sections().isEmpty()) {
        empty.add(s);
      }
    }

    empty.forEach(section::removeSection);
  }

  private static void rejectRegexpPatterns(Test test) {
    for (QuestionItem item : test.root().listTreeQuestions()) {
      if ((item.question() instanceof InputQuestion)
          && ((InputQuestion) item.question()).patterns().stream().anyMatch(p -> p instanceof RegexpPattern)) {
        throw new BadInputException("Regular expression patterns are not supported.");
      }
    }
  }
}
