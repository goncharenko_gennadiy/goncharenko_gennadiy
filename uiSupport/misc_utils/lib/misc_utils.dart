/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

import 'dart:async';
import 'dart:html';
@MirrorsUsed(metaTargets: _Ui)
import 'dart:mirrors';
import 'dart:typed_data';

import 'package:meta/meta.dart';
import 'package:quiver/core.dart' show hash2;
import 'package:web_helpers/translator.dart';
import 'package:web_helpers/web_helpers.dart';

export 'src/languages.dart' show registerMiscUtilsDictionaries;

class WebComponent extends HtmlElement {
  String _lowerCaseTagName;

  @protected final ShortcutHandler windowShortcutHandler = new ShortcutHandler.detached();
  /* nullable */StreamSubscription<KeyboardEvent> _windowShortcutSubscription;

  WebComponent.created() : super.created() {
    _lowerCaseTagName = tagName.toLowerCase();

    TemplateElement template = document.getElementById("template-$_lowerCaseTagName");
    if (template == null) {
      throw "No template for '$_lowerCaseTagName'.";
    }

    DocumentFragment content = document.importNode(template.content, true);
    _translateFragment(content);
    createShadowRoot().append(content);

    InstanceMirror mirror = reflect(this);
    mirror.type.declarations.forEach((Symbol s, DeclarationMirror m) {
      if (m.metadata.contains(_ui_mirror)) {
        String fieldName = MirrorSystem.getName(s);

        int p = fieldName.lastIndexOf("\$");
        if (p != -1) {
          fieldName = fieldName.substring(p + 1);
        }

        check(fieldName.startsWith("_"));

        //ignore: invalid_use_of_protected_member
        mirror.setField(s, findElement(fieldName.substring(1)));
      }
    });
  }

  void _translateFragment(DocumentFragment content) {
    for (Text t in _traverseNodeIterator(new NodeIterator(content, NodeFilter.SHOW_TEXT))
        .where((Node n) => n.parent is! StyleElement)) {
      _translate(t.data, (String newValue) => t.data = newValue);
    }

    for (Element e in content.querySelectorAll("[title]")) {
      _translate(e.title, (String newValue) => e.title = newValue);
    }
  }

  static Iterable<Node> _traverseNodeIterator(NodeIterator i) sync* {
    Node node;
    while ((node = i.nextNode()) != null) {
      yield node;
    }
  }

  void _translate(String s, void setter(String newValue)) {
    String trimmedLeft = s.trimLeft();
    String trimmed = trimmedLeft.trimRight();
    if (trimmed.isNotEmpty) {
      int leftSpaces = s.length - trimmedLeft.length;
      setter(s.replaceRange(leftSpaces, leftSpaces + trimmed.length, tr(trimmed)));
    }
  }

  @protected Element findElement(String id) {
    Element res = shadowRoot.getElementById(id);
    if (res == null) {
      throw "'$id' is not defined in '$_lowerCaseTagName'.";
    }
    return res;
  }

  @override void attached() {
    super.attached();
    if (!windowShortcutHandler.isEmpty) {
      _windowShortcutSubscription = window.onKeyDown.listen((KeyboardEvent e) {
        if ((offsetParent != null) && !modalDialogOpen) {
          windowShortcutHandler.handle(e);
        }
      });
    }
  }

  @override void detached() {
    _windowShortcutSubscription?.cancel();
    _windowShortcutSubscription = null;
    super.detached();
  }

  void focusComponent() {}

  @protected dynamic tr(String key) => translate(_lowerCaseTagName, key);
}

class _Ui {
  const _Ui();
}

class _Shortcut {
  final int keyCode;
  final KeyModifierState modifiers;

  _Shortcut(this.keyCode, this.modifiers);

  @override bool operator ==(Object other) => (other is _Shortcut)
      && (keyCode == other.keyCode) && (modifiers == other.modifiers);

  @override int get hashCode => hash2(keyCode, modifiers);
}

typedef _Handler();

class ShortcutHandler {
  final Map<_Shortcut, _Handler> _registry = {};

  ShortcutHandler(EventTarget target) {
    Element.keyDownEvent.forTarget(target).listen(handle);
  }

  ShortcutHandler.detached();

  void add(int keyCode, KeyModifierState modifiers, void handler()) {
    _Shortcut shortcut = new _Shortcut(keyCode, modifiers);
    check(!_registry.containsKey(shortcut));
    _registry[shortcut] = handler;
  }

  void handle(KeyboardEvent e) {
    _Handler handler = _registry[new _Shortcut(e.keyCode, getKeyModifiers(e))];
    if (handler != null) {
      stopFurtherProcessing(e);
      handler();
    }
  }

  bool get isEmpty => _registry.isEmpty;
}

enum KeyModifierState {
  NONE, SHIFT, ALT, ALT_SHIFT, CTRL, CTRL_SHIFT, CTRL_ALT, CTRL_ALT_SHIFT
}

class ModalDialog extends WebComponent {
  ModalDialog.created() : super.created();

  @protected Future<Null> open(DialogElement dialogElement) async {
    document.body.append(this);
    _openModalDialogs.add(dialogElement);
    try {
      dialogElement.showModal();
      await dialogElement.on["close"].first;
    } finally {
      _openModalDialogs.remove(dialogElement);
      remove();
    }
  }
}

final DivElement _glass = new DivElement()
    ..style.position = "fixed"
    ..style.top = "0"
    ..style.left = "0"
    ..style.bottom = "0"
    ..style.right = "0";

int _uiLockCount = 0;
bool _globalKeyHandlerInstalled = false;

const Object ui = const _Ui();
final InstanceMirror _ui_mirror = reflect(ui);

final List<DialogElement> _openModalDialogs = [];

void stopFurtherProcessing(Event event) {
  event
      ..preventDefault()
      ..stopImmediatePropagation();
}

void lockUi() {
  ++_uiLockCount;
  if (_uiLockCount == 1) {
    _installGlobalKeyHandler();
    (_openModalDialogs.isEmpty ? document.body : _openModalDialogs.last).append(_glass);
  }
}

void unlockUi() {
  if (_uiLockCount > 0) {
    --_uiLockCount;
    if (_uiLockCount == 0) {
      _glass.remove();
    }
  }
}

bool get uiLocked => _uiLockCount > 0;

void _installGlobalKeyHandler() {
  if (!_globalKeyHandlerInstalled) {
    Element.keyDownEvent.forTarget(window, useCapture: true).listen(_globalKeyHandler);
    Element.keyPressEvent.forTarget(window, useCapture: true).listen(_globalKeyHandler);

    _globalKeyHandlerInstalled = true;
  }
}

void _globalKeyHandler(KeyboardEvent e) {
  if (uiLocked) {
    stopFurtherProcessing(e);
  }
}

KeyModifierState getKeyModifiers(KeyboardEvent e) =>
    KeyModifierState.values[(e.ctrlKey ? 4 : 0) + (e.altKey ? 2 : 0) + (e.shiftKey ? 1 : 0)];

bool get modalDialogOpen => _openModalDialogs.isNotEmpty;

void disableDragAndDrop(Element element) {
  element
      ..onDragStart.listen((MouseEvent e) => e.preventDefault())
      ..onDragOver.listen((MouseEvent e) {
        e
            ..preventDefault()
            ..dataTransfer.dropEffect = "none";
      });
}

Uint8List castOrCopyToUint8List(List<int> list) => (list is Uint8List) ? list : new Uint8List.fromList(list);
