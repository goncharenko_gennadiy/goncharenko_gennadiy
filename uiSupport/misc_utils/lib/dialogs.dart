/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

@HtmlImport("dialogs.html")
library misc_utils.dialogs;

import 'dart:async';
import 'dart:html';

import 'package:web_components/web_components.dart';

import 'misc_utils.dart';

@CustomElement(ConnectionLostDialog.TAG)
class ConnectionLostDialog extends ModalDialog {
  static const String TAG = "ru-irenproject-connection-lost-dialog";

  static Future<Null> openModal() async {
    await (new Element.tag(TAG) as ConnectionLostDialog)._doOpenModal();
    window.location.reload();
  }

  @ui DialogElement _dialog;

  ConnectionLostDialog.created() : super.created();

  Future<Null> _doOpenModal() => open(_dialog);
}
