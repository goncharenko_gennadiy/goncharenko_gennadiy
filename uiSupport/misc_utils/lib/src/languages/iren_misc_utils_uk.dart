/*
  © Translation contributors, see the TRANSLATORS file

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

final Map<String, Map<String, dynamic>> uk = {
  "test_utils": {
    "Iren": "Айрен",
    "Close": "Закрити",
    "Untitled": "Безіменний",
    "Tests": "Тести",
    "All Files": "Усі файли",
    "untitled": "безіменний",
  },

  "ru-irenproject-connection-lost-dialog": {
    "No connection to the server": "Немає з'єднання з сервером",
    "Reconnect": "Підключитися",
  }
};
