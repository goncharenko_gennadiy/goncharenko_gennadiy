/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

@JS()
library misc_utils.electron;

import 'dart:js';
import 'dart:typed_data';

import 'package:initialize/initialize.dart';
import 'package:js/js.dart';
import 'package:meta/meta.dart';

@JS("remote.BrowserWindow")
class BrowserWindow {
  external void destroy();
}

@JS("remote.Menu")
class Menu {
  external Menu();
  external void append(MenuItem item);
  external void popup([num x, num y]);
}

@JS("remote.MenuItem")
class MenuItem {
  external MenuItem(MenuItemOptions options);
}

@JS() @anonymous class MenuItemOptions {
  external factory MenuItemOptions({
      ClickHandler click,
      String type,
      String label,
      String icon,
      bool enabled,
      bool checked});
}

typedef void ClickHandler(MenuItem item, BrowserWindow browserWindow);

@JS() @anonymous class ShowMessageBoxOptions {
  external factory ShowMessageBoxOptions({
      String type,
      List<String> buttons,
      String message,
      int cancelId});
  external String get title;
  external set title(String value);
  external bool get noLink;
  external set noLink(bool value);
}

@JS() @anonymous class ShowOpenDialogOptions {
  external factory ShowOpenDialogOptions({
      List<FileDialogFilter> filters,
      List<String> properties});
}

@JS() @anonymous class FileDialogFilter {
  external factory FileDialogFilter({
      @required String name,
      @required List<String> extensions});
}

@JS() @anonymous class ShowSaveDialogOptions {
  external factory ShowSaveDialogOptions({
      String defaultPath,
      List<FileDialogFilter> filters});
}

@JS("Buffer")
class Buffer {
  external ByteBuffer get buffer;
  external int get byteOffset;
  external int get byteLength;
}

@JS("remote.getCurrentWindow")
external BrowserWindow getCurrentWindow();

@JS("remote.dialog.showMessageBox")
external int dialog_showMessageBox(BrowserWindow browserWindow, ShowMessageBoxOptions options);

@JS("remote.dialog.showOpenDialog")
external /* nullable */List<String> dialog_showOpenDialog(BrowserWindow browserWindow, ShowOpenDialogOptions options);

@JS("remote.dialog.showSaveDialog")
external /* nullable */String dialog_showSaveDialog(BrowserWindow browserWindow, ShowSaveDialogOptions options);

@JS("ipcRenderer.on")
external void ipcRenderer_on(String channel, void listener(dynamic event, [dynamic data]));

@JS("ipcRenderer.once")
external void ipcRenderer_once(String channel, void listener(dynamic event, [dynamic data]));

@JS("ipcRenderer.send")
external void ipcRenderer_send(String channel);

@JS("remote.app.getAppPath")
external String app_getAppPath();

@JS("Buffer.from")
external Buffer Buffer_from(ByteBuffer arrayBuffer, int byteOffset, int length);

@JS("node_fs.writeFileSync")
external void fs_writeFileSync(String fileName, dynamic data);

@JS("node_fs.readFileSync")
external /*TODO Uint8List*/Buffer fs_readFileSync(String fileName);

Uint8List readFileSync(String fileName) {
  Buffer data = fs_readFileSync(fileName);
  return new Uint8List.view(data.buffer, data.byteOffset, data.byteLength);
}

@JS("node_fs.readFileSync")
external String fs_readFileSync_String(String fileName, String encoding);

@JS("node_path.normalize")
external String path_normalize(String path);

@JS("node_path.resolve")
external String path_resolve(String path);

@JS("process.platform")
external String get process_platform;

@JS("remote.process.argv")
external List<String> get remote_process_argv;

@JS("node_os.EOL")
external String get os_EOL;

@JS("shell.openExternal")
external bool shell_openExternal(String url);

@initMethod void initialize() {
  context.callMethod("eval", [
      "({ipcRenderer, remote, shell} = require('electron'));"
      "node_fs = require('original-fs');"
      "node_os = require('os');"
      "node_path = require('path');"
      "null"]);
}
