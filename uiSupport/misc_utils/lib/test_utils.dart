/*
  Copyright 2012-2016 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

import 'package:web_helpers/translator.dart';

import 'electron.dart';

const String _LIB = "test_utils";
const List<String> _TEST_EXTENSIONS = const ["itx", "it3", "it2"];

final bool developmentMode = remote_process_argv.contains("--devel");

final String untitledFile = _tr("Untitled");
final String testFilterName = _tr("Tests");

dynamic _tr(String key) => translate(_LIB, key);

String getFileTitle(String fileName) {
  String res;
  if (fileName.isEmpty) {
    res = untitledFile;
  } else {
    int p = fileName.lastIndexOf(new RegExp(r"/|\\"));
    String s = (p == -1) ? fileName : fileName.substring(p + 1);
    res = dropFileExtension(s, _TEST_EXTENSIONS);
  }
  return res;
}

String dropFileExtension(String s, List<String> extensions) {
  String res = s;
  for (String ext in extensions) {
    String suffix = ".$ext";
    if ((s.length >= suffix.length) && (s.substring(s.length - suffix.length).toLowerCase() == suffix)) {
      res = s.substring(0, s.length - suffix.length);
      break;
    }
  }
  return res;
}

List<String> showOpenTestDialog() => dialog_showOpenDialog(getCurrentWindow(), new ShowOpenDialogOptions(
    filters: [
        new FileDialogFilter(name: testFilterName, extensions: _TEST_EXTENSIONS),
        new FileDialogFilter(name: _tr("All Files"), extensions: ["*"])],
    properties: ["openFile", "multiSelections"])) ?? [];

/* nullable */String showSaveFileDialog(String extension, String filterName, {String defaultPathWithoutExtension}) {
  String defaultPath;
  if (defaultPathWithoutExtension == null) {
    defaultPath = (process_platform == "linux") ? "$untitledFile.$extension" : "";
  } else {
    defaultPath = "$defaultPathWithoutExtension.$extension";
  }

  return dialog_showSaveDialog(getCurrentWindow(), new ShowSaveDialogOptions(
      defaultPath: defaultPath,
      filters: (process_platform == "linux") ? [] : [new FileDialogFilter(name: filterName, extensions: [extension])]));
}

int showMessageBox(ShowMessageBoxOptions options) => dialog_showMessageBox(getCurrentWindow(), options
    ..title ??= _tr("Iren")
    ..noLink ??= true);

void showErrorMessage(String message) {
  showMessageBox(new ShowMessageBoxOptions(
      type: "error",
      buttons: [_tr("Close")],
      message: message));
}

String getPrintableSectionName(String s) => s.isEmpty ? _tr("untitled") : s;
